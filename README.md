# StudentReport

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).








<!-- 




yearas page
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from '../../service/login.service';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-year-and-department',
  templateUrl: './year-and-department.component.html',
  styleUrls: ['./year-and-department.component.css']
})
export class YearAndDepartmentComponent implements OnInit {

  departments = [];
  upload_department_Info= {};
  courseList: Array < Object >;
  constructor(private http: HttpClient) { 
    this.http.get('/api/courses')
    .subscribe((response) => {
      this.courseList = response['courses'];
      console.log(this.courseList, this.courseList.length, this.courseList[0]['name']);
    });
  }
  

years = [];
year = [];
subjects= [];
branches = [];
selectedyear:any;
signIn :any;


  ngOnInit() {
    this.http.get('/api/departments')
    .subscribe((response) => {
      this.departments = response['departments'];
      let arr = []
      this.departments.forEach(department=>{
        arr.push(department.year)
      })
      let removed_duplicate = [...new Set(arr)];
      this.year = removed_duplicate.sort(); 
      console.log('years',this.year);
    });

    this.years = [
      2001,
      2002,
      2003,
      2004,
      2005,
      2006,
      2007
    ]
  }
  setYear(event: Event) {
    this.setBranch(event); 
    document.getElementById("crumb").style.display = "block";
    this.selectedyear = (<HTMLInputElement>event.target).value;
    console.log('this.selectedyear',this.selectedyear);
    switch(this.selectedyear) { 
      case '2001': {
    
        this.subjects=['dart', 'flater','c programming', 'phython','c programming', 'phython','c programming', 'phython']
         break; 
      } 
      case '2002': { 
        this.subjects=['chart.js', 'vue.js','electronics', 'science','electronics', 'science','electronics', 'science']
         break; 
      } 
      case '2003': { 
        this.subjects=['react', 'java','react', 'java','react', 'java','react', 'java']
         break; 
      } 
      case '2004': { 
        this.subjects=['electronics', 'science','typescript', 'node','typescript', 'node','typescript', 'node']
         break; 
      } 
      case '2005': { 
        this.subjects=['c programming', 'phython','typescript', 'node','typescript', 'node','typescript', 'node']
         break; 
      } 
      case '2006': { 
        this.subjects=['typescript', 'node','typescript', 'node','typescript', 'node','typescript', 'node','typescript', 'node']
         break; 
      } 
      case '2007': { 
        this.subjects=['.net', 'angular','typescript', 'node','typescript', 'node']
         break; 
      } 
      default:{
        this.subjects=['Please select any year']
              break; 
      }
   }
  }


  setBranch(event: Event) {
    this.selectedyear = (<HTMLInputElement>event.target).value;
    console.log('this.selectedyear',this.selectedyear);
    switch(this.selectedyear) { 
      case '2001': {
        this.branches=['Mechanical','civil','E&C','BioTech','E&E'];
         break; 
      } 
      case '2002': { 
        this.branches=['Mechanical','civil','E&C','BioTech','E&E'];
         break; 
      } 
      case '2003': { 
        this.branches=['Mechanical','civil','E&C','BioTech','E&E'];
         break; 
      } 
      case '2004': { 
        this.branches=['Mechanical','civil','E&C','BioTech','E&E'];
         break; 
      } 
      case '2005': { 
        this.branches=['Mechanical','civil','E&C','BioTech','E&E'];
         break; 
      } 
      default:{
        this.branches=['Mechanical','civil','E&C','BioTech','E&E'];
              break; 
      }
   }
  }




 async call(branch1){
  document.getElementById("branch").style.display = "none";
  document.getElementById("course").style.display = "block";
  this.selectedyear = this.selectedyear +' / '+ branch1;
   console.log(branch1)
  }
 
} -->
