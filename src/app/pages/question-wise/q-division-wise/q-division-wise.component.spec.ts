import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QDivisionWiseComponent } from './q-division-wise.component';

describe('QDivisionWiseComponent', () => {
  let component: QDivisionWiseComponent;
  let fixture: ComponentFixture<QDivisionWiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QDivisionWiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QDivisionWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
