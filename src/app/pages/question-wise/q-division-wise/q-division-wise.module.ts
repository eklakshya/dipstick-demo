import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QDivisionWiseRoutingModule } from './q-division-wise-routing.module';
import { QDivisionWiseComponent } from './q-division-wise.component';
import { CoreModule } from 'src/app/core/core.module';
import { popUpTableModule } from 'src/app/commmon-modules/popUptable.module';
import { QuestionBasedReportTemplateDivisionWiseModule } from 'src/app/templates/question-based-report-template-division-wise/question-based-report-template-division-wise.module';


@NgModule({
  declarations: [QDivisionWiseComponent],
  imports: [
    CommonModule,
    QDivisionWiseRoutingModule,
    QuestionBasedReportTemplateDivisionWiseModule  ]
})
export class QDivisionWiseModule { }
