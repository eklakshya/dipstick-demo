import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QDivisionWiseComponent } from './q-division-wise.component';

const routes: Routes = [{ path: '', component: QDivisionWiseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QDivisionWiseRoutingModule { }
