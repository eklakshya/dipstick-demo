import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllParticipantsComponentQ } from './all-participants.component';

describe('QuestionWiseAllParticipantsComponent', () => {
  let component: AllParticipantsComponentQ;
  let fixture: ComponentFixture<AllParticipantsComponentQ>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllParticipantsComponentQ ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllParticipantsComponentQ);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
