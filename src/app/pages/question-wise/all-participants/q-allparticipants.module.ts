import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QAllparticipantsRoutingModule } from './q-allparticipants-routing.module';
 import { CoreModule } from 'src/app/core/core.module';
import { AllParticipantsComponentQ } from './all-participants.component';
import { popUpTableModule } from 'src/app/commmon-modules/popUptable.module';
import { QuestionBasedReportTemplateAllParticipantsModule } from 'src/app/templates/question-based-report-template-all-participants/question-based-report-template-all-participants.module';


@NgModule({
  declarations: [AllParticipantsComponentQ],
  imports: [
    CommonModule,
    QAllparticipantsRoutingModule,
    QuestionBasedReportTemplateAllParticipantsModule

  ]
})
export class QAllparticipantsModule { }
