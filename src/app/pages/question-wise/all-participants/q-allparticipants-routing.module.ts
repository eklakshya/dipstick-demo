import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

 import { AllParticipantsComponentQ } from './all-participants.component';

const routes: Routes = [{ path: '', component: AllParticipantsComponentQ }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QAllparticipantsRoutingModule { }
