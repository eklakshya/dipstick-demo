import { Component, OnInit,Input } from '@angular/core';
import {ApiService} from '../services/http.service'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, NavigationEnd } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ReportsService } from '../../core/services/reports.service'
import { constant } from 'src/app/shared/constant';

@Component({
  selector: 'app-choose-level',
  templateUrl: './choose-level.component.html',
  styleUrls: ['./choose-level.component.css']
})
export class ChooseLevelComponent implements OnInit {
  @Input() id: String;
  isAdmin: boolean;
  testdata: any;
  testlen: boolean;
  reports:any[];
  myParam:any;
  loadShow: boolean = true;
  apiresponse: any;
  load:number=0
  reportConfigurations:any[] = [];
  downloadPDF = constant.downloadPDF;
  constructor(private api:ApiService,private http: HttpClient, public router: Router, private route: ActivatedRoute, public reportService:ReportsService) {
    this.isAdmin = localStorage.getItem('user') == "admin" ? true : false;
    this.route.queryParams.subscribe(params => {
      this.myParam =params['accessToken']
      console.log(this.myParam );
     });
    if(this.myParam && this.myParam != undefined && this.myParam != null && this.myParam != ''){
      this.validateAccesstoken(this.myParam);
     }else{
      console.log("No token in params.");
     }
     this.reportService.loadReportDisplayConfigurations((reportConfigurations:any)=>{
      this.reportConfigurations = reportConfigurations;
      console.log(this.reportConfigurations);
    });
  }

  ngOnInit(): void {
    setTimeout(function(){ 
      document.getElementById('loading').style.display = 'none';
    }, 500);
      this.http.get(this.api.BASEURL + 'api/fileTest?id='+this.id,this.api.headers())
      .subscribe((response) => {
        this.testdata = response['tests'];
        this.testlen = this.testdata.length > 0 ? true : false;
        this.loadShow = false;
        console.log('testsdata', this.testdata)
      },(error:any) => {                           
        console.log('error caught for api',error);
        (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
      });

      this.reports=[{
        "report":"Report-1",
         "url":"report-1",
         "description":"All participants (Tests)",
         "topic" : "tests",
         reportType : "group-testwise-allParticipants"
        },
        {
          "report":"Report-2",
          "url":"report-2",
          "description":"By Division (Tests)",
          "topic" : "tests",
          reportType : "group-testwise-divisionWise"
        },
        {
          "report":"Report-3",
          "url":"report-3",
          "description":"All participants (Concepts)",
          "topic" : "concepts",
          reportType : "group-conceptwise-allParticipants"
        },
        {
          "report":"Report-4",
          "url":"report-4",
          "description":"By Division (Concepts)",
          "topic" : "concepts",
          reportType : "group-conceptwise-divisionWise"
        },
        {
          "report":"Report-5",
          "url":"report-5",
          "description":"All participants (Subconcepts)",
          "topic" : "subconcepts",
          reportType : "group-subconceptwise-allParticipants"
      },
      {
        "report":"Report-6",
         "url":"report-6",
         "description":" By Division (Subconcepts)",
         "topic" : "subconcepts",
         reportType : "group-subconceptwise-divisionWise"
      },
      {
        "report":"Report-7",
         "url":"report-7",
         "description":"All participants (Questions)",
         "topic" : "Questions",
         reportType : "group-questionwise-allParticipants"
      },
      {
        "report":"Report-8",
         "url":"report-8",
         "description":" By Division (Questions)",
         "topic" : "Questions",
         reportType : "group-questionwise-divisionWise"
      },
      {
        "report":"Attendence",
         "url":"attendance",
         "description":"All Students Attendance",
         "topic" : "Attendance",
         reportType : "group-attendance"
      }
      ]     

  }

  validateAccesstoken=(token:any)=>{
    this.http.get(this.api.BASEURL + "api/intigration/validate/?token="+token)
    .subscribe((response) => {
      this.apiresponse =response['decryptedJsonData'];
      if(this.apiresponse?.courseid != undefined || this.apiresponse?.departmentid != undefined){
        const dataset1:any= this.apiresponse;
        localStorage.setItem('user','user');
        sessionStorage.setItem('dataset',dataset1);
        localStorage.setItem('courseId',this.apiresponse?.courseid);
        sessionStorage.setItem('load','1')
        // alert(this.apiresponse?.courseid)
        setTimeout(()=>{
      //     this.router.navigate(['intigration/all'], {
      //       skipLocationChange: false,
      //       queryParams: { courseid : this.apiresponse?.courseid , studentid: this.apiresponse?.studentid, studentName: this.apiresponse?.studentName}
      // }).then(()=>{
        if(!sessionStorage.getItem('load')){
         location.reload();
        }
      // });
        },800);  
  }
},(error:any) => {                           
  console.log('error caught for api',error);
  (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
});
  }
  routTo(url){
    this.router.navigate(["/"+url]).then(()=>{
      console.log(url)});
  }
  checkReportVisibility(reportType:any){
    return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == reportType)?.isVisible;
  }
  reportGeneration(){}
}
