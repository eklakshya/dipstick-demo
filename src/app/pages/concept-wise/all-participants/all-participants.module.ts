import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllParticipantsRoutingModule } from './all-participants-routing.module';
import { AllParticipantsComponent } from './all-participants.component';
import { CoreModule } from 'src/app/core/core.module';
import { ConceptWiseDivModule } from 'src/app/commmon-modules/concept-wise-div.module';
import { ConceptWiseModule } from 'src/app/commmon-modules/concept-wise.module';
import { popUpTableModule } from 'src/app/commmon-modules/popUptable.module';
import { QuestionBasedReportTemplateAllParticipantsModule } from 'src/app/templates/question-based-report-template-all-participants/question-based-report-template-all-participants.module';


@NgModule({
  declarations: [AllParticipantsComponent],
  imports: [
    CommonModule,
    AllParticipantsRoutingModule,
    QuestionBasedReportTemplateAllParticipantsModule,
    ConceptWiseModule,
    popUpTableModule
  ]
})
export class AllParticipantsModule { }
