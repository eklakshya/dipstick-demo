import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllParticipantsComponent } from './all-participants.component';

const routes: Routes = [{ path: '', component: AllParticipantsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllParticipantsRoutingModule { }
