import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/http.service';
import { ReportsService } from '../../../core/services/reports.service'

@Component({
  selector: 'app-all-participants',
  templateUrl: './all-participants.component.html',
  styleUrls: ['./all-participants.component.css']
})
export class AllParticipantsComponent implements OnInit {
  courseId: string = localStorage.getItem('courseId');
  url : any = 'api/report/conceptwise/allParticipants/v1?courseid='+this.courseId;
  constructor() { }
  ngOnInit(): void {
  }

}
