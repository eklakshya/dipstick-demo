import { Component, OnInit, Input } from '@angular/core';
import { ConstantsService } from '../../../core/services/constants.service';

@Component({
  selector: 'app-student-wise-concept-wise',
  templateUrl: './student-wise-concept-wise.component.html',
  styleUrls: ['./student-wise-concept-wise.component.css']
})
export class StudentWiseConceptWiseComponent implements OnInit {

  APIURL = '';
  POPUPAPIURL = '';
  @Input() isPopupVisible : Boolean;
  constructor(private constants : ConstantsService) {  }

  ngOnInit(): void {
    const stringifiedStudentReportAssets = sessionStorage.getItem(this.constants.sessionStorage.STUDENTREPORTASSETS)
    const studentReportAssets = JSON.parse(stringifiedStudentReportAssets)
    this.APIURL = 'api/report/student/conceptwise?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid+'&studentName='+studentReportAssets.studentName;
    this.POPUPAPIURL = 'api/report/student/conceptwise/table?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid+'&studentName='+studentReportAssets.studentName;
    console.log('api/report/student/conceptwisetable?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid)
  }

}
