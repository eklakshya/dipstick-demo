import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentWiseConceptWiseComponent } from './student-wise-concept-wise.component';

describe('StudentWiseConceptWiseComponent', () => {
  let component: StudentWiseConceptWiseComponent;
  let fixture: ComponentFixture<StudentWiseConceptWiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentWiseConceptWiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentWiseConceptWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
