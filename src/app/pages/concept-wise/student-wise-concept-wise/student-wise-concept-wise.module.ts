import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

 import { StudentWiseConceptWiseComponent } from './student-wise-concept-wise.component';
import { ConceptWiseModule } from 'src/app/commmon-modules/concept-wise.module';


@NgModule({
  declarations: [StudentWiseConceptWiseComponent],
  exports: [StudentWiseConceptWiseComponent],
  imports: [
    CommonModule,
     ConceptWiseModule
    ]
})
export class StudentWiseConceptWiseModule { }
