import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConstantsService } from 'src/app/core/services/constants.service';

@Component({
  selector: 'app-attemptwise-studentwise-conceptwise',
  templateUrl: './attemptwise-studentwise-conceptwise.component.html',
  styleUrls: ['./attemptwise-studentwise-conceptwise.component.css']
})
export class AttemptwiseStudentwiseConceptwiseComponent implements OnInit {

  APIURL = '';
  POPUPAPIURL = '';
  @Input() isPopupVisible : Boolean;
  @Output() sendReportDetails = new EventEmitter();
  constructor(private constants : ConstantsService) {  }

  ngOnInit(): void {
    const stringifiedStudentReportAssets = sessionStorage.getItem(this.constants.sessionStorage.STUDENTREPORTASSETS)
    const studentReportAssets = JSON.parse(stringifiedStudentReportAssets)
    this.APIURL = 'api/report/student/conceptwiseAttemptwise?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid+'&studentName='+studentReportAssets.studentName;
    this.POPUPAPIURL = 'api/report/student/conceptwiseAttemptwise/table?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid+'&studentName='+studentReportAssets.studentName;
  }
  getConceptWiseData(data:any){
    this.sendReportDetails.emit(data);
  }

}
