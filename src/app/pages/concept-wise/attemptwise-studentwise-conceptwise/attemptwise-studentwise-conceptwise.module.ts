import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttemptwiseStudentwiseConceptwiseComponent } from './attemptwise-studentwise-conceptwise.component';
import { ConceptWiseModule } from 'src/app/commmon-modules/concept-wise.module';
import { AttemptConceptReportsAllParticipantsModule } from 'src/app/shared/attempt-concept-reports-all-participants/attempt-concept-reports-all-participants.module';


@NgModule({
  declarations: [AttemptwiseStudentwiseConceptwiseComponent],
  exports: [AttemptwiseStudentwiseConceptwiseComponent],
  imports: [
    CommonModule,
    ConceptWiseModule,
    AttemptConceptReportsAllParticipantsModule
  ]
})
export class AttemptwiseStudentwiseConceptwiseModule { }
