import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConceptwisedivisionwiseComponent } from './conceptwisedivisionwise.component';

describe('ConceptwisedivisionwiseComponent', () => {
  let component: ConceptwisedivisionwiseComponent;
  let fixture: ComponentFixture<ConceptwisedivisionwiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConceptwisedivisionwiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConceptwisedivisionwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
