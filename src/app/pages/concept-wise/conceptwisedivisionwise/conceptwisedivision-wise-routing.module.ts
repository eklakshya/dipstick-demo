import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConceptwisedivisionwiseComponent } from './conceptwisedivisionwise.component';

const routes: Routes = [{ path: '', component: ConceptwisedivisionwiseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConceptwisedivisionWiseRoutingModule { }
