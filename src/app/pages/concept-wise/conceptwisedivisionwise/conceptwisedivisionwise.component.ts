import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../core/services/reports.service'

@Component({
  selector: 'app-conceptwisedivisionwise',
  templateUrl: './conceptwisedivisionwise.component.html',
  styleUrls: ['./conceptwisedivisionwise.component.css']
})
export class ConceptwisedivisionwiseComponent implements OnInit {
  courseId: string = localStorage.getItem('courseId');
  url = 'api/report/conceptwise/divisionWise/v1?courseid='+this.courseId;
  constructor() { }
  ngOnInit(): void {
  }
}
