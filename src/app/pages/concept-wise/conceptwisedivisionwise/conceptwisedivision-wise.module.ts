import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConceptwisedivisionWiseRoutingModule } from './conceptwisedivision-wise-routing.module';
 import { ConceptwisedivisionwiseComponent } from './conceptwisedivisionwise.component';
 import { popUpTableModule } from 'src/app/commmon-modules/popUptable.module';
 import { QuestionBasedReportTemplateDivisionWiseModule } from 'src/app/templates/question-based-report-template-division-wise/question-based-report-template-division-wise.module';


@NgModule({
  declarations: [ConceptwisedivisionwiseComponent],
  imports: [
    CommonModule,
    ConceptwisedivisionWiseRoutingModule,popUpTableModule,
    QuestionBasedReportTemplateDivisionWiseModule
  ]
})
export class ConceptwisedivisionWiseModule { }
