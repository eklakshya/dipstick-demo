import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { FilterableAnalyticsRoutingModule } from './filterable-analytics-routing.module';
import { FilterableAnalyticsComponent } from './filterable-analytics.component';


@NgModule({
  declarations: [FilterableAnalyticsComponent],
  imports: [
    CommonModule,
    // FilterableAnalyticsRoutingModule
  ]
})
export class FilterableAnalyticsModule { }
