import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FilterableAnalyticsComponent } from './filterable-analytics.component';

const routes: Routes = [{ path: '', component: FilterableAnalyticsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FilterableAnalyticsRoutingModule { }
