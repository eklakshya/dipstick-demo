import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterableAnalyticsComponent } from './filterable-analytics.component';

describe('FilterableAnalyticsComponent', () => {
  let component: FilterableAnalyticsComponent;
  let fixture: ComponentFixture<FilterableAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterableAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterableAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
