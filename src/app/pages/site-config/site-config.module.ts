import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { SiteConfigRoutingModule } from './site-config-routing.module';
import { SiteConfigComponent } from './site-config.component';
import { AccessControlComponent } from '../student_access/access-control/access-control.component';
import { StudentAccessComponent } from '../student-access/student-access.component';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [SiteConfigComponent,AccessControlComponent,StudentAccessComponent],
  imports: [
    CommonModule,
    SiteConfigRoutingModule,
    FormsModule,
    ChartsModule,
     
  ]
})
export class SiteConfigModule { }
