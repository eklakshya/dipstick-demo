import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from '../../pages/services/http.service';
import { ConstantsService } from '../../core/services/constants.service';
import { ReportsService } from '../../core/services/reports.service';
// import { Pipe, PipeTransform } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChartsService } from '../../shared/service/chats.service';
import { Color, Label } from 'ng2-charts';
import { ChartsModule } from 'ng2-charts';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { monthNames, monthShortNames } from 'src/app/shared/constant';


@Component({
  selector: 'app-site-config',
  templateUrl: './site-config.component.html',
  styleUrls: ['./site-config.component.css']
})

export class SiteConfigComponent implements OnInit {

  institutes = [];
  activeTestIndex = -1;
  activeInstitute: any = {
    _id: "",
    name: ""
  };
  showConfigurationPage: Boolean = false;
  showAccessPage: Boolean = false;
  showAnalyticsPage: Boolean = false;
  showAnalyticssPage: Boolean = false;
  isReportDisplayAccessPage: Boolean = false;

  hideList: boolean = true;
  instituteConfigurations: any = [];
  saveStatus: any = {
    show: false,
    message: ""
  };
  enableButtonClicks: any = {
    saveConfigurations: true,
    uploadLogo: true
  }
  institutewiseCount: any;
  logoData: any;
  logoMinimizedData: any;
  isLogoLoaded: boolean = false;
  setColor: any;
  res: any;
  isReportDisplayConfigurationPage: Boolean = false;
  isStudentReportDisplayConfigurationPage: Boolean = false;
  reportConfigurations = [];
  
  activeReportConfigurations = [];
  studentReportTypes = ["student-testwise", "student-conceptwise", "student-subconceptwise", "student-conceptwise-popup", "student-subconceptwise-popup"];
  saveReportDisplayConfigurationSaveMessage: String;
  depcoursetest: any = {};
  currentDate: string;
  institute: string;
  successSubmissions = 11;
  failureSubmissions = 0;
  successRedirections = 0;
  failureRedirections = 0;
  public accessData: boolean;
  showKledata: boolean = true;
  analytics: any;
  todaysanalytics: any;
  todaymorning: any = new Date().toDateString()
  showLoader: boolean = false;
  showYearMessage: boolean = true;
  showDateMessage: boolean = false;
  showActivebutton: boolean;


  isConfigurableAnalytics = false;
  semesterYearsList: any = [];
  semestersList: any = [];
  yearsList: any = [];
  departmentsList: any = [];
  coursesList: any = [];
  activeYearForConfigurableAnalytics = undefined;
  activeSemesterForConfigurableAnalytics = undefined;
  activeDepartmentForConfigurableAnalytics = undefined;
  activeCourseForConfigurableAnalytics = undefined;
  activeConditionForConfigurableAnalytics = undefined;
  activeFilterForConfigurableAnalytics = undefined;
  conditionalInput1ConfigurableAnalytics: number = NaN;
  conditionalInput2ConfigurableAnalytics: number = NaN;
  isConfigurableAnalyticsExportEnabled: boolean = false;
  insCount: string;
  instituteCount: number = 0;
  onedaydetails: any = {};
  userRole: any = '';
  isDateWiseSelection: boolean;
  myForm: any;
 
  GRAPH_FILTERS = { WEEK : 7, MONTH : 30, YEAR : 365 }

  activeButton: number | string = this.GRAPH_FILTERS.WEEK;

  testIndex: number = 0;

  // Define divisions (labels for the x-axis)
  divisions: Label[] = [];

  // Define data for the line chart
  lineChartData: ChartDataSets[] = [
    {
      data: [],
      label: 'Number of records',
      borderWidth: 1,  // Line width
      pointRadius: 4,  // Point radius
      fill: false      // No fill under the line
    },
  ];

  lineChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {
          beginAtZero: true
        }
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };

  // Define colors for the line chart
  lineChartColors: Color[] = [
    {
      borderColor: '#0a3c62',
      backgroundColor: '#0a3c62',
      pointBackgroundColor: 'rgba(63,81,181,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(63,81,181,1)'
    },
  ];

  // Define legend visibility
  lineChartLegend: boolean = true;

  // Define chart type
  lineChartType: ChartType = 'line';
  activeButtonText: any;
  
  monthsNames = monthNames;

  constructor(private http: HttpClient, public charts: ChartsService, private api: ApiService, private constants: ConstantsService, public reportService: ReportsService, private el: ElementRef, private modalService: NgbModal) {
    this.userRole = localStorage.getItem('userrole') || "";

    const today = new Date(new Date().setDate(new Date().getDate() - 1));
    const year = today.getFullYear();

    const monthIndex = today.getMonth();
    const monthName = monthNames[monthIndex];
    const day = ('0' + today.getDate()).slice(-2);
    this.currentDate = `${monthName} ${day}, ${year}`;

  };


  ngOnInit(): void {
    if(this.userRole == "admin") {
      const apiUrl = this.api.BASEURL + 'api/institute';
      this.http.get(apiUrl, this.api.headers()).subscribe((response: any) => {
        this.institutes = response;
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
      this.getinstituteWiseCountApi();
      this.institute && this.tempfunction(this.institute)
    } else if (this.userRole == "faculty") {
      const instituteid = localStorage.getItem('instituteid');
      this.getYearWiseTotalCount({ _id : instituteid })
    }
    }
  

  setActiveTest(testIndex: number) {
    this.activeTestIndex = testIndex;
  }
  publish() {

  }
  confirmPublish() {

  }

  getinstituteWiseCountApi() {
    
    this.http.get(this.api.BASEURL + 'api/institute/fromcache', this.api.headers())
      .subscribe((response: any) => {
        this.institutewiseCount = response;
        // this.getinstituteWiseCount1()
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        })
  }

  getinstituteWiseCount1(ins: any) {
    const insName: string = ins.name;
    console.log(ins)
    if (this.institutewiseCount?.counts?.data.length) {
      const institution = this.institutewiseCount?.counts.data.find((ins: any) => ins.Name == insName);
      this.instituteCount = institution?.Count || 0;
    }
    return this.instituteCount;

  }

  setSiteColor(color: any, instituteid: any) {
    let options: any = {
      instituteid, color
    }
    const apiUrl = this.api.BASEURL + 'api/institute/setcolor';
    this.http.post(apiUrl, options, this.api.headers()).subscribe((response: any) => {
      console.log({ response });
    }, (error: any) => {
      console.log('error caught for api', error);
      // (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }

  selectInstituteForConfiguration(institute) {
    this.setActiveButton(this.GRAPH_FILTERS.WEEK);
    this.isReportDisplayConfigurationPage = false;
    this.activeInstitute = institute;
    this.setColor = this.activeInstitute.color;
    const apiUrl = this.api.BASEURL + 'api/configuration?instituteid=' + this.activeInstitute._id;
    this.http.get(apiUrl, this.api.headers()).subscribe((response: any) => {
      this.showConfigurationPage = response ? true : false;
      this.hideList = this.showConfigurationPage ? false : true
      this.populateConfigurations(response);
      this.getImageData();
    }, (error: any) => {
      console.log('error caught for api', error);
      (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
    });
  };

  selectInstituteForReportConfiguration(institute) {
    this.isReportDisplayConfigurationPage = true;
    this.activeInstitute = institute;
    const apiUrl = this.api.BASEURL + 'api/reportConfiguration?instituteid=' + this.activeInstitute._id;
    this.http.get(apiUrl, this.api.headers()).subscribe((response: any) => {
      console.log(response);
      this.reportConfigurations = response;
      this.showConfigurationPage = Boolean(response);
      this.hideList = this.showConfigurationPage ? false : true
      this.isStudentReportDisplayConfigurationPage = false;
      this.setConfiguredColors();
      // this.displayStudentReportConfigurations();
    }, (error: any) => {
      console.log('error caught for api', error);
      // (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  };

  selectInstituteForDipstickAccess(institute: any) {
    this.showAccessPage = true;
    this.hideList = false;
    this.activeInstitute = institute;
  };

  accessDataSet(value: boolean) {
    this.showAccessPage = value ? true : false;
    this.hideList = this.showAccessPage ? false : true
  }

  displayStudentReportConfigurations() {
    this.isStudentReportDisplayConfigurationPage = true;
    this.activeReportConfigurations = this.reportConfigurations.filter((configuration: any) => this.studentReportTypes.includes(configuration.reportType));
  }
  displayGroupReportConfigurations() {
    this.isStudentReportDisplayConfigurationPage = false;
    this.activeReportConfigurations = this.reportConfigurations.filter((configuration: any) => !this.studentReportTypes.includes(configuration.reportType));
  }

  setConfiguredColors() {
    let { styleElement2, styleElement3 } = this.reportService.setPrimaryColor();
    this.el.nativeElement.appendChild(styleElement3);
    this.el.nativeElement.appendChild(styleElement2);
  }

  setReportDisplayConfigurations(data: any) {
    const configurationIndex = this.reportConfigurations.findIndex((configuration: any) => configuration.reportType == data.name)
    console.log({ configurationIndex, data })
    this.reportConfigurations[configurationIndex].isVisible = data.checked;
    this.saveReportDisplayConfigurationSaveMessage = "";
  }

  getReportDisplayConfigurations(reportType: String) {
    const configurationIndex = this.reportConfigurations.findIndex((configuration: any) => configuration.reportType == reportType)
    return (configurationIndex > -1) && this.reportConfigurations[configurationIndex].isVisible;
  }

  saveReportDisplayConfigurations() {
    const apiUrl = this.api.BASEURL + 'api/reportConfiguration?instituteid=' + this.activeInstitute['_id'];
    this.http.post(apiUrl, { configurations: this.reportConfigurations }, this.api.headers())
      .subscribe((response: any) => {
        this.saveReportDisplayConfigurationSaveMessage = "Configurations saved successfully!!!";
        setTimeout(() => {
          this.saveReportDisplayConfigurationSaveMessage = "";
        }, 4000);
      }, (error: any) => {
        console.log('error caught for api', error);
        // (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
      })
  }

  populateConfigurations(configurations: any[]) {
    const configurationsArray = configurations.map((configuration) => {
      let confJson = {};
      confJson[configuration.elementtype] = configuration.name;
      return confJson;
    });
    this.instituteConfigurations = configurationsArray;
    this.getImageData()
  };

  getImageData() {
    console.log("Inside - getImageData")
    const headers: any = this.api.headers();
    headers.headers.set('instituteid', this.activeInstitute._id);
    this.http.get(this.api.BASEURL + 'api/configuration/file-upload/?instituteid=' + this.activeInstitute._id, headers)
      .subscribe((response: any) => {
        console.log("response ", response)
        this.logoData = response.find((data: any) => data.elementtype == 'logo');
        this.logoMinimizedData = response.find((data: any) => data.elementtype == 'logominimized');
        console.log(this.logoData, this.logoMinimizedData)
      }, (error: any) => {
        console.log('error caught for api', error);
      })
  }

  getKeys(data: any) {
    let keys = Object.keys(data);
    return keys[0];
  };
  getValues(data: any) {
    let values = Object.values(data);
    return values[0];
  };
  onChange(data: any, key: string, index: number) {
    this.instituteConfigurations[index][key] = data.target.value;
    this.setSaveStatus("");
  };
  saveConfigurations() {
    this.setSiteColor(this.setColor, this.activeInstitute._id)
    this.setSaveStatus("");
    this.enableButtonClicks.saveConfigurations = false;
    const requestdata = { configurations: this.instituteConfigurations, instituteid: this.activeInstitute._id }
    const apiUrl = this.api.BASEURL + 'api/configuration?instituteid=' + this.activeInstitute._id;
    this.http.post(apiUrl, requestdata, this.api.headers())
      .subscribe((response: any) => {
        // this.setConfigurations();
        this.enableButtonClicks.saveConfigurations = true;
        this.setSaveStatus("configurations saved successfully");
        setTimeout(() => {
          this.setSaveStatus("");
        }, 4000);
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  };
  setConfigurations() {
    localStorage.setItem(this.constants.localStorage.CONFIGURATIONS, JSON.stringify(this.instituteConfigurations))
  };

  setSaveStatus(message) {
    this.saveStatus.message = message;
    this.saveStatus.show = Boolean(message.length);
  }
  back() {
    this.activeInstitute = undefined;
    this.showConfigurationPage = false;
    this.showAccessPage = false;
    this.hideList = true;
    this.showAnalyticsPage = false;
    this.showAnalyticssPage = false;
    this.onedaydetails = {}
    this.showAnalyticsPageV1 = false;
    this.activeYearData = {};
    this.yearWiseTotalCountData = [];
    this.showFacultyAccessPage = false;
    this.dateWiseCountData = [];
    this.activeDates = {};

    this.isReportDisplayAccessPage = false;
    this.isConfigurableAnalytics = false;
    this.resetAccessPage()
  }

  clearDateInputs(): void {
    this.startdateValue = '';
    this.endDateValue = '';
  }
  
  uploadToS3(uploadedFiles: any, imageType: any) {
    console.log({ imageType });

    let formData = new FormData();
    for (let i = 0; i < uploadedFiles.length; i++) {
      formData.append('institutionlogo', uploadedFiles[i], uploadedFiles[i].name);
    }
    let options: any = {
      headers: new HttpHeaders()
        .set('instituteid', this.activeInstitute._id)
        .set('elementtype', imageType)
        .set('role', JSON.stringify(1))
        .set('authorization', JSON.stringify(localStorage.getItem('token')))
    }
    this.http.post(this.api.BASEURL + 'api/configuration/file-upload', formData, options)
      .subscribe((response: any) => {
        this.logoData = response.find((data: any) => data.elementtype == 'logo');
        this.logoMinimizedData = response.find((data: any) => data.elementtype == 'logominimized');
        this.setSaveStatus("Logo uploaded successfully");
        this.enableButtonClicks.uploadLogo = true;
        setTimeout(() => {
          this.setSaveStatus("");
        }, 4000);
      }, (error: any) => {
        console.log('error caught for api', error);
        //(error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
      })
  }

  onImageUpload(element, imageType) {
    this.setSaveStatus("");
    this.enableButtonClicks.uploadLogo = false;
    this.isLogoLoaded = false;
    this.logoData = undefined;
    this.uploadToS3(element.target.files, imageType);
  }

  onColorchange(element) {
    console.log('color', element.target.value);
    this.setColor = element.target.value;
  }
  validateFile(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    return (ext.toLowerCase() == 'xlsx')
  }
  imageLoaded() {
    this.isLogoLoaded = true;
  }
  imageLoadError() {
    this.isLogoLoaded = true;
  }

  getinstituteWiseCount(institute: any) {
    this.insCount = '';
    this.activeInstitute = institute;
  }

  tempfunction(institute) {
    this.getYearWiseTotalCount(institute);
    this.getonedayapi(institute);
  }

  formatDate(dateString) {
    const options: any = { year: 'numeric', month: 'long', day: 'numeric' };
    const formattedDate = new Date(dateString).toLocaleDateString('en-US', options);
    return formattedDate;
  };

  showAnalytics(institute: any) {
    this.activeInstitute = institute;
    this.http.get(this.api.BASEURL + 'api/statistics/?instituteid=' + institute._id, this.api.headers())
      .subscribe((response: any) => {
        this.res = response;
        this.showAnalyticsPage = response ? true : false;
        this.hideList = this.showAnalyticsPage ? false : true
        this.failureRedirections = response?.error["Number of failed redirections "]
        this.failureSubmissions = response?.error["Number of failed submissions "]
        this.successRedirections = response?.success["Number of success redirection "]
        this.successSubmissions = response?.success["Number of success submissions "]
        console.log("this.successRedirections", this.successRedirections)
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })

  }


  startDate;
  gettotaldepcoursetest(institute: any) {
    this.getYearWiseTotalCount(institute)
    this.activeInstitute = institute;
    let data:any = { headers: new HttpHeaders()
      .set('role', JSON.stringify(1))
      .set('authorization',JSON.stringify(localStorage.getItem('token'))) 
      }
    this.http.get(this.api.BASEURL + 'api/statistics/totaldepcoursetest?instituteid=' + institute._id, data)
      .subscribe((response) => {
        this.depcoursetest = response;
        this.showAnalyticsPage = response ? true : false;
        this.hideList = this.showAnalyticsPage ? false : true
        const startDate = this.depcoursetest.startDate
        const options: Intl.DateTimeFormatOptions = { year: 'numeric', month: 'long', day: 'numeric' };
        const formattedDate = new Date(startDate).toLocaleDateString('en-US', options);
        this.startDate = formattedDate;
        return formattedDate;
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }
  getAnalytics() {

    this.http.get('https://dipstick.eklakshya.com/api/institute/Analytics?instituteid=6204e34c73d03a4976729db2', this.api.headers())
      .subscribe((response) => {
        this.res = response;

        const allResponses = []

      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }

  loaderConfig: boolean = false;


  getonedayapi(institute: any) {

    if (institute?.name === 'kle technological university') {
      console.log('API call blocked for kle technological university');
      return;
    }
    this.showLoader = true;
    this.activeInstitute = institute;
    this.http.get(this.api.BASEURL + 'api/institute/1day?instituteid=' + institute._id, this.api.headers())
      .subscribe((response) => {
        this.onedaydetails = response;
        const valuess = this.onedaydetails.redsub;
        // console.log(this.onedaydetails,"mgnnn")
        this.showLoader = false;


      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")

      })

  }

  yearWiseTotalCountData = [];
  dateWiseCountData = [];
  allYearTotalCountData = {}
  showAnalyticsPageV1 = false;
  activeYearData: any;
  activeDates: any;
  startdateValue: any = 0//new Date('2020-01-01');
  endDateValue: any = 0//new Date();
  validateDates() {
    if (this.startdateValue && this.endDateValue) {
      return this.startdateValue < this.endDateValue;
    }
    return true;
  }

  convertToTimestamp(dateString: string): number {
    const date = new Date(dateString);
    return date.getTime();
  }

  getDateWiseTotalCount() {
    this.showLoader = true;
    const institute = this.activeInstitute;
    this.activeButton = undefined;
    this.showDateMessage =true ;

    const startTimestamp = this.convertToTimestamp(this.startdateValue);
    const endTimestamp = new Date(this.endDateValue).setHours(23, 59, 59, 999);

    const duration = (endTimestamp - startTimestamp) / (24 * 60 * 60 * 1000)
    
    this.getFilteredData(startTimestamp, endTimestamp, duration);
  }

  fetchCurrentMonthData() {
 
    const today = new Date();
    const startOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
    const dates = [];
    const data = [];

    for (let d = new Date(startOfMonth); d <= today; d.setDate(d.getDate() + 1)) {
      dates.push(d.toLocaleDateString()); // Format date as needed
      data.push(Math.floor(Math.random() * 100)); // Mock data, replace with actual data fetching logic
    }

    this.divisions = dates;
    this.lineChartData[0].data = data;

    this.getDateWiseTotalCount();
  }

  setPreviousMonth() {
    const today = new Date();
    const startOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
    const endOfMonth = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const dates = [];
    const data = [];

    let currentDate = new Date(startOfMonth);

    while (currentDate <= endOfMonth) {
      dates.push(currentDate.toLocaleDateString()); // Format date as needed
      data.push(Math.floor(Math.random() * 100)); // Mock data, replace with actual data fetching logic
      currentDate.setDate(currentDate.getDate() + 1);
    }

    this.divisions = dates;
    this.lineChartData[0].data = data;
  }

  getYearWiseTotalCount(institute: any) {
    
    this.isDateWiseSelection = false;
    this.activeInstitute = institute;
    this.showAnalyticsPageV1 = true;
    this.hideList = false;
    this.http.get(this.api.BASEURL + 'api/statistics/yearWiseTotalCount?instituteid=' + institute._id + '&organizationString=' + btoa(institute?.name), this.api.headers())
      .subscribe((response: any) => {
        this.getTodaysDashboardData();
        this.yearWiseTotalCountData = response?.yearWiseInfo || [];
        this.allYearTotalCountData = response?.allYearData || [];
        this.setActiveButton(this.GRAPH_FILTERS.WEEK);
        this.setAllYearsData();
        const requestData = {
          startdate: this.startdateValue,
          endDate: this.endDateValue
        }
      }, (error: any) => {
        this.getTodaysDashboardData();
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }
  setAllYearsData() {
    this.activeYearData = this.allYearTotalCountData;
  }
  setActiveYearDataForAnalytics(year: any) {
    this.isDateWiseSelection = false;
    this.activeYearData = this.yearWiseTotalCountData.find((yearData: any) => yearData.year == year);
    !this.activeYearData && this.setAllYearsData()
  }

  todaysDashboard: any = {};
  getTodaysDashboardData() {

    if (this.activeInstitute?.name !== 'kle technological university') {
      console.log('API call blocked for other istitutions');
      return;
    }
    this.http.get(this.api.BASEURL + 'api/statistics/todaysDashboard?instituteid=' + this.activeInstitute._id + '&organizationString=' + btoa(this.activeInstitute?.name), this.api.headers())
      .subscribe((response: any) => {
        this.todaysDashboard = response || {};
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }
  reloadAnalytics() {
    this.getYearWiseTotalCount(this.activeInstitute);
    this.setActiveButton(this.GRAPH_FILTERS.WEEK);
    this.clearDateInputs();

  }
  showFacultyAccessPage = false;
  facultyCreationResponse = '';
  disableCreateAccessButton = false;
  openFacultyAccessPage(institute: any) {
    this.activeInstitute = institute;
    this.showFacultyAccessPage = true;
    this.hideList = false;
    this.getFacultyList();
  }
  createFacultyRole(facultyData: any, modal: any) {
    this.disableCreateAccessButton = true;
    const facultyInfo = { ...facultyData, instituteid: this.activeInstitute._id };
    this.http.post(this.api.BASEURL + 'api/user/?instituteid=' + this.activeInstitute._id, facultyInfo, this.api.headers())
      .subscribe((response: any) => {
        this.facultyCreationResponse = response?.message || '';
        this.getFacultyList();
        this.disableCreateAccessButton = false;
        setTimeout(() => {
          this.facultyCreationResponse = '';
          modal?.dismiss('Cross click')
        }, 5000);
      }, (error: any) => {
        this.disableCreateAccessButton = false;
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      }, () => {
        this.disableCreateAccessButton = false;
      })
  }

  facultyAccessTabs = ['CREATEROLE', 'ASSIGNACCESS']
  activeFacultyAccessTab = this.facultyAccessTabs[1];

  accessedInstitutions: any = [];
  accessedYears: any = [];
  accessedDepartments: any = [];
  accessedCourses: any = [];

  instituteAccordianIsOpen = false;
  yearAccordianIsOpen = false;
  departmentAccordianIsOpen = false;
  courseAccordianIsOpen = false;
  facultiesList = [];

  setActiveTabForAccess(tab: any) {
    if (this.facultyAccessTabs.includes(tab) && this.activeFacultyAccessTab != tab) {
      this.activeFacultyAccessTab = tab;
    }
  }
  getFacultyList() {
    this.http.get(this.api.BASEURL + 'api/user/?instituteid=' + this.activeInstitute._id, this.api.headers())
      .subscribe((response: any) => {
        this.facultiesList = response?.faculties || [];
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }

  showAccessData = false;
  activeFaculty: any;
  allAccessYears: any = [];
  allAccessDepartments: any = [];
  allAccessDepartmentNames: any = [];
  selectedDepartmentIds: any = [];
  allAccessCourses: any = [];
  accessCourses: any = [];
  allAccessCourseNames: any = [];

  activeYearForAccess: any = '';
  activeDepartmentForAccess: any;
  allCoursesSelected = false;
  facultyAccessList: any;

  openYearAccordian = true;
  openDepartmentAccordian = false;
  openCourseAccordian = false;


  addAccessData(facultyData: any) {
    this.showAccessData = true;
    this.activeFaculty = facultyData;
    this.getAccessData(facultyData)
    this.getSemesters();
  };

  currentAccessYears = [];
  currentAccessDepartments: any = {};
  currentAccessCourses = {};

  getAccessData(faculty: any) {
    this.http.get(this.api.BASEURL + 'api/courseAccess?facultyid=' + faculty?._id, this.api.headers())
      .subscribe((response: any) => {
        this.currentAccessYears = [...new Set(response?.accessList.map((accessData: any) => accessData.year))];
        response?.accessList.forEach((accessData: any) => {

          const existingDepartments: string[] = this.currentAccessDepartments[accessData?.year] || []
          existingDepartments.push(accessData.departmentName);
          this.currentAccessDepartments[accessData?.year] = existingDepartments;

          const accessedCourses: string[] = accessData?.courses?.map((course: any) => response?.courses[course]?.name);
          this.currentAccessCourses[accessData.year] = this.currentAccessCourses[accessData.year] || {};
          this.currentAccessCourses[accessData.year][accessData.departmentName] = accessedCourses;
        })
      }, (error: any) => console.log(error))
  }

  resetAccessPage() {
    this.showAccessData = false;
    this.activeFaculty = undefined;

    this.activeYearForAccess = '';
    this.activeDepartmentForAccess = undefined;
    this.allCoursesSelected = false;
    this.allAccessYears = [];
    this.allAccessDepartments = [];
    this.allAccessCourses = [];
    this.accessCourses = [];
    this.allAccessDepartmentNames = [];
    this.selectedDepartmentIds = [];
    this.currentAccessYears = [];
    this.currentAccessDepartments = [];
    this.currentAccessCourses = [];
    // this.facultyAccessList;
  }

  openAccessPage(modal: any, faculty: any) {
    this.activeFaculty = faculty;
    this.getAccessList(faculty)
    this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static', keyboard: false });
  }

  open(modal) {
    this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static', keyboard: false });
  }

  getSemesters() {
    this.http.get(this.api.BASEURL + 'api/semester?instituteid=' + this.activeInstitute._id, this.api.headers())
      .subscribe((response: any) => {
        this.allAccessYears = [...new Set(response?.map((semesterInfo: any) => semesterInfo?.year))]
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }

  setActiveYearForAccess(year: any) {
    this.activeYearForAccess = year;
    this.getDepartments(year);
    this.activeDepartmentForAccess = undefined;
    this.allAccessCourses = [];
    this.accessCourses = [];
    this.allCoursesSelected = false;
    this.selectedDepartmentIds = []
    this.openDepartmentAccordian = true;
    this.openCourseAccordian = false;
  }

  getDepartments(year: any) {
    this.http.get(this.api.BASEURL + 'api/department?year=' + year + "&instituteid=" + this.activeInstitute._id, this.api.headers())
      .subscribe((response: any) => {
        this.allAccessDepartments = response?.departments || [];
        this.allAccessDepartmentNames = [...new Set(this.allAccessDepartments.map((departmentInfo: any) => departmentInfo?.name))]
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }

  setActiveDepartmentForAccess(department: any) {
    this.activeDepartmentForAccess = department;
    this.allCoursesSelected = false;
    this.allAccessCourses = [];
    this.accessCourses = [];
    this.getCourses(department);
    this.openCourseAccordian = true;
  }

  getCourses(department: any) {
    const selectedDepartmentIds = this.allAccessDepartments.filter((departmentInfo: any) => departmentInfo.name == department).map((departmentInfo: any) => departmentInfo._id);
    this.selectedDepartmentIds = selectedDepartmentIds;
    this.http.get(this.api.BASEURL + 'api/course?departmentids=' + selectedDepartmentIds.join(','), this.api.headers())
      .subscribe((response: any) => {
        this.allAccessCourses = response?.courses || [];
        this.allAccessCourseNames = [...new Set(this.allAccessCourses.map((courseInfo: any) => courseInfo.name))]
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
      })
  }

  setCourseAccess(courseName: any) {
    if (this.accessCourses.includes(courseName)) {
      const courseIndex = this.accessCourses.findIndex(course => courseName == course);
      this.accessCourses.splice(courseIndex, 1);
    } else {
      this.accessCourses.push(courseName)
    }
    this.allCoursesSelected = this.allAccessCourseNames.map(course => this.accessCourses.includes(course)).reduce((a, b) => a && b, true);
  }

  selectAllCourses() {
    if (this.allCoursesSelected) {
      this.accessCourses = [];
      this.allCoursesSelected = false;
    } else {
      this.allCoursesSelected = true;
      this.accessCourses = JSON.parse(JSON.stringify(this.allAccessCourseNames)) //this.allAccessCourses.map((courseInfo:any) => courseInfo?._id);
    }
  }

  saveAccess() {

    const data = {
      facultyid: this.activeFaculty._id,
      year: this.activeYearForAccess,
      departments: this.selectedDepartmentIds,
      departmentName: this.activeDepartmentForAccess,
      courses: this.allAccessCourses.filter((courseInfo: any) => this.accessCourses.includes(courseInfo.name)).map((courseInfo: any) => courseInfo._id)
    }

    this.http.post(this.api.BASEURL + 'api/courseAccess', data, this.api.headers())
      .subscribe((response: any) => {

        this.notificationMessage = response?.message || "Saved the access selection successfully";
        this.notification();
        this.resetAccessPage()
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
      })
  }


  getAccessList(faculty: any) {
    this.http.get(this.api.BASEURL + 'api/courseAccess?facultyid=' + faculty?._id, this.api.headers())
      .subscribe((response: any) => {
        this.facultyAccessList = response;

        response.accessList.forEach((access: any, index: any) => {
          const { facultyid, year, departmentid, departmentName } = access;
          this.coursesToRemoveAccess[index] = { facultyid, year, departmentid, courses: [], departmentName };
        })

      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
      })
  }

  coursesToRemoveAccess = [];

  removeCourse(index: number, courseid: any) {

    if (this.coursesToRemoveAccess[index].courses.includes(courseid)) {
      const courseIndex = this.coursesToRemoveAccess[index].courses.findIndex(courseId => courseid == courseId);
      this.coursesToRemoveAccess[index].courses.splice(courseIndex, 1);
    } else {
      this.coursesToRemoveAccess[index].courses.push(courseid)
    }
  }

  removeCourseAccess(index: number) {
    const accessList = [this.coursesToRemoveAccess[index]];
    this.http.post(this.api.BASEURL + 'api/courseAccess/delete', accessList, this.api.headers())
      .subscribe((response: any) => {

        this.notificationMessage = "Deleted the access selection successfully";
        this.notification()
        document.getElementById("close")?.click()
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
      })
  }

  removeAllCourseAccess() {
    const accessList = this.coursesToRemoveAccess.filter((courseInfo: any) => courseInfo?.courses?.length)
    this.http.post(this.api.BASEURL + 'api/courseAccess/delete/all', accessList, this.api.headers())
      .subscribe((response: any) => {
        this.notificationMessage = "Deleted all the access selections successfully";
        this.notification()
        document.getElementById("close")?.click()

      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
      })
  }

  notificationMessage: any = "";
  public get RemoveAllCourseSelections(): string {
    return this.coursesToRemoveAccess.map((accessList: any) => accessList.courses.length).reduce((a: any, b: any) => a + b, 0);
  }
  notification() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 5000);
  }
 
  currentMonthIndex: number = new Date().getMonth();
  currentMonth = monthNames.at(this.currentMonthIndex)
  previousMonth = monthNames.at(this.currentMonthIndex - 1)


  setActiveButton(monthAbbreviation: any ) {
    this.activeButton = monthAbbreviation;
    this.showDateMessage =false ;

    if ( [this.GRAPH_FILTERS.WEEK, this.GRAPH_FILTERS.MONTH, this.GRAPH_FILTERS.YEAR].includes(monthAbbreviation)) {
      this.activeButtonText = `Last ${monthAbbreviation} days`;
    } else {
      this.activeButtonText = monthAbbreviation;
    }
   
    this.lastNDays(monthAbbreviation);
  }

  lastNDays(n: number | string) {

    let start: number;
    let end = new Date().getTime();
    const oneDay = 24 * 60 * 60 * 1000;
    
    if (typeof n === 'number' || !isNaN(Number(n))) {
      start = end - (Number(n) * oneDay);
    }
     else if (typeof n === 'string') {
      const dates = this.getMonthStartEndDates(n);

      start = dates.start;
     
      end = dates.end;
     
    } else {
      throw new Error("Invalid input. Please provide a number of days or a month abbreviation.");
    }

    this.getFilteredData(start, end, n);
  }

  getMonthStartEndDates(month: string): { start: number, end: number } {
   
    let monthIndex = this.monthsNames.indexOf(month);
    
    const now = new Date();

    if (monthIndex === -1) {
      throw new Error("Invalid month string. Please provide a valid month abbreviation (e.g., 'Jan', 'Feb').");
    }

    const year = now.getFullYear();
    const currentMonthIndex = now.getMonth();

    let start: number;
    let end: number;

    if (monthIndex === currentMonthIndex) {
      start = new Date(year, monthIndex, 1).getTime();
      end = now.getTime();
    

    } else if (monthIndex === currentMonthIndex - 1 || (monthIndex === 11 && currentMonthIndex === 0)) {
      if (monthIndex === 11 && currentMonthIndex === 0) {
        start = new Date(year - 1, monthIndex, 1).getTime();
        end = new Date(year - 1, monthIndex + 1, 0, 23, 59, 59, 999).getTime();
      } else {
        start = new Date(year, monthIndex, 1).getTime();
        end = new Date(year, monthIndex + 1, 0, 23, 59, 59, 999).getTime();
      }
    } else {
      throw new Error("Invalid month string. Please provide the current or previous month.");
    }

    return { start, end };
  }

  sortedGraphData = [];
  totalRecords = undefined;
  lineChartLabels = [];

  getFilteredData(start: number, end: number, duration) {
    this.showLoader = true;
    this.lineChartData[0].data = [];
    this.lineChartLabels = []
    this.http.get(this.api.BASEURL + `api/statistics/filterWiseRecords?start=${start}&end=${end}&instituteid=${this.activeInstitute._id}`, this.api.headers())
      .subscribe((response: any) => {
        this.dateWiseCountData = response;
        this.totalRecords = (response?.aggregateData as any[])?.reduce((sum: any, item: any) => sum + item.count, 0)
        this.showLoader = false;
        const sortedGraphData = response.aggregateData.sort((a: any, b: any) => new Date(a._id).getTime() - new Date(b._id).getTime())
        this.lineChartData[0].data = sortedGraphData.map((d) => d.count);

        if (duration == this.GRAPH_FILTERS.YEAR ) {
          this.lineChartLabels = sortedGraphData.map((d, i) => {
            const { _id } = d;
            const [ year, month ] = _id.split("-");
            return `${monthShortNames[month-1]} - ${year}`
          })
        } else {
          this.lineChartLabels = sortedGraphData.map((d) => d._id)
        }
      });
  }

  updateChart(data: number[], labels: string[]) {
    this.lineChartData[0].data = data;
    this.divisions = labels;
  }
};


