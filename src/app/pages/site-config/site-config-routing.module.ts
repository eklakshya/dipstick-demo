import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SiteConfigComponent } from './site-config.component';

const routes: Routes = [{ path: '', component: SiteConfigComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteConfigRoutingModule { }
