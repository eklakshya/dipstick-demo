import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentReportViewComponent } from './student-report-view.component';

describe('StudentReportViewComponent', () => {
  let component: StudentReportViewComponent;
  let fixture: ComponentFixture<StudentReportViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentReportViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentReportViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
