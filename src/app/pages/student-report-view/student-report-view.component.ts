import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DownloadService } from '../../pages/services/download.service';
import { saveAs } from 'file-saver';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ApiService} from '../../pages/services/http.service'
import { constant } from 'src/app/shared/constant';
@Component({
  selector: 'app-student-report-view',
  templateUrl: './student-report-view.component.html',
  styleUrls: ['./student-report-view.component.css']
})
export class StudentReportViewComponent implements OnInit {

  viewPreAssessment: boolean = true;
  viewPostAssessment: boolean = false;

  courseList: Array < Object >;

  toDisplay: boolean = false;
  studentName: string = '';
  emailaddress: string;
  coursename: string;

  reportDetails: any;
  scoresDisplayState: boolean;

  maxPretestAttempts: number = 5;
  maxPosttestAttempts: number = 1;
  studentusn: string = '';
  legend:any = constant.legendImage;
  constructor(private http:HttpClient, private downloads: DownloadService, private modalService: NgbModal, private api : ApiService) { 
 
  }

  ngOnInit(): void {
    this.api.getCoursesById()
    .subscribe((response) => {
      this.courseList = response['courses'];
      // console.log(this.courseList, this.courseList.length, this.courseList[0]['name']);
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }

  onClickSubmit(data) {
    this.emailaddress = data.emailaddress;
    this.http.post(this.api.BASEURL + 'api/fetch-student-report', {email: this.emailaddress, course: this.coursename})
    .subscribe((response) => {
      console.log(response);
      this.reportDetails = response;
      this.toDisplay = true;
      this.studentName = response['Student Name'];
      this.studentusn = response['Student USN'];
      this.cleanScores();
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    }); 
  }

  setCourse(event: Event) {
    this.coursename = (<HTMLInputElement>event.target).value;
  }

  cleanScores() {
    let pretestCount = this.reportDetails['Pretests'].length;
    for(let i = 0; i < pretestCount; i++) {
      let examId = this.reportDetails['Pretests'][i]['examID']
      for(let j = 0; j < 5; j++) {
        if (this.reportDetails['Scores'][examId] === undefined) { 
          this.reportDetails['Scores'][examId] = [];
          this.reportDetails['Scores'][examId].push({percent: '-'});
        } else {
          if (this.reportDetails['Scores'][examId][j] === undefined) {
            this.reportDetails['Scores'][examId].push({percent: '-'});
          }
        }
      }
    }

    let posttestCount = this.reportDetails['Posttests'].length;
    for(let i = 0; i < posttestCount; i++) {
      let examId = this.reportDetails['Posttests'][i]['examID'];
      for(let j = 0; j < 1; j++) {
        if (this.reportDetails['Scores'][examId] === undefined) { 
          this.reportDetails['Scores'][examId] = [];
          this.reportDetails['Scores'][examId].push({percent: '-'});
        } else {
          if (this.reportDetails['Scores'][examId][j] === undefined) {
            this.reportDetails['Scores'][examId].push({percent: '-'});
          }
        }
      }
    }
  }

  setScoresVisible() {
    this.scoresDisplayState = true;
  }

  setScoresInvisible() {
    this.scoresDisplayState = false;
  }

  downloadReport() {
    // this.downloads.download('/api/download-report',)
    // .subscribe(blob => saveAs(blob, 'Student Report.pdf')); 
    this.http.post(this.api.BASEURL + 'api/download-report', {email: this.emailaddress}, {
      responseType: "blob",
    })
    .subscribe(
      data => {
        saveAs(data, `Student Report ${this.emailaddress}.pdf`);
      },
      error => {
        alert("Problem while downloading the file.");
        console.error(error);
        console.log('error caught for api',error);
        (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
     
      }
    );
  }

  displayPosttest() {
    this.viewPostAssessment = true;
    this.viewPreAssessment = false;
  }

  displayPretest() {
    this.viewPreAssessment = true;
    this.viewPostAssessment = false;
  }

  emailReports(data) {
    const emailRequest = {
      'Student email': this.emailaddress,
      'Username': data.username,
      'Email': data.sendersAddr,
      'Password': data.passwd,
      'Subject Line': data.subjectLine,
      'Message Body': data.msgbody
    }
    this.http.post(this.api.BASEURL + 'api/email-reports', emailRequest)
    .subscribe((response) => {
      console.log(response['message']);
    }
    ,(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }

  open(modal) {
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static',keyboard: false});
  }

}
