import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { ApiService } from '../../../pages/services/http.service';
import { ConstantsService } from '../../../core/services/constants.service';
import { HexagonLayoutService } from '../../services/hexagon-layout.service';
import { constant } from 'src/app/shared/constant';



@Component({
  selector: 'app-attemptwise-studentwise-testwise',
  templateUrl: './attemptwise-studentwise-testwise.component.html',
  styleUrls: ['./attemptwise-studentwise-testwise.component.css']
})
export class AttemptwiseStudentwiseTestwiseComponent implements OnInit {

  @Output() sendReportDetails = new EventEmitter();

  constructor(private api:ApiService,private http:HttpClient, private constants : ConstantsService, private HexagonLayout : HexagonLayoutService) { }

  courseId:string = localStorage.getItem('courseId');
  data:string = localStorage.getItem('data');
  activeCourse:any = JSON.parse(localStorage.getItem("studentReportsActiveCourse"));
  reportDetails:any = [];
  legend:any = constant.legendImage;
  testsData = [];
  totalAttempts = 0;
  ngOnInit(): void {
    this.courseId = localStorage.getItem('courseId');
    this.data  = localStorage.getItem('data');
    this.activeCourse = JSON.parse(localStorage.getItem("studentReportsActiveCourse"));
    

    // console.log({APIURL : this.APIURL})
    const stringifiedStudentReportAssets = sessionStorage.getItem(this.constants.sessionStorage.STUDENTREPORTASSETS)
    const studentReportAssets = JSON.parse(stringifiedStudentReportAssets)
    this.http.get(this.api.BASEURL + 'api/report/student/testwiseAttemptwise?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid , this.api.headers()) //+ this.courseId
    .subscribe((response:any) => {
      // console.log("response : ", response);
      // this.reportDetails = response;
      this.testsData = response.testsData;
      this.totalAttempts = response?.attemptWiseScores.length;
      response?.attemptWiseScores.forEach((cluster, index) => {
        this.getClusterLayout(cluster, index);
        setTimeout(() => {
          this.rotate();
        }, 2000);
      });
      // this.getClusterLayout()
    }, (err:any)=>{
      console.log("err : ", err)
    })
  }
  getClusterLayout(cluster:any, index:number){

    this.reportDetails[index] = this.reportDetails[index] || {  };
    const rowDistribution = this.HexagonLayout.getRowDistribution(cluster.length);
    const reportDimensionsPost = this.HexagonLayout.calcDimensions(cluster.length, 1, rowDistribution);
    const hexStructurePost: any = this.HexagonLayout.initHexStructure(rowDistribution, reportDimensionsPost.marginX, reportDimensionsPost.marginY, reportDimensionsPost.hexWidth, reportDimensionsPost.hexRadius);
    const headerCoordinatesPost = [this.HexagonLayout.getHeaderCoordinates(0, rowDistribution, hexStructurePost, reportDimensionsPost)];

    const hexCentersXPost:any = [];
    const hexCentersYPost:any = [];
    const hexPathsPost:any = [];
    const hexColorsPost:any = [];
    const { marginX, marginY, hiveHeight, hiveWidth, hexWidth} = reportDimensionsPost;
    this.reportDetails[index].testArray = cluster;
    this.reportDetails[index].viewBox = `${cluster.length>2?(marginX - ((Math.floor( Object.keys(hexStructurePost).length / 2) -1 ) * (hexWidth/2))):marginX+(hexWidth/2)} ${marginY} ${hiveWidth} ${hiveHeight}`;
    // console.log(hexStructurePost)
    for(let a = 0; a < 1; a++) {
        let testNumber: number = 0;
        const rowsInStruct = Object.keys(hexStructurePost);

        rowsInStruct.forEach((row, index) => {
            const testsInRow = Object.keys(hexStructurePost[`row${index+1}`]);
            testsInRow.forEach(test => {
                try {
                    testNumber += 1;
                    const score = cluster[testNumber-1]['score'] >= 0 ? cluster[testNumber-1]['score'] : -1;
                    hexCentersXPost.push(this.HexagonLayout.getHexCenterX(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexCentersYPost.push(this.HexagonLayout.getHexCenterY(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexPathsPost.push(this.HexagonLayout.getHexPath(hexStructurePost,testNumber, index+1, a, reportDimensionsPost));
                    hexColorsPost.push(this.HexagonLayout.getHexColor(score));
                } catch (error) {

                }
            });
        });
    }
    // console.log({ hexPathsPost, hexColorsPost, hexCentersXPost, hexCentersYPost })
    this.reportDetails[index].hexagonSvgPaths = hexPathsPost;
    this.reportDetails[index].hexagonColors = hexColorsPost;
    this.reportDetails[index].centerX = hexCentersXPost;
    this.reportDetails[index].centerY = hexCentersYPost;
    this.reportDetails[index].rowDistribution = rowDistribution;
    // console.log(this.reportDetails)
    // this.sendReportDetails.emit(this.reportDetails)
    this.generatePDF();
  }

  getSvgWidth(rowDistribution:any[]){
    return rowDistribution[Math.ceil(rowDistribution.length%2==1?rowDistribution.length/2:rowDistribution.length)-1]
  }
  getSvgheight(rowDistribution:any[],svgPathLength:number){
    var totalHex = 0;
    var totalHexHeight = 0;
    var hexHeights:number[] = rowDistribution.map((rowdata, number)=>{
    totalHex = totalHex < svgPathLength ? totalHex + rowdata : (totalHex)
    totalHexHeight = totalHex < svgPathLength ? number + 1 : totalHexHeight ;
    return totalHexHeight + 1
    })
    var lastelement = hexHeights[hexHeights.length-1]
    return lastelement
  }
  getNegativeMargin(lastelement:number,rowDistributionLength:number){
    return (rowDistributionLength - lastelement) * -50
  }
  getHexText(testIndex:number, testid:any) {
    return 'T' + (this.testsData.findIndex(testdata => testdata._id == testid) + 1) // + '-' + this.reportDetails.testArray[testIndex].score.toFixed(2)
  }
  getScore(attemptIndex:any, testid:any){
    const score = this.reportDetails[attemptIndex].testArray.find(data => data.testid == testid )?.score
    return isNaN(score) ? '-' : ((score == 100 ? '100' : score.toFixed(2)) + '%');
  }
  generatePDF(){
    const clusters = this.reportDetails.map((reportDetailData:any) => {
      return {
        clusterName : "Attempt " + reportDetailData.testArray[0].attempt,
        paths : reportDetailData.hexagonSvgPaths,
        colors : reportDetailData.hexagonColors,
        scores : reportDetailData.testArray.map((testData) => testData.score),
        centerX : reportDetailData.centerX,
        centerY : reportDetailData.centerY,
        hexagonLabels : reportDetailData.hexagonSvgPaths.map((svgPath:any, i:number) => this.getHexText(i, reportDetailData.testArray[i].testid)) 
      }
    }); // getScore(attemptIndex, posttest._id)
    const tableData = {
      headers : [ "Serial #", "Test Name", ...this.reportDetails.map((attemptWiseData:any) => "Attempt " + attemptWiseData.testArray[0].attempt)],
      data : this.testsData.map((testInfo:any, testIndex:number) => {
        return [ "T" + (testIndex+1), (testInfo.name.length>49 ? (testInfo.name.slice(0, 46).trim() + "...") : testInfo.name ), ...this.reportDetails.map((_:any, i:number) => this.getScore(i, testInfo._id)) ]
      })
    }

    const data = {
      title : "post test 1",
      clusters, tableData
    };

    (this.totalAttempts == this.reportDetails.length) && this.sendReportDetails.emit(data);
  }
  sendData(){

  }

  fullScreenCheck() {
    if (document.fullscreenElement) return;
    return document.documentElement.requestFullscreen();
  }

  async rotate( ) {
	await screen.orientation.lock("landscape");
    try {
      await this.fullScreenCheck();
    } catch (err) {
      console.error(err);
    }
}
}
