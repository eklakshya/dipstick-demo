import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttemptwiseStudentwiseTestwiseComponent } from './attemptwise-studentwise-testwise.component';



@NgModule({
  declarations: [AttemptwiseStudentwiseTestwiseComponent],
  exports: [AttemptwiseStudentwiseTestwiseComponent],
  imports: [
    CommonModule
  ]
})
export class AttemptwiseStudentwiseTestwiseModule { }
