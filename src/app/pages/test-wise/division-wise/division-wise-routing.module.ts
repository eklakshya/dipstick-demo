import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DivisionWiseComponent } from './division-wise.component';

const routes: Routes = [{ path: '', component: DivisionWiseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DivisionWiseRoutingModule { }
