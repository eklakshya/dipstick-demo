import { ComponentFixture, TestBed } from '@angular/core/testing';

import {  DivisionWiseComponent } from './division-wise.component';

describe('AllParticipantsDivisionWiseComponent', () => {
  let component: DivisionWiseComponent;
  let fixture: ComponentFixture<DivisionWiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DivisionWiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
