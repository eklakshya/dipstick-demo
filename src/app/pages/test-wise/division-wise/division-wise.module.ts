import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DivisionWiseRoutingModule } from './division-wise-routing.module';
import { DivisionWiseComponent } from './division-wise.component';
 import { QuestionBasedReportTemplateDivisionWiseModule } from 'src/app/templates/question-based-report-template-division-wise/question-based-report-template-division-wise.module';


@NgModule({
  declarations: [DivisionWiseComponent],
  imports: [
    CommonModule,
    DivisionWiseRoutingModule,
    QuestionBasedReportTemplateDivisionWiseModule
  ]
})
export class DivisionWiseModule { }
