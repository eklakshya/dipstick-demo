import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

 import { StudentWiseTestWiseComponent } from './student-wise-test-wise.component';
import { TestsModule } from './../../../commmon-modules/Tests.module';


@NgModule({
  declarations: [StudentWiseTestWiseComponent],
  exports: [StudentWiseTestWiseComponent],
  imports: [
    CommonModule,
    TestsModule
   ]
})
export class StudentWiseTestWiseModule { }
