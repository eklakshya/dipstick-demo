import { Component, OnInit } from '@angular/core';
import { constant } from 'src/app/shared/constant';
import { ConstantsService } from '../../../core/services/constants.service';

@Component({
  selector: 'app-student-wise-test-wise',
  templateUrl: './student-wise-test-wise.component.html',
  styleUrls: ['./student-wise-test-wise.component.css']
})
export class StudentWiseTestWiseComponent implements OnInit {

  APIURL = '';
  legend:any = constant.legendImage;

  constructor(private constants : ConstantsService) {  }

  ngOnInit(): void {
    const stringifiedStudentReportAssets = sessionStorage.getItem(this.constants.sessionStorage.STUDENTREPORTASSETS)
    const studentReportAssets = JSON.parse(stringifiedStudentReportAssets)
    this.APIURL = 'api/report/student/testwise?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid+'&studentName='+studentReportAssets.studentName;
  }

}
