import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentWiseTestWiseComponent } from './student-wise-test-wise.component';

describe('StudentWiseTestWiseComponent', () => {
  let component: StudentWiseTestWiseComponent;
  let fixture: ComponentFixture<StudentWiseTestWiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentWiseTestWiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentWiseTestWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
