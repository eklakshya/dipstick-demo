import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllParticipantsComponentT } from './all-participants.component';

const routes: Routes = [{ path: '', component: AllParticipantsComponentT }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllParticipantsRoutingModule { }
