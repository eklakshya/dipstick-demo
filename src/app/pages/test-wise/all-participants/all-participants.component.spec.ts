import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllParticipantsComponentT } from './all-participants.component';

describe('ViewAllReportsComponent', () => {
  let component: AllParticipantsComponentT;
  let fixture: ComponentFixture<AllParticipantsComponentT>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllParticipantsComponentT ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllParticipantsComponentT);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
