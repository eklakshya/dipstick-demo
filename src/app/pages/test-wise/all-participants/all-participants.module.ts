import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllParticipantsRoutingModule } from './all-participants-routing.module';
import { AllParticipantsComponentT } from './all-participants.component';
  import { QuestionBasedReportTemplateAllParticipantsModule } from 'src/app/templates/question-based-report-template-all-participants/question-based-report-template-all-participants.module';


@NgModule({
  declarations: [AllParticipantsComponentT],
  imports: [
    CommonModule,
    AllParticipantsRoutingModule,
    QuestionBasedReportTemplateAllParticipantsModule
  ]
})
export class AllParticipantsModule { }
