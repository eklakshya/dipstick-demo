import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Router} from "@angular/router";
import {ApiService} from '../../services/http.service'

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  departments = [];
  upload_department_Info= {};
  year:any;
   dep_id:any;
  courseList:any;
  breadcrumb:any;
  crumb: boolean = false;
  yearforCrumb: boolean=true;
  branchCrumb: boolean=false;
  courseforCrumb: boolean;
  message: String;
  len: boolean;
  institutes:any[] = [];
  semesters;

  activeInstitute:any;
  activeSemester:any;
  activeCourse:any;
  upGenaralReports:any;
  activeYear: any;
  activeDepartment: any;

  upLoadpageDetails = {
    activeInstitute:undefined,
    activeYear: undefined,
    activeSemester: undefined,
    activeDepartment: undefined,
    activeCourse: undefined
  }
  backStatus: boolean = false; 
  constructor(private http: HttpClient,private api:ApiService, public router: Router, private modalService: NgbModal) { 
  
  }

  ngOnInit(){
    let role =localStorage.getItem('user');
    role == "admin" ? localStorage.removeItem('instituteid') : console.log({role});
    this.listInstitutes();
    this. getStudentFilters();
  // this.getDepartments();
  }


  getDepartments(){
    console.log(this.activeSemester._id)
    this.http.get(this.api.BASEURL + 'api/departments?semesterid='+this.activeSemester._id, this.api.headers())
    .subscribe((response) => {
      this.departments = response['departments'];
      // let arr = []
      // this.departments.forEach(department=>{
      //   arr.push(department.year)
      // })
      // let removed_duplicate = [...new Set(arr)];
      // this.year = removed_duplicate.sort(); 
      // console.log('years',this.year);
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  }
  open(modal) {
    this.message = '';
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', size:'lg', backdrop: 'static',keyboard: false});
  }

  onClickSubmit(data) {
    // const years = data.year.split('-');
    // console.log(JSON.parse(years[0]));
    // if(years[1].length<=0){
    //   console.log(JSON.parse(years[1]));
    // }else{
    //   return 0;
    // }
    // console.log(data)
    this.upload_department_Info = {
      departmentName:data.department ,
      year: data.year || this.activeYear,
      instituteid : this.activeInstitute._id,
      semesterid : this.activeSemester._id
    }
    console.log(this.activeSemester._id)
    // console.log('added department information', this.upload_department_Info);
    // console.log(this.upload_department_Info)
    this.http.post(this.api.BASEURL + 'api/add-department', this.upload_department_Info,this.api.headers())
    .subscribe((response) => {
      if (response) {
        this.getDepartments();
        this.len = response['Isthere'].status;
        this.message = this.len ? "Department Created Successfully" : "Department Already exists"
        // console.log(this.message);
        setTimeout(function () {
          document.getElementById('close').click();
        }, 2000);
     }     
      else {
        alert('department not added please try again')    
     }
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }
  
  goupload(data) {
    this.router.navigate(['/department/'+data]);
  }


  setYear(year) {
    this.yearforCrumb = false;
    this.branchCrumb = true;
    this.crumb=true;
    localStorage.removeItem("data");
    this.activeYear = year;
    this.activeYear = this.activeYear 
    console.log('this.activeYear',this.activeYear);
    this.setUPdetails()
  }

  setSemester(semester) {
    this.branchCrumb = false;
    this.yearforCrumb = false;
    this.activeSemester = semester;
    this.breadcrumb ='upload-data/'+this.activeInstitute.name+'/'+this.activeYear+'/'+this.activeSemester.name;
     localStorage.setItem('breadcrumb',this.breadcrumb);
     this.getDepartments()
     this.setUPdetails()
  }

  async setdepartment(data) {
    this.branchCrumb = false;
    this.yearforCrumb = false;
    this.backStatus = true;
    this.goupload(data._id)
    this.activeDepartment = data;
    this.dep_id =data._id;
    this.breadcrumb ='upload-data/'+this.activeInstitute.name+'/'+this.activeYear + '/' + this.activeSemester.name + '/'+this.activeDepartment.name;
     localStorage.setItem('breadcrumb',this.breadcrumb);
     this.setUPdetails()
    }
  back(data) {
    this.branchCrumb = false;
    this.yearforCrumb = true;
    this.activeYear = '';
  }
  setInstitute(institute){
    this.activeInstitute = institute;
     this.getSemesters();
     this.setUPdetails();
  }
  getSemesters(){
    this.http.get(this.api.BASEURL + 'api/semester?instituteid='+this.activeInstitute._id,this.api.headers())
    .subscribe((response:any) => {
      this.semesters = response;
      this.listYears()
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  }
  listYears(){
    let arr = []
    this.semesters.forEach(semester=>{
      arr.push(semester.year)
    })
    let removed_duplicate = [...new Set(arr)];
    this.year = removed_duplicate.sort(); 
  }
  onInstituteRegister(data){
    this.http.post(this.api.BASEURL + 'api/institute', {name:data.institute},this.api.headers())
    .subscribe((response:any) => {
      console.log(response)
      this.message = response.institutes.length == this.institutes.length ? "Institution Already exists" : "institution Created Successfully";
      this.institutes = response.institutes
      console.log(this.message);
      setTimeout(function () {
        document.getElementById('close').click();
      }, 2000);
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  }
  listInstitutes(){
    this.http.get(this.api.BASEURL + 'api/institute',this.api.headers())
    .subscribe((response:any) => {
      this.institutes = response;
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  }
  createSemester(data) {
    console.log(data)
    const { semester, year } = data
    const semesterData = {
      name : semester,
      year : year || this.activeYear,
      instituteid : this.activeInstitute._id
    }
    this.http.post(this.api.BASEURL + 'api/semester?instituteid='+this.activeInstitute._id, semesterData, this.api.headers())
    .subscribe((response:any) => {
      console.log(response)
      this.message = response.status ? "Semester created successfully" : "Error creating semester";
      this.semesters = response.semesters;
      this.listYears()
      setTimeout(function () {
        document.getElementById('close').click();
      }, 2000);
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  }
  resetBreadcrumb(level:number){
    console.log(level)
    // level<=2 && (this.activeDepartment = undefined);
    // level<=1 && (this.activeYear = undefined);
    // level<=0 && (this.activeInstitute = undefined);
    switch (level) {
      case 0:
        [this.activeInstitute, this.activeYear, this.activeSemester, this.activeDepartment] = [ undefined, undefined, undefined, undefined ];
        // this.listInstitutes();
        this.semesters = [];
        this.departments = [];
       this.setUPdetails();
        break;
      case 1:
        [this.activeYear, this.activeSemester, this.activeDepartment] = [ undefined, undefined, undefined ];
        this.setUPdetails();
        this.departments = [];
        break;
      case 2:
        [this.activeSemester, this.activeDepartment] = [ undefined, undefined ];
        this.setUPdetails();
        this.departments = [];
        break;
      case 3:
        [this.activeDepartment] = [ undefined ];
        this.setSemester(this.activeSemester);
        this.setUPdetails();
      default:
        break;
    }
  }

  setUPdetails(){
    const { activeInstitute, activeYear, activeSemester, activeDepartment, activeCourse } = this
    this.upGenaralReports = { activeInstitute, activeYear, activeSemester, activeDepartment, activeCourse }
    // localStorage.setItem('activeYear',activeYear);
    localStorage.setItem('upf', JSON.stringify(this.upGenaralReports))
  }

  getStudentFilters() {
    const UPF = 'upf';
    const upFilters = localStorage.getItem(UPF);
    this.upGenaralReports = upFilters != null ? JSON.parse(upFilters) : { activeInstitute: undefined, activeYear: undefined, activeSemester: undefined, activeDepartment: undefined, activeCourse: undefined };
    this.activeInstitute = this.upGenaralReports.activeInstitute;
    this.activeYear = this.upGenaralReports.activeYear //|| localStorage.getItem('activeYear');
    this.activeSemester = this.upGenaralReports.activeSemester;
    //this.activeDepartment = this.upGenaralReports.activeDepartment
    //this.activeCourse = this.upGenaralReports.activeCourse;
    (!this.activeInstitute) && this.listInstitutes();
    (this.activeInstitute && !this.activeYear) && this.setInstitute(this.activeInstitute);
   (this.activeInstitute && this.activeYear && !this.activeSemester) && this.setYear(this.activeYear), this.getSemesters();
   (this.activeInstitute && this.activeYear && this.activeSemester && !this.activeDepartment) && this.setSemester(this.activeSemester);
   (this.activeInstitute && this.activeYear && this.activeSemester && this.activeDepartment) && (this.backStatus=true);
  //   (this.activeInstitute && this.activeYear && this.activeSemester && this.activeDepartment && this.activeCourse) && this.chooselevel(this.activeCourse)
   }
}
