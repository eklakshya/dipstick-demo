import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgbAlert, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/http.service'
import { timezoneList } from 'src/app/shared/timezone';

//"xlsx": "^0.16.9",
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})

export class UploadComponent {

  COURSENAME: string = "Course Name"
  uploadedFiles: Array<File>;
  fileInfo: string[] = [];
  courseList: Array<Object>;
  courseName: string;
  uploadInfo: object;
  selectedCourse: string;
  excelColumns = [];
  validColumns = [];
  columnCodes: any = {};
  uploadedExcelData: any = { missingColumns: [], isSameCourse: false };
  selectedCourseId: string
  message: String;
  sub: any;
  depId: any;
  testdata: any;
  filedata: any;
  uploadPregressBar = {
    status: 0,
    progressvalue: 0,
    initState: true,
    courseid: "",
  }
  deletedData: any;
  breadcrumb: any;
  columnNames: any[] = []
  response: any;
  hiddenTest = true;
  errMsg: boolean;
  deletedMsg: boolean;
  forTestCourseid: any
  selectedTest: any;
  CID: any;
  cName: string;
  maxFileSize: number = 1 * 8 * 1024 * 600; // 600 KB
  isLargeFile: boolean = false;
  DID: any;
  modalOption: NgbModalOptions = {};
  courseData: any = {
    courseName: '',
    courseCode: '',
    program: '',
    departmentid: ''
  };
  attempt: number = 1;
  generate: any;
  link: string;
  copiedStates: boolean[] = [];
  // maxAttempts: number = 0;
  maxNumber: number = 0;
  att: any
  moduleNames: string[] = [];
  content: any;
  activeDepartment: any;
  activeDepartmentId: any;
  activeSemester: any;
  activeSemesterId: any;
  activeYear: any;
  semestersList: any[] = [];
  yearList: any[] = [];
  departmetnList: any[] = [];
  coursesList: any[] = [];
  studentList: any[] = [];
  searchText: any
  //temp list for view in the filter
  TempsemestersList: any[] = [];
  TempyearList: any[] = [];
  TempdepartmentList: any[] = [];
  TempcoursesList: any[] = [];
  activeCourse: any;
  activeCourseId: any;
  activeCourse1: any;
  institutes: any[] = [];
  activeInstitute: any = null;
  semesters: any[] = [];
  moduleVideos: { moduleId: string, videos: { videoName: string, videoUrl: string }[] } = {
    moduleId: '',
    videos: []
  };
  extractedCourses: Array<Object>;

  editingVideoRowIndex: number | null = null;
  extractedCourseName: string
  savedId: string;

  timezoneList = timezoneList;
  activeTestDropdown: number | null = null;
  selectedTests: { [key: string]: string[] | any } = {};
  startDateTime: any;
  endDateTime: any;
  copyTests: boolean = false;
  copyModules: boolean = false;
  originalCourseId: string;
  isCloneMode: boolean;
  private modalRef: NgbModalRef;
  departments = [];
  editingModuleStartDate: '';
  editingModuleEndDate: '';
  editingModuleStartTime: '';
  editingModuleEndTime: '';
  isModuleEndDateDisabled: boolean;
  cdr: any;
  isError: boolean = false;
  isEyeOpen = false;
  selectedTestId: string | null = null;
  visibleFileIndex: number | null = null;
  visibleQuestionFileIndex: number | null = null;
  visibleNormalFileIndex: number | null = null;
  selectedCourse_Id: string | null = null;
  fileCount: number = 0;
  isLoading: boolean = false; 
  responseMessage: any;



  /**
   *  
   * @param status
   * @param initState 
   */

  constructor(public router: Router, private api: ApiService, private http: HttpClient, private modalService: NgbModal, private route: ActivatedRoute) {
    this.sub = this.route.params.subscribe(params => {
      this.depId = params['id'];
    });
  }



  ngOnInit(): void {
    this.getcourseList();
    this.breadcrumb = localStorage.getItem('breadcrumb');
  }


  videoTableData: { moduleName: string, videos: { videoName: string, videoUrl: string }[] }[] = [
    { moduleName: '', videos: [{ videoName: '', videoUrl: '' }] }
  ];



  addVideoRow() {
    this.content.push({ moduleName: '', videos: [{ videoName: '', videoUrl: '' }] });
    this.editingVideoRowIndex = this.content.length - 1;
  }

  editVideo(rowIndex: number) {
    this.editingVideoRowIndex = rowIndex;
  }

  saveVideo(rowIndex: number, moduleName: string, videos: { videoName: string, videoUrl: string }[],) {
    if (!moduleName.trim() || videos.some(video => !video.videoName.trim() || !video.videoUrl.trim())) {
      alert("Module Name, Video Name, and Video URL cannot be empty.");
      return;
    }
    const startTime: number = new Date(this.editingModuleStartDate + 'T' + this.editingModuleStartTime).getTime();
    const endTime: number = new Date(this.editingModuleEndDate + 'T' + this.editingModuleEndTime).getTime();
    const isNewModule = this.content[rowIndex]._id == null;
    console.log("Saving videos:", videos);

    if (isNewModule) {
      this.saveModules({
        moduleName, videos, courseid: this.cid, coursename: this.extractedCourseName, startTime,
        endTime
      });
    } else {
      const moduleId = this.content[rowIndex]._id;
      this.updateModule(moduleId, moduleName, videos, startTime, endTime);
    }

    this.editingVideoRowIndex = null;
  }

  addVideo(moduleIndex: number) {
    const module = this.content[moduleIndex];


    if (module.videos.some(video => !video.videoName.trim() || !video.videoUrl.trim())) {
      alert('Please fill out all existing video names and URLs before adding a new row.');
      return;
    }


    module.videos.push({ videoName: '', videoUrl: '', isNew: true });
    this.editingVideoRowIndex = moduleIndex; // Set the module to be in edit mode
    const startTime: number = new Date(this.editingModuleStartDate + 'T' + this.editingModuleStartTime).getTime();
    const endTime: number = new Date(this.editingModuleEndDate + 'T' + this.editingModuleEndTime).getTime();
    const moduleId = module._id;
    if (moduleId) {
      this.updateModule(moduleId, module.moduleName, module.videos, startTime, endTime);
    } else {
      this.saveModules({
        moduleName: module.moduleName,
        videos: module.videos,
        courseid: this.cid,
        coursename: this.extractedCourseName,
        startTime,
        endTime
      });
    }
  }


  removeVideo(moduleIndex: number, videoIndex: number) {
    this.content[moduleIndex].videos.splice(videoIndex, 1);
  }

  getcourseList() {
    this.http.get(this.api.BASEURL + 'api/courses?id=' + this.depId, this.api.headers())
      .subscribe((response) => {
        this.courseList = response['courses'];
        if (this.courseList.length > 0) {
          this.extractedCourseName = (this.courseList[0] as any).name;
        }
        console.log("Extracted courses:", this.extractedCourses);
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
      });
  }

  saveModules(dataToSave: {
    moduleName: string, videos: { videoName: string, videoUrl: string }[], courseid: string, coursename: string, startTime: number,
    endTime: number
  }) {
    this.http.post(this.api.BASEURL + 'api/videos/', dataToSave, this.api.headers())
      .subscribe((response: any) => {
        console.log('Saved successfully', response);
        this.savedId = response.data._id;
        this.getVideos(this.savedId);
        this.getModuleName(this.cid);
      }, (error: any) => {
        console.log('Error saving data', error);
      });
  }


  getModuleName(courseId: string) {
    this.content = [];
    this.http.get(this.api.BASEURL + `api/videos?courseid=${courseId}`, this.api.headers())
      .subscribe((response: any) => {
        this.moduleNames = response.moduleName;
        this.content = response.content;

      }, (error: any) => {
        console.log('Error caught for API:', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
      });
  }


  getVideos(moduleId: string) {
    this.http.get(this.api.BASEURL + `api/videos/listCourseModules?moduleid=${moduleId}`, this.api.headers())
      .subscribe((response: any) => {
        console.log('Videos for module:', moduleId, '::', response);
      }, (error: any) => {
        console.log('Error caught for API:', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
      });
  }


  updateModule(moduleId: string, moduleName: string, videos: { videoName: string; videoUrl: string; }[], startTime: number, endTime: number) {
    const dataToUpdate = {
      moduleId,
      moduleName,
      videos
    };
    this.http.put(this.api.BASEURL + `api/videos/updateModule?moduleid=${moduleId}`, dataToUpdate, this.api.headers())
      .subscribe((response) => {
        console.log('Module updated successfully', response);
      }, (error: any) => {
        console.log('Error updating module', error);
      });
  }



  toggleModule(moduleId: string, event: Event): void {
    const isChecked = (event.target as HTMLInputElement).checked;

    if (isChecked) {
      this.enableModule(moduleId);
    } else {
      this.disableModule(moduleId);
    }
  }


  enableModule(moduleId: string): void {
    this.http
      .put(this.api.BASEURL + `api/videos/enable?moduleid=${moduleId}`, this.api.headers())
      .subscribe(
        (response) => {
          console.log('Module enabled successfully', response);
        },
        (error: any) => {
          console.error('Error enabling module', error);
        }
      );
  }


  disableModule(moduleId: string): void {
    this.http
      .put(this.api.BASEURL + `api/videos/disable?moduleid=${moduleId}`, this.api.headers())
      .subscribe(
        (response) => {
          console.log('Module disabled successfully', response);
        },
        (error: any) => {
          console.error('Error disabling module', error);
        }
      );
  }




  test(coursedata: any) {
    let formData = new FormData();
    for (let i = 0; i < this.uploadedFiles.length; i++) {
      formData.append('uploads[]', this.uploadedFiles[i], this.uploadedFiles[i].name);
    }
    this.http.get(this.api.BASEURL + 'api/fileupload/validation', this.api.headers()).subscribe((response: any) => {
      this.validColumns = response.columnNames;
    })

    let options = {
      headers: new HttpHeaders()
        .set('coursedata', JSON.stringify(coursedata))
        .set('role', JSON.stringify(localStorage.getItem('user') == "user" ? 2 : 1))
        .set('authorization', JSON.stringify(localStorage.getItem('token')))
    }
    this.http.post(this.api.BASEURL + 'api/fileupload/validation', formData, options)
      .subscribe((response: any) => {
        this.uploadedExcelData = response;
        this.excelColumns = response.columnKeysJson;
        this.errMsg = this.uploadedExcelData.columns.length == 0 ? true : false;

      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }

  getFileInfo(): void {
    this.fileInfo = [];
    for (let i = 0; i < this.uploadedFiles.length; i++) {
      this.fileInfo.push(`${this.uploadedFiles[i].name} (${this.getBytes(this.uploadedFiles[i].size)})`);
    };

  }

  onFileChange(element: { target: { files: File[]; }; }, coursedata: any) {
    let files = element.target.files;
    //check file is valid
    this.isLargeFile = files[0].size >= this.maxFileSize;
    if (files[0].size >= this.maxFileSize) {
      return;
    }
    if (!this.validateFile(files[0].name)) {
      alert('Selected file format is not supported');
      return false;
    } else {
      this.uploadedFiles = element.target.files;
      this.getFileInfo();
      this.test(coursedata)
    }
  }

  validateFile(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    return (ext.toLowerCase() == 'xlsx')
  }

  onChange(validCode: string, currentCode: any) {
    this.columnCodes[currentCode.cellnum] = validCode;
    let mappedJson = {}
    mappedJson[validCode] = currentCode.value;
    this.columnNames.push(mappedJson)

  }

  upload(course: any) {
    const id = course._id;
    let formData = new FormData();
    this.uploadPregressBar = { status: 2, progressvalue: Math.floor(Math.random() * 100), courseid: this.selectedCourseId, initState: false }
    this.uploadPregressBar.progressvalue = 71
    for (let i = 0; i < this.uploadedFiles.length; i++) {
      formData.append('uploads[]', this.uploadedFiles[i], this.uploadedFiles[i].name);
    }
    this.uploadInfo = {
      assessmentType: 'posttest',
      courseName: this.selectedCourse,
      fileName: this.fileInfo[0],
      courseid: id
    }
    let options: any = {
      headers: new HttpHeaders()
        .set('mappedcolumns', JSON.stringify(this.columnCodes))
        .set('courseDetails', JSON.stringify(this.uploadInfo))
        .set('mappedColumnNames', JSON.stringify(this.columnNames))
        .set('courseInfo', JSON.stringify(course))
        .set('role', JSON.stringify(localStorage.getItem('user') == "user" ? 2 : 1))
        .set('authorization', JSON.stringify(localStorage.getItem('token')))
        .set('attempt', "" + this.attempt)
    }
    this.http.post(this.api.BASEURL + 'api/fileupload', formData, options)
      .subscribe((response) => {
        this.response = response;
        this.uploadPregressBar = { status: 2, progressvalue: 100, courseid: this.selectedCourseId, initState: false }
        this.getcourseList();
        this.uploadedFiles = [];
        this.fileInfo = [];
        this.selectedCourseId = "";
        setTimeout((parent) => {
          parent.uploadPregressBar.status = parent.response.uploaded ? 1 : -1
          setTimeout((parent) => {
            parent.uploadPregressBar.initState = true;
          }, 30 * 1000, parent);
        }, 3000, this);

      }, (error: any) => {
        this.selectedCourseId = "";
        setTimeout((parent) => {
          parent.uploadPregressBar.status = -1
          setTimeout((parent) => {
            parent.uploadPregressBar.initState = true;
          }, 6000, parent);
        }, 3000, this);
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }



  getBytes(bytes: number): string {
    const UNITS = ['Bytes', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const factor = 1024;
    let index = 0;

    while (bytes >= factor) {
      bytes /= factor;
      index++;
    }

    return `${parseFloat(bytes.toFixed(2))} ${UNITS[index]}`;
  }

  open(modal: any) {
    const modalRef = this.modalService.open(modal, {
      backdrop: 'static',
      keyboard: false,
      windowClass: "myCustomModalClass",
      size: 'lg',
    }).result.then((result)=>{
      this.filedata=''
    },(reason)=>{
      this.filedata=''
    })
    this.message = '';
  }


  openModal(modal: any, course: any, isClone: boolean) {
    this.listInstitutes();
    console.log("open called with course: ", course);
    console.log("coursecode is ", course.coursecode)
    this.courseData = {
      courseName: isClone ? `${course.name} (copy)` : course.name,
      courseCode: isClone ? `${course.coursecode} (copy)` : course.coursecode,
      courseId: course._id,
      program: course.program,
      departmentid: course.departmentid
    };
    this.originalCourseId = course._id

    this.modalRef = this.modalService.open(modal, {
      backdrop: 'static',
      keyboard: false,
      windowClass: "myCustomModalClass",
      size: 'lg',
    });
    this.isCloneMode = isClone;
    this.message = '';
  }


  advancedSetting(modal: any, ID: any) {
    this.CID = ID._id;
    this.cName = ID.name;
    console.log(this.CID, this.cName)
    this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', size: 'lg' });
  }


  tableData = [];
  isEditing = false;
  editingRowIndex = -1;
  editingTestIndex = -1;
  isToggled: boolean = true;
  editingTestStartDate = '';
  editingTestStartTime = '';
  editingTestEndDate = '';
  editingTestEndTime = '';
  editingTestTimeZone = '';

  toggleSwitch(event: any, i: number) {
    this.isToggled = event.target.checked;
    this.editingRowIndex = i;
  }

  getDependencyNames(dependencies: string[]): string[] {
    return dependencies.map(depId => {
      const dependentTest = this.tableData.find(test => test._id === depId);
      return dependentTest ? dependentTest.name : 'Unknown Test';
    });
  }

  dependentTests: any = [];

  toggleTestSelection(rowId: string, { _id, name }: any): void {
    if (!this.selectedTests[rowId]) {

      this.selectedTests[rowId] = [];
    }

    const testIndex = this.dependentTests.indexOf(_id);
    if (testIndex > -1) {
      this.dependentTests.splice(testIndex, 1);
    } else {

      this.dependentTests.push(_id)
    }

    const index = this.selectedTests[rowId].indexOf(name);
    if (index > -1) {
      this.selectedTests[rowId].splice(index, 1);
    } else {
      this.selectedTests[rowId].push(name);
    }
  }

  DATE_FILTERS = { START: 'start', END: 'end' };
  isEndDateDisabled = true;

  inputDates(event: any, type: string) {
    const value = event.target.value;

    if (type == this.DATE_FILTERS.START) {
      this.editingTestStartDate = value;
      this.isEndDateDisabled = !this.editingTestStartDate;
    } else if (type == this.DATE_FILTERS.END) {
      if (new Date(this.editingTestStartDate) > new Date(value)) {
        return;
      }
      this.editingTestEndDate = value;
    }

    console.log("Start Date: ", this.editingTestStartDate, "End Date: ", this.editingTestEndDate);
  }

  inputTimes(event: any, type: string) {
    const value = event.target.value;

    if (type == this.DATE_FILTERS.START) {
      this.editingTestStartTime = value;
    } else if (type == this.DATE_FILTERS.END) {
      const startDateTime = new Date(`${this.editingTestStartDate}T${this.editingTestStartTime}`);
      const endDateTime = new Date(`${this.editingTestEndDate}T${value}`);

      if (startDateTime >= endDateTime) {
        return;
      }
      this.editingTestEndTime = value;
    }
  }



  validateDates() {
    const startDateTime = new Date(`${this.editingTestStartDate}T${this.editingTestStartTime}`);
    const endDateTime = new Date(`${this.editingTestEndDate}T${this.editingTestEndTime}`);

    if (this.editingTestStartDate && this.editingTestEndDate && startDateTime >= endDateTime) {
      alert("Start date and time must be earlier than end date and time.");
      this.editingTestEndDate = '';
      this.editingTestEndTime = '';
    }
  }


  validateModuleDates() {
    const startDateTime = new Date(`${this.editingModuleStartDate}T${this.editingModuleStartTime}`);
    const endDateTime = new Date(`${this.editingModuleEndDate}T${this.editingModuleEndTime}`);

    // Validate that the start time is earlier than the end time
    if (this.editingModuleStartDate && this.editingModuleEndDate && startDateTime >= endDateTime) {
      alert("Module start date and time must be earlier than end date and time.");
      this.editingModuleEndDate = '';
      this.editingModuleEndTime = '';
    }
  }

  inputModuleDates(event: any, type: string) {
    const value = event.target.value;

    if (type === this.DATE_FILTERS.START) {
      this.editingModuleStartDate = value;
      this.isModuleEndDateDisabled = !this.editingModuleStartDate;
    } else if (type === this.DATE_FILTERS.END) {
      if (new Date(this.editingModuleStartDate) > new Date(value)) {
        return;
      }
      this.editingModuleEndDate = value;
    }

    console.log("Module Start Date: ", this.editingModuleStartDate, "Module End Date: ", this.editingModuleEndDate);
  }

  inputModuleTimes(event: any, type: string) {
    const value = event.target.value;

    if (type === this.DATE_FILTERS.START) {
      this.editingModuleStartTime = value;
    } else if (type === this.DATE_FILTERS.END) {
      const startDateTime = new Date(`${this.editingModuleStartDate}T${this.editingModuleStartTime}`);
      const endDateTime = new Date(`${this.editingModuleEndDate}T${value}`);

      if (startDateTime >= endDateTime) {
        return;
      }
      this.editingModuleEndTime = value;
    }
  }


  timeZone(event: any) {
    this.editingTestTimeZone = event.target.value;
  }

  addRow() {
    const newRow = { _id: '', name: '', courseid: this.cid, depatmentid: this.did, isEnabled: true };
    this.editingTestStartDate = ''
    this.editingTestStartTime = ''
    this.editingTestEndDate = ''
    this.editingTestEndTime = ''
    this.tableData.push(newRow);
    this.activeTestDropdown = null;
    this.editingRowIndex = this.tableData.length - 1;
    const footer = document.getElementsByClassName('tests-list-modal-footer')[0]
    footer.scrollIntoView();
  }
  onCoursecreate(event: any, status: string, copyTests: boolean, copyModules: boolean, originalCourseId: string) {
    console.log('Form data:', event);
    this.courseName = event.courseName;
    this.courseData = Object.assign({}, event);
    if (this.isCloneMode) {
      this.courseData.departmentid = this.activeDepartmentId;
    } else {
      this.courseData.departmentid = this.depId;
    }
    this.courseData.status = status;
    this.courseData.copyTests = copyTests;
    this.courseData.copyModules = copyModules;
    this.courseData.originalCourseId = this.originalCourseId;
    this.createCourse()
    if (this.activeDepartmentId) {
      this.router.navigate([`/department/${this.activeDepartmentId}`]);
    }
  }

  createCourse() {
    this.http.post(this.api.BASEURL + 'api/course', this.courseData, this.api.headers())
      .subscribe((response: any) => {

        this.isError = false;


        if (response['course']) {
          this.message = response['message'] || "Course Created Successfully";
          this.courseList = response['courses'];
          this.getcourseList();
        } else {
          this.isError = true;
          this.message = "Course with the same name or course code already exists";
        }

        setTimeout(() => {

          document.getElementById('close').click();
          this.message = null;
          this.cdr.detectChanges();
        }, 2000);

      }, (error: any) => {
        console.log('error caught for api', error);
        this.isError = true;
        this.message = "Course already exists";
      });
  }

  createCourseCopy(courseData: any, copyTests: boolean, copyModules: boolean, modal: any) {
    if (this.isCloneMode) {
      this.onCoursecreate(courseData, 'duplicate', copyTests, copyModules, this.originalCourseId);
      setTimeout(() => {
        if (this.modalRef) {
          this.modalRef.close();
        }
      }, 3000);
    }
    else {
      this.updateCourse(courseData);
    }

  }
  updateCourse(courseData: any) {
    this.http.put(this.api.BASEURL + `api/course/update/${this.originalCourseId}`, courseData, this.api.headers())
      .subscribe((response) => {
        this.isError = false;

        this.message = "Course updated successfully";
        // if (this.modalRef) {
        //   this.modalRef.close();
        // }
        this.getcourseList();
        setTimeout(() => {
          if (this.modalRef) {
            this.modalRef.close();
          }
        }, 2000);
      }, (error: any) => {
        console.log('error caught for api', error);
        this.isError = true;
        this.message = "Course already exists";
      });
  }

  setCourseName(selectedCourse: { [x: string]: string; name: string; }) {
    this.selectedCourse = selectedCourse.name;
    this.selectedCourseId = selectedCourse["_id"];
  }

  tests(id: string, i: any) {
    this.visibleFileIndex = null;
    this.visibleQuestionFileIndex = null;
    this.visibleNormalFileIndex = this.visibleNormalFileIndex === i ? null : i;

    this.http.get(this.api.BASEURL + 'api/fileTest?id=' + id, this.api.headers())
      .subscribe((response) => {
        this.testdata = response['tests'];
        this.forTestCourseid = this.testdata.length <= 0 ? 123412351451134 : this.testdata[0].courseid;
        this.hiddenTest = true;
        this.filedata = '';

      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }

  files(id: string) {
    this.http.get(this.api.BASEURL + 'api/fileTest/files?id=' + id, this.api.headers())
      .subscribe((response) => {
        this.filedata = response['files'];
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }

  getformatedDate(createdDate: string) {
    return new Date(createdDate).toLocaleString()
  }

  delete(testId: any, name: any) {
    this.DID = testId;
    this.selectedTest = name;
  }

  deletestudentfile(fileId: string, fileName: string) {
    this.DID = fileId;
    this.selectedTest = fileName;
  }

  deletequestionfile(fileId: string, fileName: string) {
    this.DID = fileId;
    this.selectedTest = fileName;
  }

  delete_confirm() {
    

    this.http.get(this.api.BASEURL + 'api/fileTest/deleteTest?id=' + this.DID, this.api.headers())
      .subscribe((response) => {
        this.testdata = response;
        this.testdata.deletedmarks.deletedCount;
        this.deletedMsg = this.testdata.deletedmarks.deletedCount ? true : true;
        this.hiddenTest = false;
        setTimeout((parent) => {
          parent.deletedMsg = false;
        }, 3000, this);
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }

  delete_studentfile_confirm() {
    this.isLoading = true;
    this.responseMessage = 'Deleting the file, please wait...';
    this.deletenotification()


    this.http.get(`${this.api.BASEURL}api/fileTest/deleteStudentFile?id=${this.DID}`, this.api.headers())
      .subscribe((response) => {
        this.isLoading = false;
        this.filedata = response;
        this.responseMessage = this.filedata?.message ;
        this.deletenotification()
      },
      (error) => {
        this.isLoading = false; 
        this.responseMessage = 'Failed to delete the file. Please try again.';
        console.error(error);
      }
    );
  }

  delete_questionfile_confirm() {
    this.isLoading = true;
    this.responseMessage = 'Deleting the file, please wait...';
    this.deletenotification()
    this.http.get(`${this.api.BASEURL}api/fileTest/deleteQuestionFile?id=${this.DID}`, this.api.headers())
      .subscribe((response) => {
        this.isLoading = false;
        this.filedata = response;
        this.responseMessage = this.filedata?.message ;
        this.deletenotification()
        this.filedata = '';
      },);
  }


  resetBreadcrumb() {
    this.router.navigate(['/department']);
  }

  setAttempt(attempt: number) {
    this.attempt = attempt;
  }

  TestConfig(modal: any, course: any) {
    this.cid = course._id;
    this.did = course.departmentid;
    const modalRef = this.modalService.open(modal, { backdrop: 'static', keyboard: false, size: 'lg' });
    this.message = '';
    this.getTestslist();
  }

  getFilteredTests(currentIndex: number): any[] {
    return this.tableData.filter((test, index) => index !== currentIndex);
  }

  openlms(modal: any, course: any) {
    this.cid = course._id;
    this.did = course.departmentid;

    const modalRef = this.modalService.open(modal, {
      backdrop: 'static',
      keyboard: false,
      size: 'xxl',
    }).result.then((result)=>{
      this.filedata=''
    },(reason)=>{
      this.filedata=''
    });
    this.message = '';
    this.getTestslist();
  }

  openModules(modal: any, course: any) {
    this.content = [];
    this.cid = course._id;
    this.cName = course._name;
    const modalRef = this.modalService.open(modal, {
      backdrop: 'static',
      keyboard: false,
      size: 'lg',
    });
    this.message = '';
    this.getModuleName(this.cid);
  }




  cid = ""
  tid = ""
  did = ""

  testJson = {}

  getTestslist() {
    this.testJson = {}
    this.hiddenTest = false;
    this.dependentTests = [];
    this.editingRowIndex = -1;
    this.selectedTests = {};
    this.resetTestData()
    this.http.get(this.api.BASEURL + `api/test?courseid=${this.cid}&t=${Date.now()}`, this.api.headers())
      .subscribe((response: any) => {
        this.tableData = response.tests.sort((a, b) => new Date(a?.createdAt).getTime() - new Date(b?.createdAt).getTime());
        this.tableData.forEach((test: any) => {
          this.testJson[test._id.toString()] = test;
        })
        this.tableData.forEach((test: any) => {
          this.selectedTests[test._id.toString()] = test?.dependencies?.map((t: any) => this.testJson[t.toString()]?.name)
        })

      });

  }

  storeUrl(attemptsUrl: any) {
    console.log({ attemptsUrl })
  }

  createTest(test: any, testUrl: any, max_attempts: any, maxScore: number, startDateTime: number, endDateTime: number, dependencies: string[]) {
    const testData = { courseid: this.cid, name: test, url: testUrl, maxAttempts: max_attempts, maxScore, isEnabled: this.isToggled, startDateTime, endDateTime, dependencies };

    this.http.post(this.api.BASEURL + 'api/test', testData, this.api.headers())
      .subscribe((response: any) => {
        // this.tableData = [...this.tableData, ...response.tests].filter((t: any) => t._id)
        this.getTestslist()
      });

  }

  updateTest(i: any, test: any, testUrl: any, maxAttempts: any, id: any, maxScore: number, startDateTime: number, endDateTime: number, dependencies: string[]) {
    this.tid = id

    const updateData = { courseid: this.cid, name: test, url: testUrl, maxAttempts, maxScore, isEnabled: this.isToggled, startDateTime, endDateTime, dependencies };

    this.http.put(this.api.BASEURL + 'api/test/' + this.tid, updateData, this.api.headers())
      .subscribe((response: any) => {
        this.dependentTests = []

        this.getTestslist()

      });

  }


  saveTest(i: number, testname: string, testurl: string, maxAttempts: number, maxScore: number) {

    this.activeTestDropdown = null;
    this.editingRowIndex = -1;

    const startDateTime: any = new Date(this.editingTestStartDate + 'T' + this.editingTestStartTime);
    const endDateTime: any = new Date(this.editingTestEndDate + 'T' + this.editingTestEndTime);


    if (this.isToggled || !(startDateTime == 'Invalid Date' || endDateTime == 'Invalid Date')) {

      if (this.tableData[i]._id) {
        this.updateTest(
          i,
          testname,
          testurl,
          maxAttempts,
          this.tableData[i]._id,
          maxScore,
          startDateTime.getTime(),
          endDateTime.getTime(),
          this.dependentTests
        );
      } else {
        this.createTest(
          testname,
          testurl,
          maxAttempts,
          maxScore,
          startDateTime.getTime(),
          endDateTime.getTime(),
          this.dependentTests
        );
      }

    }

  }


  resetTestData() {
    this.editingTestEndDate = ''
    this.editingTestEndTime = ''
    this.editingTestStartDate = ''
    this.editingTestStartTime = ''
    this.activeTestDropdown = null;
  }


  editTest(index: number) {
    this.editingRowIndex = index;
    this.resetTestData()

    const currentTest = this.tableData[index]

    const startDateTime = currentTest.startDateTime;
    const endDateTime = currentTest.endDateTime;

    this.isToggled = currentTest.isEnabled;

    if (startDateTime) {
      const editingStartDateTime = this.getRegionalTime(currentTest.timezone, startDateTime)
      const startDate = new Date(startDateTime)
      const date = ("" + startDate.getDate()).padStart(2, '0');
      const month = ("" + (startDate.getMonth() + 1)).padStart(2, '0');
      const year = "" + startDate.getFullYear();

      const hours = ("" + startDate.getHours()).padStart(2, "0");
      const minutes = ("" + startDate.getMinutes()).padStart(2, "0");

      this.editingTestStartDate = `${year}-${month}-${date}`;
      this.editingTestStartTime = `${hours}:${minutes}`;

    }

    if (endDateTime) {
      const endDate = new Date(endDateTime)
      const date = ("" + endDate.getDate()).padStart(2, '0');
      const month = ("" + (endDate.getMonth() + 1)).padStart(2, '0');
      const year = "" + endDate.getFullYear();

      const hours = ("" + endDate.getHours()).padStart(2, "0");
      const minutes = ("" + endDate.getMinutes()).padStart(2, "0");

      this.editingTestEndDate = `${year}-${month}-${date}`;
      this.editingTestEndTime = `${hours}:${minutes}`;

    }
    this.dependentTests = currentTest.dependencies;
  }

  uploadstudentfile() {
    this.depId = this.courseData.departmentid;
    const courseid = this.cid;
    const depid = this.did;
    const file = this.uploadedFiles[0];
    const fileName = file.name;
    const filetype = 'student';

    let formData = new FormData();
    formData.append('studentfile', this.uploadedFiles[0]);
    formData.append('filename', fileName);
    formData.append('filetype', filetype);

    this.http.post(this.api.BASEURL + 'api/student?' + 'courseid=' + courseid + '&departmentid=' + depid + '&filename=' + encodeURIComponent(fileName), formData)
      .subscribe(
        (response: any) => {
          this.response = response;
          this.notification()
          setTimeout(() => {
            this.uploadPregressBar.initState = true;
            this.uploadPregressBar.status = 0;
          }, 30 * 1000);
        },
        (error: any) => {
          this.selectedCourseId = "";
          this.uploadPregressBar.status = -1;

          setTimeout(() => {
            this.uploadPregressBar.initState = true;
            this.uploadPregressBar.status = 0;
          }, 6000);

          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
        }
      );
  }

 
  toggleViewStudentFiles(i: number): void {
    const courseid = this.cid;
    const filetype = 'student';
  
    if (this.visibleFileIndex === i) {
      this.visibleFileIndex = null;
      this.isEyeOpen = false;
    } else {
      this.visibleFileIndex = i;
      this.visibleQuestionFileIndex = null;
      this.visibleNormalFileIndex = null;
      this.isEyeOpen = true;
    }
  
    this.selectedCourse_Id = courseid;
  
    if (this.visibleFileIndex !== null) {
      this.http.get(this.api.BASEURL + 'api/fileTest/listofstudentfiles?courseid=' + courseid + '&filetype=' + filetype, this.api.headers())
        .subscribe((response) => {
          this.filedata = response;
        }, (error: any) => {
          console.log('Error caught for API', error);
          if (error.status == 401 || error.status == 403) {
            this.api.sessionExp();
          }
        });
    } else {
      this.filedata = []; 
    }

  }

  uploadquestionfile(i: string | number) {
    const testid = this.tableData[i]._id
    const courseid = this.cid;
    const file = this.uploadedFiles[0];
    const fileName = file.name;
    const testname = this.tableData[i].name;

    let formData = new FormData();
    formData.append('questionfile', file);

    this.http.post(this.api.BASEURL + 'api/question-details/?courseid=' + courseid + '&testid=' + testid + '&fileName=' + encodeURIComponent(fileName) + '&testname=' + encodeURIComponent(testname), formData)
      .subscribe((response) => {
        this.response = response;
        this.notification()

        setTimeout((parent) => {
          parent.uploadPregressBar.status = parent.response.uploaded ? 1 : -1
          setTimeout((parent) => {
            parent.uploadPregressBar.initState = true;
            parent.uploadPregressBar.status = 0;
          }, 30 * 1000, parent);
        }, 30000, this);

      }, (error: any) => {
        this.selectedCourseId = "";
        setTimeout((parent) => {
          parent.uploadPregressBar.status = -1
          setTimeout((parent) => {
            parent.uploadPregressBar.initState = true;
            parent.uploadPregressBar.status = 0;
          }, 6000, parent);
        }, 3000, this);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }



  toggleViewQuestionFiles(i: number) {
    const testid = this.tableData[i]._id;
  
    if (this.visibleQuestionFileIndex === i) {
      this.visibleQuestionFileIndex = null;
      this.isEyeOpen = false;
    } else {
      this.visibleQuestionFileIndex = i;
      this.visibleFileIndex = null;
      this.visibleNormalFileIndex = null;
      this.isEyeOpen = true; 
    }
  
    this.selectedTestId = testid;
  
    if (this.visibleQuestionFileIndex !== null) {
      this.http.get<any>(`${this.api.BASEURL}api/fileTest/listofquestionfiles?id=${testid}`, this.api.headers())
        .subscribe((response) => {
          this.filedata = response;
        }, (error: any) => {
          console.error('Error retrieving file data', error);
          if (error.status === 401 || error.status === 403) {
            this.api.sessionExp();
          }
        });
    } else {
      this.filedata = []; 
    }
  }
  

  onFileChangep(element: { target: { files: File[]; }; }) {
    let files = element.target.files[0];

    if (files.size >= this.maxFileSize) {

      return;
    }
    if (!this.validateFile(files.name)) {
      alert('Selected file format is not supported');
      return false;
    } else {
      this.uploadedFiles = [files];
    }
  }

  setCourseNamep(selectedCourse: { [x: string]: string; name: string; }) {
    this.selectedCourse = selectedCourse.name;
    this.selectedCourseId = selectedCourse["_id"];
  }

  onFileChangeq(element: { target: { files: File[]; }; }) {
    let files = element.target.files[0];
    if (files.size >= this.maxFileSize) {
      return;
    }
    if (!this.validateFile(files.name)) {
      alert('Selected file format is not supported');
      return false;
    } else {
      this.uploadedFiles = [files];
    }
  }

  generatedToken(i: string | number, token: any) {
    const testid = this.tableData[i]._id
    this.http.get(this.api.BASEURL + 'api/easy-lms-integration/?courseid=' + this.cid + '&testid=' + testid)
      .subscribe((response: any) => {
        this.response = response;
        token = this.response.token
        this.generatelink(i, token, response?.endpointURL)
        setTimeout((parent) => {
          parent.uploadPregressBar.status = parent.response.uploaded ? 1 : -1
          setTimeout((parent) => {
            parent.uploadPregressBar.initState = true;
            parent.uploadPregressBar.status = 0;
          }, 30 * 1000, parent);
        }, 30000, this);

      }, (error: any) => {
        this.selectedCourseId = "";
        setTimeout((parent) => {
          parent.uploadPregressBar.status = -1
          setTimeout((parent) => {
            parent.uploadPregressBar.initState = true;
            parent.uploadPregressBar.status = 0;
          }, 6000, parent);
        }, 3000, this);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }

  generatelink(i: any, token: string, resource: string = '') {
    const host = resource || location.origin;
    this.link = host + this.api.BASEURL + 'api/easy-lms-integration?token=' + token;
    this.copy_link(i)
  }

  copy_link(i: number) {

    var dummyInput = document.createElement("input");
    dummyInput.setAttribute("value", this.link);
    document.body.appendChild(dummyInput);
    dummyInput.select();
    document.execCommand("copy");
    document.body.removeChild(dummyInput);
    this.showNotification(i);
  };

  showNotification(i: number): void {
    this.copiedStates[i] = true;

    setTimeout(() => {
      this.copiedStates[i] = false;
    }, 2000);
  }

  notificationMessage: any = ""
  notification() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 5000);
  }

  deletenotification() {
    var x = document.getElementById("snackbardelete");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 5000);
  }

  openTestDropdown(testIndex: number) {
    this.activeTestDropdown = this.activeTestDropdown == testIndex ? -1 : testIndex;
  }

  getUTCTime(fullDate: string, fullTime: string) {

    const [year, month, date] = fullDate.split("-")
    const [hour, minute] = fullTime.split(":")

    const utcDate = new Date().setUTCDate(+date);
    const utcMonth = new Date(utcDate).setUTCMonth(+month);
    const utcYear = new Date(utcMonth).setUTCFullYear(+year);
    const utcTimeStamp = new Date(utcYear).setUTCHours(+hour, +minute);

    return utcTimeStamp
  }

  setRegionalTime(timezone: string = "-04:30", utcTimeStamp: number) {

    const SECONDS = 60;
    const MILLI_SECONDS = 1000;

    const operator = timezone[0];
    const [hourChange, minuteChange] = timezone.slice(1).split(":");

    const totalDifference = ((+hourChange * 60) + +minuteChange) * SECONDS * MILLI_SECONDS;

    console.log({ totalDifference, utcTimeStamp, operator })
    if (operator == '-') {
      return utcTimeStamp + totalDifference
    } else {
      return utcTimeStamp - totalDifference
    }
  }

  getRegionalTime(timezone: string = "+06:00", timestamp: number) {
    const SECONDS = 60;
    const MILLI_SECONDS = 1000;
    const currentTimeZone = new Date().getTimezoneOffset()

  }


  listInstitutes() {
    this.http.get(this.api.BASEURL + 'api/institute', this.api.headers())
      .subscribe((response: any) => {
        this.institutes = response;
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }


  onInstituteChange(instituteId: string) {
    this.activeInstitute = this.institutes.find(institute => institute._id === instituteId);
    this.getSemesters();
    this.updateBreadcrumb();
  }


  getSemesters() {
    if (!this.activeInstitute) return;

    this.http.get(this.api.BASEURL + 'api/semester?instituteid=' + this.activeInstitute._id, this.api.headers())
      .subscribe((response: any) => {
        this.semesters = response;
        this.listYears();
        this.filterSemestersByYear(this.activeYear);
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }


  listYears() {
    let arr = this.semesters.map(semester => semester.year);
    this.yearList = [...new Set(arr)].sort();
  }

  filterSemestersByYear(year: string | null) {
    this.TempsemestersList = this.semesters.filter(semester => semester.year === year);
    this.activeSemesterId = null;
    this.activeSemester = null;
  }


  setNewYear(yearData: any) {
    console.log({ yearData });
    this.activeYear = yearData;
    this.onYearChange();
    this.updateBreadcrumb();
  }

  onYearChange() {
    this.TempsemestersList = [];
    this.activeSemester = null;
    this.activeSemesterId = null;
    this.activeYear && this.setSemesterList();
    this.onSemesterChange();
    this.updateBreadcrumb();
  }

  setSemesterList() {
    this.TempsemestersList = this.semesters.filter(sem => this.activeYear === sem['year']);
  }


  setNewSemester(semesterId: any) {
    this.activeSemesterId = semesterId;
    this.activeSemester = this.TempsemestersList.find((semesterData: any) => semesterData._id === this.activeSemesterId);
    this.onSemesterChange();
    this.updateBreadcrumb();
  }

  onSemesterChange() {
    this.activeDepartment = null;
    this.activeDepartmentId = null;
    this.TempdepartmentList = [];
    this.activeSemesterId && this.getDepartments();
  }
  getDepartments() {
    if (!this.activeSemesterId) return;

    this.http.get(this.api.BASEURL + 'api/departments?semesterid=' + this.activeSemesterId, this.api.headers())
      .subscribe((response: any) => {
        this.departments = response['departments'];
        this.TempdepartmentList = this.departments;
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }

  setNewDepartment(departmentId: any) {
    this.activeDepartmentId = departmentId;
    this.activeDepartment = this.TempdepartmentList.find((department: any) => department._id === departmentId);
    console.log({ "this.activeDepartment": this.activeDepartment });
    this.onDepartmentChange();
    this.updateBreadcrumb();
  }

  onDepartmentChange() {
    this.activeCourse = null;
    this.activeCourseId = null;
    this.TempcoursesList = [];
    this.activeDepartmentId && this.setCourseList();
  }


  updateBreadcrumb() {
    if (this.activeInstitute && this.activeYear && this.activeSemester && this.activeDepartment) {
      this.breadcrumb = `upload-data/${this.activeInstitute.name}/${this.activeYear}/${this.activeSemester.name}/${this.activeDepartment.name}`;
      localStorage.setItem('breadcrumb', this.breadcrumb);
    }
  }
  navigateToDepartment(courseId: string) {
    const departmentId = this.activeDepartmentId;  // Assuming you have the activeDepartmentId from selection
    this.router.navigate([`/department/${departmentId}`]);  // Adjust URL as needed
  }


  setCourseList() {
    this.TempcoursesList = this.coursesList.filter((course) => this.activeDepartmentId === course['departmentid']);
  }

  setNewCourse(courseid: any) {
    this.activeCourseId = courseid;
    this.activeCourse = this.coursesList.find((coursedata: any) => coursedata._id === courseid);
    console.log({ "this.activeCourse": this.activeCourse });
  }
}