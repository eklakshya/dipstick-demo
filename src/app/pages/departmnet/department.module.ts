import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartmentRoutingModule } from './department-routing.module';
import { DepartmentComponent } from './department.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import {UploadComponent } from './upload/upload.component'
import { FormsModule } from '@angular/forms';
 import { DivisionListForAttendenceComponentModule } from 'src/app/core/division-list-for-attendence/division-list-for-attendence.module';

@NgModule({
  declarations: [DepartmentComponent,DepartmentListComponent,UploadComponent],
  imports: [
    CommonModule,
    DepartmentRoutingModule,
    FormsModule,
    DivisionListForAttendenceComponentModule
  ]
})
export class DepartmentModule { }
