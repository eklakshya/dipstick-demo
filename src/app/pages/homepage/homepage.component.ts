import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import {  constant } from '../../shared/constant'
 
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  home:any=constant.homepageImage;
  loggedIn=true;
    body:any;
  constructor(){
    this.loggedIn = (localStorage.getItem("user") || localStorage.getItem("LMS_Student")) ? false  : true;
  }
  ngOnInit(): void {
    this.body = `<html> <h1>Hi Team</h1><div> Please contact us</div></html>`
  }

}  
