import { Component, OnInit } from '@angular/core';
import { RoutingService } from '../../core/services/routing.service'

@Component({
  selector: 'app-filtered-view-group-reports',
  templateUrl: './filtered-view-group-reports.component.html',
  styleUrls: ['./filtered-view-group-reports.component.css']
})
export class FilteredViewGroupReportsComponent implements OnInit {

  constructor(public routingService : RoutingService) { }
  departmentsList = [];
  activeDepartment: any;
  semestersList = [];
  activeSemester: any;
  activeYear: any;
  courseCode: any;
  coursesList = [];
  activeCourse: any;
  breadcrumbData : any;

  /**
   * first, checking the url and then by adding 
   * calling function which brings all the accessable data (semesters, departments, courses)
   * setting the first element as default selected incase there's only 1 semester/department/course
   */

  ngOnInit(): void {
  }

  setNewSemester(semesterData: any) {
    this.activeSemester = semesterData;
    this.onSemesterChange();
  }
  onSemesterChange() { }

  setNewDepartment(departmentData: any) {
    this.activeDepartment = departmentData;
  }
  onDepartmentChange() { }

  setNewCourse(courseData: any) {
    this.activeCourse = courseData;
  }
  onCourseChange() { }


  async chooselevel(coursedata) {
    this.activeCourse = coursedata;
    // this.selectedCourse = coursedata.name;
    this.settemp({ name: this.activeCourse.name, url: "", type: 'course' })
    this.breadcrumbData = 'You are here: reports/ ';
    this.breadcrumbData += this.activeYear + '/' + this.activeSemester + '/' + this.activeDepartment + '/' + this.activeCourse.name + ' - ' + this.activeCourse.coursecode
    // console.log("coursedata", coursedata, this.breadcrumbData);
    localStorage.setItem('data', this.breadcrumbData);
    localStorage.setItem('courseId', coursedata._id);
    localStorage.setItem('coursecode', coursedata.coursecode);
  }
  settemp(data: any) {
    this.routingService.setTemparray(data);
  }
}
