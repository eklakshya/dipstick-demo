import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilteredViewGroupReportsComponent } from './filtered-view-group-reports.component';

describe('FilteredViewGroupReportsComponent', () => {
  let component: FilteredViewGroupReportsComponent;
  let fixture: ComponentFixture<FilteredViewGroupReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilteredViewGroupReportsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilteredViewGroupReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
