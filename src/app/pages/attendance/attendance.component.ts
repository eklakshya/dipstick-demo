import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ApiService} from '../services/http.service';
import { ReportsService } from '../../core/services/reports.service'

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {

  courseid : string = localStorage.getItem("courseId")
  reportConfigurations : any[] = [];
  isReportVisible:Boolean = false;
  downloadingScoreList:Boolean = false;
  downloadingAttemptList:Boolean = false;

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales : {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          max:100
        },
      }],
      xAxes: [{
        ticks: {
          beginAtZero: true
        },
        barThickness: 30,
      }],
    }
  };
    public barChartType:string = 'bar';
    public barChartLegend:boolean = false;
  
    public barChartColors:Array<any> = [
      { 
        backgroundColor: '#4285f4', // 'rgba(77,20,96,0.3)',
        borderColor: 'rgba(77,20,96,1)',
        pointBackgroundColor: 'rgba(77,20,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,20,96,1)'
      },
  ];
  
  divisionWiseData = [];
  courseDetails:any = {};

  checkPopupVisibility(popupType:any){
    this.isReportVisible = this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == popupType)?.isVisible;
    return this.isReportVisible;
  }
  setReportConfigurations(){
    this.report.loadReportDisplayConfigurations((reportConfigurations:any)=>{
      this.reportConfigurations = reportConfigurations;
    })
  }

  constructor(private http :HttpClient,private api :ApiService, private report: ReportsService) {
    this.setReportConfigurations()
  }
  
  ngOnInit(): void {
    this.courseDetails = JSON.parse(localStorage.getItem("generalreportsfilters"))?.activeCourse;
    let url = this.api.BASEURL + 'api/report/attendance/divisionWise?courseid='+this.courseid;
    this.http.get(url,this.api.headers())
    .subscribe((response:any) => {
      const tests = response?.tests || [];
      const data: any[] = response?.data || [];
      const divisionList: any[] = response?.divisionList || [];
      const divisions = Array.from(new Set(data.map(d => d.division)));
    
      const roundUp = (num: number) => Math.ceil(num / 10) * 10;
      const defaultMaxValue = 100;
    
      this.divisionWiseData = divisions.map((division: any) => {
        const currentDivisionData = data.filter(d => d.division && d.testid && d.division === division);
        
        const scoresData = currentDivisionData.map(a => a.count);
        const maxScore = scoresData.length ? Math.max(...scoresData) : 0;
    
        const testids = currentDivisionData.map(d => d.testid);
        const maxValueForDivision = divisionList.find(d => d?.name === division)?.total || 0;
        const max = Math.max(roundUp(maxScore), roundUp(maxValueForDivision) || defaultMaxValue);
    
        const graphData: any = {
          division,
          max,
          divisionMax: maxValueForDivision || defaultMaxValue,
          scores: [{ data: scoresData, label: division }],
          tests: testids.map(id => tests.find(t => t._id === id)?.name || 'Unknown'),
          barChartOptions: {
            scaleShowVerticalLines: false,
            responsive: true,
            maintainAspectRatio: false,
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  max: max
                },
              }],
              xAxes: [{
                ticks: {
                  beginAtZero: true
                },
              }],
            }
          }
        };
    
        if (scoresData.length < 15) {
          graphData.barChartOptions.scales.xAxes[0].barThickness = 50;
        }
    
        return graphData;
      });
    }
    ,(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  }

  downloadAbsenteesList (division:any){
    const url = this.api.BASEURL + 'api/report/attendance/downloadAbsenteesList?courseid=' + this.courseid + '&encryptedDivision=' + btoa(division);
    this.downloadingAttemptList = true;

    this.http.get(url, { ...{ responseType: 'blob' }, ...this.api.headers() }).subscribe((response) => {
      const filename = this.courseDetails?.name + "-Absentees-List";
      this.responseToFile(response, filename);
      this.downloadingAttemptList = false;
    },
      (error: any) => {
        console.log('error caught for api', error);
        this.downloadingAttemptList = false;
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }

  downloadScoresList (division:any){
    const url = this.api.BASEURL + 'api/report/attendance/downloadScoresList?courseid=' + this.courseid + '&encryptedDivision=' + btoa(division);
    this.downloadingScoreList = true;
    this.http.get(url, { ...{ responseType: 'blob' }, ...this.api.headers() }).subscribe((response) => {
      const filename = this.courseDetails?.name + "-Scores-List";
      this.responseToFile(response, filename)
      this.downloadingScoreList = false;
    },
      (error: any) => {
        console.log('error caught for api', error);
        this.downloadingScoreList = false;
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }

  responseToFile(response, filename){
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.style.display = 'none';
      const blob = new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      const url = window.URL.createObjectURL(blob);
      a.href = url; a.download = filename + '.xlsx'; a.click();
      window.URL.revokeObjectURL(url);
  }
  
}
