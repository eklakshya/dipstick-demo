import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AttendanceRoutingModule } from './attendance-routing.module';
import { AttendanceComponent } from './attendance.component';
 import { CoreModule } from 'src/app/core/core.module';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [AttendanceComponent],
  imports: [
    CommonModule,CoreModule,
    AttendanceRoutingModule,
    ChartsModule,
    CoreModule
  ]
})
export class AttendanceModule { }
