import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../services/http.service';

@Component({
  selector: 'app-data-exchange',
  templateUrl: './data-exchange.component.html',
  styleUrls: ['./data-exchange.component.css']
})
export class DataExchangeComponent implements OnInit {
 
  constructor(private http: HttpClient, private api:ApiService) { }

  testwiseLogCountDate = "";
  dwld:boolean = true;
  dateMatch:boolean=true;
  ngOnInit(): void { }

  onDateChange(date: any) {
    this.testwiseLogCountDate = date.value//btoa(date.value);
    console.log(" this.testwiseLogCountDate", this.testwiseLogCountDate)
    let today:any = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is  0!
    let yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    console.log({today,'pickeddate':this.testwiseLogCountDate});
    this.dateMatch =  !(today < this.testwiseLogCountDate) ;
    console.log(this.dateMatch,'this.dateMatch')
  }
  getLogFileByDate() {
    this.dwld = false;
    const url = this.api.BASEURL + 'logstatistics/?date=' + btoa(this.testwiseLogCountDate);
    this.http.get(url, { ...{ responseType: 'blob' }, ...this.api.headers() }).subscribe((response) => {
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.style.display = 'none';
      const blob = new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      const url = window.URL.createObjectURL(blob);
      a.href = url; a.download = this.testwiseLogCountDate + '-test-wise-log-export.xlsx'; a.click();
      window.URL.revokeObjectURL(url);
      url ? this.dwld = true : alert("Something went wrong, Please try again");
    },
      (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }
}
