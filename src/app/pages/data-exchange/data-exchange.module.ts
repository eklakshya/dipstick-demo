import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataExchangeRoutingModule } from './data-exchange-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DataExchangeRoutingModule
  ]
})
export class DataExchangeModule { }
