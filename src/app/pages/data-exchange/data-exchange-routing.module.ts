import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataExchangeComponent } from './data-exchange.component';



const routes: Routes = [
  { path: '', component: DataExchangeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataExchangeRoutingModule { }
