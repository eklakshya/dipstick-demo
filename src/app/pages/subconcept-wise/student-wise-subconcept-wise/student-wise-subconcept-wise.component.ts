import { Component, OnInit, Input } from '@angular/core';
import { ConstantsService } from '../../../core/services/constants.service';

@Component({
  selector: 'app-student-wise-subconcept-wise',
  templateUrl: './student-wise-subconcept-wise.component.html',
  styleUrls: ['./student-wise-subconcept-wise.component.css']
})
export class StudentWiseSubconceptWiseComponent implements OnInit {

  APIURL = '';
  POPUPAPIURL = ''
  @Input() isPopupVisible : Boolean;
  constructor(private constants : ConstantsService) {  }

  ngOnInit(): void {
    const stringifiedStudentReportAssets = sessionStorage.getItem(this.constants.sessionStorage.STUDENTREPORTASSETS)
    const studentReportAssets = JSON.parse(stringifiedStudentReportAssets)
    this.APIURL = 'api/report/student/subconceptwise?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid+'&studentName='+studentReportAssets.studentName;
    this.POPUPAPIURL = 'api/report/student/subconceptwise/table?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid+'&studentName='+studentReportAssets.studentName;
  }
}
