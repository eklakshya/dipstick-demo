import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentWiseSubconceptWiseComponent } from './student-wise-subconcept-wise.component';

describe('StudentWiseSubconceptWiseComponent', () => {
  let component: StudentWiseSubconceptWiseComponent;
  let fixture: ComponentFixture<StudentWiseSubconceptWiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentWiseSubconceptWiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentWiseSubconceptWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
