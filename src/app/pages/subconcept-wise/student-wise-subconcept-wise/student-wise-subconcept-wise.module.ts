import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

 import { StudentWiseSubconceptWiseComponent } from './student-wise-subconcept-wise.component';
import { ConceptWiseModule } from 'src/app/commmon-modules/concept-wise.module';


@NgModule({
  declarations: [StudentWiseSubconceptWiseComponent],
  exports: [StudentWiseSubconceptWiseComponent],
  imports: [
    CommonModule,
    ConceptWiseModule
   ]
})
export class StudentWiseSubconceptWiseModule { }
