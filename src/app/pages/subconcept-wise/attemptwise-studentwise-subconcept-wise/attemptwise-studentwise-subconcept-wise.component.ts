import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConstantsService } from 'src/app/core/services/constants.service';

@Component({
  selector: 'app-attemptwise-studentwise-subconcept-wise',
  templateUrl: './attemptwise-studentwise-subconcept-wise.component.html',
  styleUrls: ['./attemptwise-studentwise-subconcept-wise.component.css']
})
export class AttemptwiseStudentwiseSubconceptWiseComponent implements OnInit {

  @Output() sendReportDetails = new EventEmitter();
  APIURL = '';
  POPUPAPIURL = '';
  @Input() isPopupVisible : Boolean;
  constructor(private constants : ConstantsService) {  }

  ngOnInit(): void {
    const stringifiedStudentReportAssets = sessionStorage.getItem(this.constants.sessionStorage.STUDENTREPORTASSETS)
    const studentReportAssets = JSON.parse(stringifiedStudentReportAssets)
    this.APIURL = 'api/report/student/subconceptwiseAttemptwise?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid+'&studentName='+studentReportAssets.studentName;
    this.POPUPAPIURL = 'api/report/student/subconceptwiseAttemptwise/table?studentid='+studentReportAssets.studentid+'&courseid='+studentReportAssets.courseid+'&studentName='+studentReportAssets.studentName;
  }
  getConceptWiseData(data:any){
    this.sendReportDetails.emit(data);
  }
}
