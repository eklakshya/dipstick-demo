import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttemptwiseStudentwiseSubconceptWiseComponent } from './attemptwise-studentwise-subconcept-wise.component';
import { ConceptWiseModule } from 'src/app/commmon-modules/concept-wise.module';
import { AttemptConceptReportsAllParticipantsModule } from 'src/app/shared/attempt-concept-reports-all-participants/attempt-concept-reports-all-participants.module';


@NgModule({
  declarations: [AttemptwiseStudentwiseSubconceptWiseComponent],
  imports: [
    CommonModule,
    ConceptWiseModule,
    AttemptConceptReportsAllParticipantsModule
  ],
  exports: [AttemptwiseStudentwiseSubconceptWiseComponent]
})
export class AttemptwiseStudentwiseSubconceptWiseModule { }
