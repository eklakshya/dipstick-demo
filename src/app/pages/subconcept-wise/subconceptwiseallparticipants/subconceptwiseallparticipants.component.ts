import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subconceptwiseallparticipants',
  templateUrl: './subconceptwiseallparticipants.component.html',
  styleUrls: ['./subconceptwiseallparticipants.component.css']
})
export class SubconceptwiseallparticipantsComponent implements OnInit {
  courseId: string = localStorage.getItem('courseId');
  url : any = 'api/report/subconceptwise/allParticipants/v1?courseid='+this.courseId;
  constructor() {   }
  ngOnInit(): void {
  }

}
