import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubconceptwiseallparticipantsRoutingModule } from './subconceptwiseallparticipants-routing.module';
import { SubconceptwiseallparticipantsComponent } from './subconceptwiseallparticipants.component';
import { CoreModule } from 'src/app/core/core.module';
import { ConceptWiseModule } from 'src/app/commmon-modules/concept-wise.module';
import { popUpTableModule } from 'src/app/commmon-modules/popUptable.module';
 import { QuestionBasedReportTemplateAllParticipantsModule } from 'src/app/templates/question-based-report-template-all-participants/question-based-report-template-all-participants.module';


@NgModule({
  declarations: [SubconceptwiseallparticipantsComponent],
  imports: [
    CommonModule,
    SubconceptwiseallparticipantsRoutingModule,
    QuestionBasedReportTemplateAllParticipantsModule
  ]
})
export class SubconceptwiseallparticipantsModule { }
