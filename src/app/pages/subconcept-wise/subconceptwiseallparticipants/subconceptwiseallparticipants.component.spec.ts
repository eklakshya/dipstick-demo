import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubconceptwiseallparticipantsComponent } from './subconceptwiseallparticipants.component';

describe('SubconceptwiseallparticipantsComponent', () => {
  let component: SubconceptwiseallparticipantsComponent;
  let fixture: ComponentFixture<SubconceptwiseallparticipantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubconceptwiseallparticipantsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubconceptwiseallparticipantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
