import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubconceptwiseallparticipantsComponent } from './subconceptwiseallparticipants.component';

const routes: Routes = [{ path: '', component: SubconceptwiseallparticipantsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubconceptwiseallparticipantsRoutingModule { }
