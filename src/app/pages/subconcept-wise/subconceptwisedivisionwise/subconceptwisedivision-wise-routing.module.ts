import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

 import { SubconceptwisedivisionwiseComponent } from './subconceptwisedivisionwise.component';

const routes: Routes = [{ path: '', component: SubconceptwisedivisionwiseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubconceptwisedivisionWiseRoutingModule { }
