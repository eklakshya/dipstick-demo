import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubconceptwisedivisionwiseComponent } from './subconceptwisedivisionwise.component';

describe('SubconceptwisedivisionwiseComponent', () => {
  let component: SubconceptwisedivisionwiseComponent;
  let fixture: ComponentFixture<SubconceptwisedivisionwiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubconceptwisedivisionwiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubconceptwisedivisionwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
