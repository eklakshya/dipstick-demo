import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubconceptwisedivisionWiseRoutingModule } from './subconceptwisedivision-wise-routing.module';
 import { CoreModule } from 'src/app/core/core.module';
import { SubconceptwisedivisionwiseComponent } from './subconceptwisedivisionwise.component';
import { ConceptWiseDivModule } from 'src/app/commmon-modules/concept-wise-div.module';
import { popUpTableModule } from 'src/app/commmon-modules/popUptable.module';
import { QuestionBasedReportTemplateAllParticipantsModule } from 'src/app/templates/question-based-report-template-all-participants/question-based-report-template-all-participants.module';
import { QuestionBasedReportTemplateDivisionWiseModule } from 'src/app/templates/question-based-report-template-division-wise/question-based-report-template-division-wise.module';


@NgModule({
  declarations: [SubconceptwisedivisionwiseComponent],
  imports: [
    CommonModule,
    SubconceptwisedivisionWiseRoutingModule,
    QuestionBasedReportTemplateDivisionWiseModule



  ]
})
export class SubconceptwisedivisionWiseModule { }
