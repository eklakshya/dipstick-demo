import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../core/services/reports.service'

@Component({
  selector: 'app-subconceptwisedivisionwise',
  templateUrl: './subconceptwisedivisionwise.component.html',
  styleUrls: ['./subconceptwisedivisionwise.component.css']
})
export class SubconceptwisedivisionwiseComponent implements OnInit {
  courseId: string = localStorage.getItem('courseId');
  url = 'api/report/subconceptwise/divisionWise/v1?courseid='+this.courseId;
  constructor(public reportService:ReportsService) { }
  ngOnInit(): void {
  }
}
