import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild,Input,Output, EventEmitter  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Modal } from 'bootstrap';
import { ConstantsService } from 'src/app/core/services/constants.service';
import { RoutingService } from 'src/app/core/services/routing.service';
import { ApiService } from '../services/http.service';
 import { FormGroup, FormControl, Validators } from '@angular/forms';
 import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';
 
@Component({
  selector: 'app-student-access',
  templateUrl: './student-access.component.html',
  styleUrls: ['./student-access.component.scss']
})
export class StudentAccessComponent implements OnInit {
  apiResponce: any
  payload: any;
  activeDepartment: any;
  activeDepartmentId: any;
  activeSemester: any;
  activeSemesterId: any;
  activeYear: any;
  activeCourse: any;
  activeCourseId: any;
  activeCourse1: any;
  breadcrumbData: any;
  courseCode: any;
  studentid: any;
  searchString: any;
  matchedCourses: any;
  cid:boolean=false;
  //to store the real data
  semestersList: any[] = [];
  yearList: any[] = [];
  departmetnList: any[] = [];
  coursesList: any[] = [];
  studentList: any[] = [];
  searchText: any
  //temp list for view in the filter
  TempsemestersList: any[] = [];
  TempyearList: any[] = [];
  TempdepartmentList: any[] = [];
  TempcoursesList: any;
 
  TempstudentsList: any[] = [];
filteredStudentsList: any[] = [];

  accessToken: any;
  accessData: any;
  selectedStudent:any[] = [];
  selectedNotRegisteredList: string[] = [];
  notRegisteredListSelectionStatus: boolean = false;
  show:boolean = false;
  studentSelectionStatus:boolean = false;
allSelected:boolean  = false;
selectedStudents: any[] = [];
notRegisteredNamesArray: string[] = [];
selectedNotSentStudents: string[] = [];
selectedStudentsByCourse: { [courseId: string]: string[] } = {};
showAlertMessage = false; 
  selectedCourseId: string;
  showModal: boolean;
  filteredNotSentStudentsList: any[] = []; 
modalVisible: boolean = false; 

notSentStudents:any[]= [];
 isModalOpen = false;
 currentCourseId: string;
 isModalProgressVisible: boolean = false;

  @ViewChild('searchfieldpage') input: ElementRef;
  isSearchPopupOpen: any; 
  semesters: any;
  departments: any;
  activeStudent: any;
  activestudentId:any
  message: string = "";
  progressStatus:any= '30%';
 studentCreation:any;
  MailsentApiRes:any

 @Input() activeInstitute: any;
 
 @Output() dipstickAccessStatus = new EventEmitter<boolean>();
  registeredMessage: string;
  notregisteredMessage: string;
  notRegisteredMessage: string;
  notRegisteredSelection: any;
  showNotRegisteredCheckboxes: boolean;
  showcheck: boolean;
  filteredStudentsList1:any;
  searchTerm: string = '';
   // For Student List
  // filteredStudentsList1: any[] = [];  // For Not Registered List
  isAllSelected: boolean;
  RegstudentSelectionStatus: boolean;
  selectAllChecked: boolean;
  showErrorIcon: boolean;
  alertMessage: string;
  alertMessages: string[];
  courseModal: any;
  notSentSelectedStudents: any[];
  searchNotSentTerm: string;
  notSentStudentSelectionStatus: any;
 
  isAllNotSentSelected: any;
  showNotcheck: boolean;
  filteredNotSentStudents: any[];
  registrationSuccessMessage: string;
  courseName: any;
  departmentName: any;
  year: any;
  semName: any;
  instituteName: any;
  isLoading: boolean = false; 
  responseMessage: any;
  studendata: any;

  constructor(public api: ApiService,
    public constants: ConstantsService,
    public routingService: RoutingService,
    private http: HttpClient,
    public router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
  ) {

    this.accessData = ''
    this.accessToken = ''
    this.filteredNotSentStudents = this.notSentStudents;
  }


  visitCourse(courseId: string) {
    this.selectedCourseId = courseId;
    this.showModal = true; 
  }
  
 
  // closeModal() {
  //   this.showModal = false; // Hide the modal
  //   this.selectedCourseId = null; // Reset selected course ID
  // }

  createStudentWithLazyload(){
   let num = Math.round(this.selectedStudent.length / 10);
   let value = (100 / Number(num));
    for(let i=1;i<=num;i++){
      setTimeout(()=> {
        this.progressStatus = ""+i * value+'%';
        console.log({"loading":this.progressStatus})
      }, 10000)
    }
  
  }
  showErrorAlert() {
    this.showAlertMessage = true;
  }


  reportsFilters: any = {
    activeYear: undefined,
    activeSemester: undefined,
    activeDepartment: undefined,
    activeCourse: undefined
  }

  ngOnInit(): void {
    const modalElement = document.getElementById('courseModal');
    if (modalElement) {
      this.courseModal = new Modal(modalElement);
    }
    this.showErrorIcon = false;
    this.fetchNotSentStudents();
    this.getSemesters(); 
    console.log({"institutteid":this.activeInstitute});
  }


  removeDuplicates(arr: any) {
    return new Array(...new Set(...arr))
  }


  getSemesters() {
    console.log({"in get sem":this.activeInstitute['_id']})
    Boolean((this.activeInstitute?._id)) && this.http.get(this.api.BASEURL + 'api/semester?instituteid=' + (this.activeInstitute['_id']), this.api.headers())
      .subscribe((response: any) => {
        this.dipstickAccessStatus.emit(Boolean(response));
        console.log({"data":Boolean(response)})
        this.semesters = response;
        let arr = []
        this.semesters.forEach(semester => {
          arr.push(semester.year)
        })
        let removed_duplicate = [...new Set(arr)];
        let years = removed_duplicate.sort();
        this.listYear(years);
        this.listSems(this.semesters)
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        })
  }

  //listing years
  listYear(year: any) {
    year.forEach((data) => {
      if (!this.yearList.includes(data)) {
        this.yearList.push(data);
      }
    })
  }

  //listing the semisters
  listSems(semesters: any) {
    semesters.forEach((data) => {
      if (this.semestersList.findIndex((semData: any) => semData._id == data._id) == -1) {
        this.semestersList.push(data);
      }
    });
  }

  //departmentList
  listDep(dep:any) {
    dep.forEach((data) => {
      if (this.departmetnList.findIndex((depData: any) => depData._id == data._id) == -1) {
        this.departmetnList.push(data);
      }
    });
    this.departmetnList && console.log('==departmetnList====', this.departmetnList);
  }

  getDepartments(semId:any) {
    (semId) && this.http.get(this.api.BASEURL + 'api/departments?semesterid=' + semId, this.api.headers())
      .subscribe((response: any) => {
        this.departments = response['departments'];
        console.log("department", this.departments[0].instituteid)
        this.listDep(this.departments);
        this.TempdepartmentList = [...this.departmetnList]
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() :console.log("Something went wrong!")
        })
  }



  getCourses(department){
    this.http.get(this.api.BASEURL + 'api/courses?id=' + department._id, this.api.headers())
    .subscribe(async (response) => {
      let courses  = response['courses']; 
      this.coursesList = [...courses]; 
      this.TempcoursesList = [...this.coursesList]
      this.TempcoursesList =await this.TempcoursesList.filter((course,i) => this.activeDepartmentId == course['departmentid']);
       console.log("dasdfasfdsadf",this.TempcoursesList)
     },
      (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }





getStudentsByCourseId(courseid) {
    const apiUrl = this.api.BASEURL + 'api/report/student/studentsAccess?courseid=' + courseid;
    this.http.get(apiUrl, this.api.headers()).subscribe(async (response: any) => {
        this.studentList = response;
        this.TempstudentsList = this.studentList.filter((student) => this.activeCourseId === student['courseid']);
        this.filteredStudentsList = [...this.TempstudentsList]; 
        this.filteredStudentsList.sort();
        document.getElementById('checkBoxes').style.display = this.filteredStudentsList.length ? 'block' : 'none';
    },
    (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
    });
}

filterStudents() {
    if (this.searchTerm) {
        this.filteredStudentsList = this.TempstudentsList.filter(student => 
            student.name.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
            student.usn.toLowerCase().includes(this.searchTerm.toLowerCase())
        );
    } else {
        this.filteredStudentsList = [...this.TempstudentsList]; 
    }
}

notRegisteredSearchTerm:string = ''

filterNotRegStudents(studentCreation) {
   this.filteredStudentsList1=[]
   studentCreation.forEach((data) =>{
        if(data.value.data === false){
          this.filteredStudentsList1.push(data.value.studentData);
        }else{
         console.log("data.value.studentData",data.value.studentData)
        }
      })
      
      console.log("filteredStudentsList1",this.filteredStudentsList1);

   
}




  getFilters() {
    this.apiResponce = this.accessData;

    //listing years
    this.apiResponce.accessData.forEach((data) => {
      if (!this.yearList.includes(data.semesterData.year)) {
        this.yearList.push(data.semesterData.year);
      }
    })

    //listing the semisters
    this.apiResponce.accessData.forEach((data) => {
      if (this.semestersList.findIndex((semData: any) => semData._id == data.semesterData._id) == -1) {
        this.semestersList.push(data.semesterData);
      }
    });
   




    //departmentList
    this.apiResponce.accessData.forEach((data) => {
      if (this.departmetnList.findIndex((departmentData: any) => departmentData._id == data.departmentData._id) == -1) {
        this.departmetnList.push(data.departmentData);
      }
    });
   

    //courseList
    this.apiResponce.accessData.forEach((data) => {
      this.coursesList.push(data.courseData);
    });
   


    (localStorage.getItem('userrole') == 'student') && (this.apiResponce.accessData.forEach((data) => {
      this.studentList.push(data.studentData);
    }));

    this.yearList && this.semestersList && this.departmetnList && this.coursesList && this.getStudentFilters();
    // });
  }  
  showCheckboxes() { 
    this.show =!this.show;
      var checkboxes = 
          document.getElementById("checkBoxes");
      if (this.show) {
          checkboxes.style.display = "block";
      } else {
          checkboxes.style.display = "none";
      }
  }

  showNotRegCheckboxes() { 
    this.showcheck =!this.showcheck;
      let notRegCheckBoxes = 
          document.getElementById("notRegCheckBoxes");
      if (this.showcheck) {
        notRegCheckBoxes.style.display = "block";
      } else {
        notRegCheckBoxes.style.display = "none";
      }
  }

   


  toggleSelectAll() {
    this.isAllSelected = !this.isAllSelected;

    if (this.isAllSelected) {
     
      this.selectedStudent = this.filteredStudentsList1.map(student => student._id);
    } else {
    
      this.selectedStudent = [];
    }
  }

  



  onStudentSelection(selectedStudentId:any,selectionStatus:any){
    if(selectionStatus=="all"){
      this.allSelected = true;
      if(this.selectedStudent.length != this.TempstudentsList.length){
        this.studentSelectionStatus = true;
        this.TempstudentsList.map((student:any)=>{
          if(!this.selectedStudent.includes(student._id)){
            this.selectedStudent.push(student._id);
          }
         })
      }else {
        this.studentSelectionStatus = false;
        this.selectedStudent = [];
        this.allSelected = false;
      }
    }else{
      this.studentSelectionStatus = false;
      this.allSelected = false;
      if (!this.selectedStudent.includes(selectedStudentId)) {
        this.selectedStudent.push(selectedStudentId);
      }else{
        this.selectedStudent.splice(this.selectedStudent.indexOf(selectedStudentId),1);
      }
    }
    console.log("selected student list : ",this.selectedStudent);
  }


  onNotRegStudentSelection(studentId: string, selectionType: 'single' | 'all') {
    if (selectionType === 'all') {
      this.selectAllChecked = !this.selectAllChecked; // Toggle select all state
      if (this.selectAllChecked) {
        // Select all students
        this.selectedNotRegisteredList = this.filteredStudentsList1.map(student => student._id);
      } else {
        // Deselect all students
        this.selectedNotRegisteredList = [];
      }
    } else {
      // Handle single student selection
      if (this.selectedNotRegisteredList.includes(studentId)) {
        // If already selected, deselect it
        this.selectedNotRegisteredList = this.selectedNotRegisteredList.filter(id => id !== studentId);
      } else {
        // If not selected, select it
        this.selectedNotRegisteredList.push(studentId);
      }
  
      // Update the selectAllChecked status
      this.selectAllChecked = this.selectedNotRegisteredList.length === this.filteredStudentsList1.length;
    }
  }
  setNewYear(yearData: any) {
    this.activeYear = yearData;
    this.onYearChange();
  }
  onYearChange() {
    this.TempsemestersList = [];
    [this.activeStudent, this.activestudentId] = [undefined, undefined];
    this.TempstudentsList = [];
    this.studentCreation = [];
    this.selectedStudent =[]; 
    this.activeCourseId = '';
    this.cid = false;
    this.message = '';
    this.TempcoursesList =[];
    this.studentSelectionStatus = false;
    [this.activeSemester, this.activeSemesterId] = [undefined, undefined];
    this.activeYear && this.setSemesterList()
    this.onSemesterChange()
  }
  setSemesterList() {
    this.TempsemestersList = [...this.semestersList]
    this.TempsemestersList = this.TempsemestersList.filter((sem) => this.activeYear == sem['year']);
  }

  //semester selections
  setNewSemester(semesterId: any) {
    this.activeSemesterId = semesterId;
    let activeSemesterData = this.TempsemestersList.find((semesterData: any) => semesterData._id == this.activeSemesterId)
     this.activeSemester = activeSemesterData.name;
    this.onSemesterChange();
    // this.onCourseChange()
  }
  onSemesterChange() {
    [this.activeDepartment, this.activeDepartmentId] = [undefined, undefined];
    this.activeSemesterId && this.getDepartments(this.activeSemesterId);
    this.activeCourseId = '';
    this.TempstudentsList = [];
    this.studentCreation = [];
    this.selectedStudent =[]; 
     this.cid = false;
     this.message = '';
    this.TempcoursesList =[];
    this.departmetnList = []
    this.TempdepartmentList && this.setDepartment();  
    this.onDepartmentChange()
    this.onCourseChange();
  }
  setDepartment() { 
    this.TempdepartmentList = [...this.departmetnList] 
    this.TempdepartmentList = this.TempdepartmentList.filter((dep,i) => this.activeSemesterId == dep[i]['semesterid']);
     console.log(this.TempdepartmentList, 'this.222222TempdepartmetnList ')
  }


  //department selections
  setNewDepartment(departmentId: any) {
    this.activeDepartmentId = departmentId; 
    let departmentData = this.TempdepartmentList.find((department: any) => department._id == departmentId)
    this.activeDepartment = departmentData.name;
    departmentData && this.getCourses(departmentData);
    this.onDepartmentChange();
    //this.onCourseChange();
  }
  onDepartmentChange() {
    this.activeCourse = []
    this.activeCourse1 = []
    this.activeCourseId =''
    this.TempcoursesList= []
    this.TempcoursesList = [];
     this.cid = false;
     this.message = '';
     this.activeCourseId = '';
     this.onCourseChange()
  }

  //course selections
  async setcourse() {  
  
    this.TempcoursesList = [...this.coursesList] 
    this.TempcoursesList =await this.TempcoursesList.filter((course,i) => this.activeDepartmentId == course.departmentid);
 
  }

  onCourseChange() {
    [this.activeStudent, this.activestudentId] = [undefined, undefined];
     this.TempstudentsList = [];
     this.studentCreation = [];
     this.selectedStudent =[]; 
     this.studentSelectionStatus = false;
     this.activestudentId = [];
     console.log({' this.studentCreation': this.studentCreation.length,'this.selectedStudent':this.selectedStudent.length })
     this.onChangeStudent();
     this.message = '';
   }
   onChangeStudent(){
    this.activeCourseId &&  this.showCheckboxes();
    this.message = '';
   }

  setNewCourse(courseid: any) {
    this.activeCourse = this.coursesList.find((coursedata: any) => coursedata._id == courseid)// courseid;
    this.activeCourseId = this.activeCourse._id;
    this.activeCourseId &&( this.cid = true);
    this.activeCourseId  &&  this.getStudentsByCourseId(this.activeCourseId)
    this.onCourseChange();
  }



   //course selections
   async setStudent() {  
    this.TempcoursesList = [...this.coursesList] 
    this.TempcoursesList =await this.TempcoursesList.filter((course,i) => this.activeDepartmentId == course[i].departmentid);
  }

  setNewStudent(studentid: any) {
    this.activeStudent = this.coursesList.find((studentdata: any) => studentdata._id == studentid)// courseid;
    this.activeCourseId = this.activeCourse._id;
  }


  //setting the filtered data to the local storage to display on refresh
  setReportFilters() {
    const { activeYear, activeSemester, activeDepartment, activeCourse, activeSemesterId, activeDepartmentId, activeCourseId } = this
    this.reportsFilters = { activeYear, activeSemester, activeDepartment, activeCourse, activeSemesterId, activeDepartmentId, activeCourseId }
    localStorage.setItem(this.constants.localStorage.REPORTSFILTERS, JSON.stringify(this.reportsFilters))
  }
  getStudentFilters() {
    const studentfilterKey = this.constants.localStorage.REPORTSFILTERS;
    const reportfilters = localStorage.getItem(studentfilterKey);
    this.reportsFilters = reportfilters != null ? JSON.parse(reportfilters) : { activeYear: undefined, activeSemester: undefined, activeDepartment: undefined, activeCourse: undefined, activeSemesterId: undefined, activeDepartmentId: undefined, activeCourseId: undefined };
    this.activeYear = this.reportsFilters.activeYear;
    this.activeSemester = this.reportsFilters.activeSemester;
    this.activeDepartment = this.reportsFilters.activeDepartment
    this.activeSemesterId = this.reportsFilters.activeSemesterId;
    this.activeDepartmentId = this.reportsFilters.activeDepartmentId
    this.activeCourse = this.reportsFilters.activeCourse.name;
    this.activeCourseId = this.reportsFilters.activeCourseId;

    // this.setDepartmentList();
    this.activeYear && this.setSemesterList();
    this.activeSemesterId && this.setDepartment()
    this.activeDepartmentId && this.setcourse();
  }

  //viewing filtered reports
  filter() {
    this.activeCourseId && this.reportsChange();
    this.setReportFilters()
    this.chooselevel();
    if (localStorage.getItem('userrole') == 'student') {
      //let studentid= sessionStorage.getItem('studentid');
      let studentName = localStorage.getItem('LMS_Student');
      this.getStudentid(this.activeCourse.name) && this.router.navigate(['/integration/reports'], {
        skipLocationChange: false,
        queryParams: { courseid: this.activeCourse._id, studentid: this.studentid, studentName }
      }).then(() => {
        location.reload();
      })
    } else {
      location.reload();
    }
  }
  getStudentid(courseName: any) {
    let studentInfo: any = this.studentList.filter((data) => data.courseName == courseName);
    this.studentid = studentInfo[0]['_id'];
    return (this.studentid);
  }
  reportsChange() {
    localStorage.setItem('courseId', this.activeCourse['_id']);
    let instituteid = this.activeCourse['instituteid'];
    instituteid && localStorage.setItem('instituteid', instituteid);
  }

  //for breadcrumb data, not using presently  
  async chooselevel() {
    this.settemp({ name: this.activeCourse['name'], url: "", type: 'course' })
    this.breadcrumbData = 'You are here: reports/ ';
    this.breadcrumbData += this.activeYear + '/' + this.activeSemester + '/' + this.activeDepartment + '/' + this.activeCourse['name']
    localStorage.setItem('data', this.breadcrumbData);
    localStorage.setItem('S_bread', this.breadcrumbData);
  }
  settemp(data: any) {
    this.routingService.setTemparray(data);
  }
  searchMatchedCourses(modal, flag: number, string: any) {
    this.searchString = string.trim();
    !this.isSearchPopupOpen && this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static', keyboard: false });
    this.matchedCourses = [];
    const searchExpression = new RegExp(this.searchString, 'ig');
    this.matchedCourses = this.accessData.accessData.filter((instituteData: any) => ((
      searchExpression.test(instituteData.semesterData.name) || (instituteData.semesterData.name.toLowerCase().includes(this.searchString)) ||
      searchExpression.test(instituteData.semesterData.year) || instituteData.semesterData.year.includes(this.searchString) ||
      searchExpression.test(instituteData.departmentData.name) || (instituteData.departmentData.name.toLowerCase().includes(this.searchString)) ||
      searchExpression.test(instituteData.courseData.name) || (instituteData.courseData.name.toLowerCase().includes(this.searchString)) ||
      searchExpression.test(instituteData.courseData.coursecode) || (instituteData.courseData.coursecode.toLowerCase().includes(this.searchString))
    )));
    this.isSearchPopupOpen = (flag == 1) ? true : false;
  }

  close(modal) {
    modal.dismiss('Cross click')
    this.isSearchPopupOpen = false;
    this.input.nativeElement.value = this.searchString;
    // this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', backdrop: 'static', keyboard: false });
  }

  getSearchString(string) {
    // this.searchString = string.trim();
    this.searchText = string.trim();
    //console.log("string", string)
  }


  setCourseData(i: number, modal: any) {
    modal.dismiss('Cross click')
    this.isSearchPopupOpen = false;
    // this.searchString = '';
    this.activeYear = this.matchedCourses[i].semesterData.year;
    this.activeSemester = this.matchedCourses[i].semesterData.name;
    this.activeDepartment = this.matchedCourses[i].departmentData.name;
    this.activeCourse = this.matchedCourses[i].courseData;
    this.activeSemesterId = this.matchedCourses[i].semesterData['_id']
    this.activeDepartmentId = this.matchedCourses[i].departmentData['_id']
    this.activeCourseId = this.matchedCourses[i].courseData['_id']
    this.close(modal);
    this.filter();
    // const { year, semesterid, semesterName, departmentName, departmentid, courseName, coursecode, courseid } = this.searchedCourseInfo.matchedCourses[filterCourseIndex];
    // [this.activeYear, this.activeSemester, this.activeDepartment ] = [year, {_id : semesterid, name : semesterName}, { _id : departmentid, name : departmentName }];
    // this.chooselevel({ _id : courseid, coursecode, name : courseName })

  }
  getDisplayText(i: number) {
    let semData: any = this.matchedCourses[i].semesterData;
    let depData: any = this.matchedCourses[i].departmentData;
    let courseData: any = this.matchedCourses[i].courseData;
    return ("" + (i + 1) + ". " + semData.year + " / " + semData.name + " / " + depData.name + " / " + courseData.name + " - " + courseData.coursecode).replace(RegExp(this.searchString, 'ig'), (word) => "<b>" + word + "</b>")
  }

  clear() {
  
    document.getElementById("searchString").focus()
  }
 


  
  async createStudent(data1, isModalCall = false) {
    this.registeredMessage = ''; 
    this.notRegisteredMessage = ''; 
    this.message = '';

    document.getElementById('checkBoxes').style.display = 'none';
    document.getElementById('msg').style.display = 'block';
    this.MailsentApiRes = true;
    this.isModalProgressVisible = true;
    this.progressStatus = "35%";

    setTimeout(() => {
        !(this.studentCreation.length) ? this.progressStatus = "60%" : '';
    }, 100);
    let data = data1 !== true ? data1 : {
      studentIds: this.allSelected ? '' : this.selectedStudent,
      instituteid: this.activeInstitute?._id,
      areAllSelected: this.allSelected,
      courseid: this.activeCourse._id
    };
    this.http.post(this.api.BASEURL + 'api/studentAccountCreation', data, this.api.headers())
    .subscribe(async (response: any) => {
        this.studentCreation = response; 
        this.filterNotRegStudents(this.studentCreation);

        const trueStudents = this.studentCreation.filter(item => item.value.data === true);
        const falseStudents = this.studentCreation.filter(item => item.value.data === false);

        const trueCount = trueStudents.length;
        const RegisteredNames = trueStudents.map(item => item.value.studentData.name).join(", ");
        const falseCount = falseStudents.length;
        const notRegisteredNames = falseStudents.map(item => item.value.studentData.name).join(", ");
        this.notRegisteredNamesArray = notRegisteredNames;
        if (this.notRegisteredNamesArray.length > 0) {
          console.log("Still not registered: ", this.notRegisteredNamesArray);
        }

        console.log("Names are", this.notRegisteredNamesArray);
        
        this.studentCreation.length ? this.progressStatus = "90%" : '';
        console.log(this.studentCreation.length)

        const updateData = trueStudents.map(student => ({
          studentId: student.value.studentData._id,
          sendEmail: true 
      })).concat(falseStudents.map(student => ({
          studentId: student.value.studentData._id,
          sendEmail: "NotSent" 
      })));
        await this.updateSendEmailForStudents(updateData); 
        
    

        this.studentCreation.length && setTimeout(async () => {
            this.progressStatus = "100%";
            this.MailsentApiRes = this.studentCreation.length ? false : true;
            this.isModalProgressVisible = false;

            if (this.studentCreation.length) {
              if (isModalCall) {
                this.registrationSuccessMessage = `Registered and Email sent to ${trueCount} student(s) successfully.`;
                setTimeout(() => {
                  this.closeModal(); 
              }, 3000);
              
            } else {
                this.registeredMessage = `Registered and Email sent to ${trueCount} student(s) successfully.`;
                this.notRegisteredMessage = `Failed to Register for ${falseCount} student(s)`;
            }
        } else {
            this.message = "Error creating Students";
        }
    }, 100); 
       
    this.message && setTimeout(() => { this.message = "" }, 100);
    console.log(this.studentCreation);
}, (error: any) => {
    console.log('Error caught for API', error);
    this.message = "Something went wrong! Please try Again";
    (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
});
  }

retryCreateStudent() {
  if (this.selectedNotRegisteredList.length > 0) {
      const notRegisteredStudentIds = this.studentCreation
          .filter(item => item.value.data === false)
          .map(item => item.value.studentData._id); 
          let notRegData = {
            studentIds: this.selectedNotRegisteredList, 
            instituteid: this.activeInstitute?._id,
            courseid: this.activeCourse._id,
            areAllSelected: false,
            kk:"hi"
        }
        this.createStudent(notRegData).then(() => {
          this.selectedNotRegisteredList = [];  
          this.notRegisteredNamesArray = [];     
        });
      }
    }

  


    async updateSendEmailForStudents(students: any[]) {
      const updateData = students.map(student => ({
          studentId: student.studentId,
          sendEmail: student.sendEmail 
      }));
    
      try {
          const response = await this.http.put(this.api.BASEURL + 'api/studentAccountCreation/updateSendEmail', updateData, this.api.headers()).toPromise();
          console.log('SendEmail updated for students:', response);
      } catch (error) {
          console.log('Failed to update SendEmail for students ', error);
      }
    }
    
    
    fetchNotSentStudents() {
      this.http.get(`${this.api.BASEURL}api/studentAccountCreation/not-sent-emails`, this.api.headers())
        .subscribe((response: any) => {
          this.processStudentResponse(response);
          this.notSentStudents = response; 
          // this.showErrorIcon = this.notSentStudents && this.notSentStudents.length > 0;
        }, error => {
          console.error('Error fetching students:', error);
          this.message = "Error fetching students.";
        });
    }

    // processStudentResponse(response: any) {
    //   let courseMessages: any[] = [];
    
    //   response.forEach((department) => {
    //     department.courses.forEach((course) => {
    //       const notSentStudents = course.students.filter(student => 
    //         student.SendEmail === "NotSent" 
    //       );
    //       const notSentCount = notSentStudents.length;
    
    //       if (notSentCount > 0) {
    //         this.currentCourseId = course.courseId;
    //         courseMessages.push({
    //           messageText: `Registration Failed for ${notSentCount} students in Course Name: ${course.courseName}`,
    //           courseId: course.courseId,
    //           students: notSentStudents 
    //         });
    //       }
    //     });
    //   });
    
    //   if (courseMessages.length > 0) {
    //       this.alertMessages = courseMessages;
    //       this.showAlertMessage = true;
    //   }
    // }
    processStudentResponse(response: any) {
      let courseMessages: any[] = [];
      this.showErrorIcon = false;
      response.forEach((department) => {
        department.courses.forEach((course) => {
          const notSentStudents = course.students.filter(student => 
            student.SendEmail === "NotSent"
          );
          const notSentCount = notSentStudents.length;
    
        
          if (notSentCount > 0 && notSentStudents.some(student => student.departmentid.instituteid === this.activeInstitute._id)) {
            this.currentCourseId = course.courseId;
            courseMessages.push({
              messageText: `Registration Failed for <span style="color:red">${notSentCount}</span> students in Course Name: <strong>${course.courseName}</strong>`,
              courseId: course.courseId,
              semName: department.semesterName,
              students: notSentStudents
            }); 
            
            this.showErrorIcon = true;
          }
        });
      });
    
      if (courseMessages.length > 0) {
        this.alertMessages = courseMessages;
        this.showAlertMessage = true; 
      } else {
        this.showErrorIcon = false; 
        this.showAlertMessage = false; 
      }
    }
    
    
    closeAlert() {
      this.showAlertMessage = false;
    }


        
    openCourseModal(courseId: string, students: any[], semName: string) {
      this.isModalOpen = true;
      this.notSentStudents = students.filter(student => 
        student.SendEmail === "NotSent" && 
        student.departmentid.instituteid === this.activeInstitute._id);
    
      if (this.notSentStudents.length > 0) {
        const firstStudent = this.notSentStudents[0];
        this.courseName = firstStudent.courseid.name;
        this.departmentName = firstStudent.departmentid.name;
        this.year = firstStudent.departmentid.year;
        this.semName = semName;
        this.instituteName = this.activeInstitute.name
        this.courseModal.show();
      } else {
        console.warn('No matching students for the active institute.');
      }
    }
    toggleAlert() {
      this.showAlertMessage = !this.showAlertMessage;
    }
  
    closeModal() {
      this.fetchNotSentStudents()
      this.isModalOpen = false;
      this.selectedNotSentStudents = [];
      this.isAllNotSentSelected = false; 
       this.notSentStudents = []; 
       this.showNotcheck = false; 
       this.registrationSuccessMessage = '';
      this.courseModal.hide();
    }
  
    
    showNotSentCheckboxes() {
      this.showNotcheck = !this.showNotcheck;
      const notSentCheckBoxes = document.getElementById("NotSentCheckboxes");
      console.log("gdfagasfg", notSentCheckBoxes)
      if (notSentCheckBoxes) {
          notSentCheckBoxes.style.display = this.showNotcheck ? "block" : "none"; 
      }
  }
  
onStudentSelectionNotSent(student) {
  
  console.log("selected list ", this.selectedNotSentStudents);
  const index = this.selectedNotSentStudents.indexOf(student._id);
  if (index === -1) {
    this.selectedNotSentStudents.push(student._id);
  } else {
    this.selectedNotSentStudents.splice(index, 1);
  }
  this.isAllNotSentSelected = this.selectedNotSentStudents.length === this.notSentStudents.length;
}


toggleSelectAllNotSent() {
  if (this.isAllNotSentSelected) {
    this.selectedNotSentStudents = [];
  } else {
    this.selectedNotSentStudents = this.notSentStudents.map(student => student._id);
  }
  this.isAllNotSentSelected = !this.isAllNotSentSelected;

} 


async newCreateStudent(isNew: boolean) {
  console.log('fucntion called');
  if (this.selectedNotSentStudents.length > 0) {
      const studentdata = {
          studentIds: this.selectedNotSentStudents, 
          instituteid: this.activeInstitute?._id, 
          courseid:this.notSentStudents[0].courseid._id,
          areAllSelected: false 
      };
      console.log("student data", studentdata);

      await this.createStudent(studentdata, true).then(() => {
         
        setTimeout(() => {
          if( this.isModalOpen = false){ 
             this.selectedNotSentStudents = [];
          }   
      }, 9000); 
      }).catch((error) => {
       console.error('Error creating students:', error);
          
      });
  } else {
      console.warn('No students selected for creation.');
  }
} 

filterNotSentStudents(searchNotSentTerm: string) {
  searchNotSentTerm = searchNotSentTerm.toLowerCase();
  this.filteredNotSentStudents = this.notSentStudents.filter(student =>
    student.name.toLowerCase().includes(searchNotSentTerm)
  );
}

canCreateNew(): boolean {
  return !!this.activeCourseId;
}

open(modal: any) {
  const modalRef = this.modalService.open(modal, {
    backdrop: 'static',
    keyboard: false,
    windowClass: "myCustomModalClass",
    size: 'lg',
  });
  this.message = '';
}

notificationMessage: any = ""
  

deletenotification() {
  var x = document.getElementById("snackbardelete");
  x.className = "show";
  setTimeout(function () { x.className = x.className.replace("show", ""); }, 5000);
}

waitedeletenotification() {
  var x = document.getElementById("snackbardelete");
  x.className = "show"; // Show the notification
}

hideNotification() {
  var x = document.getElementById("snackbardelete");
  x.className = x.className.replace("show", ""); 
}


deleteStudent(studentid: any, studentName: any, student: any) {
  this.isLoading = true;
  this.responseMessage = 'Deleting the student, please wait...';
  this.waitedeletenotification(); 
  const courseId = student.courseid;

  this.http.get(this.api.BASEURL + 'api/report/student/deleteStudent?courseid=' + courseId + '&studentId=' + studentid, this.api.headers())
    .subscribe(
      (response: any) => {
        this.studendata = response;
        this.isLoading = false;
        
        this.TempstudentsList = this.TempstudentsList.filter(student => student._id !== studentid);

        this.filteredStudentsList = this.filteredStudentsList.filter(student => student._id !== studentid);

        this.filterStudents();

        this.responseMessage = this.studendata.message || 'Student deleted successfully!';
        this.deletenotification(); 
        this.hideNotification();


      },
      (error) => {
        this.isLoading = false;
        this.responseMessage = 'Failed to delete the student. Please try again.';
        this.deletenotification(); 
        console.error(error);
        this.hideNotification();

      }
    );
}

onCoursecreate(formValue: any, modal: any) {

  if (formValue) {
  }
  if (!this.activeCourseId) {
    this.message = "No course selected!";
    return;
  }

  const studentData = {
    ...formValue,
    courseid: this.activeCourseId,
    departmentid: this.activeDepartmentId
  };

  const apiUrl = this.api.BASEURL + 'api/report/student/addStudent';
  this.http.post(apiUrl, studentData, this.api.headers()).subscribe(
    (response: any) => {
      this.message = `User added successfully!`;
      this.getStudentsByCourseId(this.activeCourseId);
      setTimeout(() => {
        this.close(modal);  
      }, 3000);
    },
    (error: any) => {
      this.message = `User already exists!`;
      setTimeout(() => {
        this.close(modal);  
      }, 3000);
      console.log('Error updating student data:', error);
      (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!");
    }
  );

}
   
}




