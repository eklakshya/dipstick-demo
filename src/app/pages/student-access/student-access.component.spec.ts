import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAccessComponent } from './student-access.component';

describe('StudentAccessComponent', () => {
  let component: StudentAccessComponent;
  let fixture: ComponentFixture<StudentAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentAccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
