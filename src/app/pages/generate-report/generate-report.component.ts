import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { DownloadService } from '../../pages/services/download.service';
import { saveAs } from 'file-saver';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ApiService} from '../../pages/services/http.service'

@Component({
  selector: 'app-generate-report',
  templateUrl: './generate-report.component.html',
  styleUrls: ['./generate-report.component.css']
})
export class GenerateReportComponent {

  courseList: Array < Object >;
  selectedCourse: string;
  // assessmentType: string;
  isGenerated: boolean = true;
  noOfStudents: number = 0;
  remainingReports: number = 0;
  sendersAddr: string;
  messageBody: string;
  password: string;
  subjectLine:string;
  username: string;

  constructor(private http: HttpClient, private downloads: DownloadService, private modalService: NgbModal, private api : ApiService) { 
    this.http.get(this.api.BASEURL + 'api/courses')
    .subscribe((response) => {
      this.courseList = response['courses'];
      console.log(this.courseList, this.courseList.length, this.courseList[0]['name']);
    });
  }

  setCourse(event: Event) {
    this.selectedCourse = (<HTMLInputElement>event.target).value;
    this.http.post(this.api.BASEURL + 'api/student-count', {courseName: this.selectedCourse})
    .subscribe((response) => {
      this.noOfStudents = response['count'];
      this.remainingReports = this.noOfStudents;
      console.log(this.noOfStudents);
    });
  }

  // setAssessmentType(event: Event) {
  //   const assessment = (<HTMLInputElement>event.target).value;
  //   if (assessment === 'Pre assessment') {
  //     this.assessmentType = 'pretest';
  //   } else if ( assessment === 'Post assessment') {
  //     this.assessmentType = 'posttest'
  //   } else {
  //     this.assessmentType = '';
  //   }

  //   console.log(this.assessmentType);
  // }

  generateReports(batchStart: number, batchSize: number) {
    console.log(this.selectedCourse);
    if(this.remainingReports > 0) {
      let batchEnd = batchSize + batchStart;

      if(batchEnd > this.noOfStudents) {
        batchEnd = this.noOfStudents;
      }

      this.http.post(this.api.BASEURL + 'api/generate-reports', {
        'Course name': this.selectedCourse, 
        'start batch': batchStart, 
        'end batch': batchEnd
      })
      .subscribe((response) => {
        console.log('One batch completed');
        batchStart += batchSize;
        this.remainingReports -= batchSize;
        this.generateReports(batchStart, batchSize)
      });
    } else {
      this.isGenerated = true;
    }
  }

  downloadReports() {
    this.downloads.download(this.api.BASEURL + 'api/generated-reports')
    .subscribe(blob => saveAs(blob, 'student-reports.zip'))
  }

  open(modal) {
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static',keyboard: false});
  }

  onClickSubmit(data) {
    this.sendersAddr = data.emailid;
    this.password = data.passwd;
    this.messageBody = data.msgbody;
    this.subjectLine = data.subjectLine;
    this.username = data.username;
  }

  emailReports() {
    const emailRequest = {
      'CourseName': this.selectedCourse,
      'Username': this.username,
      'Email': this.sendersAddr,
      'Password': this.password,
      'Subject Line': this.subjectLine,
      'Message Body': this.messageBody
    }
    this.http.post(this.api.BASEURL + 'api/email-reports', emailRequest)
    .subscribe((response) => {
      console.log(response['message']);
    });
  }

}
