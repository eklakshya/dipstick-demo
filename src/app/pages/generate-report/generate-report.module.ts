import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateReportComponent } from './generate-report.component';



@NgModule({
  declarations: [GenerateReportComponent],
  imports: [
    CommonModule
  ]
})
export class GenerateReportModule { }
