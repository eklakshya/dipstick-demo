import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild,Input,Output, EventEmitter  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConstantsService } from 'src/app/core/services/constants.service';
import { RoutingService } from 'src/app/core/services/routing.service';
import { ApiService } from '../../services/http.service';
 import { FormGroup, FormControl, Validators } from '@angular/forms';
 import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';
 
@Component({
  selector: 'app-access-control',
  templateUrl: './access-control.component.html',
  styleUrls: ['./access-control.component.scss']
})
export class AccessControlComponent implements OnInit {
  apiResponce: any
  payload: any;
  activeDepartment: any;
  activeDepartmentId: any;
  activeSemester: any;
  activeSemesterId: any;
  activeYear: any;
  activeCourse: any;
  activeCourseId: any;
  activeCourse1: any;
  breadcrumbData: any;
  courseCode: any;
  studentid: any;
  searchString: any;
  matchedCourses: any;

  //to store the real data
  semestersList: any[] = [];
  yearList: any[] = [];
  departmetnList: any[] = [];
  coursesList: any[] = [];
  studentList: any[] = [];
  searchText: any
  //temp list for view in the filter
  TempsemestersList: any[] = [];
  TempyearList: any[] = [];
  TempdepartmentList: any[] = [];
  TempcoursesList: any;
  TempstudentsList:any;
  accessToken: any;
  accessData: any;
  selectedStudent:any[] = [];
  show:boolean = false;
  studentSelectionStatus:boolean = false;
allSelected:boolean  = false

  @ViewChild('searchfieldpage') input: ElementRef;
  isSearchPopupOpen: any; 
  semesters: any;
  departments: any;
  activeStudent: any;
  activestudentId:any
  message: string;
  progressStatus:any= '10%';
 studentCreation:any;
 @Input() activeInstitute: any;
 
 @Output() dipstickAccessStatus = new EventEmitter<boolean>();

  constructor(public api: ApiService,
    public constants: ConstantsService,
    public routingService: RoutingService,
    private http: HttpClient,
    public router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
  ) {

    this.accessData = ''
    this.accessToken = ''

  }


  createStudentWithLazyload(){
   let num = Math.round(this.selectedStudent.length / 10) ;
   let value = (100 / Number(num));
    for(let i=1;i<=num;i++){
      setTimeout(()=> {
        this.progressStatus = ""+i * value+'%';
        console.log({"loading":this.progressStatus})
      }, 10000)
    }
  }

  reportsFilters: any = {
    activeYear: undefined,
    activeSemester: undefined,
    activeDepartment: undefined,
    activeCourse: undefined
  }

  ngOnInit(): void {
    alert("kkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
 alert(this.activeInstitute)
    this.getSemesters(); 
  }


  removeDuplicates(arr: any) {
    return new Array(...new Set(...arr))
  }


  getSemesters() {
    Boolean((this.activeInstitute._id || this.activeInstitute)) && this.http.get(this.api.BASEURL + 'api/semester?instituteid=' + (this.activeInstitute._id || this.activeInstitute), this.api.headers())
      .subscribe((response: any) => {
        this.dipstickAccessStatus.emit(Boolean(response));
        console.log({"data":Boolean(response)})
        alert(Boolean(response))
        this.semesters = response;
        let arr = []
        this.semesters.forEach(semester => {
          arr.push(semester.year)
        })
        let removed_duplicate = [...new Set(arr)];
        let years = removed_duplicate.sort();
        this.listYear(years);
        this.listSems(this.semesters)
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        })
  }

  //listing years
  listYear(year: any) {
    year.forEach((data) => {
      if (!this.yearList.includes(data)) {
        this.yearList.push(data);
      }
    })
  }

  //listing the semisters
  listSems(semesters: any) {
    semesters.forEach((data) => {
      if (this.semestersList.findIndex((semData: any) => semData._id == data._id) == -1) {
        this.semestersList.push(data);
      }
    });
  }

  //departmentList
  listDep(dep:any) {
    dep.forEach((data) => {
      if (this.departmetnList.findIndex((depData: any) => depData._id == data._id) == -1) {
        this.departmetnList.push(data);
      }
    });
    this.departmetnList && console.log('==departmetnList====', this.departmetnList);
  }

  getDepartments(semId:any) {
    (semId) && this.http.get(this.api.BASEURL + 'api/departments?semesterid=' + '634d14fe4eaf526f7d00e5be', this.api.headers())
      .subscribe((response: any) => {
        this.departments = response['departments'];
        this.listDep(this.departments);
        this.TempdepartmentList = [...this.departmetnList]
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() :console.log("Something went wrong!")
        })
  }



  getCourses(department){
    this.http.get(this.api.BASEURL + 'api/courses?id=' + department._id, this.api.headers())
    .subscribe(async (response) => {
      let courses  = response['courses']; 
      this.coursesList = [...courses]; 
      this.TempcoursesList = [...this.coursesList]
      this.TempcoursesList =await this.TempcoursesList.filter((course,i) => this.activeDepartmentId == course[i].departmentid);
   
     },
      (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }


  getStudentsByCourseId(courseid){
    const apiUrl = this.api.BASEURL + 'api/report/student/students?courseid=' + courseid;
    this.http.get(apiUrl, this.api.headers()).subscribe(async (response:any) => {
      this.studentList = response;
      this.TempstudentsList = [...this.studentList]
      this.TempstudentsList =await this.TempstudentsList.filter((student,i) => this.activeCourseId == student[i].courseid);
      this.TempstudentsList =  this.TempstudentsList.sort();
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  } 
  


  getFilters() {
    this.apiResponce = this.accessData;
    //console.log('this.apiResponce', this.apiResponce)

    //listing years
    this.apiResponce.accessData.forEach((data) => {
      if (!this.yearList.includes(data.semesterData.year)) {
        this.yearList.push(data.semesterData.year);
      }
    })
    //console.log('==yearList====', this.yearList)

    //listing the semisters
    this.apiResponce.accessData.forEach((data) => {
      if (this.semestersList.findIndex((semData: any) => semData._id == data.semesterData._id) == -1) {
        this.semestersList.push(data.semesterData);
      }
    });
    //this.semestersList && (this.semestersList = this.getUniqueListBy(this.semestersList,'name'));
    //console.log('==semestersList====', this.semestersList);




    //departmentList
    this.apiResponce.accessData.forEach((data) => {
      if (this.departmetnList.findIndex((departmentData: any) => departmentData._id == data.departmentData._id) == -1) {
        this.departmetnList.push(data.departmentData);
      }
      // this.departmetnList.push(data.departmentData);
    });
    //this.departmetnList && (this.departmetnList = this.getUniqueListBy(this.departmetnList,'name'));
    //console.log('==departmetnList====', this.departmetnList);

    //courseList
    this.apiResponce.accessData.forEach((data) => {
      this.coursesList.push(data.courseData);
    });
    //this.coursesList && (this.coursesList = this.getUniqueListBy(this.departmetnList,'name'));
    //console.log('===courseList===', this.coursesList);


    (localStorage.getItem('userrole') == 'student') && (this.apiResponce.accessData.forEach((data) => {
      this.studentList.push(data.studentData);
    }));

    this.yearList && this.semestersList && this.departmetnList && this.coursesList && this.getStudentFilters();
    // });
  }



  
  showCheckboxes() { 
    this.show =!this.show;
      var checkboxes = 
          document.getElementById("checkBoxes");
      if (this.show) {
          checkboxes.style.display = "block";
      } else {
          checkboxes.style.display = "none";
      }
  }

  onStudentSelection(selectedStudentId:any,selectionStatus:any){
    if(selectionStatus=="all"){
      this.allSelected = true;
      if(this.selectedStudent.length != this.TempstudentsList.length){
        this.studentSelectionStatus = true;
        this.TempstudentsList.map((student:any)=>{
          if(!this.selectedStudent.includes(student._id)){
            this.selectedStudent.push(student._id);
          }
         })
      }else {
        this.studentSelectionStatus = false;
        this.selectedStudent = [];
        this.allSelected = false;
      }
    }else{
      this.studentSelectionStatus = false;
      this.allSelected = false;
      if (!this.selectedStudent.includes(selectedStudentId)) {
        this.selectedStudent.push(selectedStudentId);
      }else{
        this.selectedStudent.splice(this.selectedStudent.indexOf(selectedStudentId),1);
      }
    }
    console.log("selected student list : ",this.selectedStudent);
  }
 
  //year selections
  setNewYear(yearData: any) {
    //console.log({ yearData });
    this.activeYear = yearData;
    this.onYearChange();
  }
  onYearChange() {
    this.TempsemestersList = [];
    [this.activeSemester, this.activeSemesterId] = [undefined, undefined];
    this.activeYear && this.setSemesterList()
    this.onSemesterChange()
  }
  setSemesterList() {
    this.TempsemestersList = [...this.semestersList]
    this.TempsemestersList = this.TempsemestersList.filter((sem) => this.activeYear == sem['year']);
    //console.log(this.TempsemestersList, 'this.TempsemestersList ')
  }

  //semester selections
  setNewSemester(semesterId: any) {
    this.activeSemesterId = semesterId;
    let activeSemesterData = this.TempsemestersList.find((semesterData: any) => semesterData._id == this.activeSemesterId)
     this.activeSemester = activeSemesterData.name;
    this.onSemesterChange();
  }
  onSemesterChange() {
    [this.activeDepartment, this.activeDepartmentId] = [undefined, undefined];
    this.activeSemesterId && this.getDepartments(this.activeSemesterId);
    this.TempdepartmentList && this.setDepartment();
    this.onDepartmentChange()
  }
  setDepartment() { 
    this.TempdepartmentList = [...this.departmetnList] 
    this.TempdepartmentList = this.TempdepartmentList.filter((dep,i) => this.activeSemesterId == dep[i]['semesterid']);
     console.log(this.TempdepartmentList, 'this.222222TempdepartmetnList ')
  }


  //department selections
  setNewDepartment(departmentId: any) {
    this.activeDepartmentId = departmentId; 
    let departmentData = this.TempdepartmentList.find((department: any) => department._id == departmentId)
    this.activeDepartment = departmentData.name;
    departmentData && this.getCourses(departmentData);
    this.onDepartmentChange()
  }
  onDepartmentChange() {
    [this.activeCourse, this.activeCourseId] = [undefined, undefined];
    [this.activeCourse1] = [undefined];
    this.TempcoursesList = [];
 //  this.activeDepartmentId && this.setcourse();
  }

  //course selections
  async setcourse() {  
    this.TempcoursesList = [...this.coursesList] 
    this.TempcoursesList =await this.TempcoursesList.filter((course,i) => this.activeDepartmentId == course[i].departmentid);
  }

  setNewCourse(courseid: any) {
    this.activeCourse = this.coursesList.find((coursedata: any) => coursedata._id == courseid)// courseid;
    this.activeCourseId = this.activeCourse._id;
    this.activeCourseId  &&  this.getStudentsByCourseId(this.activeCourseId )
  }



   //course selections
   async setStudent() {  
    this.TempcoursesList = [...this.coursesList] 
    this.TempcoursesList =await this.TempcoursesList.filter((course,i) => this.activeDepartmentId == course[i].departmentid);
  }

  setNewStudent(studentid: any) {
    this.activeStudent = this.coursesList.find((studentdata: any) => studentdata._id == studentid)// courseid;
    this.activeCourseId = this.activeCourse._id;
  }


  //setting the filtered data to the local storage to display on refresh
  setReportFilters() {
    const { activeYear, activeSemester, activeDepartment, activeCourse, activeSemesterId, activeDepartmentId, activeCourseId } = this
    this.reportsFilters = { activeYear, activeSemester, activeDepartment, activeCourse, activeSemesterId, activeDepartmentId, activeCourseId }
    localStorage.setItem(this.constants.localStorage.REPORTSFILTERS, JSON.stringify(this.reportsFilters))
  }
  getStudentFilters() {
    const studentfilterKey = this.constants.localStorage.REPORTSFILTERS;
    const reportfilters = localStorage.getItem(studentfilterKey);
    this.reportsFilters = reportfilters != null ? JSON.parse(reportfilters) : { activeYear: undefined, activeSemester: undefined, activeDepartment: undefined, activeCourse: undefined, activeSemesterId: undefined, activeDepartmentId: undefined, activeCourseId: undefined };
    this.activeYear = this.reportsFilters.activeYear;
    this.activeSemester = this.reportsFilters.activeSemester;
    this.activeDepartment = this.reportsFilters.activeDepartment
    this.activeSemesterId = this.reportsFilters.activeSemesterId;
    this.activeDepartmentId = this.reportsFilters.activeDepartmentId
    this.activeCourse1 = this.reportsFilters.activeCourse.name;
    this.activeCourseId = this.reportsFilters.activeCourseId;

    // this.setDepartmentList();
    this.activeYear && this.setSemesterList();
    this.activeSemesterId && this.setDepartment()
    this.activeDepartmentId && this.setcourse();
  }

  //viewing filtered reports
  filter() {
    this.activeCourseId && this.reportsChange();
    this.setReportFilters()
    this.chooselevel();
    if (localStorage.getItem('userrole') == 'student') {
      //let studentid= sessionStorage.getItem('studentid');
      let studentName = localStorage.getItem('LMS_Student');
      this.getStudentid(this.activeCourse.name) && this.router.navigate(['/integration/reports'], {
        skipLocationChange: false,
        queryParams: { courseid: this.activeCourse._id, studentid: this.studentid, studentName }
      }).then(() => {
        location.reload();
      })
    } else {
      location.reload();
    }
  }
  getStudentid(courseName: any) {
    let studentInfo: any = this.studentList.filter((data) => data.courseName == courseName);
    this.studentid = studentInfo[0]['_id'];
    return (this.studentid);
  }
  reportsChange() {
    // const courseid = this.coursesList.filter((data)=>data.name === this.activeCourse);
    //console.log('course id after selecting the course in the drop down', this.activeCourse['_id']);
    localStorage.setItem('courseId', this.activeCourse['_id']);
    let instituteid = this.activeCourse['instituteid'];
    instituteid && localStorage.setItem('instituteid', instituteid);
  }

  //for breadcrumb data, not using presently  
  async chooselevel() {
    this.settemp({ name: this.activeCourse['name'], url: "", type: 'course' })
    this.breadcrumbData = 'You are here: reports/ ';
    this.breadcrumbData += this.activeYear + '/' + this.activeSemester + '/' + this.activeDepartment + '/' + this.activeCourse['name']
    // //console.log("coursedata", coursedata, this.breadcrumbData);
    localStorage.setItem('data', this.breadcrumbData);
    localStorage.setItem('S_bread', this.breadcrumbData);
  }
  settemp(data: any) {
    this.routingService.setTemparray(data);
  }
  searchMatchedCourses(modal, flag: number, string: any) {
    this.searchString = string.trim();
    !this.isSearchPopupOpen && this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static', keyboard: false });
    this.matchedCourses = [];
    //console.log("this.accessData", this.accessData);
    const searchExpression = new RegExp(this.searchString, 'ig');
    this.matchedCourses = this.accessData.accessData.filter((instituteData: any) => ((
      searchExpression.test(instituteData.semesterData.name) || (instituteData.semesterData.name.toLowerCase().includes(this.searchString)) ||
      searchExpression.test(instituteData.semesterData.year) || instituteData.semesterData.year.includes(this.searchString) ||
      searchExpression.test(instituteData.departmentData.name) || (instituteData.departmentData.name.toLowerCase().includes(this.searchString)) ||
      searchExpression.test(instituteData.courseData.name) || (instituteData.courseData.name.toLowerCase().includes(this.searchString)) ||
      searchExpression.test(instituteData.courseData.coursecode) || (instituteData.courseData.coursecode.toLowerCase().includes(this.searchString))
    )));
    this.isSearchPopupOpen = (flag == 1) ? true : false;
    //console.log("this.matchedCourses =", this.matchedCourses);
  }

  close(modal) {
    modal.dismiss('Cross click')
    this.isSearchPopupOpen = false;
    this.input.nativeElement.value = this.searchString;
    // this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', backdrop: 'static', keyboard: false });
  }

  getSearchString(string) {
    // this.searchString = string.trim();
    this.searchText = string.trim();
    //console.log("string", string)
  }


  setCourseData(i: number, modal: any) {
    modal.dismiss('Cross click')
    this.isSearchPopupOpen = false;
    // this.searchString = '';
    this.activeYear = this.matchedCourses[i].semesterData.year;
    this.activeSemester = this.matchedCourses[i].semesterData.name;
    this.activeDepartment = this.matchedCourses[i].departmentData.name;
    this.activeCourse = this.matchedCourses[i].courseData;
    this.activeSemesterId = this.matchedCourses[i].semesterData['_id']
    this.activeDepartmentId = this.matchedCourses[i].departmentData['_id']
    this.activeCourseId = this.matchedCourses[i].courseData['_id']
    this.close(modal);
    this.filter();
    // const { year, semesterid, semesterName, departmentName, departmentid, courseName, coursecode, courseid } = this.searchedCourseInfo.matchedCourses[filterCourseIndex];
    // [this.activeYear, this.activeSemester, this.activeDepartment ] = [year, {_id : semesterid, name : semesterName}, { _id : departmentid, name : departmentName }];
    // this.chooselevel({ _id : courseid, coursecode, name : courseName })

  }
  getDisplayText(i: number) {
    let semData: any = this.matchedCourses[i].semesterData;
    let depData: any = this.matchedCourses[i].departmentData;
    let courseData: any = this.matchedCourses[i].courseData;
    return ("" + (i + 1) + ". " + semData.year + " / " + semData.name + " / " + depData.name + " / " + courseData.name + " - " + courseData.coursecode).replace(RegExp(this.searchString, 'ig'), (word) => "<b>" + word + "</b>")
  }

  clear() {
    //console.log("reset triggered!")
    document.getElementById("searchString").focus()
    // alert("clear");
  }
 
  createStudent(data) {  
  data = {studentIds:this.allSelected ? '':(this.selectedStudent), instituteid:this.activeInstitute, areAllSelected:this.allSelected, courseid:this.allSelected ? (this.activeCourse._id) : '' }
  this.http.post(this.api.BASEURL + 'api/studentAccountCreation',data, this.api.headers())
  .subscribe((response:any) => {
    console.log(response)
    this.message = response.status ? "Students created successfully" : "Error creating Students" ;
    this.studentCreation  = response ;
    console.log(this.studentCreation)
    setTimeout(function () {
      document.getElementById('close').click();
    }, 2000);
  },(error:any) => {                           
    console.log('error caught for api',error);
    (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
  })
  }
}
