import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntigrationRoutingModule } from './intigration-routing.module';
import { IntigrationComponent } from './intigration.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StudentWiseConceptWiseModule } from '../concept-wise/student-wise-concept-wise/student-wise-concept-wise.module';
import { StudentWiseTestWiseModule } from '../test-wise/student-wise-test-wise/student-wise-test-wise.module';
import { StudentWiseSubconceptWiseModule } from '../subconcept-wise/student-wise-subconcept-wise/student-wise-subconcept-wise.module';
import { ConceptWiseModule } from 'src/app/commmon-modules/concept-wise.module';
import { CoreModule } from 'src/app/core/core.module';
import { AttemptwiseStudentwiseTestwiseModule } from '../test-wise/attemptwise-studentwise-testwise/attemptwise-studentwise-testwise.module';
import { AttemptwiseStudentwiseConceptwiseModule } from '../concept-wise/attemptwise-studentwise-conceptwise/attemptwise-studentwise-conceptwise.module';
import { AttemptwiseStudentwiseSubconceptWiseModule } from '../subconcept-wise/attemptwise-studentwise-subconcept-wise/attemptwise-studentwise-subconcept-wise.module';
import { FilterReportsComponent } from 'src/app/core/layout/filter-reports/filter-reports.component';


@NgModule({
  declarations: [IntigrationComponent],
  imports: [
    CommonModule,
    IntigrationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    StudentWiseConceptWiseModule,
    StudentWiseTestWiseModule,
    StudentWiseSubconceptWiseModule,
    ConceptWiseModule,
    CoreModule,
    AttemptwiseStudentwiseTestwiseModule,
    AttemptwiseStudentwiseConceptwiseModule,
    AttemptwiseStudentwiseSubconceptWiseModule,
  ]
})
export class IntigrationModule { }
