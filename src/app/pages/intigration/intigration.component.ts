import { Component, OnInit, AfterViewChecked, ElementRef, ViewChild, OnDestroy, AfterViewInit, AfterContentChecked, AfterContentInit } from '@angular/core';
import { ConstantsService } from '../../core/services/constants.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { ApiService } from '../services/http.service';
import { ReportsService } from '../../core/services/reports.service'
import { constant } from 'src/app/shared/constant';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-intigration',
	templateUrl: './intigration.component.html',
	styleUrls: [ './intigration.component.css' ]
})
export class IntigrationComponent implements OnInit, OnDestroy, AfterViewInit,AfterContentChecked, AfterContentInit,AfterViewChecked {
	name: any;
	studentid: any;
	courseid: any;
	studentName: any;
	user: any;
  istrue:boolean = false;
  count:number=0
  instituteid:any;
  styleElement2:any=null;
  styleElement3:any=null;
  StudentData:any;
  ulineobj:any =`.uline2{
    border-top: 4px solid --color !important;
    border-top: 4px solid #33b35a;
    padding: 12px 0;
    }`;
  fontColor:any=`
  .active-top-navigation-bar{
    color: --font !important;
    color: #33b35a;
}`;

legend:any = constant.legendImage;
downloadPDF:any = constant.downloadPDF;

	constructor(public reportService : ReportsService,
		private api : ApiService, 
		private http:HttpClient,
		private constants: ConstantsService, 
		private route: ActivatedRoute,
		private el :ElementRef,
		private modalService:NgbModal
		) {
		this.route.queryParams.subscribe((params) => {
			this.courseid = params['courseid'];
			this.studentid = params['studentid'];
			this.studentName = params['studentName'];
      console.log(this.courseid,this.studentid,this.studentName)
			this.user = this.courseid != undefined || this.studentid != undefined ? localStorage.setItem('userrole',(localStorage.getItem('userrole') == "dipstickstudent") ? 'dipstickstudent' : 'student') : '';
      this.istrue = this.user != '' || this.user != undefined && (localStorage.getItem('userrole') == "student" || localStorage.getItem('userrole') == "dipstickstudent") ? true : false;


	  this.StudentData = JSON.parse(localStorage.getItem('getcallApi'))?.accessData[0]?.studentData;

      console.log(this.studentid, '-------<sid cid>------', this.courseid);
			localStorage.setItem('LMS_Student', this.studentName);
			localStorage.setItem('user', this.studentName);
			this.setTemproaryAssets(this.courseid, this.studentid,this.studentName);

		});

		//To load the configrations
		this.reportService.loadReportDisplayConfigurations((reportConfigurations:any)=>{
			console.log("reportConfigurations : ", reportConfigurations)
			this.reportConfigurations = reportConfigurations;
			this.activeTab = (this.tabs.find((tab:any) => reportConfigurations.find((configuration:any) => ((configuration.reportType == tab.reportType) && configuration.isVisible))))
			this.checkReportNotFound()
		  });
	  // this.rotate("constructor")
	}
	ngAfterViewChecked(){
		//this.rotate("ngAfterViewChecked")
	}
	ngAfterContentInit(){
		setTimeout(() => {
			//document.getElementById("btnid").click();
		}, 2000);
		//this.rotate("ngAfterContentInit");
	}
	ngAfterContentChecked(){
	//this.rotate("ngAfterContentChecked");
	}
	ngAfterViewInit(){
		//this.rotate('1');

		//this.rotate("ngAfterViewInit");
	}
 
	ngOnDestroy() {
		sessionStorage.removeItem('lms');
	}

	tabs : any[] = [
		{ title : "Test", configurationtype:"test", type : "TESTWISE", index : 1, reportType : "student-testwise"},
		{ title : "Concept", configurationtype:"concept", type : "CONCEPTWISE", index : 2, reportType : "student-conceptwise"},
		{ title : "Subconcept", configurationtype:"subconcept", type : "SUBCONCEPTWISE", index : 3, reportType : "student-subconceptwise"},
	  ];
	activeTab: any = this.tabs[0];
	reportConfigurations : any[] = [];
	ngOnInit(): void {
		let colorCode= localStorage.getItem('setColor');
		let style: string = this.ulineobj.replace('--color',colorCode);
		this.createStyle(style);
		let font: string = this.fontColor.replace('--font',colorCode);
		this.createfontStyle(font);
	   

		if(sessionStorage.getItem('count') != 'true'){
		// setTimeout(function(){
		location.reload();
		// },500);
		}
		sessionStorage.setItem('count','true');
		this.instituteid = localStorage.getItem('instituteid');
		this.instituteid && this.selectInstitute(this.instituteid);
		 }


	setTemproaryAssets(courseid, studentid,studentName) {
		sessionStorage.setItem('lms', 'true');
		const studentReportAssets = { courseid, studentid,studentName };
		sessionStorage.setItem(this.constants.sessionStorage.STUDENTREPORTASSETS, JSON.stringify(studentReportAssets));
	}

	
	selectInstitute(instituteid){
		const apiUrl = this.api.BASEURL + 'api/configuration?instituteid=' +instituteid ;
		this.http.get(apiUrl, this.api.headers()).subscribe((response:any) => {
		  this.populateConfigurations(response);
		},(error:any) => {                           
			console.log('error caught for api',error);
			(error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
		  });
	  };
	  populateConfigurations(configurations:any[]){
		const configurationsArray = configurations.map((configuration)=>{
		  let confJson = {};
		  confJson[configuration.elementtype] = configuration.name;
		  return confJson;
		});
		this.setConfigurations(configurationsArray);
		// this.setConfigurations()
	  };
	  setConfigurations(configurations){
		localStorage.setItem(this.constants.localStorage.CONFIGURATIONS, JSON.stringify(configurations));
	  }
	  
	  createStyle(style: string): void {
		if(this.styleElement2){
		  this.styleElement2.removeChild(this.styleElement2.firstChild);
		} else {
		  this.styleElement2 = document.createElement('style');
		}
		this.styleElement2.appendChild(document.createTextNode(style));
		this.el.nativeElement.appendChild(this.styleElement2);
	}
	
	createfontStyle(style: string): void {
	  if(this.styleElement3){
		this.styleElement3.removeChild(this.styleElement3.firstChild);
	  } else {
		this.styleElement3 = document.createElement('style');
	  }
	  this.styleElement3.appendChild(document.createTextNode(style));
	  this.el.nativeElement.appendChild(this.styleElement3);
	}

	checkReportVisibility(reportType:any){
		return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == reportType)?.isVisible;
	  }
	  checkPopupVisibility(popupType:any){
		return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == popupType)?.isVisible;
	  }
	  checkReportNotFound(){
		let tabVisibilities = this.tabs.map((tab:any) => !this.checkReportVisibility(tab.reportType))
		return ([...new Set(tabVisibilities)].length == 1) && tabVisibilities[0]
	  }

	  exportToPDFData:any = {};
	  isDownloadAvailable = false;
	  getReportData(type:string, data:any){
		  // this.exportToPDFData.courseName = this.courseName;
		  // this.exportToPDFData.studentUSN = this.studentUSN;
		  // this.exportToPDFData.breadcrumbPath = localStorage.getItem("S_bread") || null;
		// this.exportToPDFData.breadcrumbPath = "student - reports/" + this.courseName + "/" + this.studentUSN + "-" + this.studentName;
		this.exportToPDFData[type] = data;
		this.exportToPDFData.reportNames = {}
		this.tabs.forEach((tab:any, i:number) => {
			this.exportToPDFData.reportNames[tab.configurationtype] = this.reportService.replaceTopic(tab.title, tab.configurationtype)
		})
		if(Object.keys(this.exportToPDFData).length == 3){
		  this.isDownloadAvailable = true;
		  // this.reportGeneration();
		} else {
		  const availableReportTypes = Object.keys(this.exportToPDFData);
		  const availableReports = this.tabs.filter((tabData:any) => this.checkReportVisibility(tabData.reportType)).map((tabData:any) => availableReportTypes.includes(tabData.reportType));
		  if(availableReports.reduce((a:boolean,b:boolean) => a && b)){
			this.isDownloadAvailable = true;
			// this.reportGeneration()
		}
	}
	// this.rotate()
}
reportGeneration(){
	const redirectionDataResponse = JSON.parse(localStorage.getItem("getcallApi"));
	const { studentData } = redirectionDataResponse.accessData[0];
	this.exportToPDFData.studentName = this.studentName;
	this.exportToPDFData.courseName = studentData.courseName;
	this.exportToPDFData.studentUSN = studentData.usn;
	this.exportToPDFData.breadcrumbPath = "student - reports/" + studentData.courseName + "/" + studentData.usn + "-" + this.studentName;
	
	this.isDownloadAvailable ? this.http.post(this.api.BASEURL + 'api/download/v1', { clusterJson : this.exportToPDFData }, {...this.api.headers(), responseType: 'blob' as 'json'})
    .subscribe((response:any) => {
      let dataType = response.type;
      let binaryData = [];
      binaryData.push(response);
      let downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
      downloadLink.setAttribute('download', `${this.exportToPDFData.studentName}-${this.exportToPDFData.studentUSN}-Dipstick Reports.pdf`);
      document.body.appendChild(downloadLink);
      downloadLink.click();
  }, (e) => {
      console.log(e)
    }) : alert("We are still downloading the reports!!! please try again later");
}
fullScreenCheck() {
    if (document.fullscreenElement) return;
    return document.documentElement.requestFullscreen();
  }
  async rotate(daata='fa') {
  }
//   async rotate(daata='fa') {
//      try {
//       await this.fullScreenCheck();
//     } catch (err) {
//       console.error(err);
//     }
// 	await screen.orientation.lock("landscape");

//   }
//   open(modal) {
//     this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', backdrop: 'static', keyboard: false });
//    }

 
}
