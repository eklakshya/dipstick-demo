import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntigrationComponent } from './intigration.component';

const routes: Routes = [{ path: '', component: IntigrationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntigrationRoutingModule { }
