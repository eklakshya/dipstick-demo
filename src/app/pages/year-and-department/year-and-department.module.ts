import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { YearAndDepartmentRoutingModule } from './year-and-department-routing.module';
import { YearAndDepartmentComponent } from './year-and-department.component';
import { ChooseLevelComponent } from '../choose-level/choose-level.component';
import { CoreModule } from 'src/app/core/core.module';
import { UnAuthorizedModule } from 'src/app/core/helper/un-authorized/un-authorized.module';


@NgModule({
  declarations: [YearAndDepartmentComponent, ChooseLevelComponent,
  ],
  imports: [
    CommonModule,
    YearAndDepartmentRoutingModule,
    UnAuthorizedModule
  ]
})
export class YearAndDepartmentModule { }
