import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YearAndDepartmentComponent } from './year-and-department.component';

describe('YearAndDepartmentComponent', () => {
  let component: YearAndDepartmentComponent;
  let fixture: ComponentFixture<YearAndDepartmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YearAndDepartmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YearAndDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
