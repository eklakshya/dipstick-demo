import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { YearAndDepartmentComponent } from './year-and-department.component';

const routes: Routes = [{ path: '', component: YearAndDepartmentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class YearAndDepartmentRoutingModule { }
