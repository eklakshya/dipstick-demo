import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from '../../core/services/login.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import { RoutingService } from '../../core/services/routing.service'
import { ApiService } from '../../pages/services/http.service';
import { ConstantsService } from '../../core/services/constants.service';
import { catchError, map, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ReportsService } from 'src/app/core/services/reports.service';

@Component({
  selector: 'app-year-and-department',
  templateUrl: './year-and-department.component.html',
  styleUrls: ['./year-and-department.component.css']
})
export class YearAndDepartmentComponent implements OnInit {
  hoverValue: string;
  breadCrumbData: any;
  departments: any = [];
  upload_department_Info = {};
  year = [];
  subjects = [];
  activeDepartment: any;
  activeYear: any;
  activeCourse: any;
  signIn: any;
  years: true;
  data: any;
  branch: boolean = false;
  course = false;
  dep_id: any;
  level1: any = false;
  kk = false;
  forCourseid: any;
  isAdmin: boolean;
  loadShow: boolean = true;
  noTests: boolean = false;
  courseList: Array<Object>;
  institutes: any[] = [];
  activeInstitute;
  semesters: any[] = [];
  activeSemester: any
  isloggedIn: boolean = false;
  response: Observable<any>;
  showError: boolean;
  error: string;
  searchString = '';
  searchedCourseInfo = {
    searchString : "",
    matchedCourses : []
  };
  isSearchPopupOpen = false;
  isUser:boolean =true;
  inst_id:any;
  instituteCount: number = 0;
  yearCount: number = 0;
  @ViewChild('searchfieldpage') input:ElementRef;
  yearwiseCount:any
  institutewiseCount:any  = {}
  insCount:any;
  
  genaralReports: any = {
    activeYear: undefined,
    activeSemester: undefined,
    activeDepartment: undefined,
    activeCourse: undefined
  }
  reportConfigurations:any = [];
  constructor(private api: ApiService, private http: HttpClient, private router: Router, public routingService: RoutingService, private constants: ConstantsService, private modalService: NgbModal, public reportService : ReportsService) {
    let self = this;
    this.isUser = (localStorage.getItem('user')=='user') ? false : true;
    this.inst_id =localStorage.getItem(this.constants.localStorage.INSTITUTEID);
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url == '/') {
        }
        else {
          if (val.url.toString().search('/reports') == 0)
            console.log('entered val')
        }
        self.breadCrumbData = val.url.split('/');
      }
    },
      (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
    this.isloggedIn = localStorage.getItem('user') ? true : false;
    this.isAdmin = localStorage.getItem('user') == "admin" ? true : false;
    this.loadInstitute()
    // this.flushSelectors()
    let repfil = localStorage.getItem('generalreportsfilters');

  }

  ngOnInit() {

    // this.getyearWiseCountApi();
    this.getinstituteWiseCountApi();

   
    
    let role = localStorage.getItem('user');
    //role == "admin" ? localStorage.removeItem('instituteid') : console.log({ role });
    // setTimeout(function(){ 
    //   document.getElementById('loading').style.display = 'none';
    // }, 500);
    // console.log(this.isloggedIn, this.isAdmin, this.activeInstitute);
    (this.isloggedIn && this.isAdmin) ? this.listInstitutes() : this.setInstituteForuser(this.inst_id),this.getSemesters();
    (this.isloggedIn && !this.isAdmin) && ((this.activeInstitute && this.activeInstitute.name) ? this.getSemesters() : this.getInstituteData());
    // this.getDepartments()
    this.getStudentFilters();
    // this.getinstituteWiseCount1();
  }



//---------------------------------institutewise------------------------------------------------------
  listInstitutes() {
    // this.getinstituteWiseCount();
     this.http.get(this.api.BASEURL + 'api/institute', this.api.headers())
      .subscribe(
        (response: any) => {
          this.institutes = response;
          let statusCode = response.statuscode;
          // console.log({ statusCode });
          this.activeInstitute && this.getyearWiseCountApi()
          if (statusCode == 401 || statusCode == 403) this.api.sessionExp();
          throw new Error('my error');
        },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        }
      )
  }

  // getinstituteWiseCount1(){
  //   console.log("Institute api count called")
  //   this.institutewiseCount =[];
  //  this.http.get(this.api.BASEURL + 'api/semester/record?instituteid=' + (this.activeInstitute._id || this.activeInstitute), this.api.headers())
  //   .subscribe((response: any) => {
  //     this.institutewiseCount = response;
  //   },
  //     (error: any) => {
  //       console.log('error caught for api', error);
  //       (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
  //     })
  // }
  getCurrentYearDisplay(yearName:any){
    console.log(this.yearwiseCount?.counts, "12mgnnnnnnnnnnnnnnnnnnnnn!!!!!!")
    console.log(this.yearwiseCount?.counts, "mgnnnnnnnnnnnnnnnnnnnnn!!!!!!")
    this.yearCount = this.yearwiseCount?.counts.find((ins1:any) => ins1.Year == yearName)?.Count || 0;

    // const yeart = this.yearwiseCount?.counts.find((ins:any) => ins.Year == year);
    // this.yearCount = yeart?.Count || 0;
  }
  // 'http://localhost:3002/api/semester/getyearcountfromcache?instituteid=nologicaluniversity' , this.api.headers())

  getyearWiseCountApi(){
  //  alert("mgninstazone" + JSON.stringify(this.activeInstitute) + JSON.stringify(this.institutes))
  console.log(this.institutes, this.activeInstitute)
  const activeInstitute = this.activeInstitute.name ? this.activeInstitute : this.institutes.find(ins => ins._id == this.activeInstitute)
    activeInstitute.name && console.log({"===================================":activeInstitute.name})
    activeInstitute.name && console.log((activeInstitute.name).replaceAll(/\s+/g, '').toLowerCase())
    this.http.get(this.api.BASEURL + 'api/semester/getyearcountfromcache?instituteid='+(activeInstitute.name).replaceAll(/\s+/g, '').toLowerCase(), this.api.headers())
    .subscribe((response: any) => {
     this.yearwiseCount = response;
     console.log({"this.yearwiseCount":this.yearwiseCount} )
    //  this.yearCount = this.yearwiseCount?.counts.find((ins:any) => ins.Year == this.year)?.Count || 0;
   },
     (error: any) => {
       console.log('error caught for api', error);
       (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
     })
  }

  getyearWiseCount(insy:any){
    const yearName: string = insy; 
    if (this.yearwiseCount?.counts?.length) { 
      // this.getCurrentYearDisplay(insYear); 
      //  const yearss = this.yearwiseCount?.counts.find((insy:any) => insy.Year == yearName);
      //   this.instituteCount = yearss?.Count || 0;

      this.yearCount = this.yearwiseCount?.counts.find((insy:any) => insy.Year == yearName)?.Count || 0;

    }


   
  }


  //---------------------------------instituteWise------------------------------------------------------

  // getCurrentInstituteDisplay(insName:any){
  //   console.log("hiiiii")
  //   const institution = this.institutewiseCount?.counts.data.find((ins:any) => ins.Name == insName);
  //   this.instituteCount = institution?.Count || 0;
  //   console.log(this.instituteCount,"consoleeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee")
  // }

  getinstituteWiseCountApi(){
    this.http.get(this.api.BASEURL + 'api/institute/fromcache' , this.api.headers())
     .subscribe((response: any) => {
      this.institutewiseCount = response;
      // this.getinstituteWiseCount()
    },
      (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      })
  }

  getinstituteWiseCount(ins: any) {
    const insName: string = ins.name; 

    if (this.institutewiseCount?.counts?.data.length) { 
      const institution = this.institutewiseCount?.counts.data.find((ins:any) => ins.Name == insName);
      this.instituteCount = institution?.Count || 0;
    }

  }
 

  //---------------------------------yearwise------------------------------------------------------
  getSemesters() {
    this.getConfigurations();
    // this.getyearWiseCount();
    Boolean((this.activeInstitute._id || this.activeInstitute)) && this.http.get(this.api.BASEURL + 'api/semester?instituteid=' + (this.activeInstitute._id || this.activeInstitute), this.api.headers())
      .subscribe((response: any) => {
        this.semesters = response;
        let arr = []
        this.semesters.forEach(semester => {
          arr.push(semester.year)
        })
        let removed_duplicate = [...new Set(arr)];
        this.year = removed_duplicate.sort();
        console.log('years', this.year);
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        })
        
    // this.getyearWiseCountApi()
  }

  // getyearWiseCount(year:any){
  //   this.yearwiseCount =[];
  //   this.yearCount ='';
  //  this.http.get(this.api.BASEURL + 'api/semester/records?instituteid=' + (this.activeInstitute._id || this.activeInstitute)+'&year='+year, this.api.headers())
  //   .subscribe((response: any) => {
  //     this.yearwiseCount = response;
  //     this.yearCount && (this.yearCount = '');
  //     //const find = this.yearwiseCount.find((data)=>data.Year == year)
  //      this.yearCount = this.yearwiseCount['count'];
  //   },
  //     (error: any) => {
  //       console.log('error caught for api', error);
  //       (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
  //     })
  // }

// getyearWiseCount(){
//   this.yearwiseCount =[];
//  this.http.get(this.api.BASEURL + 'api/semester/records?instituteid=' + (this.activeInstitute._id || this.activeInstitute), this.api.headers())
//   .subscribe((response: any) => {
//     this.yearwiseCount = response;
//   },
//     (error: any) => {
//       console.log('error caught for api', error);
//       (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
//     })
// }


















  getDepartments() {
    (this.activeSemester && this.activeSemester._id) && this.http.get(this.api.BASEURL + 'api/departments?semesterid=' + (this.activeSemester._id), this.api.headers())
      .subscribe((response: any) => {
        this.departments = response['departments'];
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        })
  }



  setYear(year) {
    localStorage.removeItem("data");
    this.activeDepartment = '';
    this.activeCourse = '';
    this.branch = true;
    this.level1 = false;
    this.course = false;
    this.activeYear = year;
    console.log('this.activeYear', this.activeYear);
    this.settemp({ name: this.activeYear, url: "", type: 'year' })
    this.setGeneralReportsFilters()
  }

  setSemester(semester) {
    this.activeSemester = semester;
    this.activeSemester && this.getDepartments();
    this.setGeneralReportsFilters()
  }

  async setdepartment(data) {
    console.log(data)
    this.activeDepartment = data;
    //  this.activeDepartment = data.name;
    this.settemp({ name: this.activeDepartment, url: "", type: 'department' })
    this.branch = false;
    this.course = true;
    this.kk = true;
    this.dep_id = data._id;
    this.http.get(this.api.BASEURL + 'api/courses?id=' + data._id, this.api.headers())
      .subscribe((response) => {
        this.courseList = response['courses'];
        this.loadShow = false;
        this.noTests = this.courseList.length >= 1 ? false : true;
        this.setGeneralReportsFilters();
        //console.log(this.courseList, this.courseList.length, this.courseList[0]['name']);
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        });
  }
  async chooselevel(coursedata) {
    this.activeCourse = coursedata;
    this.setGeneralReportsFilters()
    this.forCourseid = coursedata._id;
    let courseName = coursedata['name']
    this.settemp({ name: courseName, url: "", type: 'course' })
    console.log(this.activeInstitute)
    this.data = 'You are here: reports/ ' + (this.isAdmin ? (this.activeInstitute)?.name + '/' : '');
    this.data += this.activeYear + '/' +this.activeSemester?.name + '/' + this.activeDepartment.name + '/' + this.activeCourse.name + ' - ' + this.activeCourse.coursecode
    console.log("coursedata", coursedata, this.data);
    localStorage.setItem('data', this.data);
    localStorage.setItem('courseId', coursedata._id);
    localStorage.setItem('coursecode', coursedata.coursecode);
    this.course = false;
    this.branch = false;
    this.level1 = true;
  }
  settemp(data: any) {
    this.routingService.setTemparray(data);
  }

  OnCourseChange() {

    this.setReportConfigurations();
    this.http.get(this.api.BASEURL + 'api/courses?id=' + this.activeDepartment._id, this.api.headers())
      .subscribe((response) => {
        this.courseList = response['courses'];
        this.loadShow = false;
        this.noTests = this.courseList.length >= 1 ? false : true;
        this.setGeneralReportsFilters();
        //console.log(this.courseList, this.courseList.length, this.courseList[0]['name']);
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        });
  }

  resetYear() {
    this.activeYear = undefined;
    this.resetSemester()
    this.departments = []
  }

  resetSemester() {
    this.activeSemester = undefined;
    this.resetDepartment();
    this.activeSemester = "";
    this.getSemesters();
  }
  resetDepartment() {
    this.activeDepartment = undefined;
    this.courseList = []
    this.resetCourse();
    this.getDepartments();

  }
  resetALL() {
    localStorage.removeItem(this.constants.localStorage.INSTITUTEID);
    localStorage.removeItem(this.constants.localStorage.GENERALREPOTSFILTERS);
    this.listInstitutes()
  }

  resetSEM() {
    this.activeSemester = undefined;
    this.activeSemester = "";
    this.getSemesters();
    this.resetDepartment();
    this.departments = []
  }
  resetCourse() {
    this.activeCourse = "";
    // console.log(this.activeDepartment)
    this.activeDepartment && this.setdepartment(this.activeDepartment);
    this, this.setGeneralReportsFilters();
  }

  bck() {
    this.activeYear = "";
    this.bck1()
  }


  bck1() {
    this.activeDepartment = "";
    this.activeCourse = "";
  }
  bck2() {
    if (this.level1) {
      this.activeCourse = undefined;
    } else {
      console.log('u cannot go back');
    }

  }
  bck3(data) {
    if (this.course && !this.level1) {
      this.activeCourse = '';
      this.branch = false;
      this.course = false;
      this.level1 = true;
    } else {
      console.log('u cannot go back');
    }

  }
  resetInstitute() {
    this.activeInstitute = "";
    this.resetSearch()
    localStorage.removeItem(this.constants.localStorage.INSTITUTEID);
    this.bck()
    this.year = []
    this.semesters = [];
    this.departments = [];
    this.courseList = [];
    this.yearwiseCount = [];
    this.resetYear()
  }
  setInstituteForuser(institute) {
    this.activeInstitute = {'_id':institute};
    //this.getInstitute(this.instituteid)
    this.onInstituteChange();
    this, this.setGeneralReportsFilters();
    localStorage.setItem(this.constants.localStorage.INSTITUTEID, this.activeInstitute._id);
    localStorage.setItem('instituteidconfigurations', this.activeInstitute._id)
    localStorage.setItem('instituteidGroupReports', this.activeInstitute._id)
  }
  
  setInstitute(institute) {
    this.activeInstitute = institute;
 console.log("+++++++++++++++++++++",this.activeInstitute)
    //this.getInstitute(this.instituteid)
    this.onInstituteChange();
    this, this.setGeneralReportsFilters();
    localStorage.setItem(this.constants.localStorage.INSTITUTEID, this.activeInstitute._id);
    localStorage.setItem('instituteidconfigurations', this.activeInstitute._id)
    localStorage.setItem('instituteidGroupReports', this.activeInstitute._id)
    this.activeInstitute && this.getyearWiseCountApi()
  }
  onInstituteChange() {
    this.bck();
    // this.getDepartments();
    this.getSemesters();
    this.departments = [];
    [this.activeYear, this.activeSemester, this.activeDepartment, this.activeCourse] = [undefined, undefined, undefined, undefined]
    this.setGeneralReportsFilters();
    // this.getConfigurations();
    }
  getInstitute(instituteid: any) {
    return (this.institutes.find(institute => instituteid == institute['_id']));
  }
  onInstituteRegister(data) {
    this.http.post(this.api.BASEURL + 'api/institute', { name: data.institute }, this.api.headers())
      .subscribe((response) => {
        this.listInstitutes();
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        });
  };
  loadInstitute() {
    this.activeInstitute = localStorage.getItem(this.constants.localStorage.INSTITUTEID);
    const typeOfInstitute = (this.activeInstitute == "undefined" || this.activeInstitute == 'undefined');
    typeOfInstitute && (this.activeInstitute = undefined)
  }
  getConfigurations() {
    const apiUrl = this.api.BASEURL + 'api/configuration?instituteid=' + (this.activeInstitute._id || this.activeInstitute);
    this.http.get(apiUrl, this.api.headers()).subscribe((response: any) => {
      this.populateConfigurations(response);
    },
      (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  };
  populateConfigurations(configurations: any[]) {
    const configurationsArray = configurations.map((configuration) => {
      let confJson = {};
      confJson[configuration.elementtype] = configuration.name;
      return confJson;
    });
    this.setConfigurations(configurationsArray);
    // this.setConfigurations()
  };
  setConfigurations(configurations) {
    localStorage.setItem(this.constants.localStorage.CONFIGURATIONS, JSON.stringify(configurations))
  }
  getInstituteData() {
    this.http.get(this.api.BASEURL + 'api/institute/' + this.activeInstitute._id, this.api.headers())
      .subscribe((response: any) => {
        this.institutes = [];
        this.institutes.push(response)
      },
        (error: any) => {
          console.log('error caught for api', error);
          //  (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
        })
  }
  flushSelectors() {
    this.isAdmin ? this.resetInstitute() : this.onInstituteChange();
  }
  ngOnDestroy() {
    // console.log("ngOnDestroy()");
    console.log("ngOnDestroy")
    // this.resetInstitute()
  }

  setGeneralReportsFilters() {
    const { activeInstitute, activeYear, activeSemester, activeDepartment, activeCourse } = this;
    this.genaralReports = { activeInstitute, activeYear, activeSemester, activeDepartment, activeCourse }
    // localStorage.setItem('activeYear',activeYear);
    localStorage.setItem(this.constants.localStorage.GENERALREPOTSFILTERS, JSON.stringify(this.genaralReports))
  }
  getStudentFilters() {
    const generalREportsFilterKey = this.constants.localStorage.GENERALREPOTSFILTERS;
    const studentfilters = localStorage.getItem(generalREportsFilterKey);
    this.genaralReports = studentfilters != null ? JSON.parse(studentfilters) : { activeInstitute: undefined, activeYear: undefined, activeSemester: undefined, activeDepartment: undefined, activeCourse: undefined };
    this.activeInstitute = this.genaralReports.activeInstitute;
    this.activeYear = this.genaralReports.activeYear //|| localStorage.getItem('activeYear');
    this.activeSemester = this.genaralReports.activeSemester;
    this.activeDepartment = this.genaralReports.activeDepartment
    this.activeCourse = this.genaralReports.activeCourse;
    (!this.activeInstitute) && this.onInstituteChange();
    (this.activeInstitute && !this.activeYear) && this.onInstituteChange();
    (this.activeInstitute && this.activeYear && !this.activeSemester) && this.setYear(this.activeYear), this.getSemesters();
    (this.activeInstitute && this.activeYear && this.activeSemester && !this.activeDepartment) && this.setSemester(this.activeSemester);
    (this.activeInstitute && this.activeYear && this.activeSemester && this.activeDepartment && !this.activeCourse) && this.OnCourseChange();
    (this.activeInstitute && this.activeYear && this.activeSemester && this.activeDepartment && this.activeCourse) && this.chooselevel(this.activeCourse)
  }

  getSearchString(data:any){
    this.searchString = data.trim();
  }
  searchMatchedCourses(modal,popUp_status:boolean){
     this.setReportConfigurations();
    !this.isSearchPopupOpen && popUp_status && (this.searchString.toLowerCase())  && this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', size:'lg', backdrop: 'static',keyboard: false});
    (this.searchString.toLowerCase()) && (this.searchString.toLowerCase() != this.searchedCourseInfo.searchString.toLowerCase()) && (this.searchedCourseInfo.matchedCourses = []);
    const url = this.api.BASEURL + "api/search?instituteid=" + this.activeInstitute._id + "&searchStringCrypted=" + btoa(this.searchString);
    (this.searchString.toLowerCase()) && (this.searchString.toLowerCase() != this.searchedCourseInfo.searchString.toLowerCase()) && this.http.get(url, this.api.headers())
      .subscribe((response: any) => {
        this.searchedCourseInfo = response;
        this.isSearchPopupOpen = true;
      },
        (error: any) => {
          console.log('error caught for api', error);
        })
  }

  setCourseData(filterCourseIndex:number, modal:any){
    const { year, semesterid, semesterName, departmentName, departmentid, courseName, coursecode, courseid } = this.searchedCourseInfo.matchedCourses[filterCourseIndex];
    [this.activeYear, this.activeSemester, this.activeDepartment ] = [year, {_id : semesterid, name : semesterName}, { _id : departmentid, name : departmentName }];
    this.chooselevel({ _id : courseid, coursecode, name : courseName })
    localStorage.setItem("courseId", courseid)
    this.closeModal(modal);
    this.searchString = this.searchedCourseInfo.searchString;
    // reportType: "group-testwise-allParticipants"
    this.reportConfigurations.filter(report => report.reportType == "group-testwise-allParticipants")[0]?.isVisible && this.router.navigate(["/report-1"]).then(()=>{});
  }
  getDisplayText(i:number){
    const { year, semesterName, departmentName, courseName, coursecode } = this.searchedCourseInfo.matchedCourses[i];
    return ("" + (i+1) + ". " + year + " / " + semesterName + " / " + departmentName + " / " + courseName + " - " + coursecode).replace(RegExp(this.searchedCourseInfo.searchString, 'ig'), (word) => "<b>" + word + "</b>")
  }
  closeModal(modal:any){
    modal.dismiss('Cross click')
    this.isSearchPopupOpen = false;
    this.input.nativeElement.value = this.searchString;
  }
  setReportConfigurations(){
    this.reportService.loadReportDisplayConfigurations((reportConfigurations:any)=>{
      this.reportConfigurations = reportConfigurations;
    })
  }
  resetSearch(){
    this.searchString = "";
    this.input.nativeElement.value = "";
    this.searchedCourseInfo.matchedCourses = [];
    this.searchedCourseInfo.searchString = ""
  }
}
