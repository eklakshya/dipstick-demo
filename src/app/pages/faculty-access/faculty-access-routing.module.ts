import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FacultyAccessComponent } from './faculty-access.component';

const routes: Routes = [{ path: '', component: FacultyAccessComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacultyAccessRoutingModule { }
