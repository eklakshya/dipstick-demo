import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacultyAccessRoutingModule } from './faculty-access-routing.module';
import { FacultyAccessComponent } from './faculty-access.component';


@NgModule({
  declarations: [FacultyAccessComponent],
  imports: [
    CommonModule,
    FacultyAccessRoutingModule
  ]
})
export class FacultyAccessModule { }
