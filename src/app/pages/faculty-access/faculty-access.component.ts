import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/http.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-faculty-access',
  templateUrl: './faculty-access.component.html',
  styleUrls: ['./faculty-access.component.css']
})
export class FacultyAccessComponent implements OnInit {

  constructor(private http :HttpClient, private api: ApiService,private routers: Router, private route: ActivatedRoute) { }

  
  response:any;
  ngOnInit(): void {

    this.http.get(this.api.BASEURL + 'api/courseAccess', this.api.headers())
      .subscribe((response: any) => {
        const { accessList, departments, courses } = response;
        const coursesList = Object.keys(courses).map((courseId:any) => {
          return { _id : courseId, ...courses[courseId] }
        }).sort((a:any, b:any) => a.createdAt - b.createdAt);

          this.response = { accessList, departments, courses };
          localStorage.setItem('faculty-access', JSON.stringify(this.response));
          const activeCourseId = coursesList[0]._id;
          const activeDepartmentId = coursesList[0].departmentid;

          const activeCourse = coursesList[0];
          const activeDepartment = { _id : activeDepartmentId, name : departments[activeDepartmentId] };
          const year =  accessList.find((list:any) => list.courses.includes(activeCourseId)).year // accessList[0]?.year;
          localStorage.setItem('generalreportsfilters', JSON.stringify({ activeCourse, activeDepartment, activeSemester : { year } }));
          localStorage.setItem('courseId', activeCourseId);
          this.route.queryParams.subscribe(params => {
            const page = params['page'];
            this.routers.navigate([ page == 'attendance' ? 'attendance' : 'report-1'], {
              skipLocationChange: false,
            }).then(() => {
              sessionStorage.setItem("attendance", `${Number(page == 'attendance')}`);
              location.reload();
            })
        });
        },(error: any) => {
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        }
      )
  }

}
