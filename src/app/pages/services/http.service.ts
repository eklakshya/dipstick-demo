import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
// const environments = require('../../../environments/environment.prod')
import {environment}  from "../../../environments/environment.prod"
import {shareReplay }  from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  isAdmin: number = 2;
  token:any;
  BASEURL = (environment.production ? environment.PRODUCTIONBASEURL : environment.DEVELOPMENTBASEURL);
  constructor(private http: HttpClient,
  public router :Router
  ) {
    this.isAdmin = localStorage.getItem('user') == "user" ? 1 : 1;
    this.token = localStorage.getItem('token')
   }
  headers(){
    let data:any = { headers: new HttpHeaders()
        .set('role', JSON.stringify(this.isAdmin))
        .set('authorization',JSON.stringify(this.token)) 
        }
  return data;
  }  
  noResp(data) {
    if (data > 0) {
      return true;
    } else {
      if(this.isAdmin == 1 ){
        alert('E00:No test data available for this course.');
        this.router.navigate(['/'], {
          skipLocationChange: false,
           }).then(()=>{  
           // location.reload();
          });
         // localStorage.removeItem('courseId');
        return false;
      }else{
        alert("E00: No test data available for this course.");
        this.router.navigate(['/'], {
          skipLocationChange: false,
           }).then(()=>{
          //location.reload();
          });
          //localStorage.removeItem('courseId');
          return false;
      }
    }
  }

 

  displaySideBarMenu() {
    return ((window.location.pathname.search(/report-/i)== 1) || (window.location.pathname.search(/attendance/i)==1))
  }


getCoursesById(id:String='all'){
  return  this.http.get(this.BASEURL + 'api/courses?id='+id, this.headers()).pipe(shareReplay());
}


sessionExp(){ 
 if(localStorage.getItem('user')) {
  alert("Your session has expired.");
  if((localStorage.getItem('user')) == "admin" || (localStorage.getItem('user')) == "user"){
    localStorage.clear();
    sessionStorage.clear();
    sessionStorage.setItem('sessionTime','true');
    this.router.navigate(["/"]).then(() => {
    location.reload();
    });
  }else{
     localStorage.clear();
    sessionStorage.clear();
    //  sessionStorage.setItem("session","lms");
    this.router.navigate(["/"]).then(() => {
     location.reload();
  });
  }
  }else{
    console.log("already show the alert.");
  } 
}

fullScreenCheck() {
  if (document.fullscreenElement) return;
  return document.documentElement.requestFullscreen();
}

async rotate() {
  try {
    await this.fullScreenCheck();
  } catch (err) {
    console.error(err);
  }
  await screen.orientation.lock("landscape");
}

 }
      // setTimeout(function(){ 
      //   location.reload();
      // }, 500);