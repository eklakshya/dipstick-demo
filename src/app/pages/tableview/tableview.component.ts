import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { constant } from 'src/app/shared/constant';
import {ApiService} from '../../pages/services/http.service'

@Component({
  selector: 'app-tableview',
  templateUrl: './tableview.component.html',
  styleUrls: ['./tableview.component.css']
})
export class TableviewComponent implements OnInit {
  response: any = {	
    levelWiseAgregation:[],
    conceptnumbers:[],
    levels:[]
  };
  @Input() type : string;
  @Input() division : string;
  @Input() testid : string;
  @Input() viewType : string;
  object: Object;
  courseid : string = localStorage.getItem('courseId');
  range : number[] = [1,2,3,4,5,6]
  levelLimit : number = 6;
  conceptLimit : number = 6;
  legend:any = constant.legendImage;

  constructor(private http:HttpClient,  private modalService: NgbModal, private api:ApiService) { 
  }

  getSvgWidth(rowDistribution:any[]){
    return rowDistribution[Math.ceil(rowDistribution.length%2==1?rowDistribution.length/2:rowDistribution.length)-1]
  }
  getSvgheight(rowDistribution:any[],svgPathLength:number){
    var totalHex = 0;
    var totalHexHeight = 0;
    var hexHeights:number[] = rowDistribution.map((rowdata, number)=>{
    totalHex = totalHex < svgPathLength ? totalHex + rowdata : (totalHex)
    totalHexHeight = totalHex < svgPathLength ? number + 1 : totalHexHeight ;
    return totalHexHeight + 1
    })
    var lastelement = hexHeights[hexHeights.length-1]
    return lastelement
  }
  ngOnInit(): void {
    let viewType = this.viewType || 'conceptWise';
    let apiEndPoint : string =  `${this.api.BASEURL}api/report/questionwise/table/${viewType}/${this.type}?courseId=${this.courseid}&testid=${this.testid}` + ( this.division ? `&divisionBuffer=${btoa(this.division)}` : ``)
    this.http.get(apiEndPoint).subscribe((response:any) => {
      this.response = response;
      console.log(response)
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }
  getNegativeMargin(lastelement:number,rowDistributionLength:number){
    return (rowDistributionLength - lastelement) * -50
  }
  ngAfterViewInit(){
  }
  open(modal) {
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', backdrop: 'static',keyboard: false}); // {ariaLabelledBy: 'modal-basic-title', size:'xl'}
  }
  questionsList(conceptData:any){
    let concept = Object.keys(conceptData)[0];
    return conceptData[concept]
  }
  conceptNumber(index:number){
    let concept = this.response.levelWiseAgregation[0][index]
    return Object.keys(concept.conceptData)[0]
  }
  getSubtopic(index:number){
    let concepts = [...this.response.concepts];
    return concepts.find(element => element._id.conceptnumber == this.conceptNumber(index))
  }
  findLevel(index:number){
    let levelWiseData:any[] = this.response.levelWiseAgregation
    let leveldata = levelWiseData.find(element =>element[0].bl == 'L'+index)
    return (leveldata && leveldata.length) ? true : false;
  }
  findLevelIndex(index:number){
    let levelWiseData:any[] = this.response.levelWiseAgregation
    let levelIndex = levelWiseData.findIndex(element =>element[0].bl == 'L'+index)
    return levelIndex;
  }
  getSizedArray(size:number){
    return new Array(size)
  }
}
