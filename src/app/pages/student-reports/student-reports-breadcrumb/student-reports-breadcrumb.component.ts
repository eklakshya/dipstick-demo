import { Component, OnInit } from '@angular/core';
import { ConstantsService } from '../../../core/services/constants.service';

@Component({
  selector: 'app-student-reports-breadcrumb',
  templateUrl: './student-reports-breadcrumb.component.html',
  styleUrls: ['./student-reports-breadcrumb.component.css']
})
export class StudentReportsBreadcrumbComponent implements OnInit {

  constructor(private constants : ConstantsService) { }

  ngOnInit(): void {
    this.getStudentFilters();
    this.getStudentData()
  }
  activeYear:number;
  activeSemester:any;
  activeDepartment:any;
  activeCourse:any;
  studentFilters : any = {
    activeYear : undefined,
    activeSemester : undefined,
    activeDepartment : undefined,
    activeCourse : undefined
  }
  activeStudent:any
  getStudentFilters(){
    const studentfilterKey = this.constants.localStorage.STUDENTSFILTERS;
    const studentfilters = localStorage.getItem(studentfilterKey);
    this.studentFilters = studentfilters!=null ? JSON.parse(studentfilters) : { activeYear : undefined, activeSemester : undefined, activeDepartment : undefined, activeCourse : undefined};
    this.activeYear = this.studentFilters.activeYear;
    this.activeSemester = this.studentFilters.activeSemester
    this.activeDepartment = this.studentFilters.activeDepartment
    this.activeCourse = this.studentFilters.activeCourse;
    console.log({studentfilterKey,studentfilters,})
    console.log('this.studentFilters',this.studentFilters+'this.activeYear',this.activeYear+' this.activeDepartment', this.activeDepartment+'this.activeCourse',this.activeCourse)
  }
  getStudentData(){
    const studentDataKey = this.constants.localStorage.STUDENTDATA;
    const studentData = localStorage.getItem(studentDataKey);
    this.activeStudent = JSON.parse(studentData) || null;
  }
  setStudentFilters(){
    const { activeYear, activeSemester, activeDepartment, activeCourse } = this
    this.studentFilters = { activeYear, activeSemester, activeDepartment, activeCourse }
    localStorage.setItem(this.constants.localStorage.STUDENTSFILTERS, JSON.stringify(this.studentFilters))
  }
  setStudentData(){
    localStorage.setItem(this.constants.localStorage.STUDENTDATA, JSON.stringify(this.activeStudent))
  }
  reset(levelIndex:any){
    console.log(levelIndex)
    /**
     * 
    0 : hard reset 
    1 : keep year 
    2 : keep department 
    3 : keep course
     */
  }
}
