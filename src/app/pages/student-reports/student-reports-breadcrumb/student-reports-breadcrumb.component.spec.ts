import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentReportsBreadcrumbComponent } from './student-reports-breadcrumb.component';

describe('StudentReportsBreadcrumbComponent', () => {
  let component: StudentReportsBreadcrumbComponent;
  let fixture: ComponentFixture<StudentReportsBreadcrumbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentReportsBreadcrumbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentReportsBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
