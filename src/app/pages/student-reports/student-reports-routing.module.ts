import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentReportsFilterComponent } from './student-reports-filter/student-reports-filter.component';

const routes: Routes = [{ path: '', component: StudentReportsFilterComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentReportsRoutingModule { }
