import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentReportsFilterComponent } from './student-reports-filter.component';

describe('StudentReportsFilterComponent', () => {
  let component: StudentReportsFilterComponent;
  let fixture: ComponentFixture<StudentReportsFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentReportsFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentReportsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
