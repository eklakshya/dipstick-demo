import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { ApiService } from '../../../pages/services/http.service';
import { ConstantsService } from '../../../core/services/constants.service';
import { ReportsService } from '../../../core/services/reports.service'

@Component({
  selector: 'app-student-reports-filter',
  templateUrl: './student-reports-filter.component.html',
  styleUrls: ['./student-reports-filter.component.css']
})
export class StudentReportsFilterComponent implements OnInit {
  isUser:boolean=false;
  inst_id:any;
  deviceInfo: string;
  isMobileDevice: boolean;
  constructor(private api : ApiService, private http:HttpClient, private constants : ConstantsService, public reportService : ReportsService) {
    
    this.isUser = (localStorage.getItem('user')=='user') ? true : false;
    this.inst_id =localStorage.getItem('instituteid');
   }

  data:any;
  activeInstitute:any;
  activeYear:number;
  activeSemester:any;
  activeDepartment:any;
  activeCourse:any;
  allDepartments:any[] = []
  // allCourses:any[] = []
  yearsList:any[]
  departmentsList:any[] = []
  semestersList:any[] = [];
  allSemesters:any[] = [];
  coursesList:any[] = []
  studentsList:any[] = []
  studentFilters : any = {
    activeYear : undefined,
    activeSemester : undefined,
    activeDepartment : undefined,
    activeCourse : undefined
  }
  activeStudent:any
  displayReports:boolean;
  institutes:any[];
  userRole:any = '';
  ngOnInit(): void {
    this.userRole = localStorage.getItem('userrole');
    this.deviceInfo =this.getMobileOperatingSystem();
    if(this.deviceInfo == "Android"){
     this.isMobileDevice = true;
     }else{
       this.isMobileDevice = false;
     }
     
    let role =localStorage.getItem('user');
    let studfil = localStorage.getItem('studentfilters');
    this.data  = localStorage.getItem('data');
    (role == "admin" && !studfil) ? localStorage.removeItem('instituteid') : console.log({role});
    
    if(this.userRole == 'faculty'){
      this.setNewInstituteforUser(localStorage.getItem('instituteid'))
    } else {
      
      this.isUser ? this.setNewInstituteforUser(this.inst_id) : this.listInstitutes()
     // this.getYearAndDepartmentList()
     this.getStudentData()
      this.activeInstitute?._id && this.selectInstitute();
    }
  }
  getYearAndDepartmentList(){
    const apiUrl = this.api.BASEURL + 'api/departments?semesterid='+this.activeSemester["_id"];
    this.http.get(apiUrl, this.api.headers()).subscribe((response:any) => {
      this.allDepartments = response.departments;
      this.departmentsList = response.departments;
      // const yearslist = this.allDepartments.map((department)=>{
      //   return (department.year)
      // })
      // this.yearsList = [...new Set(yearslist.sort((a, b) => a - b))]
      // this.getCoursesListByDepartmentId()
      // this.setDepartmentList()
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }
  setDepartmentList(){
    // const departmentslist = this.allDepartments.map((department)=>{
    //   return department.year == this.activeYear ? department : undefined
    // })
    // this.departmentsList = departmentslist.filter(function(e){return e})
  }
  setSemesterList(){
    const semestersList = this.allSemesters.map((semester)=>{
      return semester.year == this.activeYear ? semester : undefined
    })
    this.semestersList = semestersList.filter(function(e){return e})
  }
  getYearAndSemesterList(){
    const apiUrl = this.api.BASEURL + 'api/semester?instituteid='+this.activeInstitute["_id"];
    this.http.get(apiUrl, this.api.headers()).subscribe((response:any) => {
      this.allSemesters = response;
      const yearslist = this.allSemesters.map((semester)=>{
        return (semester.year)
      })
      this.yearsList = [...new Set(yearslist.sort((a, b) => a - b))]
      this.setSemesterList()
      // this.getCoursesListByDepartmentId()
      // this.setDepartmentList()
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }
  getCoursesListByDepartmentId(){
    localStorage.setItem("instituteidconfigurations",this.activeInstitute._id)
    const apiUrl = this.api.BASEURL + 'api/courses?id=' + this.activeDepartment?._id
    this.http.get(apiUrl, this.api.headers()).subscribe((response:any) => {
      this.coursesList = response?.courses;
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  }
  getStudentsByCourseId(){
    const apiUrl = this.api.BASEURL + 'api/report/student/students?courseid=' + this.activeCourse?._id;
    this.http.get(apiUrl, this.api.headers()).subscribe((response:any) => {
      this.studentsList = response;
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  }
  setNewInstitute(instituteid){
    this.activeInstitute = this.institutes.filter((institute)=>{
      return institute._id == instituteid;
    })[0];
    localStorage.setItem('instituteid',this.activeInstitute['_id']);
    this.onInstituteChange();
  }

  setNewInstituteforUser(instituteid){
    this.activeInstitute = {'_id':instituteid}
     localStorage.setItem('instituteid',this.activeInstitute['_id']);
     this.onInstituteChange();
    }

    setNewYear(year:any){
    this.activeYear = year;
    this.onYearChange();
  }
  setNewSemester(semesterid:any){
    this.activeSemester = this.allSemesters.filter((semester)=>{
      return semester._id == semesterid;
    })[0];
    this.onSemesterChange()
    this.getYearAndDepartmentList();
  }
  setNewDepartment(departmentid:any){
    this.activeDepartment = this.allDepartments.filter((department)=>{
      return department._id == departmentid;
    })[0];
    this.onDepartmentChange();
    this.getCoursesListByDepartmentId();
    this.studentsList = [];
  }
  setNewCourse(courseid:any){
    this.activeCourse = this.coursesList.filter((course)=>{
      return course._id == courseid;
    })[0];
    localStorage.setItem("studentReportsActiveCourse",JSON.stringify(this.activeCourse));
    this.getStudentsByCourseId();
    this.onCourseChange();
  }
  setNewStudent(usn:string){
    this.activeStudent = this.studentsList.filter((student)=>{
      return student.usn == usn;
    })[0];
    this.setStudentData()
  }
  onInstituteChange(){
    [this.activeYear] = [undefined];
    this.onYearChange();
    this.selectInstitute();
    // this.getYearAndDepartmentList();
    this.getYearAndSemesterList()
    localStorage.setItem('studentReportsInstituteid',this.activeInstitute['_id']);
    this.reportService.instituteSelected.emit(this.activeInstitute._id);
  }
  onYearChange(){
    [this.activeSemester] = [undefined];
    this.activeYear && this.setSemesterList()
    this.onSemesterChange()
  }
  onSemesterChange(){
    [this.activeDepartment] = [undefined];
    this.coursesList = [];
    this.onDepartmentChange();
    this.activeSemester && this.getYearAndDepartmentList();
    // this.setDepartmentList();
  }
  onDepartmentChange(){
    this.activeCourse = undefined;
    this.studentsList = [];
    this.onCourseChange();
  }
  onCourseChange(){
    this.setStudentFilters();
    this.activeStudent = null;
    this.setStudentData();
    this.viewReports();
  }
  hotReset(){
    if(this.userRole == 'faculty'){
      this.setNewInstituteforUser(localStorage.getItem('instituteid'))
    } else {
      [this.activeInstitute, this.activeYear, this.activeDepartment, this.activeCourse, this.activeStudent] = [null, null, null, null,null]
      this.viewReports();
      this.onCourseChange();
    }
  }
  setStudentFilters(){
    const { activeInstitute, activeYear, activeSemester, activeDepartment, activeCourse } = this
    this.studentFilters = { activeInstitute, activeYear, activeSemester, activeDepartment, activeCourse }
    localStorage.setItem(this.constants.localStorage.STUDENTSFILTERS, JSON.stringify(this.studentFilters))
  }
  setStudentData(){
    localStorage.setItem(this.constants.localStorage.STUDENTDATA, JSON.stringify(this.activeStudent))
    this.setTemproaryAssets()
  }
  setTemproaryAssets(){
    const studentReportAssets = { courseid : this.activeCourse?._id, studentid : this.activeStudent?._id}
    sessionStorage.setItem(this.constants.sessionStorage.STUDENTREPORTASSETS, JSON.stringify(studentReportAssets))
  }
  getStudentFilters(){
    const studentfilterKey = this.constants.localStorage.STUDENTSFILTERS;
    const studentfilters = localStorage.getItem(studentfilterKey);
    this.studentFilters = studentfilters!=null ? JSON.parse(studentfilters) : { activeInstitute : undefined, activeYear : undefined, activeSemester : undefined, activeDepartment : undefined, activeCourse : undefined};
    this.activeInstitute = this.studentFilters.activeInstitute;
    this.activeYear = this.studentFilters.activeYear;
    this.activeSemester = this.studentFilters.activeSemester;
    this.activeDepartment = this.studentFilters.activeDepartment
    this.activeCourse = this.studentFilters.activeCourse;
    this.activeInstitute && this.getYearAndSemesterList() //this.getYearAndDepartmentList();
    // this.setDepartmentList();
    this.activeYear && this.setSemesterList();
    this.activeSemester && this.getYearAndDepartmentList()
    this.activeDepartment && this.getCoursesListByDepartmentId();
    this.getStudentsByCourseId()
  }
  getStudentData(){
    const studentDataKey = this.constants.localStorage.STUDENTDATA;
    const studentData = localStorage.getItem(studentDataKey);
    this.activeStudent = JSON.parse(studentData) || null;
  }
  viewReports(){
    this.setTemproaryAssets()
    this.displayReports = (this.activeInstitute && this.activeYear && this.activeSemester && this.activeSemester._id && this.activeDepartment && this.activeDepartment._id && this.activeCourse && this.activeCourse?._id && this.activeStudent && this.activeStudent?._id);
    let S_bread = 'Students-reports'+'/'+this.activeInstitute?.name;
    S_bread += '/'+this.activeYear+'/'+this.activeSemester?.name+'/'+this.activeDepartment?.name+'/'+this.activeCourse?.name+'/'+this.activeStudent?.usn+'-'+this.activeStudent?.name;
    localStorage.setItem("S_bread", S_bread )
    this.rotate();
  }
  listInstitutes(){
    this.http.get(this.api.BASEURL + 'api/institute',this.api.headers())
    .subscribe((response:any) => {
      this.institutes = response;
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
    this.getStudentFilters()
  }
  selectInstitute(){
    const apiUrl = this.api.BASEURL + 'api/configuration?instituteid=' + this.activeInstitute['_id'];
    this.http.get(apiUrl, this.api.headers()).subscribe((response:any) => {
      this.populateConfigurations(response);
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  };
  populateConfigurations(configurations:any[]){
    const configurationsArray = configurations.map((configuration)=>{
      let confJson = {};
      confJson[configuration.elementtype] = configuration.name;
      return confJson;
    });
    this.setConfigurations(configurationsArray);
    // this.setConfigurations()
  };
  setConfigurations(configurations){
    localStorage.setItem(this.constants.localStorage.CONFIGURATIONS, JSON.stringify(configurations));
  }

  fullScreenCheck() {
    if (document.fullscreenElement) return;
    return document.documentElement.requestFullscreen();
  }

  async rotate() {
    if(this.isMobileDevice){
      
    try {
      await this.fullScreenCheck();
    } catch (err) {
      console.error(err);
    }
    await screen.orientation.lock('landscape');
  }else{
    console.log("Not mobile")
  } 
}
 

  getMobileOperatingSystem() {
    let userAgent = navigator.userAgent || navigator.vendor;
    console.log("=========================="+userAgent); 
    if (/windows/i.test(userAgent)) {
        return "Windows";
    }
    if (/android/i.test(userAgent)) {
        return "Android";
    }
     if (/iPad|iPhone|iPod/.test(userAgent)) {
        return "iOS";
    }
    return "unknown";
}
}

 