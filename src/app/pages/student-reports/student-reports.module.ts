import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentReportsRoutingModule } from './student-reports-routing.module';
import { StudentReportsFilterComponent } from './student-reports-filter/student-reports-filter.component';
import { StudentReportsViewComponent } from './student-reports-view/student-reports-view.component';
import { StudentReportsBreadcrumbComponent } from './student-reports-breadcrumb/student-reports-breadcrumb.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StudentWiseConceptWiseModule } from '../concept-wise/student-wise-concept-wise/student-wise-concept-wise.module';
import { StudentWiseTestWiseModule } from '../test-wise/student-wise-test-wise/student-wise-test-wise.module';
import { StudentWiseSubconceptWiseModule } from '../subconcept-wise/student-wise-subconcept-wise/student-wise-subconcept-wise.module';
import { ConceptWiseModule } from 'src/app/commmon-modules/concept-wise.module';
import { popUpTableModule } from 'src/app/commmon-modules/popUptable.module';
import { CoreModule } from 'src/app/core/core.module';
import { AttemptwiseStudentwiseTestwiseModule } from '../test-wise/attemptwise-studentwise-testwise/attemptwise-studentwise-testwise.module';
import { AttemptwiseStudentwiseConceptwiseModule } from '../concept-wise/attemptwise-studentwise-conceptwise/attemptwise-studentwise-conceptwise.module';
import { AttemptwiseStudentwiseSubconceptWiseModule } from '../subconcept-wise/attemptwise-studentwise-subconcept-wise/attemptwise-studentwise-subconcept-wise.module';



@NgModule({
  declarations: [
    StudentReportsFilterComponent,
    StudentReportsViewComponent,
    StudentReportsBreadcrumbComponent],
  imports: [
    CommonModule,
    StudentReportsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    StudentWiseConceptWiseModule,
    StudentWiseTestWiseModule,
    StudentWiseSubconceptWiseModule,
    ConceptWiseModule,
    AttemptwiseStudentwiseTestwiseModule,
    AttemptwiseStudentwiseConceptwiseModule,
    AttemptwiseStudentwiseSubconceptWiseModule
     
  ]
})
export class StudentReportsModule { }
