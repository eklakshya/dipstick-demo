import { HttpClient } from '@angular/common/http';
import { Component, OnInit,ElementRef, Output, EventEmitter, Input  } from '@angular/core';
import { ReportsService } from '../../../core/services/reports.service'
import { ApiService } from '../../services/http.service';
import { constant } from 'src/app/shared/constant';

@Component({
  selector: 'app-student-reports-view',
  templateUrl: './student-reports-view.component.html',
  styleUrls: ['./student-reports-view.component.css']
})
export class StudentReportsViewComponent implements OnInit {

  @Input() studentName : any;
  @Input() courseName : any;
  @Input() studentUSN : any;
  legend:any = constant.legendImage;
  downloadPDF:any = constant.downloadPDF;
  isMobileDevice: any;
  deviceInfo: string;
  isDownLoadGroupReportsBegin = false;
  constructor(public reportService : ReportsService,private el : ElementRef, private api: ApiService, public http: HttpClient) { 
    this.reportService.loadReportStudentDisplayConfigurations((reportConfigurations:any)=>{
      this.reportConfigurations = reportConfigurations;
      this.activeTab = (this.tabs.find((tab:any) => reportConfigurations.find((configuration:any) => ((configuration.reportType == tab.reportType) && configuration.isVisible))))
      this.checkReportNotFound();
      // this.getColor()
    });
    this.getColor()
    screen.orientation.addEventListener("change", () => {
      // this.show();
    });

    window.addEventListener("load", () => {
      // this.show();
    });
    this.rotate()
  }
  reportConfigurations : any[] = [];
  tabs : any[] = [
    { title : "Test", configurationtype:"test", type : "TESTWISE", index : 1, reportType : "student-testwise"},
    { title : "Concept", configurationtype:"concept", type : "CONCEPTWISE", index : 2, reportType : "student-conceptwise"},
    { title : "Subconcept", configurationtype:"subconcept", type : "SUBCONCEPTWISE", index : 3, reportType : "student-subconceptwise"},
  ];
  activeTab : any = this.tabs[0];
  styleElement2:any=null;
  styleElement3:any=null;
  ulineobj:any =`.uline2{
    border-top: 4px solid --color !important;
    border-top: 4px solid #33b35a;
    padding: 12px 0;
    }`;
    fontColor:any=`
    .active-top-navigation-bar{
      color: --font !important;
      color: #33b35a;
    cursor:text !important;
  }`;

  exportToPDFData:any = {};
  isDownloadAvailable = false;
  ngOnInit(): void {

    this.deviceInfo =this.getMobileOperatingSystem();
    if(this.deviceInfo == "Android"){
     this.isMobileDevice = true;
     }else{
       this.isMobileDevice = false;
     }

     
    this.getColor()
  }
  createStyle(style: string): void {
    if(this.styleElement2){
      this.styleElement2.removeChild(this.styleElement2.firstChild);
    } else {
      this.styleElement2 = document.createElement('style');
    }
    this.styleElement2.appendChild(document.createTextNode(style));
    this.el.nativeElement.appendChild(this.styleElement2);
}

  createfontStyle(style: string): void {
    if(this.styleElement3){
      this.styleElement3.removeChild(this.styleElement3.firstChild);
    } else {
      this.styleElement3 = document.createElement('style');
    }
    this.styleElement3.appendChild(document.createTextNode(style));
    this.el.nativeElement.appendChild(this.styleElement3);
  }
  checkReportVisibility(reportType:any){
    return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == reportType)?.isVisible;
  }
  checkPopupVisibility(popupType:any){
    return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == popupType)?.isVisible;
  }
  checkReportNotFound(){
    let tabVisibilities = this.tabs.map((tab:any) => !this.checkReportVisibility(tab.reportType))
    return ([...new Set(tabVisibilities)].length == 1) && tabVisibilities[0]
  }
  getColor(){
    const apiUrl = this.api.BASEURL + 'api/institute/'+ localStorage.getItem("studentReportsInstituteid");
    this.http.get(apiUrl, this.api.headers()).subscribe((response: any) => {
      console.log("response : ", response);
      this.setColor(response?.color)
    })
  }
  setColor(colorCode="#33b35a"){
    let style: string = this.ulineobj.replaceAll('--color',colorCode);
    this.createStyle(style);
    let font: string = this.fontColor.replaceAll('--font',colorCode);
    this.createfontStyle(font);
  }
  generatePDF(){
    // const clusters = this.reportDetails.map((reportDetailData:any) => {
    //   return {
    //     clusterName : "Attempt " + reportDetailData.testArray[0].attempt,
    //     paths : reportDetailData.hexagonSvgPaths,
    //     colors : reportDetailData.hexagonColors,
    //     label : "T",
    //     scores : reportDetailData.testArray.map((testData) => testData.score),
    //     centerX : reportDetailData.centerX,
    //     centerY : reportDetailData.centerY,
    //   }
    // });
    // console.log(clusters);
    const data = {
      title : "post test 1",
      // clusters
    }

    this.http.post(this.api.BASEURL + 'api/download/v1', data, this.api.headers())
    .subscribe((data) => {
      console.log(data)
    }, (e) => {
      console.log(e)
    })

  }
  getReportData(type:string, data:any){
    this.exportToPDFData.studentName = this.studentName;
    this.exportToPDFData.courseName = this.courseName;
    this.exportToPDFData.studentUSN = this.studentUSN;
    // this.exportToPDFData.breadcrumbPath = localStorage.getItem("S_bread") || null;
    this.exportToPDFData.breadcrumbPath = "student - reports/" + this.courseName + "/" + this.studentUSN + "-" + this.studentName;
    this.exportToPDFData[type] = data;
    this.exportToPDFData.reportNames = {}
    this.tabs.forEach((tab:any, i:number) => {
      this.exportToPDFData.reportNames[tab.configurationtype] = this.reportService.replaceTopic(tab.title, tab.configurationtype)
    })
    if(Object.keys(this.exportToPDFData).length == 3){
      this.isDownloadAvailable = true;
      // this.reportGeneration();
    } else {
      const availableReportTypes = Object.keys(this.exportToPDFData);
      const availableReports = this.tabs.filter((tabData:any) => this.checkReportVisibility(tabData.reportType)).map((tabData:any) => availableReportTypes.includes(tabData.reportType));
      if(availableReports.reduce((a:boolean,b:boolean) => a && b)){
        this.isDownloadAvailable = true;
        // this.reportGeneration()
      }
    }
  }

  downloadStatus:string = 'not-downloaded';
  reportGeneration(){

    this.downloadStatus = 'in-progress'
    this.isDownloadAvailable ? this.http.post(this.api.BASEURL + 'api/download/v1', { clusterJson : this.exportToPDFData }, {...this.api.headers(), responseType: 'blob' as 'json'})
    .subscribe((response:any) => {
      let dataType = response.type;
      let binaryData = [];
      binaryData.push(response);
      let downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
      downloadLink.setAttribute('download', `${this.exportToPDFData.studentName}-${this.exportToPDFData.studentUSN}-Dipstick Reports.pdf`);
      document.body.appendChild(downloadLink);
      downloadLink.click();
      this.downloadStatus = 'downloaded'
  }, (e) => {
      console.log(e)
    }) : alert("We are still downloading the reports!!! please try again later");
  }
  fullScreenCheck() {
    if (document.fullscreenElement) return;
    return document.documentElement.requestFullscreen();
  }

  async rotate() {
    if(this.isMobileDevice){
     try {
      await this.fullScreenCheck();
    } catch (err) {
      console.error(err);
    }
    await screen.orientation.lock('landscape');
  }else{
    console.log("Not mobile")
  } 
}
 

  getMobileOperatingSystem() {
    let userAgent = navigator.userAgent || navigator.vendor;
    console.log("=========================="+userAgent); 
    if (/windows/i.test(userAgent)) {
        return "Windows";
    }
    if (/android/i.test(userAgent)) {
        return "Android";
    }
     if (/iPad|iPhone|iPod/.test(userAgent)) {
        return "iOS";
    }
    return "unknown";
}
}
