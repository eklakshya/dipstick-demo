import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentReportsViewComponent } from './student-reports-view.component';

describe('StudentReportsViewComponent', () => {
  let component: StudentReportsViewComponent;
  let fixture: ComponentFixture<StudentReportsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentReportsViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentReportsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
