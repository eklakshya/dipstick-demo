import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { PopupTableViewConceptwiseComponent } from '../shared/popup-table-view-conceptwise/popup-table-view-conceptwise.component';



@NgModule({
  declarations: [PopupTableViewConceptwiseComponent ],
  exports: [PopupTableViewConceptwiseComponent ],
  imports: [
    CommonModule
  ]
})
export class popUpTableModule { }
