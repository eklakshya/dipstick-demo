import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConceptReportsAllParticipantsComponent } from '../shared/concept-reports-all-participants/concept-reports-all-participants.component';
import { popUpTableModule } from './popUptable.module';
import { ConceptwisedivisionWiseModule } from '../pages/concept-wise/conceptwisedivisionwise/conceptwisedivision-wise.module';
import { DownloadGroupReportsModule } from '../shared/download-group-reports/download-group-reports.module';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [ConceptReportsAllParticipantsComponent],
  exports: [ConceptReportsAllParticipantsComponent],
  imports: [
    CommonModule,
    popUpTableModule,
    DownloadGroupReportsModule,
    CoreModule
  ]
})
export class ConceptWiseModule { }
