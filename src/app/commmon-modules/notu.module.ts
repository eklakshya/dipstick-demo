import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DivisionListForAttendenceComponent } from '../core/division-list-for-attendence/division-list-for-attendence.component';
import { StudentReportViewComponent } from '../pages/student-report-view/student-report-view.component';
import { DivisionChartComponent } from '../shared/division-chart/division-chart.component';



@NgModule({
  declarations: [DivisionListForAttendenceComponent,StudentReportViewComponent,DivisionChartComponent],
  imports: [
    CommonModule
  ]
})
export class NotuModule { }
