import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupTableViewConceptwiseComponent } from '../shared/popup-table-view-conceptwise/popup-table-view-conceptwise.component';
import { ConceptReportsDivisionWiseComponent } from '../shared/concept-reports-division-wise/concept-reports-division-wise.component';
import { popUpTableModule } from './popUptable.module';
import { DownloadGroupReportsModule } from '../shared/download-group-reports/download-group-reports.module';
import { CoreModule } from '../core/core.module';



@NgModule({
  declarations: [ConceptReportsDivisionWiseComponent],
  exports: [ConceptReportsDivisionWiseComponent],
  imports: [
    CommonModule,
    popUpTableModule,
    DownloadGroupReportsModule,
    CoreModule
  ]
})
export class ConceptWiseDivModule { }
