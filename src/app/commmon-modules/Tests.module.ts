import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestWiseReportsSingleClusterComponent } from '../shared/test-wise-reports-single-cluster/test-wise-reports-single-cluster.component';



@NgModule({
  declarations: [TestWiseReportsSingleClusterComponent],
  exports: [TestWiseReportsSingleClusterComponent],
  imports: [
    CommonModule
  ]
})
export class TestsModule { }
