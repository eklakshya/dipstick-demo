import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntigrationMainpageComponent } from './intigration-mainpage.component';

describe('IntigrationMainpageComponent', () => {
  let component: IntigrationMainpageComponent;
  let fixture: ComponentFixture<IntigrationMainpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntigrationMainpageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntigrationMainpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
