import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntigrationMainpageComponent } from './intigration-mainpage.component';

const routes: Routes = [{ path: '', component: IntigrationMainpageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntigrationMainpageRoutingModule { }
