import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntigrationMainpageRoutingModule } from './intigration-mainpage-routing.module';
import { IntigrationMainpageComponent } from './intigration-mainpage.component';


@NgModule({
  declarations: [IntigrationMainpageComponent],
  imports: [
    CommonModule,
    IntigrationMainpageRoutingModule
  ]
})
export class IntigrationMainpageModule { }
