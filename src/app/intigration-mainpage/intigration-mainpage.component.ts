import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ApiService} from '../pages/services/http.service'
import { Router, NavigationEnd } from '@angular/router';
import { ConstantsService } from '../core/services/constants.service';

//  import { Routes, RouterModule, ActivatedRoute } from '@angular/router'
 import { ActivatedRoute } from '@angular/router';
 
@Component({
  selector: 'app-intigration-mainpage',
  templateUrl: './intigration-mainpage.component.html',
  styleUrls: ['./intigration-mainpage.component.css']
})
export class IntigrationMainpageComponent implements OnInit {
  myParam:any;
  actRoute: any;
  id:any;
  apiresponse: any;
  response:any;
  payload:any[]=[];

  coursecode:any;
  courseId:any;
  department:any;
  semester:any;
  studentid:any;
  studentName:any;
  breadcrumbData:any;
  reportsFilters:any;
  academicyear:any;
  program:any;
    //to store the real data
    semestersList:any[] = [];
    yearList:any[] = [];
    departmentList:any[] = [];
    coursesList:any[]=[];
    studentData:any[]=[];

  constructor(public constants:ConstantsService, private router: ActivatedRoute,private http: HttpClient,private modalService: NgbModal, private api:ApiService, private route : Router) { 
    this.router.queryParams.subscribe(async params => {
      console.log(params)
      this.myParam =params['accessToken'];
      params.course && (this.coursecode = (params['course']).replaceAll("%20", " "));
      params.department && (this.department = (params['department']).replaceAll("%20", " "));
      params.semester && (this.semester = (params['semester']).replaceAll("%20", " "));
      params.program && (this.program = (params['program']).replaceAll("%20", " "));
      params.academicyear && (this.academicyear = params['academicyear']);
      params.accessToken && sessionStorage.setItem('token',this.myParam);
      //this.api.rotate();
      if(this.coursecode && this.myParam){
        this.payload.push({coursecode:this.coursecode},{department : this.department},{semester : this.semester});
        localStorage.setItem('payLoad',JSON.stringify(this.payload));
      }
      console.log(this.myParam);
    },(error:any) => {                           
      console.log('error caught for api',error);
      // (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
      if(this.myParam && this.myParam != undefined && this.myParam != null && this.myParam != ''){
        this.getfilterData(this.myParam)
     }else{
       console.log("No token in params.");
       this.clear();
       alert("accessToken Issue, please check it and try again.");
     }
  }
  ngOnInit() {
    //this.api.rotate()
  }


async getfilterData(token:any){
  this.http.get(this.api.BASEURL + "api/integration/GetcallApi?token="+token)
  .subscribe(async (response) => {
    this.apiresponse =response;
    if(Boolean(this.apiresponse.accessData)){   
      let List:any,CI:any;
      (this.semester == 0) ? this.semester = "all" : console.log(this.semester);
      List = await this.dataList();
      List && (CI = await this.coursecodeValidation());
      if(this.apiresponse?.userrole != undefined){  //||
        let instituteid =this.coursesList[0]['instituteid'];
        localStorage.setItem('instituteid',instituteid);
        localStorage.setItem('getcallApi',JSON.stringify(this.apiresponse));
        
        localStorage.setItem('token',this.apiresponse.jwtToken)
        //this.api.rotate()
        if(this.apiresponse?.userrole == 'student'){
          await this.studentRole(); 
        }else{
          await this.otherRoles();
        } 
      }else{
        alert("Something went wrong, Please try again.")
        this.clear();
      }
    }else{
      this.clear();
      alert("No test data available for this course")
    }
  },(error:any) => {                           
    console.log('error caught for api',error);
    (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
  });
}

yearbyCoursecode(coursecode:any){
  //  let yearString:any;
  return coursecode.trim().split("_")[1];
  // let years = yearString.split("-");
  // let firstyear = years[0];
  // let secondyear = years.slice(-1)[0].slice(-2);
  // let year = firstyear + "-" + secondyear;
  // return yearString;
}
async coursecodeValidation(){
  if(Boolean(this.coursecode)){
    if(this.semester){
      let setfiltersemesterData:any = await this.setfiltersemesterData();
      !setfiltersemesterData ? this.latestreportsData() : console.log({setfiltersemesterData}); 
    }if(this.department){
      let setdepartmentFilterdata = await this.setdepartmentFilterforteacherrole();
      !setdepartmentFilterdata ? this.latestreportsData() : console.log({setdepartmentFilterdata});
    }if(!this.department && !this.semester){
      this.latestreportsData();
    }if(this.apiresponse?.userrole == 'student' && !this.studentid){
      let activeStudentData = this.studentData.find((data:any) => data.coursecode == this.coursecode);
      activeStudentData != undefined && (this.studentid = activeStudentData._id);
      activeStudentData != undefined && (this.studentName =activeStudentData.name);
      
      let activeCourseData = this.coursesList.find((courseData) => courseData.coursecode == this.coursecode);
      localStorage.setItem("studentReportsActiveCourse", activeCourseData)
    }}else if(this.academicyear && this.department){
      let setdepartmentFilterdata = await this.setdepartmentFilter();
      !setdepartmentFilterdata ? this.latestreportsData() : console.log({setdepartmentFilterdata});
    }else{
      this.latestreportsData();
    }if(this.courseId) return 1  
    else return 0
  }

async latestreportsData(){
  console.log('courseid of the firstdata in the redirection reports',this.coursesList[0]['_id']);
  this.courseId =this.coursesList[0]['_id'];
  localStorage.setItem("studentReportsActiveCourse", this.coursesList[0])
  localStorage.setItem('courseId',this.courseId );
  this.setActivedata();
  alert("E001: No test data available for this course. Please continue to view available reports.");
}

async setfiltersemesterData(){
  let year:any,semesters,semid:any,filtersemName:any,filterdep:any,filterdepName:any,filterdepid:any,filterCourse:any,filtercourseName:any,filtercid:any,activeStudentData:any;
  // if(this.semester == "all" || this.semester == 0){
    year = await this.yearbyCoursecode(this.coursecode);
    this.coursecode && (filterCourse= await this.coursesList.find(course=>course.coursecode == this.coursecode));  
    filterCourse && (filtercourseName = filterCourse.name,filtercid = filterCourse._id, filterdepid = filterCourse.departmentid);

    filtercid && filterdepid && (filterdep = await this.departmentList.find(dep=> dep._id == filterdepid && dep.year == year));
    filterdep && (filterdepName = filterdep.name,filterdepid=filterdep._id,semid=filterdep.semesterid);

    semid && (semesters = await this.semestersList.find(sem=> sem._id == semid && sem.year == year));
    console.log("redirected semister filtered one ",semesters);
    semesters &&  (filtersemName = semesters.name, semid = semesters._id);
    
    filtercid && semesters && (activeStudentData = this.studentData.find((data:any) => data.coursecode == this.coursecode && data.courseId == filtercid));
    activeStudentData != undefined && (this.studentid = activeStudentData._id);
    activeStudentData != undefined && (this.studentName =activeStudentData.name);
    filtercid && semid && filterdepid &&  (this.courseId=filtercid,localStorage.setItem('courseId',filtercid ),this.chooselevel(year,semesters,filterdep,filterCourse));
    (!filtercid || !semid || !filterdepid || filterCourse == undefined ) ?  (localStorage.removeItem('reportfilters'),localStorage.removeItem('data')) : console.log('All the data is present.')
    if(filtercid && semid && filterdepid) return 1
    else return 0;
  }
  
  async studentRole(){
  //this.api.rotate()
  
  let activeCourseData = this.coursesList.find((courseData) => (courseData._id == this.courseId || courseData.coursecode == this.coursecode));
  localStorage.setItem("studentReportsActiveCourse", JSON.stringify(activeCourseData))

  this.studentid = this.studentid || this.studentData[0]['_id'];
  this.studentName = this.studentName || this.studentData[0]['name'];
  localStorage.setItem('userrole', this.apiresponse?.userrole);
  sessionStorage.setItem('studentid',this.studentid);
  sessionStorage.setItem('dataset','true');
  setTimeout(()=>{
  this.route.navigate(['integration/reports'], {
  skipLocationChange: false,
  queryParams: { courseid : this.courseId , studentid: this.studentid, studentName: this.studentName}
  }).then(()=>{
  location.reload();
  });
  },1000);
}

async otherRoles(){
  const dataset1:any= this.apiresponse;
  Boolean(this.apiresponse?.userrole) ? localStorage.setItem('user',this.apiresponse?.userrole) :localStorage.setItem('user','user');
  localStorage.setItem('userrole', this.apiresponse?.userrole);
  sessionStorage.setItem('dataset',dataset1);
  if(Boolean(this.courseId)){
  setTimeout(()=>{
  this.route.navigate(['report-1'], {
  skipLocationChange: false,
  // queryParams: { accessToken : this.myParam}
  }).then(()=>{
  location.reload();
  });
  },1000);
  }
}



async setdepartmentFilter(){
  let year:any,semesters,filterdepsemid:any,semid:any,filtersemName:any,filterdep:any,filterdepName:any,filterdepid:any,filterCourse:any,filtercourseName:any,filtercid:any;
  
  //find the department based on department name or semname
  this.academicyear && (filterdep =await this.departmentList.find(dep=> dep.name == this.department && dep.year == this.academicyear));
 filterdep && (filterdepName = filterdep.name,filterdepid=filterdep._id,filterdepsemid= filterdep.semesterid);
  
  
  //find the data based on the year
  filterdep && this.academicyear && (semesters = await this.semestersList.find(sem=> sem._id == filterdepsemid));
  semesters && (year = semesters.year,filtersemName =semesters.name,semid=semesters._id);

 //for teacher role
  this.coursecode && filterdepid && (filterCourse= await this.coursesList.find(course=>course.coursecode == this.coursecode && course.depName == this.department && course.program == this.program));  
  //for admin and hod and manager.
  !this.coursecode && filterdepid && (filterCourse= await this.coursesList.find(course=>course.departmentid == filterdepid && course.depName == this.department && course.program == this.program));
  filterCourse && (filtercourseName = filterCourse.name,filtercid = filterCourse._id,this.courseId=filterCourse._id);

  filterdep && semesters && filtercid && (localStorage.setItem('courseId',filtercid ),this.chooselevel(year,semesters,filterdep,filterCourse));
  !filterdep && !semesters && filterCourse == undefined ?  (localStorage.removeItem('reportfilters'),localStorage.removeItem('data')) : console.log('All the data is present.');
  
  if(filterdep == undefined || semesters== undefined || filtercid == undefined || !filterCourse || !this.courseId) return false
  else return true 

}

async setdepartmentFilterforteacherrole(){
  let year:any,semesters,filterdepsemid:any,semid:any,filtersemName:any,filterdep:any,filterdepName:any,filterdepid:any,filterCourse:any,filtercourseName:any,filtercid:any;
  
  this.academicyear && this.coursecode &&  (filterCourse= await this.coursesList.find(course=>course.depName == this.department && course.coursecode == this.coursecode && course.program == this.program));
  filterCourse && (filtercourseName = filterCourse.name,filtercid = filterCourse._id,this.courseId=filterCourse._id,filterdepid=filterCourse.departmentid);
  //find the department based on department name or semname
  filterCourse && this.academicyear && (filterdep =await this.departmentList.find(dep=> dep.name == this.department && dep.year == this.academicyear && dep._id == filterdepid));
 filterdep && (filterdepName = filterdep.name,filterdepid=filterdep._id,filterdepsemid= filterdep.semesterid);
  
  //find the data based on the year
  filterdep && this.academicyear && (semesters = await this.semestersList.find(sem=> sem._id == filterdepsemid));
  semesters && (year = semesters.year,filtersemName =semesters.name,semid=semesters._id);

  filterdep && semesters && filtercid && (localStorage.setItem('courseId',filtercid ),this.chooselevel(year,semesters,filterdep,filterCourse));
  !filterdep && !semesters && filterCourse == undefined ?  (localStorage.removeItem('reportfilters'),localStorage.removeItem('data')) : console.log('All the data is present.');
  
  if(filterdep == undefined || semesters== undefined || filtercid == undefined || !filterCourse || !this.courseId) return false
  else return true 
}

async setActivedata(){
  let yearsdata = this.yearList[0].year;
  let courseName=this.coursesList[0];
  let departmentName=this.departmentList[0];
  let semesterName = this.semestersList[0];
  yearsdata && courseName && departmentName && semesterName && this.chooselevel(yearsdata,semesterName,departmentName,courseName);
}


//for breadcrumb data, not using presently  
async chooselevel(Year:any,semester:any,Department:any,courseName:any) {
  //this.settemp({ name: this.activeCourse['name'], url: "", type: 'course' })
  this.breadcrumbData = 'You are here: reports/ ';
  this.breadcrumbData += Year + '/' + semester.name + '/' + Department.name + '/' + courseName.name
   console.log("breadcrumbData in intigration main page", this.breadcrumbData);
  localStorage.setItem('data', this.breadcrumbData);
  localStorage.setItem('S_bread', this.breadcrumbData);
  this.reportsFilters = {activeYear:Year, activeSemester:semester.name,activeDepartment:Department.name,activeCourseId:courseName._id, activeCourse:courseName,activeSemesterId:semester._id,activeDepartmentId:Department._id}
  localStorage.setItem(this.constants.localStorage.REPORTSFILTERS, JSON.stringify(this.reportsFilters))
  return 1;
  }
  
async dataList(){
  //List the courses
  await this.apiresponse.accessData.forEach((data)=>{
  this.coursesList.push(data.courseData);
  });      
  console.log(this.coursesList);
  //list the student data
 await this.apiresponse.accessData.forEach((data)=>{
  this.studentData.push(data.studentData);
  }); 

  //listing years
  await this.apiresponse.accessData.forEach((data)=>{
  //if (!this.yearList.includes(data.semesterData.year)) {
  this.yearList.push(data.semesterData); 
  //}
  })
  console.log('==yearList====',this.yearList)

  //listing the semisters
  await this.apiresponse.accessData.forEach((data)=>{
  this.semestersList.push(data.semesterData);
  });
  console.log('==semestersList====', this.semestersList);

  //departmentList
  await this.apiresponse.accessData.forEach((data)=>{
  this.departmentList.push(data.departmentData);
  });
  console.log('==departmetnList====', this.departmentList);

  if(this.yearList && this.semestersList && this.departmentList &&  this.coursesList) return 1;
  else return 0;
}

clear(){
  sessionStorage.clear();
  localStorage.clear();
}

// createStyle(style: string): void {
// if(this.styleElement){
//  this.styleElement.removeChild(this.styleElement.firstChild);
// } else {
//  this.styleElement = document.createElement('style');
// }
// this.styleElement.appendChild(document.createTextNode(style));
// this.el.nativeElement.appendChild(this.styleElement);
// } 
}


  
// filterdep =await this.departmentList.find(dep=> dep.name == this.department);
// filterdep && (filterdepName = filterdep.name,filterdepid=filterdep._id,semid=filterdep.semesterid);
// this.academicyear && filterdepid && (  semesters = await this.semestersList.find(sem=> sem._id == semid && sem.year == this.academicyear));
// !this.academicyear && filterdepid && (  semesters = await this.semestersList.filter(sem=> sem._id == semid));
// semesters && (year = semesters.year,filtersemName =semesters.name);
// this.coursecode && filterdepid && (filterCourse= await this.coursesList.find(course=>course.coursecode == this.coursecode && course.departmentid == filterdepid));  
// !this.coursecode && filterdepid && (filterCourse= await this.coursesList.find(course=>course.departmentid == filterdepid));
// filterCourse && (filtercourseName = filterCourse.name,filtercid = filterCourse._id,this.courseId=filterCourse._id);
// filterdep && semesters && filtercid && (localStorage.setItem('courseId',filtercid ),this.chooselevel(year,semesters,filterdep,filterCourse));
// !filterdep && !semesters && filterCourse == undefined ?  (localStorage.removeItem('reportfilters'),localStorage.removeItem('data')) : console.log('All the data is present.');
// (filterdep == undefined || semesters== undefined || filtercid == undefined) ? this.latestreportsData() : console.log('All the data is present.');
// if(filterCourse && this.courseId) return true
// else return false




  //       let courseName:any,departmentName:any,activedep:any,activesem:any,semName:any,yearName:any;
  //       let activeCourse:any = this.coursesList.find((courseData:any) => courseData.coursecode == this.coursecode)
  //       activeCourse != undefined  && (this.courseId = activeCourse._id,
  //       courseName =activeCourse.name,
  //       departmentName =activeCourse.depName)
  //       activeCourse != undefined   &&  (localStorage.setItem('courseId', this.courseId ))
  //       console.log('courseid from the quesry( in the redirection reports)',activeCourse); 
  //       activeCourse != undefined  && departmentName && (activedep = this.departmentList.find((depData:any) => depData.name==departmentName))
  //       activeCourse != undefined  && departmentName && activedep && (activesem = this.semestersList.find((semData:any) => semData.name == activedep.semName))
  //       activeCourse != undefined  && departmentName && activedep &&  activesem && (semName = activesem['name'],yearName=activesem['year'])
  //       activeCourse != undefined  && departmentName && activedep &&  activesem && semName && yearName && this.chooselevel(yearName,semName,departmentName,activeCourse);
  //       (activeCourse == undefined  && departmentName== undefined || activedep== undefined ||  activesem== undefined || semName== undefined || yearName== undefined) ?  (localStorage.removeItem('reportfilters'),localStorage.removeItem('data')) : console.log('All the data is present.')
  
//teacher
    //     filterdep =await this.departmentList.filter(dep=> dep.name == this.department);
  //     filterdep && (filterdepName = filterdep.name,filterdepid=filterdep._id,semid=filterdep.semesterid);
  //     filterdepid && (  semesters = await this.semestersList.filter(sem=> sem._id == semid));
  //     semesters && (year = semesters.year,filtersemName =semesters.name);
  //     filterdepid && (filterCourse= await this.coursesList.filter(course=>course.coursecode == this.coursecode && course.departmentid == filterdepid));  
  //     filterCourse && (filtercourseName = filterCourse.name,filtercid = filterCourse._id);
  //     filtercid && (localStorage.setItem('courseId',filtercid ),this.chooselevel(year,filtersemName,filterdepName,filterCourse));
  //     filterCourse == undefined ?  (localStorage.removeItem('reportfilters'),localStorage.removeItem('data')) : console.log('All the data is present.')
  //  }else if(!this.department && !filterCourse || !semesters || filterCourse== undefined ){


  //other roles
     // filterdep =await this.departmentList.filter(dep=> dep.name == this.department);
    // filterdep && (filterdepName = filterdep.name,filterdepid=filterdep._id,semid=filterdep.semesterid);
    // filterdepid && (  semesters = await this.semestersList.filter(sem=> sem._id == semid && sem.year == this.academicyear));
    // semesters && (year = semesters.year,filtersemName =semesters.name);
    // filterdepid && (filterCourse= await this.coursesList.filter(course=>course.coursecode == this.coursecode && course.departmentid == filterdepid));  
    // filterCourse && (filtercourseName = filterCourse.name,filtercid = filterCourse._id);
    // filtercid && (localStorage.setItem('courseId',filtercid ),this.chooselevel(year,filtersemName,filterdepName,filterCourse));
    // filterCourse == undefined ?  (localStorage.removeItem('reportfilters'),localStorage.removeItem('data')) : console.log('All the data is present.')




