import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionBasedReportTemplateAllParticipantsComponent } from './question-based-report-template-all-participants.component';

describe('QuestionBasedReportTemplateAllParticipantsComponent', () => {
  let component: QuestionBasedReportTemplateAllParticipantsComponent;
  let fixture: ComponentFixture<QuestionBasedReportTemplateAllParticipantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionBasedReportTemplateAllParticipantsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionBasedReportTemplateAllParticipantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
