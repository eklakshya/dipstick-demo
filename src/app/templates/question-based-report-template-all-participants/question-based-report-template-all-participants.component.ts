import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../../pages/services/http.service';
import { ReportsService } from '../../core/services/reports.service'
import { DownloadService } from '../../pages/services/download.service';
import { HexagonLayoutService } from 'src/app/pages/services/hexagon-layout.service';


@Component({
  selector: 'app-question-based-report-template-all-participants',
  templateUrl: './question-based-report-template-all-participants.component.html',
  styleUrls: ['./question-based-report-template-all-participants.component.css']
})
export class QuestionBasedReportTemplateAllParticipantsComponent implements OnInit {
  courseId: string;
  data:any;
  viewBoxes:string[];
  activeTestid:string;
  activeTest:any;
  @Input() accessType : string;
  @Input() reportType : string;
  @Input() url : string;
  courseList: Array < Object >;
  coursename: string;
  reportDetails: any = {};
  maxPretestAttempts: number = 5;
  maxPosttestAttempts: number = 1;
  paths ='';
  testslength=0;
  APIURL = 'api/report/webview/all?id=';
  reportConfigurations:any[] = [];
  isReportVisible : Boolean;
  errorAlertDisplayed = false;
  reports:String = "reports";
  maxPageCount = 1;
  page = 1;
  pagenationLoader:boolean=true;
  r1:boolean=true;
  courseDetails;
  kk:any ={"maxPageCount":1,"testArrays":[{"status":true,"publish":true,"_id":"62fa4941d5d1c04d5bad07d1","name":"Post Test-1_15-8-22","type":"posttest"},{"status":true,"publish":true,"_id":"63286c6fd5d1c04d5bcd7eea","name":"Post Test -2","type":"posttest"}],"scores":[{"62fa4941d5d1c04d5bad07d1":[{"rightAns":2,"numOfAppearence":5,"score":40,"qnno":"12"},{"rightAns":3,"numOfAppearence":4,"score":75,"qnno":"13"},{"rightAns":3,"numOfAppearence":4,"score":75,"qnno":"14"},{"rightAns":3,"numOfAppearence":3,"score":100,"qnno":"16"},{"rightAns":1,"numOfAppearence":5,"score":20,"qnno":"18"},{"rightAns":5,"numOfAppearence":6,"score":83.33333333333333,"qnno":"3"},{"rightAns":4,"numOfAppearence":5,"score":80,"qnno":"4"},{"rightAns":3,"numOfAppearence":3,"score":66.66666666666667,"qnno":"6"},{"rightAns":4,"numOfAppearence":5,"score":80,"qnno":"7"},{"rightAns":4,"numOfAppearence":7,"score":57.142857142857146,"qnno":"9"},{"rightAns":0,"numOfAppearence":4,"score":0,"qnno":"10"},{"rightAns":2,"numOfAppearence":3,"score":66.66666666666667,"qnno":"17"},{"rightAns":3,"numOfAppearence":3,"score":100,"qnno":"19"},{"rightAns":3,"numOfAppearence":6,"score":50,"qnno":"2"},{"rightAns":2,"numOfAppearence":4,"score":50,"qnno":"20"},{"rightAns":3,"numOfAppearence":4,"score":75,"qnno":"5"},{"rightAns":2,"numOfAppearence":4,"score":50,"qnno":"11"},{"rightAns":2,"numOfAppearence":3,"score":66.66666666666667,"qnno":"15"},{"rightAns":1,"numOfAppearence":2,"score":50,"qnno":"1"}]},{"63286c6fd5d1c04d5bcd7eea":[{"rightAns":4,"numOfAppearence":4,"score":100,"qnno":"1"},{"rightAns":0,"numOfAppearence":4,"score":0,"qnno":"13"},{"rightAns":2,"numOfAppearence":4,"score":50,"qnno":"11"},{"rightAns":3,"numOfAppearence":3,"score":100,"qnno":"19"},{"rightAns":1,"numOfAppearence":6,"score":16.666666666666668,"qnno":"2"},{"rightAns":2,"numOfAppearence":5,"score":40,"qnno":"20"},{"rightAns":3,"numOfAppearence":4,"score":75,"qnno":"6"},{"rightAns":1,"numOfAppearence":3,"score":33.333333333333336,"qnno":"3"},{"rightAns":2,"numOfAppearence":3,"score":66.66666666666667,"qnno":"4"},{"rightAns":2,"numOfAppearence":4,"score":50,"qnno":"8"},{"rightAns":2,"numOfAppearence":2,"score":100,"qnno":"14"},{"rightAns":5,"numOfAppearence":5,"score":100,"qnno":"16"},{"rightAns":3,"numOfAppearence":4,"score":75,"qnno":"17"},{"rightAns":2,"numOfAppearence":2,"score":100,"qnno":"15"},{"rightAns":6,"numOfAppearence":6,"score":100,"qnno":"5"},{"rightAns":4,"numOfAppearence":4,"score":100,"qnno":"10"},{"rightAns":3,"numOfAppearence":3,"score":100,"qnno":"18"},{"rightAns":3,"numOfAppearence":3,"score":100,"qnno":"7"},{"rightAns":0,"numOfAppearence":1,"score":0,"qnno":"9"}]}],"rowDistributions":[[3,4,5,4,3],[3,4,5,4,3]],"headerCoordinates":[[{"x":352,"y":120}],[{"x":352,"y":120}]],"course":{"_id":"62fa4941d5d1c04d5bad07ce","coursecode":"15EMEE403_2022-2023","name":"Design & Analysis of Experiments"},"hexagonSvgPaths":[["M 284 160 L 318 140 L 352 160 L 352 200 L 318 220 L 284 200 Z","M 352 160 L 386 140 L 420 160 L 420 200 L 386 220 L 352 200 Z","M 420 160 L 454 140 L 488 160 L 488 200 L 454 220 L 420 200 Z","M 250 220 L 284 200 L 318 220 L 318 260 L 284 280 L 250 260 Z","M 318 220 L 352 200 L 386 220 L 386 260 L 352 280 L 318 260 Z","M 386 220 L 420 200 L 454 220 L 454 260 L 420 280 L 386 260 Z","M 454 220 L 488 200 L 522 220 L 522 260 L 488 280 L 454 260 Z","M 216 280 L 250 260 L 284 280 L 284 320 L 250 340 L 216 320 Z","M 284 280 L 318 260 L 352 280 L 352 320 L 318 340 L 284 320 Z","M 352 280 L 386 260 L 420 280 L 420 320 L 386 340 L 352 320 Z","M 420 280 L 454 260 L 488 280 L 488 320 L 454 340 L 420 320 Z","M 488 280 L 522 260 L 556 280 L 556 320 L 522 340 L 488 320 Z","M 250 340 L 284 320 L 318 340 L 318 380 L 284 400 L 250 380 Z","M 318 340 L 352 320 L 386 340 L 386 380 L 352 400 L 318 380 Z","M 386 340 L 420 320 L 454 340 L 454 380 L 420 400 L 386 380 Z","M 454 340 L 488 320 L 522 340 L 522 380 L 488 400 L 454 380 Z","M 284 400 L 318 380 L 352 400 L 352 440 L 318 460 L 284 440 Z","M 352 400 L 386 380 L 420 400 L 420 440 L 386 460 L 352 440 Z","M 420 400 L 454 380 L 488 400 L 488 440 L 454 460 L 420 440 Z"],["M 284 160 L 318 140 L 352 160 L 352 200 L 318 220 L 284 200 Z","M 352 160 L 386 140 L 420 160 L 420 200 L 386 220 L 352 200 Z","M 420 160 L 454 140 L 488 160 L 488 200 L 454 220 L 420 200 Z","M 250 220 L 284 200 L 318 220 L 318 260 L 284 280 L 250 260 Z","M 318 220 L 352 200 L 386 220 L 386 260 L 352 280 L 318 260 Z","M 386 220 L 420 200 L 454 220 L 454 260 L 420 280 L 386 260 Z","M 454 220 L 488 200 L 522 220 L 522 260 L 488 280 L 454 260 Z","M 216 280 L 250 260 L 284 280 L 284 320 L 250 340 L 216 320 Z","M 284 280 L 318 260 L 352 280 L 352 320 L 318 340 L 284 320 Z","M 352 280 L 386 260 L 420 280 L 420 320 L 386 340 L 352 320 Z","M 420 280 L 454 260 L 488 280 L 488 320 L 454 340 L 420 320 Z","M 488 280 L 522 260 L 556 280 L 556 320 L 522 340 L 488 320 Z","M 250 340 L 284 320 L 318 340 L 318 380 L 284 400 L 250 380 Z","M 318 340 L 352 320 L 386 340 L 386 380 L 352 400 L 318 380 Z","M 386 340 L 420 320 L 454 340 L 454 380 L 420 400 L 386 380 Z","M 454 340 L 488 320 L 522 340 L 522 380 L 488 400 L 454 380 Z","M 284 400 L 318 380 L 352 400 L 352 440 L 318 460 L 284 440 Z","M 352 400 L 386 380 L 420 400 L 420 440 L 386 460 L 352 440 Z","M 420 400 L 454 380 L 488 400 L 488 440 L 454 460 L 420 440 Z"]],"hexagonColors":[["#ffc000","#fffd87","#fffd87","#00b050","#ff0000","#92d050","#fffd87","#fffd87","#fffd87","#ffc000","#ff0000","#fffd87","#00b050","#ffc000","#ffc000","#fffd87","#ffc000","#fffd87","#ffc000"],["#00b050","#ff0000","#ffc000","#00b050","#ff0000","#ffc000","#fffd87","#ff0000","#fffd87","#ffc000","#00b050","#00b050","#fffd87","#00b050","#00b050","#00b050","#00b050","#00b050","#ff0000"]],"centerX":[[314,382,450,280,348,416,484,246,314,382,450,518,280,348,416,484,314,382,450],[314,382,450,280,348,416,484,246,314,382,450,518,280,348,416,484,314,382,450]],"centerY":[[176,176,176,236,236,236,236,296,296,296,296,296,356,356,356,356,416,416,416],[176,176,176,236,236,236,236,296,296,296,296,296,356,356,356,356,416,416,416]],"viewBoxes":["216 140 340 320","216 140 340 320"]}
  userRole: string = '';

  
  // data:any;
  // url : any = 'api/report/subconceptwise/allParticipants?courseid='+this.courseId;
  // constructor(public reportService:ReportsService) {
  //   this.data  = localStorage.getItem('data');
  //  }
  constructor(private downloads: DownloadService,  private http :HttpClient,  private modalService: NgbModal,public api:ApiService, public reportService:ReportsService,private hexagonLayout: HexagonLayoutService) {
    

  }
  
  ngOnInit(): void {
    this.userRole = localStorage.getItem('userrole') || "";

    this.courseId = localStorage.getItem('courseId');
    this.reportService.loadReportDisplayConfigurations((reportConfigurations:any)=>{
      this.reportConfigurations = reportConfigurations;
      console.log(this.reportConfigurations);
    });
    this.data  = localStorage.getItem('data');
    this.reportType == 'testwise' ? this.testwiseNgOnInit() : ( this.reportType == 'questionwise' ? this.questionWiseV1() : this.getReport());
  }

  testwiseNgOnInit() {
      this.api.getCoursesById()
          .subscribe((response) => {
            this.courseList = response['courses'];
          },
          (error:any) => {                           
            console.log('error caught for api',error);
            (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
          });
          this.courseId = localStorage.getItem('courseId');
          this.APIURL = 'api/report/webview/all/v1?id=' + this.courseId
          this.data  = localStorage.getItem('data');
          // this.http.get(this.api.BASEURL + 'api/report/webview/all?id='+this.courseId, this.api.headers())
          // .subscribe((response) => {
          //   this.reportDetails = response;
          //   console.log(response)
          //   // this.loads()
          //   let testslength = this.reportDetails['testArray'].length;
          //   this.api.noResp(testslength);
          //  this.paths =this.reportDetails.viewBox;
          // },
          // (error:any) => {                           
          //   console.log('error caught for api',error);
          //   (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
          // }); 
  }

  loads(){
    return setTimeout(function(){
      this.r1 = (this.reportDetails['testArray'].length) ? false : true;
      document.getElementById('loading').style.display = 'none';
    }, 2000);
  }

  getReport(){
    // alert("getReport")
    // this.http.get(this.api.BASEURL + 'api/report/' + this.reportType + '/allParticipants?courseid='+this.courseId,this.api.headers())
    // .subscribe((response) => { 
    //   // setTimeout(function(){ 
    //   //   document.getElementById('loading').style.display = 'none';
    //   // }, 500);
    //   this.reportDetails = response
    //   console.log(response)
    //   let testslength = this.reportDetails['testArrays'].length;
    //   this.api.noResp(testslength);
    // },
    // (error:any) => {                           
    //   console.log('error caught for api',error);
    //   (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    // });
  }

 questionWise() {
    this.http.get(this.api.BASEURL + 'api/report/questionwise/allParticipants/v1?courseid=' + this.courseId + '&page=' + this.page, this.api.headers())
      .subscribe((response: any) => {
        setTimeout(function () {
          document.getElementById('loading').style.display = 'none';
        }, 500);
        const centerX = this.reportDetails.centerX || [];
        const centerY = this.reportDetails.centerY || [];
        const headerCoordinates = this.reportDetails.headerCoordinates || [];
        const hexagonColors = this.reportDetails.hexagonColors || [];
        const hexagonSvgPaths = this.reportDetails.hexagonSvgPaths || [];
        const rowDistributions = this.reportDetails.rowDistributions || [];
        const testArrays = this.reportDetails.testArrays || [];
        const scores = this.reportDetails.scores || [];
        const overAllScores = [...scores, ...response.scores]
        const testids:any = overAllScores.map(s => Object?.keys(s))
        const scoresAll = overAllScores.map((a, i) => {
          return a[testids[i]]?.sort((b,c) => +b?.qnno?.match(/\d/g)?.join('') > +c?.qnno?.match(/\d/g)?.join('') ? 1 : -1)
        })
        // console.log({scoresAll})
        // console.log({testids, scores,  scoresAll : scoresAll?.map(a => a?.sort((b,c) => b.qnno.match(/\d/g) > c.qnno.match(/\d/g) ? 1 : -1))})
        const viewBoxes = this.reportDetails.viewBoxes || [];
        this.reportDetails.centerX = [...centerX, ...response.centerX]
        this.reportDetails.centerY = [...centerY, ...response.centerY]
        this.reportDetails.headerCoordinates = [...headerCoordinates, ...response.headerCoordinates]
        this.reportDetails.hexagonColors = [...hexagonColors, ...response.hexagonColors]
        this.reportDetails.hexagonSvgPaths = [...hexagonSvgPaths, ...response.hexagonSvgPaths]
        this.reportDetails.rowDistributions = [...rowDistributions, ...response.rowDistributions]
        this.reportDetails.scores = [...scores, ...response.scores]
        // console.log(this.reportDetails.scores, "i am meghana")                                                                                                                                                                                 ")
        this.reportDetails.viewBoxes = [...viewBoxes, ...response.viewBoxes]
        this.reportDetails.testArrays = [...testArrays, ...response.testArrays]
        //this.reportDetails.testArrays = response.testArrays;
        this.reportDetails.course = response.course;
        this.maxPageCount = response.maxPageCount;
        
        
        // console.log(this.reportDetails.scores)                                                                                                                                                                              ")
        // console.log("------------")
        // console.log(this.reportDetails.hexagonColors, "kyaaakaru")

        // console.log(this.reportDetails.scores, "mgnnnnnnaik")



        
        
        // const indexNumbers: number[] = [];
        // for (let i = 0; i < this.reportDetails.scores.length; i++) {
        //   indexNumbers.push(i);
        //   let qns = this.reportDetails.scores[indexNumbers[i]];

        //   const qnsdetails = Object.keys(qns)
        //   // console.log(qns, "questionsliiiiiiiiiiiio", qnsdetails)

        //   const qnnoArray: string[] = qns[qnsdetails[0]].map(item => item.qnno);
        //   const slicedQnnoArray: string[] = qnnoArray.slice(0,);
        //   console.log(slicedQnnoArray, "before sort")

        //   slicedQnnoArray.sort((a, b) => {
        //     console.log(a,"satyalog", parseInt(a.slice(0).replace(/[^0-9]/g, ""), 10))
        //     console.log(b,"satyalogggg")
        //     const numA =  parseInt(a.slice(0).replace(/[^0-9]/g, ""), 10) || parseInt(a.slice(1), 10) || parseInt(a.slice(2), 10) || parseInt(a.slice(3), 10) ;
        //     const numB =   parseInt(b.slice(0).replace(/[^0-9]/g, ""), 10) || parseInt(b.slice(1), 10) || parseInt(b.slice(2), 10) || parseInt(b.slice(3), 10); 
        //     // console.log(numA," console.log console.log")
        //     // console.log(numB," logggggggggggg")
        //     if (numA < numB) {
        //       return -1;
        //     } else if (numA > numB) {
        //       return 1;
        //     } else {
        //       return 0;
        //     }
            
        //   });
        //   // console.log(slicedQnnoArray, "------------sorted")
        //   const sortedqnno = slicedQnnoArray;

        //   const updatedQns = [];
        //   const sortedResponseArray = sortedqnno.map(qnno => {
        //     const qnsArray = Object.values(qns)[0] as Array<{ rightAns: number, numOfAppearence: number, score: number, qnno: string }>;
        //     const foundObject = qnsArray.find(obj => obj.qnno === qnno);
        //     // console.log(foundObject, "eeeennnndddd");
        //     updatedQns.push(foundObject);

        //   });
        //   // console.log(updatedQns, "qqqqqqqqqqqqqqqqqqqqqqqqqqqq")
        //   // qns = updatedQns;
        //   qns[qnsdetails[0]] = updatedQns;

        //   // console.log(qns[qnsdetails[0]], "rrrrrrrrrrrrrrrrrr")      
        // }





        // console.log("questionwise function execution")
        let testslength = this.reportDetails['testArrays'].length;
        this.api.noResp(testslength);
        if (++this.page <= this.maxPageCount) {
          this.questionWise()
        } else {
          this.pagenationLoader = false;
          document.getElementById('loading1').style.display = 'none'
        }
      },
        (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        });
  
  }
  questionWiseV1(){
    this.courseDetails = JSON.parse(localStorage.getItem('generalreportsfilters')).activeCourse;
    this.http.get(this.api.BASEURL + 'api/report/questionwise/allParticipants/v1?courseid=' + this.courseId + '&page=' + this.page, this.api.headers())
      .subscribe((resp: any) => {
        const response = resp.response;
        this.reportDetails = []
        response.forEach((cluster:any, clusterIndex:number) => {

        this.reportDetails[clusterIndex] = {}
      const rowDistribution = this.hexagonLayout.getRowDistribution(cluster.scores.length);
      const reportDimensionsPost = this.hexagonLayout.calcDimensions(cluster.scores.length, 1, rowDistribution);
      const hexStructurePost: any = this.hexagonLayout.initHexStructure(rowDistribution, reportDimensionsPost.marginX, reportDimensionsPost.marginY, reportDimensionsPost.hexWidth, reportDimensionsPost.hexRadius);
      const hexCentersXPost: any = [];
      const hexCentersYPost: any = [];
      const hexPathsPost: any = [];
      const hexColorsPost: any = [];
      const displayScores: any = []
      const { marginX, marginY, hiveHeight, hiveWidth, hexWidth } = reportDimensionsPost;
      this.reportDetails[clusterIndex].topics = cluster;
      this.reportDetails[clusterIndex].viewBox = `${cluster.scores.length > 2 ? (marginX - ((Math.floor(Object.keys(hexStructurePost).length / 2) - 1) * (hexWidth / 2))) : marginX + (hexWidth / 2)} ${marginY} ${hiveWidth} ${hiveHeight}`;
      
      for (let a = 0; a < 1; a++) {
        let testNumber: number = 0;
        const rowsInStruct = Object.keys(hexStructurePost);
  
        rowsInStruct.forEach((row, index) => {
          const testsInRow = Object.keys(hexStructurePost[`row${index + 1}`]);
          testsInRow.forEach(test => {
            try {
              testNumber += 1;
              
              const scoreIndex = cluster.scores[testNumber-1]['score'];
              const score = (scoreIndex != null && scoreIndex > -1 ) ? scoreIndex : -1;
              // console.log({ activeTestid, currentIndex, score })
              displayScores[testNumber - 1] = score;
              hexCentersXPost.push(this.hexagonLayout.getHexCenterX(hexStructurePost, a, index + 1, testNumber, reportDimensionsPost) - 4);
              hexCentersYPost.push(this.hexagonLayout.getHexCenterY(hexStructurePost, a, index + 1, testNumber, reportDimensionsPost) - 4);
              hexPathsPost.push(this.hexagonLayout.getHexPath(hexStructurePost, testNumber, index + 1, a, reportDimensionsPost));
              hexColorsPost.push(this.hexagonLayout.getHexColor(score));
            } catch (error) {
            }
          });
        });
      }
      this.reportDetails[clusterIndex].centerX = hexCentersXPost;
      this.reportDetails[clusterIndex].hexagonSvgPaths = hexPathsPost;
      this.reportDetails[clusterIndex].hexagonColors = hexColorsPost;
      this.reportDetails[clusterIndex].centerY = hexCentersYPost;
      this.reportDetails[clusterIndex].rowDistribution = rowDistribution;
      this.reportDetails[clusterIndex].scores = displayScores;
      console.log(this.reportDetails)
        })
        })
  }

  getScoreColor(score1:any ) {
    const score =Math.round(score1);
    if (score > 90) {
      return '#00b050';
    } else if (score > 80 && score <= 90 ) {
      return '#92d050';
    } else if (score > 60 && score <= 80 ) {
      return '#fffd87';
    } else if (score >= 40 && score <= 60 ) {
      return '#ffc000';
    } else if (score >= 0 && score < 40) {
      return '#ff0000';
    } 
    else {
      return 'white';
    }
  }

  testsList:any;
  scoresList:any = [];
  // questionWiseV1(){
  //   this.http.get(this.api.BASEURL + 'api/report/questionwise/getTestsList?courseid='+this.courseId + '&page=' + this.page ,this.api.headers())
  //   .subscribe((response:any) => {
  //     // console.log(response)
  //     this.testsList = response?.tests;
  //     let i = 0;
  //     const loadScoresList = () => {
  //       i<this.testsList.length && this.http.get(this.api.BASEURL + 'api/report/questionwise/allParticipants/v1?testid='+ this.testsList[i]?._id.toString() + '&page=' + this.page ,this.api.headers())
  //       .subscribe((response:any) => {
  //         i += response.err ? 0 : 1;
  //         this.reportType == 'questionwise' && loadScoresList();
  //       });
  //     }
  //     loadScoresList()
  //   })
  // }

  viewBoxesData(key){
    return this.reportDetails.viewBoxes[key];
  }
  
  open(modal) {
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', size:'xl', backdrop: 'static',keyboard: false});
  }
  getSvgWidth(rowDistribution:any[]){
    return rowDistribution[Math.ceil(rowDistribution.length%2==1?rowDistribution.length/2:rowDistribution.length)-1]
  }
  getSvgheight(rowDistribution:any[],svgPathLength:number){
    var totalHex = 0;
    var totalHexHeight = 0;
    var hexHeights:number[] = rowDistribution.map((rowdata, number)=>{
    totalHex = totalHex < svgPathLength ? totalHex + rowdata : (totalHex)
    totalHexHeight = totalHex < svgPathLength ? number + 1 : totalHexHeight ;
    return totalHexHeight + 1
    })
    var lastelement = hexHeights[hexHeights.length-1]
    return lastelement
  }
  getNegativeMargin(lastelement:number,rowDistributionLength:number){
    return (rowDistributionLength - lastelement) * -50
  }
  setCourse(event: Event) {
    this.coursename = (<HTMLInputElement>event.target).value;
  }
  getHexText(testIndex:number) {
    return 'T' + (testIndex + 1) // + '-' + this.reportDetails.testArray[testIndex].score.toFixed(2)
  }
  renameTopic(str:string, find:string){
    return this.reportService.replaceTopic(str, find)
  }
  checkReportVisibility(){
    this.isReportVisible = Boolean(this.checkReportVisible());
    // if(!this.isReportVisible && !this.errorAlertDisplayed){
    //   this.errorAlertDisplayed = true;
    //   alert("this report is not available " + this.isReportVisible)
    // }
    return this.isReportVisible;
  }
  checkReportVisible(){
    if(window.location.pathname.search(/report-1/i) > -1){
      return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-testwise-allParticipants')?.isVisible;
    } else if(window.location.pathname.search(/report-3/i) > -1){
      return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-conceptwise-allParticipants')?.isVisible;
    } else if(window.location.pathname.search(/report-5/i) > -1){
      return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-subconceptwise-allParticipants')?.isVisible;
    } else if(window.location.pathname.search(/report-7/i) > -1){
      return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-questionwise-allParticipants')?.isVisible;
    }
  }
  checkPopupVisibility(popupType:any){
    return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == popupType)?.isVisible;
  }
}
