import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

 import { QuestionBasedReportTemplateAllParticipantsComponent } from './question-based-report-template-all-participants.component';
import { CoreModule } from 'src/app/core/core.module';
import { popUpTableModule } from 'src/app/commmon-modules/popUptable.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { TestsModule } from 'src/app/commmon-modules/Tests.module';
import { ConceptwisedivisionWiseModule } from 'src/app/pages/concept-wise/conceptwisedivisionwise/conceptwisedivision-wise.module';
import { ConceptWiseModule } from 'src/app/commmon-modules/concept-wise.module';
import { DownloadGroupReportsModule } from 'src/app/shared/download-group-reports/download-group-reports.module';


@NgModule({
  declarations: [QuestionBasedReportTemplateAllParticipantsComponent],
  exports : [QuestionBasedReportTemplateAllParticipantsComponent],
   imports: [
    CommonModule,
     CoreModule,
     popUpTableModule,
     NgbModule,
     FormsModule,
     TestsModule,
     ConceptWiseModule,
     DownloadGroupReportsModule
   ]
})
export class QuestionBasedReportTemplateAllParticipantsModule { }
