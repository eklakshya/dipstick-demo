import { Component, OnInit, Input, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../../pages/services/http.service';
import { ReportsService } from '../../core/services/reports.service'
import { constant } from 'src/app/shared/constant';
import { Color, Label } from 'ng2-charts';
import { ChartsService } from '../../shared/service/chats.service';
import { HexagonLayoutService } from 'src/app/pages/services/hexagon-layout.service';

@Component({
  selector: 'app-question-based-report-template-division-wise',
  templateUrl: './question-based-report-template-division-wise.component.html',
  styleUrls: ['./question-based-report-template-division-wise.component.css']
})
export class QuestionBasedReportTemplateDivisionWiseComponent implements OnInit {
  courseId: string;
  viewBoxes: string[];
  view: any
  data: any;
  activeDivision: string;
  activeTest: any;
  @Input() accessType: string;
  @Input() reportType: string;
  @Input() url: string;

  courseList: Array<Object>;
  lineChartLabels = this.charts.lineChartLabels;
  lineChartOptions = this.charts.lineChartOptions;
  lineChartColors: Color[] = this.charts.lineChartColors;
  lineChartLegend = this.charts.lineChartLegend;
  lineChartPlugins = [];
  lineChartType = this.charts.lineChartType;
  lineChartpointBackgroundColor = '#ffffff';

  object: Object;
  viewBox: string;
  viewBoxValue: any
  iconPath: any;
  paths: any;
  lineChartColorsList: Color[][];
  legend: any = constant.legendImage;
  chartData: any;
  responceData: any = {};
  page = 1;
  maxPageCount = 1;
  reports: String = "reports";
  popUp: string = "popUp"
  reportConfigurations: any[] = [];
  isReportVisible: boolean;

  testwiseResults: any;
  courseDetails: any;
  tableData: any;
  constructor(private http: HttpClient, private modalService: NgbModal, public api: ApiService, public reportService: ReportsService, public charts: ChartsService, private hexagonLayout: HexagonLayoutService) {
  }
  reportDetails: any;
  pagenationLoader: boolean = true;
  //  resp:any;
  divisionWiseTests: any;
  userRole: string = '';

  ngOnInit(): void {
    this.userRole = localStorage.getItem('userrole') || "";

    this.courseId = localStorage.getItem('courseId');
    this.reportService.loadReportDisplayConfigurations((reportConfigurations: any) => {
      this.reportConfigurations = reportConfigurations;
    });
    this.data = localStorage.getItem('data');
    this.reportType == 'testwise' ? this.testwiseNgOnInit() : (this.reportType == 'questionwise' ? this.questionWiseV1() : this.tests());
    this.reportDetails = this.reportType == 'testwise' ? [] : {}
  }

  questionWise2(page: number) {
    this.http.get(this.api.BASEURL + 'api/report/' + this.reportType + '/divisionWise?courseid=' + this.courseId + '&page=' + page, this.api.headers())
      .subscribe((response: any) => {
        setTimeout(function () {
          document.getElementById('loading').style.display = 'none';
        }, 500);
        // console.log(response)
        // this.reportDetails = {}
        this.reportDetails.course = response?.course;
        const organisedquestions = response?.divisions.map(d => d?.testsData[d?.test?._id].sort((b, c) => +b.qnno.match(/\d/g).join('') > +c.qnno.match(/\d/g).join('') ? 1 : -1))
        console.log({ organisedquestions })
        this.reportDetails.divisions = [...(this.reportDetails.divisions || []), ...response.divisions]
        this.maxPageCount = response.maxPageCount;

        // console.log(this.reportDetails.divisions, "mgnnnnnnaikgan")
        // const indexNumbers: number[] = [];

        // for (let i = 0; i < this.reportDetails?.divisions?.length; i++) {
        //   indexNumbers.push(i);
        //   let qnso = this.reportDetails.divisions[indexNumbers[i]];
        //   // console.log(qnso, "qwwwwwwwwwwwwww")
        //   let qns = qnso.testsData;
        //   // console.log(qns, "jjjjjjjjj")
        //   const qnsdetails = Object.keys(qns)
        //   // console.log(qns, "questionsliiiiiiiiiiiio", qnsdetails)

        //   const qnnoArray: string[] = qns[qnsdetails[0]].map(item => item.qnno);
        //   const slicedQnnoArray: string[] = qnnoArray.slice(0,);
        //   // console.log(slicedQnnoArray, "before sort")

        //   slicedQnnoArray.sort((a, b) => {
        //     const numA =  parseInt(a.slice(0).replace(/[^0-9]/g, ""), 10) || parseInt(a.slice(1), 10) || parseInt(a.slice(2), 10) || parseInt(a.slice(3), 10) ;
        //     const numB =   parseInt(b.slice(0).replace(/[^0-9]/g, ""), 10) || parseInt(b.slice(1), 10) || parseInt(b.slice(2), 10) || parseInt(b.slice(3), 10); 
        //     // const numA: number = parseInt(a.slice(2), 10);
        //     // const numB: number = parseInt(b.slice(2), 10);
        //     // parseInt(a.split("-")[1], 10) || parseInt(a, 10)

        //     if (numA < numB) {
        //       return -1;
        //     } else if (numA > numB) {
        //       return 1;
        //     } else {
        //       return 0;
        //     }
        //   });
        //   // console.log(slicedQnnoArray, "------------sorted")
        //   // ...................................
        //   const sortedqnno = slicedQnnoArray;
        //   const updatedQns = [];
        //   const sortedResponseArray = sortedqnno.map(qnno => {
        //     const qnsArray = Object.values(qns)[0] as Array<{ rightAns: number, numOfAppearence: number, score: number, qnno: string }>;
        //     const foundObject = qnsArray.find(obj => obj.qnno === qnno);
        //     // console.log(foundObject, "eeeennnndddd");
        //     updatedQns.push(foundObject);

        //   });
        //   // console.log(updatedQns, "qqqqqqqqqqqqqqqqqqqqqqqqqqqq")
        //   // qns = updatedQns;
        //   qns[qnsdetails[0]] = updatedQns;

        //   // console.log(qns, "rrrrrrrrrrrrrrrrrr")
        // }



        let testslength = this.reportDetails['divisions'].length;
        this.api.noResp(testslength);
        if (page < response.maxPageCount && (window.location.pathname.search(/report-8/i) > -1)) {
          this.questionWise2(page + 1)
        } else {
          this.pagenationLoader = false;
          document.getElementById('loading1').style.display = 'none'
        }
      });
  }

  testsList = [];
  testsWithDivisions = [];

  questionWiseV1() {
    this.http.get(this.api.BASEURL + 'api/report/' + this.reportType + '/getTestsList?courseid=' + this.courseId, this.api.headers())
      .subscribe((response: any) => {
        this.reportDetails = [];
        this.testsList = response.tests;
        this.testsWithDivisions = response.testsWithDivisions;
        this.courseDetails = JSON.parse(localStorage.getItem('generalreportsfilters'))?.activeCourse;
        response.testsWithDivisions.forEach((testWithDivision: any, testWithDivisionIndex: number) => {
          const { _id: { division, testid } } = testWithDivision;
          const name = response?.tests?.find(t => t._id.toString() == testid)?.name;
          this.getQuestionWiseCluster({ testid, division, name }, testWithDivisionIndex);
        })
      })
  }
  getQuestionWiseCluster(metadata: any, testWithDivisionIndex: number) {
    const { testid, division } = metadata;
    this.http.get(this.api.BASEURL + 'api/report/' + this.reportType + '/divisionWise/v1?testid=' + testid + '&divisionBuffer=' + btoa(division), this.api.headers())
      .subscribe((response: any) => {
        this.reportDetails[testWithDivisionIndex] = {}
        const scoresList = response?.response;
        const rowDistribution = this.hexagonLayout.getRowDistribution(scoresList.length)

        const reportDimensionsPost = this.hexagonLayout.calcDimensions(scoresList.length, 1, rowDistribution);
        const hexStructurePost: any = this.hexagonLayout.initHexStructure(rowDistribution, reportDimensionsPost.marginX, reportDimensionsPost.marginY, reportDimensionsPost.hexWidth, reportDimensionsPost.hexRadius);

        const hexCentersXPost: any = [];
        const hexCentersYPost: any = [];
        const hexPathsPost: any = [];
        const hexColorsPost: any = [];
        const displayScores: any = []
        const { marginX, marginY, hiveHeight, hiveWidth, hexWidth } = reportDimensionsPost;
        this.reportDetails[testWithDivisionIndex].topics = scoresList;
        this.reportDetails[testWithDivisionIndex].viewBox = `${scoresList.length > 2 ? (marginX - ((Math.floor(Object.keys(hexStructurePost).length / 2) - 1) * (hexWidth / 2))) : marginX + (hexWidth / 2)} ${marginY} ${hiveWidth} ${hiveHeight}`;
        // const testIds = cluster.map(c => c.testid)
        for (let a = 0; a < 1; a++) {
          let testNumber: number = 0;
          const rowsInStruct = Object.keys(hexStructurePost);

          rowsInStruct.forEach((row, index) => {
            const testsInRow = Object.keys(hexStructurePost[`row${index + 1}`]);
            testsInRow.forEach(test => {
              try {
                testNumber += 1;
                const score = scoresList[testNumber - 1]['score'] != null ? scoresList[testNumber - 1]['score'] : -1;
                displayScores[testNumber - 1] = score;
                hexCentersXPost.push(this.hexagonLayout.getHexCenterX(hexStructurePost, a, index + 1, testNumber, reportDimensionsPost) - 4);
                hexCentersYPost.push(this.hexagonLayout.getHexCenterY(hexStructurePost, a, index + 1, testNumber, reportDimensionsPost) - 4);
                hexPathsPost.push(this.hexagonLayout.getHexPath(hexStructurePost, testNumber, index + 1, a, reportDimensionsPost));
                hexColorsPost.push(this.hexagonLayout.getHexColor(score));
              } catch (error) {

              }
            });
          });
        }
        this.reportDetails[testWithDivisionIndex].centerX = hexCentersXPost;
        this.reportDetails[testWithDivisionIndex].hexagonSvgPaths = hexPathsPost;
        this.reportDetails[testWithDivisionIndex].hexagonColors = hexColorsPost;
        this.reportDetails[testWithDivisionIndex].centerY = hexCentersYPost;
        this.reportDetails[testWithDivisionIndex].rowDistribution = rowDistribution;
        this.reportDetails[testWithDivisionIndex].scores = displayScores;
        this.reportDetails[testWithDivisionIndex].metadata = metadata;
      });
  }

  getScoreColor(score1: any) {
    const score = Math.round(score1);
    // console.log(score,"scorescorescore")
    if (score > 90) {
      return '#00b050';
    } else if (score > 80 && score <= 90.5) {
      return '#92d050';
    } else if (score > 60 && score <= 80) {
      return '#fffd87';
    } else if (score >= 40 && score <= 60) {
      return '#ffc000';
    } else if (score >= 0 && score < 40) {
      return '#ff0000';
    }
    else {
      return 'white';
    }
  }

  //it's before lay loading implimentations
  testwiseNgOnInit1(): void {
    this.api.getCoursesById().subscribe((response) => {
      this.courseList = response['courses'];
    });
    this.courseId = localStorage.getItem('courseId');
    this.data = localStorage.getItem('data');
    this.http.get(this.api.BASEURL + 'api/report/webview/division-wise?id=' + this.courseId, this.api.headers())
      .subscribe((response: any) => {
        this.reportDetails = response;

        let testslength = this.reportDetails['testArrays'].length;
        this.api.noResp(testslength);
        // this.paths = this.reportDetails.viewBox[0];
        // alert(this.paths);
      });

  }

  //------------------lazy loading for report-2----------------------------------------------------------------
  testwiseNgOnInit() {
    this.api.getCoursesById().subscribe((response) => {
      this.courseList = response['courses'];
    });
    this.courseId = localStorage.getItem('courseId');
    this.data = localStorage.getItem('data');
    // this.http.get(this.api.BASEURL + 'api/report/webview/division-wise?id='+this.courseId + '&page=' + this.page,this.api.headers())
    // .subscribe((response:any) => {
    //   //this.reportDetails =response;
    //   setTimeout(function(){ 
    //     document.getElementById('loading').style.display = 'none';
    //   }, 500);
    //   const centerX = this.reportDetails.centerX || [];
    //   const centerY = this.reportDetails.centerY || [];
    //   const headerCoordinates = this.reportDetails.headerCoordinates || [];
    //   const hexagonColors = this.reportDetails.hexagonColors || [];
    //   const hexagonSvgPaths = this.reportDetails.hexagonSvgPaths || [];
    //   const rowDistributions = this.reportDetails.rowDistributions || [];
    //   const tests = this.reportDetails.tests || [];
    //   const divisions = this.reportDetails.divisions || [];
    //   const testArrays = this.reportDetails.testArrays || [];
    //   const viewBoxes = this.reportDetails.viewBoxes || [];
    //   this.reportDetails.centerX = [ ...centerX, ...response.centerX]
    //   this.reportDetails.centerY = [ ...centerY, ...response.centerY]
    //   this.reportDetails.headerCoordinates = [...headerCoordinates, ...response.headerCoordinates]
    //   this.reportDetails.hexagonColors = [...hexagonColors, ...response.hexagonColors, ]
    //   this.reportDetails.hexagonSvgPaths = [...hexagonSvgPaths, ...response.hexagonSvgPaths, ]
    //   this.reportDetails.rowDistributions = [...rowDistributions, ...response.rowDistributions, ]
    //   this.reportDetails.testArrays = [...testArrays, ...response.testArrays, ]
    //   this.reportDetails.viewBoxes = [...viewBoxes, ...response.viewBoxes, ]
    //   this.reportDetails.testArrays = [...testArrays, ...response.testArrays, ]
    //   this.reportDetails.tests =response.tests
    //    this.reportDetails.course = response.course;
    //    this.reportDetails.divisions = response.divisions

    //   this.maxPageCount = response.maxPageCount;
    //   console.log("questionwise function execution", )
    //   let testslength = this.reportDetails['testArrays'].length;
    //   this.api.noResp(testslength);
    //   if(++this.page <= this.maxPageCount){
    //     this.testwiseNgOnInit()
    //   }else{
    //   this.pagenationLoader = false;
    //   document.getElementById('loading1').style.display = 'none'
    //   }
    // },
    // (error:any) => {                           
    //   console.log('error caught for api',error);
    //   (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    // });
    this.testWiseReports()
  }
















  tests() {
    // this.http.get(this.api.BASEURL + 'api/report/' + this.reportType + '/divisionWise?courseid='+this.courseId,this.api.headers())
    // .subscribe((response) => { 
    //   setTimeout(function(){ 
    //     document.getElementById('loading').style.display = 'none';
    //   }, 100);
    //   //  this.resp =response['divisions'];
    //   this.reportDetails = response
    //   console.log(this.reportDetails)
    //   let testslength = this.reportDetails['divisions'].length;
    //   this.api.noResp(testslength);
    //   const divisionWiseTests = this.reportDetails.divisions.map((divisionData:any, divisionIndex : number) => {
    //     return divisionData.testArray.map((test:any, testIndex:number) => {
    //       const { name, _id } = test;
    //       const { division } = divisionData
    //       return ({ name, _id, division, divisionIndex, testIndex})
    //     })
    //   })
    //   this.divisionWiseTests = Array.prototype.concat(...divisionWiseTests)
    //   console.log(this.divisionWiseTests)
    // });
  }

  questionWise() {
    this.http.get(this.api.BASEURL + 'api/report/' + this.reportType + '/divisionWise?courseid=' + this.courseId + '&page=' + this.page, this.api.headers())
      .subscribe((response: any) => {
        setTimeout(function () {
          document.getElementById('loading').style.display = 'none';
        }, 500);
        this.reportDetails.course = response.course;
        this.reportDetails.divisions = [...this.reportDetails.divisions, ...response.divisions]

        this.maxPageCount = response.maxPageCount;

        this.updateQuestionWiseDom();
      });
  }

  // @HostListener('window:scroll', ['$event']) 
  onScroll(event: any) {
    // console.log(window.pageYOffset)
    //   if ((250 + window.scrollY) >= document.body.scrollHeight) {
    //     // you're at the bottom of the page
    //     console.log("at end")
    // }
    // console.log(( (document.body.scrollHeight > 800) , ((250 + window.scrollY) >= document.body.scrollHeight) ), ( "page : " + this.page + "maxPageCount : " + this.maxPageCount))
    // if (( (document.body.scrollHeight > 800) || ((250 + window.scrollY) >= document.body.scrollHeight) )&& (this.page < this.maxPageCount)) {
    //   this.page++;
    //   this.questionWise()
    // }
    this.reportType == 'questionwise' && this.updateDom()

  }

  updateDom() {
    // alert(this.maxPageCount);
    // alert(this.page++ < this.maxPageCount);
    // console.log({ 'maxPageCount': this.maxPageCount, 'page': this.page });
    // console.log(this.page++ < this.maxPageCount);
    if (this.page++ < this.maxPageCount) {
      this.questionWise()
      document.getElementById('loading1').style.display = 'none'
    } else {
      document.getElementById('loading1').style.display = 'none'
      this.pagenationLoader = false;
    }
  }
  onScroll2(event: any) {
    console.log("onScroll2")
  }

  updateQuestionWiseDom() {
    // this.resp =this.reportDetails['divisions'];
    // console.log(this.reportDetails)
    let testslength = this.reportDetails['divisions'].length;
    this.api.noResp(testslength);
    const divisionWiseTests = this.reportDetails.divisions.map((divisionData: any, divisionIndex: number) => {
      return divisionData.testArray.map((test: any, testIndex: number) => {
        const { name, _id } = test;
        const { division } = divisionData
        return ({ name, _id, division, divisionIndex, testIndex })
      })
    })
    this.divisionWiseTests = Array.prototype.concat(...divisionWiseTests)
    // console.log(this.divisionWiseTests)
    // this.maxPageCount = this.reportDetails.maxPageCount;
    this.updateDom()
  }
  getSvgWidth(rowDistribution: any[]) {
    return rowDistribution[Math.ceil(rowDistribution.length % 2 == 1 ? rowDistribution.length / 2 : rowDistribution.length) - 1]
  }

  getSvgheight(rowDistribution: any[], svgPathLength: number) {
    var totalHex = 0;
    var totalHexHeight = 0;
    var hexHeights: number[] = rowDistribution.map((rowdata, number) => {
      totalHex = totalHex < svgPathLength ? totalHex + rowdata : (totalHex)
      totalHexHeight = totalHex < svgPathLength ? number + 1 : totalHexHeight;
      return totalHexHeight + 1
    })
    var lastelement = hexHeights[hexHeights.length - 1]
    return lastelement
  }

  getNegativeMargin(lastelement: number, rowDistributionLength: number) {
    return (rowDistributionLength - lastelement) * -50
  }
  getTestCountOfRowDistributionCount(rowDistribution: number[]) {
    return rowDistribution.reduce((a, b) => a + b, 0) > 29
  }

  //  test wise reports functions

  getNumberOfHexagons(rowDistribution: number[]) {
    // console.log(rowDistribution)
    return rowDistribution.reduce((a, b) => a + b, 0)
  }

  getHexText(testid: string) {
    // console.log(divisionIndex)
    const testIndex = this.tableData.findIndex(t => t._id == testid)
    // console.log({testid, testIndex}, this.tableData)
    return 'T' + (testIndex + 1) // + '-' + this.reportDetails.testArrays[divisionIndex][testIndex].score.toFixed(2)
  }



  jsonData(i: any) {
    this.chartData = this.responceData.responseFormat[i];
    return this.chartData
  }

  getScore(divisionIndex: number, testIndex: number) {
    const score = this.reportDetails[divisionIndex].topics[testIndex].percentage;
    return score.toFixed() == '-1' ? '' : ((score == 100 ? 100 : score.toFixed(2)) + '%')
  }
  checkReportVisibility() {
    this.isReportVisible = Boolean(this.checkReportVisible());
    // alert(this.isReportVisible);
    return this.isReportVisible;
  }
  checkReportVisible() {
    if (window.location.pathname.search(/report-2/i) > -1) {
      return this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-testwise-divisionWise')?.isVisible;
    } else if (window.location.pathname.search(/report-4/i) > -1) {
      return this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-conceptwise-divisionWise')?.isVisible;
    } else if (window.location.pathname.search(/report-6/i) > -1) {
      return this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-subconceptwise-divisionWise')?.isVisible;
    } else if (window.location.pathname.search(/report-8/i) > -1) {
      return this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-questionwise-divisionWise')?.isVisible;
    }
  }
  checkPopupVisibility(popupType: any) {
    return this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == popupType)?.isVisible;
  }
  testWiseReports() {
    this.reportDetails = [];
    this.http.get(this.api.BASEURL + 'api/report/webview/division-wise/v1?id=' + this.courseId, this.api.headers())
      .subscribe((response: any) => {
        this.courseDetails = JSON.parse(localStorage.getItem('generalreportsfilters'))?.activeCourse;
        this.testwiseResults = response?.response;
        this.tableData = response?.tableData;
        const tableData = response?.tableData;
        this.testwiseResults.forEach((t, i) => this.getClusterLayout(t?.scores, i, tableData));
      })
  }

  getClusterLayout(cluster: any, clusterIndex: number, testsList: any[]) {
    // this.reportDetails = [];
    this.reportDetails[clusterIndex] = {}
    const rowDistribution = this.hexagonLayout.getRowDistribution(testsList.length);
    const reportDimensionsPost = this.hexagonLayout.calcDimensions(testsList.length, 1, rowDistribution);
    const hexStructurePost: any = this.hexagonLayout.initHexStructure(rowDistribution, reportDimensionsPost.marginX, reportDimensionsPost.marginY, reportDimensionsPost.hexWidth, reportDimensionsPost.hexRadius);
    // alert(JSON.stringify(rowDistribution) + JSON.stringify(reportDimensionsPost) + JSON.stringify(hexStructurePost))

    const hexCentersXPost: any = [];
    const hexCentersYPost: any = [];
    const hexPathsPost: any = [];
    const hexColorsPost: any = [];
    const displayScores: any = []
    const { marginX, marginY, hiveHeight, hiveWidth, hexWidth } = reportDimensionsPost;
    this.reportDetails[clusterIndex].topics = cluster;
    this.reportDetails[clusterIndex].viewBox = `${testsList.length > 2 ? (marginX - ((Math.floor(Object.keys(hexStructurePost).length / 2) - 1) * (hexWidth / 2))) : marginX + (hexWidth / 2)} ${marginY} ${hiveWidth} ${hiveHeight}`;
    const testIds = cluster.map(c => c.testid)
    for (let a = 0; a < 1; a++) {
      let testNumber: number = 0;
      const rowsInStruct = Object.keys(hexStructurePost);

      rowsInStruct.forEach((row, index) => {
        const testsInRow = Object.keys(hexStructurePost[`row${index + 1}`]);
        testsInRow.forEach(test => {
          try {
            testNumber += 1;
            const activeTestid = testsList[testNumber - 1]._id
            const currentIndex = testIds.findIndex(t => t == activeTestid)
            const score = currentIndex > -1 ? cluster[currentIndex]['percentage'] : -1;
            displayScores[testNumber - 1] = score;
            hexCentersXPost.push(this.hexagonLayout.getHexCenterX(hexStructurePost, a, index + 1, testNumber, reportDimensionsPost) - 4);
            hexCentersYPost.push(this.hexagonLayout.getHexCenterY(hexStructurePost, a, index + 1, testNumber, reportDimensionsPost) - 4);
            hexPathsPost.push(this.hexagonLayout.getHexPath(hexStructurePost, testNumber, index + 1, a, reportDimensionsPost));
            hexColorsPost.push(this.hexagonLayout.getHexColor(score));
          } catch (error) {

          }
        });
      });
    }
    this.reportDetails[clusterIndex].centerX = hexCentersXPost;
    this.reportDetails[clusterIndex].hexagonSvgPaths = hexPathsPost;
    this.reportDetails[clusterIndex].hexagonColors = hexColorsPost;
    this.reportDetails[clusterIndex].centerY = hexCentersYPost;
    this.reportDetails[clusterIndex].rowDistribution = rowDistribution;
    this.reportDetails[clusterIndex].scores = displayScores;
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl', backdrop: 'static', keyboard: false });
  }



  totalstudents: number[] = [];

  openHistogram(modal, division) {
    this.http.get(this.api.BASEURL + 'api/report/histograms/?courseid=' + this.courseId + '&division=' + division, this.api.headers())
      .subscribe(async (response) => {
        this.responceData = response;
        this.lineChartColorsList = this.responceData.responseFormat.map(() => {
          return JSON.parse(JSON.stringify(this.charts.lineChartColors));
        })

        this.totalstudents = []; 

        this.lineChartColorsList.forEach((lineChartColors: Color[], testIndex) => {
          const scores = this.responceData.responseFormat[testIndex][0].data;

          const sumOfScoresGreaterThanZero = scores
            .filter(score => score > 0)
            .reduce((sum, score) => sum + score, 0);

          this.totalstudents.push(sumOfScoresGreaterThanZero);
          console.log(this.totalstudents[testIndex], "..........mgn")

          console.log(sumOfScoresGreaterThanZero, "sumOfScoresGreaterThanZerosumOfScoresGreaterThanZero")
          this.lineChartColorsList[testIndex][0].pointBackgroundColor = this.responceData.responseFormat[testIndex][0].data.map((score) => {
            let color = score == 0 ? "" : "#FF6C00";
            return color
          })

        });

        this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static', keyboard: false });
      });
  }



  open1(modal) {
    this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', size: 'xl', backdrop: 'static', keyboard: false });
  }

}