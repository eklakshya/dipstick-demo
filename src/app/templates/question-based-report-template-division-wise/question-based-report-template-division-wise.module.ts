import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

 import { QuestionBasedReportTemplateDivisionWiseComponent } from './question-based-report-template-division-wise.component';
import { CoreModule } from 'src/app/core/core.module';
import { popUpTableModule } from 'src/app/commmon-modules/popUptable.module';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ConceptWiseDivModule } from 'src/app/commmon-modules/concept-wise-div.module';
import { DownloadGroupReportsModule } from 'src/app/shared/download-group-reports/download-group-reports.module';


@NgModule({
  declarations: [QuestionBasedReportTemplateDivisionWiseComponent],
  exports : [QuestionBasedReportTemplateDivisionWiseComponent],
   imports: [
    CommonModule,
     CoreModule,
     popUpTableModule,
     ChartsModule,
     NgbModule,
     FormsModule,
     ConceptWiseDivModule,
     DownloadGroupReportsModule
  ]
})
export class QuestionBasedReportTemplateDivisionWiseModule { }
