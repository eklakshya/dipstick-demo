import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionBasedReportTemplateDivisionWiseComponent } from './question-based-report-template-division-wise.component';

describe('QuestionBasedReportTemplateDivisionWiseComponent', () => {
  let component: QuestionBasedReportTemplateDivisionWiseComponent;
  let fixture: ComponentFixture<QuestionBasedReportTemplateDivisionWiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionBasedReportTemplateDivisionWiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionBasedReportTemplateDivisionWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
