import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnAuthorizedComponent } from './un-authorized.component';



@NgModule({
  declarations: [UnAuthorizedComponent],
  exports: [UnAuthorizedComponent],
  imports: [
    CommonModule
  ]
})
export class UnAuthorizedModule { }
