import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-un-authorized',
  templateUrl: './un-authorized.component.html',
  styleUrls: ['./un-authorized.component.css']
})
export class UnAuthorizedComponent implements OnInit {

  constructor( public router: Router) { }
  @ViewChild('myButton') myButton : ElementRef;

  open(){ 
       let el: HTMLElement = this.myButton.nativeElement as HTMLElement;
       console.log("auto clicked")
       this.logout1()
    }
    logout1() {
      localStorage.clear();
      this.router.navigate(["/"]).then(() => {
        location.reload();
      });
    }
  ngOnInit(): void {
  }

}
