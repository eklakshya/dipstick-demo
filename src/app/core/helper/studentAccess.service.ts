import { Injectable } from '@angular/core';
import {CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';

@Injectable()
export class StudentaccessService implements CanActivate {
  
  // you would usually put this in it's own service and not access it directly!
  // this is just for the sake of the demo.
  isloggedIn: boolean = false;
  isheStudent:boolean =false;
  constructor( private router: Router ) {
    this.isloggedIn = localStorage.getItem('user') ? true : false;
    this.isheStudent = localStorage.getItem('userrole') == 'student' ? true : false;
    console.log('-------this.isheStudent--------',this.isheStudent);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.isloggedIn && !this.isheStudent) {
      console.log('---if----this.isloggedIn && !this.isheStudent--------',this.isloggedIn && !this.isheStudent);
      return true;
    } else {
      console.log('---else----this.isloggedIn && !this.isheStudent--------',this.isloggedIn && !this.isheStudent);
      this.router.navigate(['/']);
      return false;
    }
  }
}