import { Injectable } from '@angular/core';
import {CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';

@Injectable()
export class IndexService implements CanActivate {
  
  // you would usually put this in it's own service and not access it directly!
  // this is just for the sake of the demo.
  isloggedIn: boolean = false;
  isAdmin: boolean = false;
  constructor(
    private router: Router
  ) {
    this.isloggedIn = localStorage.getItem('user') ? true : false;
    this.isAdmin = (localStorage.getItem('user') == "admin") || (localStorage.getItem('user') == "faculty") ? true : false;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.isloggedIn && this.isAdmin) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
}