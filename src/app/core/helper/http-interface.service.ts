import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
export enum requestType {
  GET, POST, PUT, DELETE
}

@Injectable({
    providedIn: 'root',
    })
export class HttpInterfaceService {
  urlPrefix = environment;
  constructor(private http: HttpClient) { }

  executeRequest(request, optons?: any) {
    request.URL = this.urlPrefix + request.URL + '?t=' + Date.now();
    if (request.params) {
      request.URL = request.URL + '&' + request.params;
    }
    switch (request.method) {
      case requestType.GET:
        this.get(request.URL, optons).subscribe(
          data => this.onSuccessResponse(data, request.passthroughData, request.delegate), // success path
          error => this.onErrorHandler(error, request.passthroughData, request.delegate) // error path
        );
        break;
      case requestType.POST:
        this.post(request.URL, request.payload).subscribe(
          data => this.onSuccessResponse(data, request.passthroughData, request.delegate), // success path
          error => this.onErrorHandler(error, request.passthroughData, request.delegate) // error path
        );
        break;
      case requestType.PUT:
        this.put(request.URL, request.payload).subscribe(
          data => this.onSuccessResponse(data, request.passthroughData, request.delegate), // success path
          error => this.onErrorHandler(error, request.passthroughData, request.delegate) // error path
        );
        break;
      case requestType.DELETE:
        this.delete(request.URL).subscribe(
          data => this.onSuccessResponse(data, request.passthroughData, request.delegate), // success path
          error => this.onErrorHandler(error, request.passthroughData, request.delegate) // error path
        );
        break;
      default:
        break;
    }
  }
  get(url, optons?: any) {
    return this.http.get(url, optons);
  }
  post(url, payload) {
    return this.http.post(url, payload);
  }
  put(url, payload) {
    return this.http.put(url, payload);
  }
  delete(url) {
    return this.http.delete(url);
  }
  onSuccessResponse(res, passthroughData, delegate) {
    if (delegate && delegate.onSuccessResponse) {
      delegate.onSuccessResponse(res, passthroughData);
    }
  }
  onErrorHandler(error, passthroughData, delegate) {
    if (delegate && delegate.onErrorHandler) {
      delegate.onErrorHandler(error, passthroughData);
    }
  }
}