import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbDisplayComponent } from '../shared/breadcrumb-display/breadcrumb-display.component';
import { FooterComponent } from './layout/footer/footer.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { BreadcrumbComponent } from '../shared/breadcrumb/breadcrumb.component';
import { FilterReportsComponent } from './layout/filter-reports/filter-reports.component';
import { ResizeBorderComponent } from './../shared/resize-border/resize-border.component';



@NgModule({
  declarations: [
    BreadcrumbDisplayComponent,
    SidebarComponent,
    FooterComponent,
    BreadcrumbComponent,
    FilterReportsComponent,
    ResizeBorderComponent
    // UnAuthorizedComponent,
    // AuthenticateComponent,FilterReportsComponent
   ],

  exports: [
    BreadcrumbDisplayComponent,
    SidebarComponent,
    FooterComponent,
    BreadcrumbComponent,
    FilterReportsComponent,
    ResizeBorderComponent
    // UnAuthorizedComponent,
    // AuthenticateComponent 
  ],
  imports: [
    CommonModule,
    // UnAuthorizedModule
  ]
})
export class CoreModule { }
