import { Component, OnInit } from '@angular/core';
 import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Router} from "@angular/router";
import {ApiService} from './../../../pages/services/http.service'

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {
   instituteId:any  
   res:any;
   data:any;
   
  constructor(private http: HttpClient,private api:ApiService, public router: Router, private modalService: NgbModal) { 
  }
  //api/statistics/?instituteid=61dd3d0356ead73895954fa4
  ngOnInit(): void {
    this.instituteId = localStorage.getItem('instituteid');
    this.res = {"success":{"status":"success","Number of success submissions":0,"Number of success redirection":0},"error":{"status":"error","Number of failed submissions":0,"Number of failed redirections":0},"total":{"status":"total","Number of total failed submissions":0,"Number of total submissions":0}}
  }

getAnalytics(){
   this.http.get(this.api.BASEURL + 'api/statistics/?instituteid='+this.instituteId,this.api.headers())
  .subscribe((response) => {
    this.res = response;
    console.log({'res':this.res});
  },(error:any) => {                           
    console.log('error caught for api',error);
    (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
  })
}

  

}
