import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  @Input() sidebarWidth: any;
  screenWidth :any;
  widgetWidth : number;
  constructor() {
    this.screenWidth = screen.width
  }

  ngOnInit(): void {
    console.log(this.sidebarWidth, this.screenWidth)
    this.widgetWidth = this.screenWidth - this.sidebarWidth - 80
  }

}
