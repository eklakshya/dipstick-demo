import { Component, Input, OnInit, ElementRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from '../../../core/services/login.service';
// import { Router, NavigationEnd } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { requestType, HttpInterfaceService } from '../../services/http-interface.service';
import { ActivatedRoute } from '@angular/router';
import { ReportsService } from '../../../core/services/reports.service';
import { ApiService } from 'src/app/pages/services/http.service';
import { ConstantsService } from '../../services/constants.service';
import { environment } from 'src/environments/environment';
import styleObj from '../../../../assets/css/sibarcolor';
import scrolBar from '../../../../assets/css/scrollbar';
import { constant } from 'src/app/shared/constant';

import { Router, Event } from '@angular/router';
import { NavigationStart, NavigationError, NavigationEnd } from '@angular/router';
 
@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css']
})
export class MenusComponent implements OnInit {
  breadCrumbData: any = [];
  @Input() password: string = '';
  isSignedIn = false;
  isloggedIn = false;
  loginStr = 'Login';
  departments = [];
  courseId = false;
  err: any;
  res: any;
  isAdmin = false;
  reports = false;
  showReports: any;
  sidebarWidth: number = 118;
  showReportsList = false;
  hidesideBar: boolean = false;
  studentName: any;
  studentaccess: boolean = false;
  instituteid = "";
  institutes: any;
  // @ViewChild('username') usernameRef: ElementRef;
  logoMinimizedData: any;
  logoData: any;
  version = environment.version;
  updateLogo: boolean = false;
  primaryColor: any = "#33b35a"
  styleElement = null;
  styleElement1 = null;
  logo: any = constant.eklLogo;
  miniLogo: any = constant.eklminiImage;
  attendence: boolean = true;
  accessToken: any;
  hideSignIn: boolean = true;
  showPwd: boolean = true;
  reportConfigurations: any[] = [];
  load: boolean = false;
  apiresponse: any;
  role:any;
  configurationsObject:any = {
    "concept":"Subtopic",
    "concepts":"Subtopic",
    "subconcept":"Concept statement",
    "subconcepts":"Concept statement",
    "test":"Post Test",
    "tests":"Post Tests"
  }
  isAttendancePage = false;
  constructor(private el: ElementRef, public http: HttpClient, private api: ApiService, private report: ReportsService, private route: ActivatedRoute, private modalService: NgbModal, private login: LoginService, public router: Router, private constants: ConstantsService, private routers: Router) {
    this.studentName = (localStorage.getItem("LMS_Student") && localStorage.getItem("userrole")=="student") ? localStorage.getItem("user") : ((localStorage.getItem("userrole")=="dipstickstudent" ) ? this.capitalize(localStorage.getItem('LMS_Student')) : this.capitalize(localStorage.getItem('user')))
    this.hidesideBar = (sessionStorage.getItem('dataset') || localStorage.getItem("userrole")=="dipstickstudent" ) ? true : false;
    console.log({"this.hidesideBar":this.hidesideBar ,"fsdfsd":sessionStorage.getItem('dataset') ,"fasdfasdf":localStorage.getItem("user")=="dipstickstudent"})
    console.log('session', sessionStorage.getItem("session") == 'lms');
    //this.api.rotate();
    if (sessionStorage.getItem("session") == 'lms' && sessionStorage.getItem('sessionTime') == 'true') {
      this.hideSignIn = true;
    } else if (sessionStorage.getItem("session") == 'lms') {
      this.hideSignIn = false;
    } else {
      this.hideSignIn = true;
    }
    // localStorage.getItem('lms') ?'' : alert('afsdfasdfasd');

    this.loadConfigurations()

    if (localStorage.getItem('user')) {
      this.isSignedIn = login.getValue();
      this.loginStr = localStorage.getItem("LMS_Student") ? this.capitalize(localStorage.getItem("user")) : this.capitalize(localStorage.getItem('user'));
      this.http.get(api.BASEURL + 'apigetStudent', this.api.headers())
        .subscribe((response) => {
          this.departments = response['departments'];
        }, (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        });
      this.courseId = this.report.setcsiState();
      let self = this;
      router.events.subscribe((val) => {
        if (val instanceof NavigationEnd) {
          this.reportsListDisplay();
          // !this.isStudentReport() && this.getColor();
          this.getColor();
          this.changeLogo();
          if (val.url == '/') {
          }
          else {
            if (val.url.toString().search('/Years') == 0) { this.departments.concat((val.url.replace('/', ''))); }
            this.displaySideBarMenu() && this.loadConfigurations();
          }
          self.breadCrumbData = val.url.split('/');
        }
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
    }
    this.showReports = this.api.displaySideBarMenu();
    //  this.getColor();
  }

  ngOnInit(): void {
    this.studentaccess = localStorage.getItem('d-user') ? true : false;
    this.isloggedIn = localStorage.getItem('user') ? true : false;
    this.accessToken = sessionStorage.getItem('token');
    this.isAttendancePage = Boolean(+sessionStorage.getItem("attendance"));

    // alert( this.isloggedIn);
    localStorage.getItem("userrole") && (this.role = localStorage.getItem("userrole"));
    this.isAdmin = localStorage.getItem('user') == "admin";
    this.reportsListDisplay();
    this.getInstituteidAndPopulate();
    // alert( this.isAdmin);
    (localStorage.getItem("user") != "admin") && this.setInstituteWiseConfigurations();
    this.report.instituteSelected.subscribe((instituteid: any) => {
      this.setInstituteWiseConfigurations()
    })
    //this.api.rotate();
    
  }

  setInstituteWiseConfigurations() {
    this.getColor();
    this.changeLogo();
  }

  async getColor() {
    const instituteid = localStorage.getItem(localStorage.getItem("user") != "admin" ? "instituteid" : (this.isStudentReport() ? 'studentReportsInstituteid' : 'instituteidGroupReports'))// ||test-server =  "61dd3d0356ead73895954fa4" , prod - 6203ac4373d03a4976719052
    let color = null;
    const apiUrl = this.api.BASEURL + 'api/institute/' + instituteid;
    this.http.get(apiUrl, this.api.headers()).subscribe((response: any) => {
      this.institutes = response;
      this.institutes && (color = this.institutes['color'])
      const primaryColor = ((this.displaySideBarMenu() || this.isStudentReport() || (localStorage.getItem("user") != "admin")) && color) ? color : this.primaryColor;
      localStorage.setItem('setColor', primaryColor);
      console.log('primaryColor', this.institutes, primaryColor);
      let style: string = styleObj.replace('--color', primaryColor);
      this.createStyle(style);
      let scroll: string = scrolBar.replace('--bckcolor', primaryColor);
      this.createStyleforscroll(scroll);
      this.institutes && (this.attendence = !((this.institutes['name'] == "kle technological university")));
      this.attendence && console.log(this.attendence);
    }, (error: any) => {
      console.log('error caught for api', error);
      //(error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }

  createStyle(style: string): void {
    if (this.styleElement) {
      this.styleElement.removeChild(this.styleElement.firstChild);
    } else {
      this.styleElement = document.createElement('style');
    }
    this.styleElement.appendChild(document.createTextNode(style));
    this.el.nativeElement.appendChild(this.styleElement);
  }

  createStyleforscroll(style: string): void {
    if (this.styleElement1) {
      this.styleElement1.removeChild(this.styleElement1.firstChild);
    } else {
      this.styleElement1 = document.createElement('style');
    }
    this.styleElement1.appendChild(document.createTextNode(style));
    this.el.nativeElement.appendChild(this.styleElement1);
  }

  open(modal) {
    this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', backdrop: 'static', keyboard: false });
    this.err = '';
  }

  onClickSubmit(data) {
    this.isAdmin = data.userName == "admin" ? true : false;
    let password = btoa(data.password);
    let obj: any = { password, userName: data.userName };
    this.http.post(this.api.BASEURL + 'api/athenticate', obj)
      .subscribe((response) => {
        this.res = response;
        if (this.res.status) {
          this.setInstituteid(this.res.result.instituteid)
          this.isSignedIn = true;
          this.loginStr = (this.res.result.role).toUpperCase();
          localStorage.setItem('userrole', this.res.result.role);
          localStorage.setItem('user', this.res.result.role);
          //this.res.result.role == 'dipstickstudent' ? localStorage.setItem('user', this.res.result.role): localStorage.setItem('user', this.res.result.userName);
          localStorage.setItem('token', this.res.token);
          this.role = this.res.result.role;
          (this.res.result.role == 'dipstickstudent') ? this.studentRedirection(this.res.result.userName,this.res.result.instituteid) : location.reload();
          
        } else {
          this.isSignedIn = false;
          this.loginStr = 'Login';
          this.err = 'Incorrect User Name/ Password. Please try again';
        }
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }

  authenticate(data) {
    const password = btoa(data.password);
    const obj: any = { password, userName: data.userName };
    this.http.post(this.api.BASEURL + 'api/authenticate', obj)
      .subscribe((response:any) => {
        this.res = response;
        // debugger
        if (response?.status) {
          this.setInstituteid(response?.instituteid);
          this.isSignedIn = true;
          this.role = response?.role;
          this.loginStr = (response?.role || "").toUpperCase();
          localStorage.setItem('userrole', response?.role);
          localStorage.setItem('user', response?.role);
          localStorage.setItem('token', response?.token);

          if(response?.role == 'faculty'){
            this.routers.navigate(['site-config'], {
              skipLocationChange: false,
              // queryParams : { role : 'faculty' }
              // queryParams: { courseid: this.apiresponse.courseData['_id'], studentid: this.apiresponse.studentData['_id'], studentName: this.apiresponse.studentData['name']}
            }).then(() => {
              this.load = false;
              location.reload();
            }).catch((error:any) => {
              alert("wrong")
            });
          } else {
            (response?.role == 'dipstickstudent') ? this.studentRedirection(response?.userName,response?.instituteid) : location.reload();
          }
        } else {
          this.isSignedIn = false;
          this.loginStr = 'Login';
          this.err = 'Incorrect User Name/ Password. Please try again';
        }
      }, (error: any) => {
        console.log('error caught for api', error);
        (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
      });
  }


  studentRedirection(usn,insId) {
    localStorage.setItem("LMS_Student",usn);
    this.load = true;
    this.http.get(this.api.BASEURL + `api/studentAccountCreation/getStudentAccessData?usn=${usn}&instituteid=${insId}`)
      .subscribe((response) => {
        let apiData = response;
        this.apiresponse = response['accessData'][0];
        // let instituteid =  this.apiresponse.semesterData[0]['instituteid'];
        //edit storage parts..
        // localStorage.setItem('instituteid',instituteid);
        localStorage.setItem('getcallApi',JSON.stringify(apiData));
        localStorage.setItem("studentReportsActiveCourse", JSON.stringify(this.apiresponse.courseData))
       //localStorage.setItem('token',this.apiresponse.jwtToken)
        this.load = false;
        console.log({ courseid: this.apiresponse.courseData['_id'], studentid: this.apiresponse.studentData['_id'], studentName: this.apiresponse.studentData['name']})
        this.routers.navigate(['integration/reports'], {
          skipLocationChange: false,
          queryParams: { courseid: this.apiresponse.courseData['_id'], studentid: this.apiresponse.studentData['_id'], studentName: this.apiresponse.studentData['name']}
        }).then(() => {
          this.load = false;
          location.reload();
        });
      })
  }


  capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

  displaySideBarMenu() {
    return ((window.location.pathname.search(/report-/i) > -1) || (window.location.pathname.search(/attendance/i) > -1))
  }

  isStudentReport() {
    return ((window.location.pathname.search(/student-report/i) > -1))
  }

  // logout(values){
  //   let log = confirm('Do you want to logout');
  //   if(log){
  //     localStorage.clear();
  //     this.router.navigate(["/"]).then(()=>{
  //       location.reload();
  //     });
  //   }
  // }

  signOut() {
    console.log("Logout functions clicked")
    localStorage.clear();
    sessionStorage.clear();
    this.router.navigate(["/"]).then(() => {
      location.reload();
    });
  }
  reset() {
    this.err = '';
    console.log("reset triggered!")
    // this.usernameRef.nativeElement.focus()
    document.getElementById("userName").focus()
  }
  reportsListDisplay() {
    // console.log(this.courseId, this.displaySideBarMenu())
    this.courseId = this.report.setcsiState();
    // this.showReportsList != (this.courseId && this.displaySideBarMenu()) ? this.changeLogo(this.courseId && this.displaySideBarMenu()) : null;
    this.showReportsList = this.courseId && this.displaySideBarMenu();//((window.location.pathname.search(/report-/i)>-1) || (window.location.pathname.search(/attendance/i)>-1))
    // if (this.showReportsList != (this.courseId && this.displaySideBarMenu())) {
    //   this.checkLogoDisplay(this.showReportsList, (this.courseId && this.displaySideBarMenu()))
    // }
    // this.checkLogoDisplay(this.showReportsList, (this.courseId && this.displaySideBarMenu()))
    // this.showReportsList = this.courseId && this.displaySideBarMenu();

    // let isStudentReport = (window.location.pathname.search(/student-report/i) > -1);
    // if ((this.updateLogo != isStudentReport) ) {
    // this.changeLogo(!isStudentReport);
    //   console.log(isStudentReport);

    // }
    // console.log(isStudentReport);

  }
  changeLogo() {
    // console.log(this.getInstituteid)
    this.displaySideBarMenu() || this.isStudentReport() || (localStorage.getItem("user") != "admin") ? this.setImageData() : this.flushImageData();
  }
  checkLogoDisplay(previousPageState: boolean, nextPageState: boolean) {
    let isGroupReportDisplay = (previousPageState != nextPageState) && nextPageState;
    let isStudentReportDisplay = (window.location.pathname.search(/student-report/i) > -1)
    // this.changeLogo(isGroupReportDisplay)
    console.clear()
    console.log({ isGroupReportDisplay })
  }
  // ngDoCheck() {
  //   this.reportsListDisplay()
  //   // console.log(this.showReportsList)
  //   // this.setImageData()
  // }
  populateConfigurations() {
    if (this.isloggedIn) {
      let configurations = [{ "subconcept": "subconcept" }, { "test": "test" }, { "concept": "concept" }];
      // this.isAdmin && this.setConfigurations(configurations)
      if (!this.isAdmin) {
        const apiUrl = this.api.BASEURL + 'api/configuration?instituteid=' + this.instituteid;
        this.http.get(apiUrl, this.api.headers()).subscribe((response: any) => {
          let configurationArray = response.map((configuration) => {
            let confJSON = {};
            confJSON[configuration.elementtype] = configuration.name;
            return confJSON;
          });
          this.setConfigurations(configurationArray)
        }, (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        });
      }
    }
    this.setImageData()
  }
  setConfigurations(data) {
    localStorage.setItem(this.constants.localStorage.CONFIGURATIONS, JSON.stringify(data));
    
    const result = data.reduce((acc, obj) => {
      for (const [key, value] of Object.entries(obj)) {
        acc[key] = value;
      }
      return acc;
    }, {});
    
    const jsonResult = JSON.stringify(result);
    localStorage.setItem('configurationsObject',jsonResult );
    this.configurationsObject = result;
  }
  setInstituteid(instituteid) {
    localStorage.setItem(this.constants.localStorage.INSTITUTEID, instituteid)
  }
  getInstituteid() {
    return localStorage.getItem(localStorage.getItem("user") != "admin" ? "instituteid" : (this.isStudentReport() ? 'studentReportsInstituteid' : 'instituteidGroupReports'))// localStorage.getItem(this.isStudentReport() ? 'studentReportsInstituteid' : 'instituteidGroupReports');
  }
  getInstituteidAndPopulate() {
    this.instituteid = this.getInstituteid()
    this.populateConfigurations()
  }
  setImageData() {
    this.instituteid = this.getInstituteid()
    console.log("setImageData", this.instituteid);
    let options: any = {
      headers: new HttpHeaders()
        .set('instituteid', this.instituteid)
        .set('role', JSON.stringify(1))
        .set('authorization',JSON.stringify(localStorage.getItem('token'))) 

    }
    this.http.get(this.api.BASEURL + 'api/configuration/file-upload?instituteid=' + this.instituteid, options).subscribe((response: any) => {
      //this.logoData = response // || {name : 'eklakshya.com/dipstick/1637920494326-unnamed.png'} //configurations.find((configuration: any) => (configuration.configurationtype == "image" && configuration.elementtype == "logo"))
      // console.log(this.logoData);
      // alert(JSON.stringify({response}))
      this.logoData = response.find((data: any) => data.elementtype == 'logo');
      this.logoMinimizedData = response.find((data: any) => data.elementtype == 'logominimized');
      // console.log({logoData : this.logoData, logoMinimizedData :this.logoMinimizedData, response})
      // this.setSaveStatus("configurations saved successfully");
      // let logoData = {logoMinimizedData : this.logoMinimizedData, logoData : this.logoData};
      // this.isAdmin && localStorage.setItem(this.constants.localStorage.logoImages, JSON.stringify(logoData))



      const apiUrl = this.api.BASEURL + 'api/configuration?instituteid=' + this.instituteid;
        this.http.get(apiUrl, this.api.headers()).subscribe((response: any) => {
          let configurationArray = response.map((configuration) => {
            let confJSON = {};
            confJSON[configuration.elementtype] = configuration.name;
            return confJSON;
          });
          this.setConfigurations(configurationArray)
        }, (error: any) => {
          console.log('error caught for api', error);
          (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
        });

    }, (error: any) => {
      console.log('error caught for api', error);
      (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
    })
  }
  flushImageData() {
    [this.logoData, this.logoMinimizedData] = [null, null];
  }
  // ngOnChanges(){
  //   console.log("ngOnChanges()");
  // }
  // ngAfterContentInit(){
  //   console.log("ngAfterContentInit()");
  // }
  // ngAfterContentChecked(){
  //   console.log("ngAfterContentChecked()");
  //   this.setImageData()
  // }
  // ngAfterViewInit(){
  //   console.log("ngAfterViewInit()");
  // }
  // ngAfterViewChecked(){
  //   console.log("ngAfterViewChecked()");
  // }
  // ngOnDestroy(){
  //   console.log("ngOnDestroy()");
  // }
  checkReportVisibility(reportType: any) {
    // console.log(reportType, this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == reportType)?.isVisible)
    return this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == reportType)?.isVisible;
  }
  loadConfigurations() {
    this.report.loadReportDisplayConfigurations((reportConfigurations: any) => {
      this.reportConfigurations = reportConfigurations;
    });
  }
}
