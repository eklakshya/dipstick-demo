import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from '../../../core/services/login.service';
import { Router, NavigationEnd } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { requestType, HttpInterfaceService } from '../../services/http-interface.service';
import { ActivatedRoute } from '@angular/router';
import { ReportsService } from '../../../core/services/reports.service';
import {  ApiService } from 'src/app/pages/services/http.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  breadCrumbData: any = [];
  @Input() password: string = '';
  isSignedIn = false;
  isloggedIn = false;
  loginStr = 'Login';
  departments = [];
  courseId=false;
  err:any;
  res:any;
  isAdmin = false;
  reports = false;
  showReports: any;
  constructor(public http :HttpClient,private api:ApiService, private report:ReportsService, private route: ActivatedRoute, private modalService: NgbModal, private login: LoginService, public router: Router, ) {
    if(localStorage.getItem('user')){
      this.isSignedIn = login.getValue();
      this.loginStr = (localStorage.getItem('user')).toUpperCase();;
      this.http.get(api.BASEURL + 'api/departments', this.api.headers())
    .subscribe((response) => {
      this.departments = response['departments'];
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
    this.courseId = this.report.setcsiState();
      let self = this;
      router.events.subscribe((val) => {
        if (val instanceof NavigationEnd) {
          if (val.url == '/') {
          }
          else {
            if (val.url.toString().search('/Years') == 0)
            this.departments.concat((val.url.replace('/', '')));
          }
          self.breadCrumbData = val.url.split('/');
        }
      });
    }
  }

  ngOnInit(): void {
  this.isloggedIn = localStorage.getItem('user') ? true : false;
  this.isAdmin = localStorage.getItem('user') == "admin";
  // console.log(window.location.pathname.search(/report/i))
  }

 


  open(modal) {
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', backdrop: 'static',keyboard: false});
    this.err = '';
  }

  updatePassword(event: Event) {
    this.password = (<HTMLInputElement>event.target).value;
  }
 
  // authenticate() {
  //   if(this.password === 'dipstick') {
  //     this.login.setLoginState(true);
  //     localStorage.setItem('user','dipstick')
  //     location.reload();
  //   }
  //   // this.isSignedIn = this.login.getValue();
  //   // this.loginStr = this.login.getLoginString();
  //   this.isSignedIn = false;
  //   this.loginStr = 'Login';
  //    this.err='LoggedIn failed, Try Again';
  // }

  onClickSubmit(data) {
    this.isAdmin=  data.userName == "admin" ? true : false
    this.http.get(this.api.BASEURL +'api/athenticate/?userName='+data.userName+'&password='+data.password)
    .subscribe((response) => {
      this.res=response;
      if (this.res.status) {
       this.isSignedIn = true;
      this.loginStr = (this.res.result[0].role).toUpperCase();
        localStorage.setItem('user',this.res.result[0].role);
         location.reload();
    }else{
      this.isSignedIn = false;
      this.loginStr = 'Login';
      this.err='LoggedIn failed, Try Again';
    }
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
 }


}

