import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ApiService} from '../../../pages/services/http.service'

@Component({
  selector: 'app-publish',
  templateUrl: './publish.component.html',
  styleUrls: ['./publish.component.css']
})
export class PublishComponent implements OnInit {
testdata:any;
filedata:any;
deletedData:any;
publishData:any;
activeTestIndex:number = -1;
testLen: number;
publishmsg:any;
publish_id:any;
publishStatus:any;

  constructor(private http: HttpClient,private modalService: NgbModal, private api:ApiService) { }

  ngOnInit(): void {
    let role =localStorage.getItem('user');
    role == "admin" ? localStorage.removeItem('instituteid') : console.log({role});
    setTimeout(function(){ 
      document.getElementById('loading').style.display = 'none';
    }, 2000);
    this.tests();
  }
  openSm(content,testId,status) {
    this.modalService.open(content, { size: 'sm' });
  }

  tests(){
    this.http.get(this.api.BASEURL + 'api/fileTest?id=all',this.api.headers())
    .subscribe((response) => {
      this.testdata = response['tests'];
      this.testLen = this.testdata.length;
      document.getElementById('loading').style.display = 'none';
      console.log('testsdata',this.testdata,this.testLen)
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }

  files(id){
    this.http.get(this.api.BASEURL + 'api/fileTest/files?id='+id,this.api.headers())
    .subscribe((response) => {
      this.filedata =response['files'];
      console.log('filesdata',this.filedata)
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }
  
  getformatedDate(createdDate:string){
    return new Date(createdDate).toLocaleString()
  }



  publish(testId,status,name){
    this.publish_id = testId ;
    this.publishStatus = status;
    this.publishmsg = status ?  "Do you want to Unpublish "+name+" ?" :  "Do you want to publish "+name +" ?";
 }


  publishConfirmation(){
    this.http.get(this.api.BASEURL + 'api/fileTest/publish?id='+this.publish_id+'&'+'publish='+this.publishStatus,this.api.headers())
    .subscribe((response) => {
      this.publishData =response['publish'];
      this.tests();
      console.log('publishedData',this.publishData);
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }






  // publish(testId,status,name){
  //   this.publishmsg = status ?  confirm("Do you want to Unpublish "+name+" ?") :  confirm("Do you want to publish "+name +" ?");
  //   let data = status ?  confirm("Do you want to Unpublish "+name+" ?") :  confirm("Do you want to publish "+name +" ?")
  //     if(data){
  //     this.http.get(this.api.BASEURL + 'api/fileTest/publish?id='+testId+'&'+'publish='+status)
  //     .subscribe((response) => {
  //       this.publishData =response['publish'];
  //       this.tests();
  //       console.log('publishedData',this.publishData);
  //     });
  //   }
  // }

 //delete the tests 
 delete(testId,courseId){
  let data = confirm("Do you want to delete this test ?");
  if(data){
    this.http.get(this.api.BASEURL + 'api/fileTest/deleteTest?id='+testId,this.api.headers())
    .subscribe((response) => {
      this.testdata =response['delete'];
      console.log('deletedData',this.deletedData);
      // this.tests(courseId);
      this.files(courseId);
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }
 }
 setActiveTest(testIndex:number){
  //  console.log(testIndex)
  this.activeTestIndex = testIndex;
 }
}
