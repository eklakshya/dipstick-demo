import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../core/services/reports.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  courseId = false;
  constructor(public report:ReportsService) { }

  ngOnInit(): void {
    this.courseId = this.report.setcsiState();
  }

  callsme() {
    this.courseId = this.report.setcsiState();
  }
}
