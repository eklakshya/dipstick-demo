import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { RoutingService } from '../../services/routing.service'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, NavigationEnd } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ConstantsService } from '../../../core/services/constants.service';
import { ApiService } from '../../../pages/services/http.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { concat } from 'rxjs';

@Component({
  selector: 'app-filter-reports',
  templateUrl: './filter-reports.component.html',
  styleUrls: ['./filter-reports.component.scss']
})
export class FilterReportsComponent implements OnInit {
  apiResponce: any
  payload: any;
  activeDepartment: any;
  activeDepartmentId: any;
  activeSemester: any;
  activeSemesterId: any;
  activeYear: any;
  activeCourse: any;
  activeCourseId: any;
  activeCourse1: any;
  breadcrumbData: any;
  courseCode: any;
  studentid: any;
  searchString: any;
  matchedCourses: any;
  //to store the real data
  semestersList: any[] = [];
  yearList: any[] = [];
  departmetnList: any[] = [];
  coursesList: any[] = [];
  studentList: any[] = [];
  searchText: any
  //temp list for view in the filter
  TempsemestersList: any[] = [];
  TempyearList: any[] = [];
  TempdepartmentList: any[] = [];
  TempcoursesList: any[] = [];
  accessToken: any;
  accessData: any;
  isSearchPopupOpen: boolean = false;
  role: any;
  @ViewChild('searchfieldpage') input: ElementRef;
  constructor(public api: ApiService,
    public constants: ConstantsService,
    public routingService: RoutingService,
    private http: HttpClient,
    public router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
  ) {
    this.payload = JSON.parse(localStorage.getItem('payLoad'))
    console.log(this.payload)
    this.accessData = JSON.parse(localStorage.getItem('getcallApi'));
    this.role = localStorage.getItem('userrole');
    // this.coursesList.filter((data)=>{
    //    if(data['coursecode'] == this.payload[0]['coursecode']){
    //      console.log(data['courseid']);
    //    }
    // })
    this.accessToken = sessionStorage.getItem('token');
    console.log('acccesstoken from the session storage', this.accessToken);
    this.role == 'faculty' && this.getFacultyAccessList()
  }

  reportsFilters: any = {
    activeYear: undefined,
    activeSemester: undefined,
    activeDepartment: undefined,
    activeCourse: undefined
  }

  ngOnInit(): void {
    (this.accessToken || this.role == 'dipstickstudent') && this.getFilters();
    let courseInformation: any, activeCourse1: any;
    this.accessToken && (courseInformation = this.payload.find((payloadData: any) => payloadData.coursecode))
    this.accessToken && (activeCourse1 = this.coursesList.find((courseData: any) => courseData.coursecode == courseInformation.coursecode))
    //console.log(courseInformation, courseInformation.coursecode, activeCourse1)
    //console.log('this.coursesList[0].coursecode', this.coursesList[0].coursecode);
  }
  removeDuplicates(arr: any) {
    return new Array(...new Set(...arr))
  }
  getFilters() {
    this.apiResponce = this.accessData;
    console.log('this.apiResponce else', this.apiResponce)
    //listing years
    this.apiResponce.accessData.forEach((data) => {
      if (!this.yearList.includes(data.semesterData.year)) {
        this.yearList.push(data.semesterData.year);
      }
    })
    console.log('==yearList====', this.yearList)

    //listing the semisters
    this.apiResponce.accessData.forEach((data) => {
      if (this.semestersList.findIndex((semData: any) => semData._id == data.semesterData._id) == -1) {
        this.semestersList.push(data.semesterData);
      }
    });
    //this.semestersList && (this.semestersList = this.getUniqueListBy(this.semestersList,'name'));
    console.log('==semestersList====', this.semestersList);

    //departmentList
    this.apiResponce.accessData.forEach((data) => {
      if (this.departmetnList.findIndex((departmentData: any) => departmentData._id == data.departmentData._id) == -1) {
        this.departmetnList.push(data.departmentData);
      }
      // this.departmetnList.push(data.departmentData);
    });
    //this.departmetnList && (this.departmetnList = this.getUniqueListBy(this.departmetnList,'name'));
    console.log('==departmetnList====', this.departmetnList);

    //courseList
    this.apiResponce.accessData.forEach((data) => {
      this.coursesList.push(data.courseData);
    });
    //this.coursesList && (this.coursesList = this.getUniqueListBy(this.departmetnList,'name'));
    console.log('===courseList===', this.coursesList);


    (localStorage.getItem('userrole') == 'student' || localStorage.getItem('userrole') == 'dipstickstudent') && (this.apiResponce.accessData.forEach((data) => {
      this.studentList.push(data.studentData);
    }));

    this.yearList && this.semestersList && this.departmetnList && this.coursesList && this.getStudentFilters();
    // });
  }

  //  getUniqueListBy(arr, key) {
  //   return [...new Map(arr.map(item => [item[key], item])).values()]
  // }

  //year selections
  setNewYear(yearData: any) {
    console.log({ yearData });
    this.activeYear = yearData;
    this.onYearChange();
  }
  onYearChange() {
    this.TempsemestersList = [];
    [this.activeSemester, this.activeSemesterId] = [undefined, undefined];
    this.activeYear && this.setSemesterList()
    this.onSemesterChange()
  }
  setSemesterList() {
    this.TempsemestersList = [...this.semestersList]
    this.TempsemestersList = this.TempsemestersList.filter((sem) => this.activeYear == sem['year']);
    console.log(this.TempsemestersList, 'this.TempsemestersList ')
  }

  //semester selections
  setNewSemester(semesterId: any) {
    this.activeSemesterId = semesterId;
    let activeSemesterData = this.TempsemestersList.find((semesterData: any) => semesterData._id == this.activeSemesterId)
    // console.log({semesterData});
    this.activeSemester = activeSemesterData.name;
    this.onSemesterChange();
  }
  onSemesterChange() {
    [this.activeDepartment, this.activeDepartmentId] = [undefined, undefined];
    this.activeSemesterId && this.setDepartment();
    this.onDepartmentChange()
  }
  setDepartment() {
    this.TempdepartmentList = [...this.departmetnList]
    // console.log({activesemesterid : this.activeSemesterId, TempdepartmentList : this.TempdepartmentList});

    this.TempdepartmentList = this.TempdepartmentList.filter((dep) => this.activeSemesterId == dep['semesterid']);
    console.log(this.TempdepartmentList, 'this.TempdepartmetnList ')
  }

  //department selections
  setNewDepartment(departmentId: any) {
    this.activeDepartmentId = departmentId;
    // console.log({departmentData});
    let departmentData = this.TempdepartmentList.find((department: any) => department._id == departmentId)
    this.activeDepartment = departmentData.name;
    this.onDepartmentChange()
  }
  onDepartmentChange() {
    [this.activeCourse, this.activeCourseId] = [undefined, undefined];
    [this.activeCourse1] = [undefined];
    this.TempcoursesList = [];
    this.activeDepartmentId && this.setcourse();
  }

  //course selections
  setcourse() {
    this.TempcoursesList = [...this.coursesList]
    this.TempcoursesList = this.TempcoursesList.filter((course) => this.activeDepartmentId == course['departmentid']);
  }

  setNewCourse(courseid: any) {
    console.log({ courseid })
    this.activeCourseId = courseid;
    this.activeCourse = this.coursesList.find((coursedata: any) => coursedata._id == courseid)// courseid;
    console.log({ "this.activeCourse": this.activeCourse })

  }
  onCourseChange() {

  }

  //setting the filtered data to the local storage to display on refresh
  setReportFilters() {
    const { activeYear, activeSemester, activeDepartment, activeCourse, activeSemesterId, activeDepartmentId, activeCourseId } = this
    this.reportsFilters = { activeYear, activeSemester, activeDepartment, activeCourse, activeSemesterId, activeDepartmentId, activeCourseId }
    localStorage.setItem(this.constants.localStorage.REPORTSFILTERS, JSON.stringify(this.reportsFilters))
  }
  getStudentFilters() {
    const studentfilterKey = this.constants.localStorage.REPORTSFILTERS;
    const reportfilters = localStorage.getItem(studentfilterKey);
    this.reportsFilters = reportfilters != null ? JSON.parse(reportfilters) : { activeYear: undefined, activeSemester: undefined, activeDepartment: undefined, activeCourse: undefined, activeSemesterId: undefined, activeDepartmentId: undefined, activeCourseId: undefined };
    this.activeYear = this.reportsFilters.activeYear;
    this.activeSemester = this.reportsFilters.activeSemester;
    this.activeDepartment = this.reportsFilters.activeDepartment
    this.activeSemesterId = this.reportsFilters.activeSemesterId;
    this.activeDepartmentId = this.reportsFilters.activeDepartmentId
    this.activeCourse1 = this.reportsFilters.activeCourse.name;
    this.activeCourseId = this.reportsFilters.activeCourseId;

    // this.setDepartmentList();
    this.activeYear && this.setSemesterList();
    this.activeSemesterId && this.setDepartment()
    this.activeDepartmentId && this.setcourse();
  }

  //viewing filtered reports
  filter() {
    localStorage.setItem("studentReportsActiveCourse", JSON.stringify(this.activeCourse));
    console.log({ "this.activeCourseId in the filter search": this.activeCourseId })
    this.activeCourseId && this.reportsChange();
    this.setReportFilters()
    this.chooselevel();
    if ((localStorage.getItem('userrole') == 'student') || (localStorage.getItem('userrole') == 'dipstickstudent')) {
      let studentid = sessionStorage.getItem('studentid');
      let studentName = (localStorage.getItem('userrole') == 'student') ? localStorage.getItem('LMS_Student') : localStorage.getItem('LMS_Student');
      this.getStudentid(this.activeCourseId) && this.router.navigate(['/integration/reports'], {
        skipLocationChange: false,
        queryParams: { courseid: this.activeCourse['_id'], studentid: this.studentid, studentName }
      }).then(() => {
        location.reload();
      })
    } else {
      //alert(this.activeCourseId);
      location.reload();
    }
  }
  getStudentid(courseId: any) {
    let studentInfo: any = this.studentList.filter((data) => data.courseId == courseId);
    studentInfo.length > 0 && (this.studentid = studentInfo[0]['_id']);
    console.log("student info after selecting the course", studentInfo);
    return (this.studentid);
  }
  reportsChange() {
    // const courseid = this.coursesList.filter((data)=>data.name === this.activeCourse);
    console.log('course id after selecting the course in the drop down', this.activeCourse['_id']);
    localStorage.setItem('courseId', this.activeCourse['_id']);
    let instituteid = this.activeCourse['instituteid'];
    instituteid && localStorage.setItem('instituteid', instituteid);
  }

  //for breadcrumb data, not using presently  
  async chooselevel() {
    this.settemp({ name: this.activeCourse['name'], url: "", type: 'course' })
    this.breadcrumbData = 'You are here: reports/ ';
    this.breadcrumbData += this.activeYear + '/' + this.activeSemester + '/' + this.activeDepartment + '/' + this.activeCourse['name']
    // console.log("coursedata", coursedata, this.breadcrumbData);
    localStorage.setItem('data', this.breadcrumbData);
    localStorage.setItem('S_bread', this.breadcrumbData);
  }
  settemp(data: any) {
    this.routingService.setTemparray(data);
  }
  searchMatchedCourses(modal, flag: number, string: any) {
    this.searchString = string.trim();
    !this.isSearchPopupOpen && this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static', keyboard: false });
    this.matchedCourses = [];
    console.log("this.accessData", this.accessData);
    const searchExpression = new RegExp(this.searchString, 'ig');
    this.matchedCourses = this.accessData.accessData.filter((instituteData: any) => ((
      searchExpression.test(instituteData.semesterData.name) || (instituteData.semesterData.name.toLowerCase().includes(this.searchString)) ||
      searchExpression.test(instituteData.semesterData.year) || instituteData.semesterData.year.includes(this.searchString) ||
      searchExpression.test(instituteData.departmentData.name) || (instituteData.departmentData.name.toLowerCase().includes(this.searchString)) ||
      searchExpression.test(instituteData.courseData.name) || (instituteData.courseData.name.toLowerCase().includes(this.searchString)) ||
      searchExpression.test(instituteData.courseData.coursecode) || (instituteData.courseData.coursecode.toLowerCase().includes(this.searchString))
    )));
    this.isSearchPopupOpen = (flag == 1) ? true : false;
    console.log("this.matchedCourses =", this.matchedCourses);
  }

  close(modal) {
    modal.dismiss('Cross click')
    this.isSearchPopupOpen = false;
    this.input.nativeElement.value = this.searchString;
    // this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', backdrop: 'static', keyboard: false });
  }

  getSearchString(string) {
    // this.searchString = string.trim();
    this.searchText = string.trim();
    console.log("string", string)
  }


  setCourseData(i: number, modal: any) {
    modal.dismiss('Cross click')
    this.isSearchPopupOpen = false;
    // this.searchString = '';
    this.activeYear = this.matchedCourses[i].semesterData.year;
    this.activeSemester = this.matchedCourses[i].semesterData.name;
    this.activeDepartment = this.matchedCourses[i].departmentData.name;
    this.activeCourse = this.matchedCourses[i].courseData;
    this.activeSemesterId = this.matchedCourses[i].semesterData['_id']
    this.activeDepartmentId = this.matchedCourses[i].departmentData['_id']
    this.activeCourseId = this.matchedCourses[i].courseData['_id']
    this.close(modal);
    this.filter();
    // const { year, semesterid, semesterName, departmentName, departmentid, courseName, coursecode, courseid } = this.searchedCourseInfo.matchedCourses[filterCourseIndex];
    // [this.activeYear, this.activeSemester, this.activeDepartment ] = [year, {_id : semesterid, name : semesterName}, { _id : departmentid, name : departmentName }];
    // this.chooselevel({ _id : courseid, coursecode, name : courseName })

  }
  getDisplayText(i: number) {
    let semData: any = this.matchedCourses[i].semesterData;
    let depData: any = this.matchedCourses[i].departmentData;
    let courseData: any = this.matchedCourses[i].courseData;
    return ("" + (i + 1) + ". " + semData.year + " / " + semData.name + " / " + depData.name + " / " + courseData.name + " - " + courseData.coursecode).replace(RegExp(this.searchString, 'ig'), (word) => "<b>" + word + "</b>")
  }

  clear() {
    console.log("reset triggered!")
    document.getElementById("searchString").focus()
    //alert("clear");
  }

  facultyAccessList: any;
  facultyAccessYears: any = [];
  facultyAccessDepartments: any = [];
  facultyAccessCourses: any = [];
  facultyAccessActiveSelectors = {
    year: '',
    department: '',
    course: '',
    search: ''
  };
  facultyAccessFilteredList = {
    departments: [],
    courses: []
  }
  filteredCourses = []
  getFacultyAccessList() {
    const facultyAccessInfo = JSON.parse(localStorage.getItem('faculty-access') || '{}');
    const { accessList, courses, departments } = facultyAccessInfo;
    const years = [...new Set(accessList.map((access: any) => access?.year))];
    const activeSelectors = JSON.parse(localStorage.getItem('generalreportsfilters') || '{}');
    const { activeSemester: { year }, activeDepartment, activeCourse } = activeSelectors;
    
    this.facultyAccessActiveSelectors = { year, department: activeDepartment._id, course: activeCourse._id, search: '' };
    this.facultyAccessYears = years;
    this.facultyAccessDepartments = departments;
    this.facultyAccessCourses = courses;
    this.facultyAccessList = accessList;
    this.setYearWiseDepartments(year);
    // alert("getFacultyAccessList " + activeDepartment?._id)
    this.setDepartmentWiseCourses(activeDepartment?._id);
  }

  onAccessYearChange(year: any) {
    this.facultyAccessActiveSelectors.department = undefined;
    this.facultyAccessActiveSelectors.course = undefined;
    this.facultyAccessActiveSelectors.year = year;

    this.facultyAccessFilteredList.departments = [];
    this.facultyAccessFilteredList.courses = [];
    this.setYearWiseDepartments(year);
  }

  setYearWiseDepartments(year: any) {
    this.facultyAccessFilteredList.departments = this.facultyAccessList
      .filter((accessInfo: any) => accessInfo?.year == year)
      .map((accessInfo: any) => accessInfo.departments[0]);

    // console.log("active departments : ", this.facultyAccessFilteredList.departments)
  }
  onAccessDepartmentChange(department: any) {
    this.facultyAccessActiveSelectors.course = undefined;
    this.facultyAccessFilteredList.courses = [];
    this.setDepartmentWiseCourses(department)
    this.facultyAccessActiveSelectors.department = department;
  }
  setDepartmentWiseCourses(department: any) {
    const departmentName = this.facultyAccessDepartments[department];
    // alert("setDepartmentWiseCourses : " + department + departmentName)
    const courses = this.facultyAccessList.filter((accessList:any) => (accessList.departmentName == departmentName) && ( accessList.year == this.facultyAccessActiveSelectors.year)).map((accessList) => accessList.courses);

    // alert(department + " ---- " + departmentName  + " ---- " + courses  + " ---- " +  this.facultyAccessList)
    this.facultyAccessFilteredList.courses = [...new Set([].concat(...courses))]
    // alert(this.facultyAccessFilteredList.courses)
    // this.facultyAccessFilteredList.courses = Object.keys(this.facultyAccessCourses)
    //   .filter((courseid: any) => this.facultyAccessCourses[courseid].departmentid == department)
      // alert(this.facultyAccessFilteredList.courses)
    // .map((courseid:any) => this.facultyAccessCourses[courseid]._id);
  }
  onAccessCourseChange(course) {
    this.facultyAccessActiveSelectors.course = course;
  }
  setActiveFacultyReport() {
    const currentCourseId = localStorage.getItem('courseId');
    if(currentCourseId != this.facultyAccessActiveSelectors.course){
      const activeDepartmentId = this.facultyAccessActiveSelectors.department;
      const activeCourseId = this.facultyAccessActiveSelectors.course;
      const activeCourseInfo = this.facultyAccessCourses[activeCourseId];
  
      const activeSemester = { year: this.facultyAccessActiveSelectors.year };
      const activeDepartment = { _id: activeDepartmentId, name: this.facultyAccessDepartments[activeDepartmentId] }
      const activeCourse = { _id: activeCourseId, name: activeCourseInfo.name, coursecode: activeCourseInfo.coursecode, departmentid: activeCourseInfo.departmentid }
  
      const activeSelectors = { activeCourse, activeDepartment, activeSemester }
      localStorage.setItem('generalreportsfilters', JSON.stringify(activeSelectors));
      localStorage.setItem('courseId', this.facultyAccessActiveSelectors.course);
      location.reload()
    }
  }
  onSearch(modal, isSearchPopupOpen: number, string: any) {
    this.searchString = string.trim();
    !this.isSearchPopupOpen && this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static', keyboard: false });
    this.filteredCourses = [];
    const search = new RegExp(this.searchString, 'ig');

    const departmentWiseCourseList = this.facultyAccessList.map((accessData: any) => {
      const { year, departmentName, courses } = accessData;
      return courses.map((courseId) => {
        // const departmentName = this.facultyAccessDepartments[departmentid]
        return { _id: courseId, departmentName, year, ...this.facultyAccessCourses[courseId] };
      })
    })

    const courseList = [].concat(...departmentWiseCourseList);

    this.filteredCourses = courseList.filter((courseInfo: any) =>
      search.test(courseInfo?.coursecode) ||
      search.test(courseInfo?.departmentName) ||
      search.test(courseInfo?.name) ||
      search.test(courseInfo?.year)
    )
    this.isSearchPopupOpen = Boolean(isSearchPopupOpen);
  }
  getInfoText(i: any) {
    const courseInfo = this.filteredCourses[i];
    return ("" + (i + 1) + ". " + courseInfo.year + " / " + courseInfo.departmentName + " / " + courseInfo.name + " - " + courseInfo.coursecode).replace(RegExp(this.searchString, 'ig'), (txt) => "<b>" + txt + "</b>")
  }
  setActiveCourse(i: number, modal: any) {
    modal.dismiss('Cross click')
    this.isSearchPopupOpen = false;
    const courseInfo = this.filteredCourses[i];
    const activeSemester = { year : courseInfo.year }
    const activeDepartment = { _id : courseInfo.departmentid, name : courseInfo.departmentName };

    const activeCourse = { _id : courseInfo._id, coursecode : courseInfo.coursecode, name : courseInfo.name };
    localStorage.setItem('generalreportsfilters', JSON.stringify( { activeCourse, activeDepartment, activeSemester } ))
    localStorage.setItem('courseId', courseInfo._id);

    this.close(modal);
    location.reload()
  }
}
