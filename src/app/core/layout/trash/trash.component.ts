import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {ApiService} from '../../../pages/services/http.service'
 
@Component({
  selector: 'app-trash',
  templateUrl: './trash.component.html',
  styleUrls: ['./trash.component.css']
})
export class TrashComponent implements OnInit {
  fileslen: number;
  filedata:any;  
    constructor(private http: HttpClient, private api:ApiService ) { }
  
    ngOnInit(): void {
      let role =localStorage.getItem('user');
      role == "admin" ? localStorage.removeItem('instituteid') : console.log({role});
      this.files();
    }
 
    files(){
      this.http.get(this.api.BASEURL + 'api/fileTest/files?id=trash',this.api.headers())
      .subscribe((response) => {
        this.filedata = response['Trashfiles'];
        this.fileslen = this.filedata.length;
        document.getElementById('loading').style.display = 'none';
        console.log('filesdata',this.filedata,this.fileslen)
      },(error:any) => {                           
        console.log('error caught for api',error);
        (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
      });
    }
    
    getformatedDate(createdDate:string){
      return new Date(createdDate).toLocaleString()
    }
 

}
