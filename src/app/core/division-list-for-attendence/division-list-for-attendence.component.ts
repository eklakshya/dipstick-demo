import { Component, OnInit,Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ApiService} from '../../pages/services/http.service'

@Component({
  selector: 'app-division-list-for-attendence',
  templateUrl: './division-list-for-attendence.component.html',
  styleUrls: ['./division-list-for-attendence.component.css']
})
export class DivisionListForAttendenceComponent implements OnInit {

  courseid : string = localStorage.getItem("courseId");
  response : any = { allDivisions : [], addedDivisions : []}
  requestPayload : any = {};
  defaultDivisionStudentCount:number = 100;
  status:number;
  message : string = "";
  @Input() cid: string;
  @Input() cname: string
  constructor(private http :HttpClient,private api:ApiService) {
 
  }

  ngOnInit(): void {
    let url = this.api.BASEURL + 'api/report/attendance/divisionList?courseid='+this.cid;
    this.http.get(url,this.api.headers()).subscribe((response) => {
      this.response = response;
      console.log(response)
      this.response.allDivisions.forEach((divisionName:string) => {
        this.requestPayload[divisionName] = this.getStudentCount(divisionName);
      });
    },
    (error:any) => {                           
       console.log('error caught for api',error);
       (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
     })

  }

  onChange(event:any){
    this.requestPayload[event.target.name] = +event.target.value;
    console.log(this.requestPayload)
    this.status = 0
  }

  onSubmit(){
    // divisionsData
    let url = this.api.BASEURL + 'api/report/attendance/divisionList?courseid='+this.cid;
    this.http.put(url, {divisionsData:this.requestPayload},this.api.headers())
    .subscribe((response:any) => {
      console.log(response)
      this.status = response.status ? 1 : -1
      this.message = response.message
      this.goTop()
    },(error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    }, ()=>{
      // this.status = -1
      // this.message = "Count not saved. Please try again"
      // this.goTop()
    })
    
  }
  getStudentCount(division:string){
    let divisionIndex = this.response.addedDivisions.findIndex(divisionData => divisionData.name == division)
    return divisionIndex>=0?this.response.addedDivisions[divisionIndex].total:this.defaultDivisionStudentCount;
  }
  goTop() {
    document.querySelector(".modal").scrollTo({top:0,behavior:'smooth'});
  }
}
