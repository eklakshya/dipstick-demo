import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DivisionListForAttendenceComponent } from './division-list-for-attendence.component';

 

@NgModule({
  declarations: [DivisionListForAttendenceComponent],
  exports: [DivisionListForAttendenceComponent],
  imports: [CommonModule]
})
export class DivisionListForAttendenceComponentModule { }
