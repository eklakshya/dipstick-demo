import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisionListForAttendenceComponent } from './division-list-for-attendence.component';

describe('DivisionListForAttendenceComponent', () => {
  let component: DivisionListForAttendenceComponent;
  let fixture: ComponentFixture<DivisionListForAttendenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DivisionListForAttendenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionListForAttendenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
