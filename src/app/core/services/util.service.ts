import { Injectable, ViewContainerRef} from '@angular/core';
import { environment} from './../../environments/environment';

export enum Menu {
	PARENT = 'parent',
	EXTERNAL = 'external',
	INTERNAL = 'internal'
}
export enum Pages {
	TEAM = 'team',
}
@Injectable()
export class UtilsService {
	appContainerRef: ViewContainerRef;
	mediaUrlPrefix = environment.mediaUrlPrefix;
	apiEndPoint = environment.api;
	Pages = Pages;
	public appAlerts: any[] = [
	];
	public bradcrumbData:any[] = [];
	constructor() { }
	appAlertsPush(alert){
		this.appAlerts.push(alert);
		setTimeout(() => {
			this.appAlertsDissmiss(alert);	
		}, 5000);
	}
	appAlertsDissmiss(alert){
		this.appAlerts = this.appAlerts.filter((data)=>{
			return data.msg!=alert.msg
		})
	}
	urlResolver(url){
		return this.mediaUrlPrefix+""+url;
	}
}
