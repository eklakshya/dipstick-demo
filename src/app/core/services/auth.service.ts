import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { requestType,HttpInterfaceService } from './http-interface.service';


@Injectable()
export class AuthService {
jwtHelper:any;
loggedIn = false;
  constructor(private http : HttpInterfaceService) {
  	 this.jwtHelper = new JwtHelperService();
  }

  // ...
  public isAuthenticated(): boolean {

    const token = this.getToken();

    // Check whether the token is expired and return
    // true or false
    this.loggedIn = !this.jwtHelper.isTokenExpired(token);
    return this.loggedIn
  }
  public getToken(): string {
    return localStorage.getItem('token');
  }
}