import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {

  constructor() { }
  public localStorage = {
    COURSEID : "courseId",
    USER : "user",
    DATA : "data",
    STUDENTSFILTERS : "studentfilters",
    STUDENTDATA : "studentdata",
    STUDENTREPORTASSETS : "student-report-assets",
    CONFIGURATIONS : "configurations",
    INSTITUTEID : "instituteid",
    logoImages : "logoImages",
    REPORTSFILTERS:"reportfilters",
    GENERALREPOTSFILTERS : "generalreportsfilters",
  }
  public sessionStorage = {
    STUDENTREPORTASSETS : "student-report-assets"
  }
 
}
