import { Injectable } from '@angular/core';
import { breadcrumbData } from './types.service'
@Injectable({
  providedIn: 'root'
})
export class RoutingService {

  constructor() { }
  temparray : breadcrumbData[] = []
  setTemparray(data:any){
    this.temparray.push(data)
    console.log(this.temparray)
  }
  getTemparray(){
    return this.temparray;
  }
}
