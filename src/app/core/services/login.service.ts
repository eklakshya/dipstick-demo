import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private isSignedIn = false;
  private loginStr: string = "Admin";
  private login =localStorage.getItem('user');
  constructor() {}

  setLoginState(isSignedIn: boolean) {
    this.isSignedIn = isSignedIn;
    if(this.login) { 
      this.loginStr = 'Admin';
      this.isSignedIn =true;
    }else{
      this.loginStr = 'login'; 
    }
  }
  isAuthenticated(){
    return this.isSignedIn;
  }
  getValue() {
    return this.isSignedIn ;
  }

  getLoginString() {
    return this.loginStr;
  }
}
