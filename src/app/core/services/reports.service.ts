import { EventEmitter, Injectable, NgZone } from '@angular/core';
import { ConstantsService } from './constants.service'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from 'src/app/pages/services/http.service';
@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  private isciSet = false;
  private isSet = localStorage.getItem('courseId');
  configurations: any;
  activeInstituteId : any;
  reportConfigurations:any[] =  [
    {"reportType":"group-testwise-allParticipants","isVisible":true},
    {"reportType":"group-conceptwise-allParticipants","isVisible":true,},
    {"reportType":"group-subconceptwise-allParticipants","isVisible":true,},
    {"reportType":"group-questionwise-allParticipants","isVisible":true,},
    {"reportType":"group-testwise-divisionWise","isVisible":true,},
    {"reportType":"group-conceptwise-divisionWise","isVisible":true,},
    {"reportType":"group-subconceptwise-divisionWise","isVisible":true,},
    {"reportType":"group-questionwise-divisionWise","isVisible":true,},
    {"reportType":"group-attendance","isVisible":true,},
    {"reportType":"student-testwise","isVisible":true,},
    {"reportType":"student-conceptwise","isVisible":true,},
    {"reportType":"student-subconceptwise","isVisible":true,},
    {"reportType":"group-conceptwise-allParticipants-popup","isVisible":true,"referenceReport":"group-conceptwise-allParticipants",},
    {"reportType":"group-subconceptwise-allParticipants-popup","isVisible":true,"referenceReport":"group-subconceptwise-allParticipants",},
    {"reportType":"group-questionwise-allParticipants-popup","isVisible":true,"referenceReport":"group-questionwise-allParticipants",},
    {"reportType":"group-testwise-divisionWise-popup","isVisible":true,"referenceReport":"group-testwise-divisionWise",},
    {"reportType":"group-conceptwise-divisionWise-popup","isVisible":true,"referenceReport":"group-conceptwise-divisionWise"},
    {"reportType":"group-subconceptwise-divisionWise-popup","isVisible":true,"referenceReport":"group-subconceptwise-divisionWise"},
    {"reportType":"group-questionwise-divisionWise-popup","isVisible":true,"referenceReport":"group-questionwise-divisionWise"},
    {"reportType":"student-conceptwise-popup","isVisible":true,"referenceReport":"student-conceptwise"},
    {"reportType":"student-subconceptwise-popup","isVisible":true,"referenceReport":"student-subconceptwise"}
  ];
  reportTerminologies:any = {};
  instituteSelected = new EventEmitter<any>();
  constructor(private constants: ConstantsService,public http: HttpClient, private api: ApiService, private ngZone : NgZone) {
    // print()
    this.activeInstituteId = localStorage.getItem("instituteid");
  }

  setcsiState() {
    return localStorage.getItem('courseId') ? true : false;//this.isSet ? true : false;
  }
  isInstituteSet() {
    let instituteid = localStorage.getItem(this.constants.localStorage.INSTITUTEID);
    return instituteid ? [true, instituteid] : [false, undefined]
  }
  replaceString(str: string, find: string, replace) {
    var escapedFind = find.replace(/([.*+?^=!:${}()|\[\]\/\\])/gi, "\\$1");
    return str.replace(new RegExp(escapedFind, 'gi'), replace);
  }
  replaceTopic(str: string, find: string) {
    this.configurations = JSON.parse(localStorage.getItem(this.constants.localStorage.CONFIGURATIONS))
    // console.log(this.configurations, find, str)
    const topic = this.configurations.find(configuration => Object.keys(configuration)[0] == find);
    // console.log(topic)
    return this.replaceString(str, find, Object.values(topic)[0])
  }
  // isAuthenticated(){
  //   return this.isSignedIn;
  // }
  // getValue() {
  //   return this.isSignedIn ;
  // }

  // getLoginString() {
  //   return this.loginStr;
  // }


  styleElement2: any = null;
  styleElement3: any = null;
  uline: any = `.uline2{
    border-top: 4px solid --color !important;
    border-top: 4px solid #33b35a;
    padding: 12px 0;
  }`;
  fontColor: any = `
  .active-top-navigation-bar{
    color: --font !important;
    color: #33b35a;
  }`;
  setPrimaryColor() {
    let colorCode = localStorage.getItem('setColor');
    let style: string = this.uline.replace('--color', colorCode);
    this.createStyle(style);
    let font: string = this.fontColor.replace('--font', colorCode);
    this.createfontStyle(font);
    return { styleElement2: this.styleElement2, styleElement3: this.styleElement3 }

  }
  createStyle(style: string): void {
    if (this.styleElement2) {
      this.styleElement2.removeChild(this.styleElement2.firstChild);
    } else {
      this.styleElement2 = document.createElement('style');
    }
    this.styleElement2.appendChild(document.createTextNode(style));
    // this.el.nativeElement.appendChild(this.styleElement2);
  }

  createfontStyle(style: string): void {
    if (this.styleElement3) {
      this.styleElement3.removeChild(this.styleElement3.firstChild);
    } else {
      this.styleElement3 = document.createElement('style');
    }
    this.styleElement3.appendChild(document.createTextNode(style));
    // this.el.nativeElement.appendChild(this.styleElement3);
  }


  loadReportDisplayConfigurations(callback:Function){
    this.activeInstituteId = localStorage.getItem(localStorage.getItem("user") == "admin" ? "instituteidconfigurations" : "instituteid"   );
    const apiUrl = this.api.BASEURL + 'api/reportConfiguration?instituteid=' + this.activeInstituteId;
    this.http.get(apiUrl, this.api.headers()).subscribe((response: any) => {
      this.reportConfigurations = response;
      callback(this.reportConfigurations)
    },(error:any) => {                           
      console.log('error caught for api',error);
      //(error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  }
  loadReportStudentDisplayConfigurations(callback:Function){
    this.activeInstituteId = localStorage.getItem("studentReportsInstituteid");
    const apiUrl = this.api.BASEURL + 'api/reportConfiguration?instituteid=' + this.activeInstituteId;
    this.http.get(apiUrl, this.api.headers()).subscribe((response: any) => {
      this.reportConfigurations = response;
      callback(this.reportConfigurations)
    },(error:any) => {                           
      console.log('error caught for api',error);
      //(error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    })
  }
  downloadGroupReports(){
    this.ngZone.runOutsideAngular(() => {
      console.log("ngZone")
    });
  }
}
