import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TypesService {

  constructor() { }
}
export interface breadcrumbData {
  name : string,
  url : string,
  type: string,
}