import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisionChartComponent } from './division-chart.component';

describe('DivisionChartComponent', () => {
  let component: DivisionChartComponent;
  let fixture: ComponentFixture<DivisionChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DivisionChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
