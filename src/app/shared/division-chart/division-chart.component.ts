import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { ChartsService } from '../../shared/service/chats.service';
import { ApiService } from '../../pages/services/http.service';

@Component({
  selector: 'app-division-chart',
  templateUrl: './division-chart.component.html',
  styleUrls: ['./division-chart.component.css']
})
export class DivisionChartComponent implements OnInit {
chartData:any ;
responceData:any={};
count=0;
  constructor(private api : ApiService,public charts:ChartsService, private modalService: NgbModal,private http:HttpClient) { }
  open(modal) {
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', size:'lg', backdrop: 'static',keyboard: false});
  }
  object:Object;
  ngOnInit(): void {
   this.tests();
  }

  tests(){
    this.http.get(this.api.BASEURL + 'api/report/histograms/test?courseid=60477e139863132984e3c269&division=B',this.api.headers())
    .subscribe((response) => {
      this.responceData =response;
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }

  jsonData(i:any){
    this.chartData =this.responceData.responseFormat[i];
    return this.chartData
  }
  lineChartLabels= this.charts.lineChartLabels;
  lineChartOptions = this.charts.lineChartOptions;
  lineChartColors: Color[] = this.charts.lineChartColors;
  lineChartLegend = this.charts.lineChartLegend;
  lineChartPlugins = [];
  lineChartType = this.charts.lineChartType;
}












// jsonToArray(jsondata:any={}){
//   console.log({jsondata, values:Object.values(jsondata)})
//   return Object.values(jsondata)
// }

//   Testdatasets:ChartDataSets[][] =[
//     [
//       { data: [0,1,2,1,0,2,1,1,1,0,0], label: 'kvl', borderWidth:1},
//     ], [
//       { data: [20, 4, 11, 10, 5, 9], label: 'DE-Sem', borderWidth:1},
//     ], [
//       { data: [12, 34, 11, 10, 15, 9], label: 'kcl', borderWidth:1},
//     ], [
//       { data: [32, 5, 15, 10, 5, 9], label: 'adc', borderWidth:1},
//     ], [
//       { data: [30, 44, 15, 10, 5, 9], label: 'mosphete', borderWidth:1},
//     ]
//   ]
