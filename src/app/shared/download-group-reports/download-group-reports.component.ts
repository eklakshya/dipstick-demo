import { Component, OnInit } from '@angular/core';
import { ReportsService } from 'src/app/core/services/reports.service';
import { ApiService } from 'src/app/pages/services/http.service';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-download-group-reports',
  templateUrl: './download-group-reports.component.html',
  styleUrls: ['./download-group-reports.component.css']
})
export class DownloadGroupReportsComponent implements OnInit {

  isDownLoadGroupReportsBegin = false;
  isPDFDownloaded = false;
  reportConfigurations: any = [];
  clusterJson: any = {};
  clusterJsonV1: any = {};
  downloadStatus: string = ''; // 
  constructor(public http: HttpClient, public api: ApiService, public reportService: ReportsService, private router: Router) {
    // this.reportService.loadReportDisplayConfigurations((reportConfigurations: any) => {
    //   this.reportConfigurations = reportConfigurations;
    //   // this.isDownLoadGroupReportsBegin && this.beginDownload();
    //   this.checkDownloadReport();
    //   // alert("ssss")
    //   // this.loadReports();
    //   // this.loadReportsV1();
    // });
  }

  checkDownloadReport(buttonClick = false) {
    const loadedCourseId = localStorage.getItem("download-group-reports-courseId");
    const courseid = localStorage.getItem('courseId');
    if (loadedCourseId == courseid) {
      this.isDownLoadGroupReportsBegin = Boolean(localStorage.getItem("isDownLoadGroupReportsBegin") || "");
      this.isPDFDownloaded = Boolean(localStorage.getItem("isPDFDownloaded") || "");
      if (this.isDownLoadGroupReportsBegin) {
        this.clusterJsonV1 = JSON.parse(localStorage.getItem('download-group-reports'));
        this.loadReportsV1();
      } else {
        this.clusterJsonV1 = {};
        localStorage.removeItem("isPDFDownloaded");
        localStorage.removeItem('download-group-reports');
        localStorage.removeItem("isDownLoadGroupReportsBegin");
        buttonClick && this.loadReportsV1();
      }
    } else {
      this.clusterJsonV1 = {};
      this.isDownLoadGroupReportsBegin = false;
      this.isPDFDownloaded = false;
      localStorage.removeItem("isPDFDownloaded");
      localStorage.removeItem('download-group-reports');
      localStorage.removeItem("isDownLoadGroupReportsBegin");
      buttonClick && this.loadReportsV1();
    }
  }

  startDownload() {
    const testWiseAllParticipants = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-testwise-allParticipants')?.isVisible ? this.clusterJson['group-testwise-all'].downloaded : true;
    const testWiseDivisionWise = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-testwise-divisionWise')?.isVisible ? this.clusterJson['group-testwise-division-wise'].downloaded : true;
    const conceptWiseAllParticipants = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-conceptwise-allParticipants')?.isVisible ? this.clusterJson['group-conceptwise-all'].downloaded : true;
    const conceptWiseDivisionWise = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-conceptwise-divisionWise')?.isVisible ? this.clusterJson['group-conceptwise-division-wise'].downloaded : true;
    const subconceptWiseAllParticipants = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-subconceptwise-allParticipants')?.isVisible ? this.clusterJson['group-subconceptwise-all'].downloaded : true;
    const subconceptWiseDivisionWise = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-subconceptwise-divisionWise')?.isVisible ? this.clusterJson['group-subconceptwise-division-wise'].downloaded : true;
    const questionWiseAllParticipants = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-questionwise-allParticipants')?.isVisible ? this.clusterJson['group-questionwise-all'].downloaded : true;
    const questionWiseDivisionWise = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-questionwise-divisionWise')?.isVisible ? this.clusterJson['group-questionwise-division-wise'].downloaded : true;

    return (testWiseAllParticipants && testWiseDivisionWise && conceptWiseAllParticipants && conceptWiseDivisionWise && subconceptWiseAllParticipants && subconceptWiseDivisionWise && questionWiseAllParticipants && questionWiseDivisionWise);
  }

  loadReports() {
    this.isDownLoadGroupReportsBegin = true;
    localStorage.setItem('isDownLoadGroupReportsBegin', '1');
    const courseid = localStorage.getItem('courseId');
    localStorage.setItem('download-group-reports-courseId', courseid);
    const { activeCourse = {}, activeDepartment = {}, activeYear = {}, activeSemester = {} } = JSON.parse(localStorage.getItem("generalreportsfilters")) || {};
    const { name, coursecode } = activeCourse;
    const reportTitles = JSON.parse(localStorage.getItem('configurations')) || [];
    this.clusterJson.courseInfo = { name, coursecode, departmentName: activeDepartment?.name, reportTitles, activeYear, semesterName: activeSemester?.name };
    if (this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-testwise-allParticipants')?.isVisible) {
      !this.clusterJson['group-testwise-all']?.downloaded && this.http.get(this.api.BASEURL + 'api/report/webview/all?id=' + courseid, this.api.headers()).subscribe((response: any) => {
        const { centerX, centerY, hexagonSvgPaths, hexagonColors, testArray } = response;
        this.clusterJson['group-testwise-all'] = {}
        this.clusterJson['group-testwise-all'].downloaded = true;
        this.clusterJson['group-testwise-all'].clusters = [{ centerX, centerY, paths: hexagonSvgPaths, colors: hexagonColors, hexagonLabels: testArray.map((t: any, i: number) => `T${i + 1}`), scores: testArray.map((t: any) => t.score || 0) }]
        this.clusterJson['group-testwise-all'].tableData = { headers: ["Serial #", "Test Name", "Average Score", "Number Of Records"], data: [] };
        this.clusterJson['group-testwise-all'].tableData.data = testArray.map((testData: any, i: number) => ["T" + (i + 1), testData?.name || "", (testData?.score || 0).toFixed(2), testData?.count]);
        // console.log(response, this.clusterJson);
        this.bufferReports()
      })
    }
    if (this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-testwise-divisionWise')?.isVisible) {
      let page = 1;
      const clusterJson = { centerX: [], centerY: [], hexagonSvgPaths: [], hexagonColors: [], testArrays: [], tests: [], divisions: [] }
      const divisionWise = () => {
        this.http.get(this.api.BASEURL + 'api/report/webview/division-wise?id=' + courseid + "&page=" + page, this.api.headers()).subscribe((response: any) => {
          const { maxPageCount } = response;
          const { centerX, centerY, hexagonSvgPaths, hexagonColors, testArrays, tests, divisions, } = response;
          clusterJson.centerX = [...clusterJson.centerX, ...centerX]
          clusterJson.centerY = [...clusterJson.centerY, ...centerY]
          clusterJson.hexagonColors = [...clusterJson.hexagonColors, ...hexagonColors]
          clusterJson.hexagonSvgPaths = [...clusterJson.hexagonSvgPaths, ...hexagonSvgPaths]
          clusterJson.testArrays = [...clusterJson.testArrays, ...testArrays]
          clusterJson.tests = [...clusterJson.tests, ...tests]
          clusterJson.divisions = [...clusterJson.divisions, ...divisions]
          if (++page <= maxPageCount) {
            divisionWise()
          } else {
            const { centerX, centerY, hexagonSvgPaths, hexagonColors, testArrays, tests, divisions } = clusterJson;
            this.clusterJson['group-testwise-division-wise'] = {}
            this.clusterJson['group-testwise-division-wise'].downloaded = true;
            this.clusterJson['group-testwise-division-wise'].clusters = hexagonSvgPaths.map((paths: string[], i: number) => {
              return { centerX: centerX[i], centerY: centerY[i], paths, colors: hexagonColors[i], hexagonLabels: tests.map((t: any, j: number) => `T${j + 1}`), scores: testArrays[i].map((t: any) => t.score || 0), clusterName: divisions[i]?._id }
            })
            this.clusterJson['group-testwise-division-wise'].tableData = { headers: ["Serial #", "Test Name"], data: [] };
            this.clusterJson['group-testwise-division-wise'].tableData.data = tests.map((testData: any, i: number) => ["T" + (i + 1), testData?.name || ""]);
            this.bufferReports()
          }
          // console.log(response)
        })
      }
      !this.clusterJson['group-testwise-division-wise']?.downloaded && divisionWise()
    }
    if (this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-conceptwise-allParticipants')?.isVisible) {
      !this.clusterJson['group-conceptwise-all']?.downloaded && this.http.get(this.api.BASEURL + 'api/report/conceptwise/allParticipants?courseid=' + courseid, this.api.headers()).subscribe((response: any) => {
        const { centerX, centerY, hexagonSvgPaths, hexagonColors, allTopics, scores } = response;
        this.clusterJson['group-conceptwise-all'] = {}
        this.clusterJson['group-conceptwise-all'].downloaded = true;
        this.clusterJson['group-conceptwise-all'].clusters = [{ centerX, centerY, paths: hexagonSvgPaths, colors: hexagonColors, hexagonLabels: scores.map((s: any, i: number) => `C${i + 1}`), scores: scores.map((c: any) => c.score || 0) }]
        this.clusterJson['group-conceptwise-all'].tableData = { headers: ["Serial #", "Concept Number  -  Subtopic Name", "Average Score"], data: [] };
        this.clusterJson['group-conceptwise-all'].tableData.data = scores.map((conceptData: any, i: number) => ["C" + (i + 1), conceptData?.conceptnumber + " - " + conceptData?.subtopic?.subtopic, Number.isInteger((conceptData.score || 0)) ? (conceptData.score || 0) : conceptData.score.toFixed(2)]);
        this.bufferReports()
      })
    }

    if (this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-conceptwise-divisionWise')?.isVisible) {
      let page = 1;
      const clusterJson: any = { divisionWiseScores: [], topicssorder: [] }
      const conceptWise = () => {
        this.http.get(this.api.BASEURL + 'api/report/conceptwise/divisionWise?courseid=' + courseid + "&page=" + page, this.api.headers()).subscribe((response: any) => {
          // const {allTopics, course, divisionWiseScores, divisions, topicslisted, topicssorder } = response
          clusterJson.divisionWiseScores = [...clusterJson.divisionWiseScores, ...response.divisionWiseScores]
          clusterJson.topicssorder = [...clusterJson.topicssorder, ...response.topicssorder]
          clusterJson.allTopics = response.allTopics;
          clusterJson.course = response.course;
          clusterJson.divisions = response.divisions;
          clusterJson.topicslisted = response.topicslisted;
          const maxPageCount = response.divisionWiseScores[0].maxPageCount;

          if (++page <= maxPageCount) {
            conceptWise()
          } else {
            const { allTopics, course, divisionWiseScores, divisions, topicslisted, topicssorder } = clusterJson
            const centerX = divisionWiseScores.map((d) => d.centerX);
            const centerY = divisionWiseScores.map((d) => d.centerY);
            const hexagonColors = divisionWiseScores.map((d) => d.hexagonColors);
            const hexagonSvgPaths = divisionWiseScores.map((d) => d.hexagonSvgPaths);
            const scores = divisionWiseScores.map((d) => d.scores);
            console.log(centerX, centerY, hexagonColors, hexagonSvgPaths, scores)
            this.clusterJson['group-conceptwise-division-wise'] = {};
            this.clusterJson['group-conceptwise-division-wise'].downloaded = true;
            const getConceptNumber = (conceptIndex: number, divisionIndex: number) => {
              console.log({ conceptIndex, divisionIndex }, " conceptwise division wise ")
              const topic = divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.subtopic?.subtopic || divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.conceptstatement?.conceptstatement;
              const index = allTopics.findIndex(element => (element._id["conceptstatement"] == topic || element._id["subtopic"] == topic))
              return index
            }
            this.clusterJson['group-conceptwise-division-wise'].clusters = hexagonSvgPaths.map((paths: string[], i: number) => {
              return { centerX: centerX[i], centerY: centerY[i], paths, colors: hexagonColors[i], hexagonLabels: centerX[i].map((t: any, j: number) => `C${getConceptNumber(j, i) + 1}`), scores: centerX[i].map((t: any, j: number) => divisionWiseScores[i].scores[j].score || 0), clusterName: divisions[i] }
            });
            this.clusterJson['group-conceptwise-division-wise'].tableData = { headers: ["Serial #", "Concept Number  -  Subtopic Name"], data: [] };
            this.clusterJson['group-conceptwise-division-wise'].tableData.data = allTopics.map((conceptData: any, i: number) => ["C" + (i + 1), conceptData?.conceptnumber + " - " + conceptData?._id?.subtopic || ""]);
            this.bufferReports()
          }
        })
      }
      !this.clusterJson['group-conceptwise-division-wise']?.downloaded && conceptWise()
    }
    if (this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-subconceptwise-allParticipants')?.isVisible) {
      !this.clusterJson['group-subconceptwise-all']?.downloaded && this.http.get(this.api.BASEURL + 'api/report/subconceptwise/allParticipants?courseid=' + courseid, this.api.headers()).subscribe((response: any) => {
        const { centerX, centerY, hexagonSvgPaths, hexagonColors, allTopics, scores } = response;
        this.clusterJson['group-subconceptwise-all'] = {}
        this.clusterJson['group-subconceptwise-all'].downloaded = true;
        this.clusterJson['group-subconceptwise-all'].clusters = [{ centerX, centerY, paths: hexagonSvgPaths, colors: hexagonColors, hexagonLabels: scores.map((s: any, i: number) => `SC${i + 1}`), scores: scores.map((c: any) => c.score || 0) }]
        this.clusterJson['group-subconceptwise-all'].tableData = { headers: ["Serial #", "Concept Statement", "Average Score"], data: [] };
        this.clusterJson['group-subconceptwise-all'].tableData.data = scores.map((conceptData: any, i: number) => ["SC" + (i + 1), conceptData?.conceptstatement.conceptstatement, Number.isInteger((conceptData.score || 0)) ? (conceptData.score || 0) : conceptData.score.toFixed(2)]);
        this.bufferReports()
      })
    }

    if (this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-subconceptwise-divisionWise')?.isVisible) {
      let page = 1;
      const clusterJson: any = { divisionWiseScores: [], topicssorder: [] }
      const subconceptwise = () => {
        this.http.get(this.api.BASEURL + 'api/report/subconceptwise/divisionWise?courseid=' + courseid + "&page=" + page, this.api.headers()).subscribe((response: any) => {
          // const {allTopics, course, divisionWiseScores, divisions, topicslisted, topicssorder } = response
          clusterJson.divisionWiseScores = [...clusterJson.divisionWiseScores, ...response.divisionWiseScores]
          clusterJson.topicssorder = [...clusterJson.topicssorder, ...response.topicssorder]
          clusterJson.allTopics = response.allTopics;
          clusterJson.course = response.course;
          clusterJson.divisions = response.divisions;
          clusterJson.topicslisted = response.topicslisted;
          const maxPageCount = response.divisionWiseScores[0].maxPageCount;

          if (++page <= maxPageCount) {
            subconceptwise()
          } else {
            const { allTopics, course, divisionWiseScores, divisions, topicslisted, topicssorder } = clusterJson
            const centerX = divisionWiseScores.map((d) => d.centerX);
            const centerY = divisionWiseScores.map((d) => d.centerY);
            const hexagonColors = divisionWiseScores.map((d) => d.hexagonColors);
            const hexagonSvgPaths = divisionWiseScores.map((d) => d.hexagonSvgPaths);
            const scores = divisionWiseScores.map((d) => d.scores);
            this.clusterJson['group-subconceptwise-division-wise'] = {};
            this.clusterJson['group-subconceptwise-division-wise'].downloaded = true;
            const getConceptNumber = (conceptIndex: number, divisionIndex: number) => {
              const topic = divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.subtopic?.subtopic || divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.conceptstatement?.conceptstatement;
              const index = allTopics.findIndex(element => (element._id["conceptstatement"] == topic || element._id["subtopic"] == topic))
              return index
            }
            this.clusterJson['group-subconceptwise-division-wise'].clusters = hexagonSvgPaths.map((paths: string[], i: number) => {
              return { centerX: centerX[i], centerY: centerY[i], paths, colors: hexagonColors[i], hexagonLabels: centerX[i].map((t: any, j: number) => `SC${getConceptNumber(j, i) + 1}`), scores: centerX[i].map((t: any, j: number) => divisionWiseScores[i].scores[j].score || 0), clusterName: divisions[i] }
            });
            this.clusterJson['group-subconceptwise-division-wise'].tableData = { headers: ["Serial #", "Concept Statement"], data: [] };
            this.clusterJson['group-subconceptwise-division-wise'].tableData.data = allTopics.map((conceptData: any, i: number) => ["SC" + (i + 1), conceptData?._id.conceptstatement]);
            this.bufferReports()
          }

        })
      }

      !this.clusterJson['group-subconceptwise-division-wise']?.downloaded && subconceptwise()
    }
    if (this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-questionwise-allParticipants')?.isVisible) {
      let page = 1
      const globalThis: any = this;
      const questionWiseReportDetails: any = {}
      const questionWise = () => {
        this.http.get(this.api.BASEURL + 'api/report/questionwise/allParticipants?courseid=' + courseid + '&page=' + page, this.api.headers())
          .subscribe((response: any) => {
            const centerX = questionWiseReportDetails.centerX || [];
            const centerY = questionWiseReportDetails.centerY || [];
            const headerCoordinates = questionWiseReportDetails.headerCoordinates || [];
            const hexagonColors = questionWiseReportDetails.hexagonColors || [];
            const hexagonSvgPaths = questionWiseReportDetails.hexagonSvgPaths || [];
            const rowDistributions = questionWiseReportDetails.rowDistributions || [];
            const testArrays = questionWiseReportDetails.testArrays || [];
            const scores = questionWiseReportDetails.scores || [];
            const viewBoxes = questionWiseReportDetails.viewBoxes || [];
            questionWiseReportDetails.centerX = [...centerX, ...response.centerX]
            questionWiseReportDetails.centerY = [...centerY, ...response.centerY]
            questionWiseReportDetails.headerCoordinates = [...headerCoordinates, ...response.headerCoordinates]
            questionWiseReportDetails.hexagonColors = [...hexagonColors, ...response.hexagonColors]
            questionWiseReportDetails.hexagonSvgPaths = [...hexagonSvgPaths, ...response.hexagonSvgPaths]
            questionWiseReportDetails.rowDistributions = [...rowDistributions, ...response.rowDistributions]
            questionWiseReportDetails.scores = [...scores, ...response.scores]
            questionWiseReportDetails.viewBoxes = [...viewBoxes, ...response.viewBoxes]
            questionWiseReportDetails.testArrays = [...testArrays, ...response.testArrays]
            //questionWiseReportDetails.testArrays = response.testArrays;
            questionWiseReportDetails.course = response.course;
            const maxPageCount = response.maxPageCount;

            if (++page <= maxPageCount) {
              questionWise()
            } else {
              const { centerX, centerY, hexagonColors, hexagonSvgPaths, scores, testArrays } = questionWiseReportDetails;
              globalThis.clusterJson['group-questionwise-all'] = {};
              globalThis.clusterJson['group-questionwise-all'].downloaded = true;

              globalThis.clusterJson['group-questionwise-all'].clusters = hexagonSvgPaths.map((paths: string[], i: number) => {
                return { centerX: centerX[i], centerY: centerY[i], paths, colors: hexagonColors[i], hexagonLabels: centerX[i].map((t: any, j: number) => `${scores[i][testArrays[i]._id][j].qnno}`), scores: centerX[i].map((t: any, j: number) => scores[i][testArrays[i]._id][j].score || 0), clusterName: testArrays[i].name }
              });
              globalThis.bufferReports()
            }
          },
            (error: any) => {
              console.log('error caught for api', error);
              (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
            });
      }
      !this.clusterJson['group-questionwise-all']?.downloaded && questionWise()
    }
    if (this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-questionwise-divisionWise')?.isVisible) {
      let page = 1
      const divisionWiseReportDetails: any = {};
      divisionWiseReportDetails.divisions = []
      const questionWise = () => {
        this.http.get(this.api.BASEURL + 'api/report/questionwise/divisionWise?courseid=' + courseid + '&page=' + page, this.api.headers())
          .subscribe((response: any) => {
            divisionWiseReportDetails.divisions = [...divisionWiseReportDetails.divisions, ...(response.divisions || [])]

            const { maxPageCount } = response;
            if (++page <= maxPageCount) {
              questionWise()
            } else {
              this.clusterJson['group-questionwise-division-wise'] = {};
              this.clusterJson['group-questionwise-division-wise'].downloaded = true;
              this.clusterJson['group-questionwise-division-wise'].clusters = divisionWiseReportDetails?.divisions.filter((divisionInfo: any) => divisionInfo?.centerX[0].length).map((divisionInfo: any) => {
                const { centerX, centerY, division, hexagonColors, hexagonSvgPaths, test, testsData } = divisionInfo;
                return { centerX: centerX[0], centerY: centerY[0], colors: hexagonColors[0], paths: hexagonSvgPaths[0], scores: testsData[test._id].map((testData: any) => testData?.score || 0), hexagonLabels: testsData[test._id].map((testData: any) => testData?.qnno || ""), clusterName: test?.name + " / " + division }
              })
              this.bufferReports()
            }
          },
            (error: any) => {
              console.log('error caught for api', error);
              (error.status == 401 || error.status == 403) ? this.api.sessionExp() : console.log("Something went wrong!")
            });
      }
      !this.clusterJson['group-questionwise-all']?.downloaded && questionWise()
    }
    this.isPDFDownloaded = Boolean(localStorage.getItem("isPDFDownloaded"))
    !this.isPDFDownloaded && this.bufferReports()
  }
  bufferReports() {
    localStorage.setItem('download-group-reports', JSON.stringify(this.clusterJson))
    const areAllReportsdownloaded = this.startDownload();
    areAllReportsdownloaded && this.reportGeneration()
  }

  ngOnInit(): void {
    this.reportService.loadReportDisplayConfigurations((reportConfigurations: any) => {
      this.reportConfigurations = reportConfigurations;
      this.checkDownloadReport();
    });
  }
  startGeneratingReports() {
    // this.isCheckedStatus = true;


  }
  checkReportVisible() {
    if (window.location.pathname.search(/report-1/i) > -1) {
      return this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-testwise-allParticipants')?.isVisible;
    } else if (window.location.pathname.search(/report-3/i) > -1) {
      return this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-conceptwise-allParticipants')?.isVisible;
    } else if (window.location.pathname.search(/report-5/i) > -1) {
      return this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-subconceptwise-allParticipants')?.isVisible;
    } else if (window.location.pathname.search(/report-7/i) > -1) {
      return this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-questionwise-allParticipants')?.isVisible;
    }
  }
  reportGeneration() {
    this.http.post(this.api.BASEURL + 'api/download/v2', { clusterJson: this.clusterJson }, { ...this.api.headers(), responseType: 'blob' as 'json' })
      .subscribe((response: any) => {
        this.isPDFDownloaded = true;
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));

        const { name, coursecode } = JSON.parse(localStorage.getItem("generalreportsfilters") || JSON.stringify({}))?.activeCourse;

        downloadLink.setAttribute('download', `${name + " - " + coursecode} - Dipstick Reports.pdf`);
        document.body.appendChild(downloadLink);
        downloadLink.click();
        localStorage.setItem("isPDFDownloaded", "1")
      }, (e) => {
        console.log(e)
      });
  }

  downloadReportsV1() {
    this.downloadStatus = 'pdf';
    this.http.post(this.api.BASEURL + 'api/download/v3', { clusterJson: this.clusterJsonV1 }, { ...this.api.headers(), responseType: 'blob' as 'json' })
      .subscribe((response: any) => {
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));

        const { name, coursecode } = JSON.parse(localStorage.getItem("generalreportsfilters") || JSON.stringify({}))?.activeCourse;

        downloadLink.setAttribute('download', `${name + " - " + coursecode} - Dipstick Reports.pdf`);
        document.body.appendChild(downloadLink);
        downloadLink.click();
        this.isPDFDownloaded = true;
        this.downloadStatus = '';
        localStorage.setItem("isPDFDownloaded", "1")
        // localStorage.removeItem("isDownLoadGroupReportsBegin")
      }, (e) => {
        console.log(e)
      });
  }

  loadReportsV1() {
    const courseid = localStorage.getItem('courseId');

    this.isDownLoadGroupReportsBegin = true;
    localStorage.setItem('isDownLoadGroupReportsBegin', '1');
    localStorage.setItem('download-group-reports-courseId', courseid);
    const { activeCourse = {}, activeDepartment = {}, activeYear = {}, activeSemester = {} } = JSON.parse(localStorage.getItem("generalreportsfilters")) || {};
    const { name, coursecode } = activeCourse;
    const reportTitles = JSON.parse(localStorage.getItem('configurations')) || [];

    const report1Conf = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-testwise-allParticipants')
    const isReport1Visible = report1Conf?.isVisible;

    const report2Conf = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-testwise-divisionWise');
    const isReport2Visible = report2Conf?.isVisible;

    const report3Conf = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-conceptwise-allParticipants');
    const isReport3Visible = report3Conf?.isVisible;

    const report4Conf = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-conceptwise-divisionWise');
    const isReport4Visible = report4Conf?.isVisible;

    const report5Conf = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-subconceptwise-allParticipants');
    const isReport5Visible = report5Conf?.isVisible;

    const report6Conf = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-subconceptwise-divisionWise');
    const isReport6Visible = report6Conf?.isVisible;

    const report7Conf = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-questionwise-allParticipants');
    const isReport7Visible = report7Conf?.isVisible;

    const report8Conf = this.reportConfigurations.find((reportConfiguration: any) => reportConfiguration.reportType == 'group-questionwise-divisionWise');
    const isReport8Visible = report8Conf?.isVisible;

    const reportTitleJson: any = {};
        reportTitles.forEach((topic: any) => {
            const keyName = Object.keys(topic)[0]
            reportTitleJson[keyName] = topic[keyName];
        })
    const clusterJsonV1: any = Object.keys(this.clusterJsonV1 || {}).length ? this.clusterJsonV1 : {
      metadata: { courseName: name, coursecode, departmentName: activeDepartment?.name, reportTitles, activeYear, semesterName: activeSemester?.name },
      report1 : {},
      report2 : {},
      report3 : {},
      report4 : {},
      report5 : {},
      report6 : {},
      report7 : {},
      report8 : {}
    };

    const checkForDownload = () => {

      if(
        clusterJsonV1?.report1?.downloaded &&
        clusterJsonV1?.report2?.downloaded &&
        clusterJsonV1?.report3?.downloaded &&
        clusterJsonV1?.report4?.downloaded &&
        clusterJsonV1?.report5?.downloaded &&
        clusterJsonV1?.report6?.downloaded &&
        clusterJsonV1?.report7?.downloaded &&
        clusterJsonV1?.report8?.downloaded &&
        !this.isPDFDownloaded
      ) {
        this.clusterJsonV1 = clusterJsonV1;
        localStorage.setItem('download-group-reports', JSON.stringify(clusterJsonV1))
        this.downloadReportsV1()
      } else {
        localStorage.setItem('download-group-reports', JSON.stringify(clusterJsonV1))
      }

    }

    const numify = (num:number) => Number.isInteger(num) ? num : Number(num).toFixed(2);

    const loadReport1 = () => {
      this.downloadStatus = 'report1';
      if (isReport1Visible && !clusterJsonV1.report1.downloaded) {
        this.http.get(this.api.BASEURL + 'api/report/webview/all/v1?id=' + courseid, this.api.headers()).subscribe((response: any) => {
          const format = {
            prefix: "T",
            reportTitle: `Report 1 : ${reportTitleJson["test"] || 'Post Test'} report - All Participants`,
            report: [
              {
                title: "All Divisions",
                cluster: response.tests
              }
            ],
            table: {
              headers: [
                { label: "Serial #", property: 'slNo', width : 80 },
                { label: "Test Name", property: 'testName', width : 550 },
                { label: "Average Score", property: 'averageScore', width : 100 },
                { label: "Number Of Records", property: 'numberOfRecords', width : 100 },
              ],
              datas: response.tests.map((t: any, i: number) => {
                return { slNo: 'T' + (i + 1), testName: t.name, averageScore: numify(t.percentage), numberOfRecords: t.count }
              })
            }
          }
          clusterJsonV1.report1.info = format;
          clusterJsonV1.report1.downloaded = true;
          loadReport2();
          checkForDownload();
        })
      } else {
        clusterJsonV1.report1.downloaded = true;
        loadReport2();
        checkForDownload();
      }
    }

    const loadReport2 = () => {
      this.downloadStatus = 'report2';
      if (isReport2Visible && !clusterJsonV1.report2.downloaded) {
        this.http.get(this.api.BASEURL + 'api/report/webview/division-wise/v1?id=' + courseid, this.api.headers()).subscribe((response: any) => {
          const format = {
            prefix: "T",
            reportTitle: `Report 2 : ${reportTitleJson["test"] || 'Post Test'} report - Division Wise`,
            "report": response.response.map((t: any) => {
              return { title: t.title, cluster: t.scores }
            }),
            "table": {
              headers: [
                { label: "Serial #", property: 'slNo', width : 80 },
                { label: "Test Name", property: 'testName', width : 750 }
              ],
              datas: response.tableData.map((d: any, i: number) => {
                return { slNo: 'T' + (i + 1), testName: d.name }
              })
            }
          }
          clusterJsonV1.report2.info = format;
          clusterJsonV1.report2.downloaded = true;
          loadReport3()
          checkForDownload()
        })
      } else {
        clusterJsonV1.report2.downloaded = true;
        loadReport3()
        checkForDownload()
      }
    }
    
    const loadReport3 = () => {
      this.downloadStatus = 'report3';
      if (isReport3Visible && !clusterJsonV1.report3.downloaded) {
        this.http.get(this.api.BASEURL + 'api/report/conceptwise/allParticipants/v1?courseid=' + courseid, this.api.headers()).subscribe((response: any) => {
          const format = {
            prefix: "C",
            reportTitle: `Report 3 : ${reportTitleJson["concept"] || 'Subtopic'} report - All Participants`,
            report: [
              {
                title: "All Divisions",
                cluster: response.response
              }
            ],
            table: {
              headers: [
                { label: "Serial #", property: 'slNo', width : 80 },
                { label: "Concept Number - Subtopic Name", property: 'conceptNumberSubtopicName', width : 650 },
                { label: "Average Score", property: 'averageScore', width : 100 }
              ],
              datas: response.response.map((c: any, i: number) => {
                return { slNo: 'C' + (i + 1), conceptNumberSubtopicName: c.conceptnumber + " - " + c.subtopic, averageScore: numify(c.score) }
              })
            }
          }
          clusterJsonV1.report3.info = format;
          clusterJsonV1.report3.downloaded = true;
          loadReport4();
          checkForDownload();
        })
      } else {
        clusterJsonV1.report3.downloaded = true;
        loadReport4();
        checkForDownload();
      }
    }

    const loadReport4 = () => {
      this.downloadStatus = 'report4';
      if (isReport4Visible && !clusterJsonV1.report4.downloaded) {
        this.http.get(this.api.BASEURL + 'api/report/conceptwise/divisionWise/v1?courseid=' + courseid, this.api.headers()).subscribe((response: any) => {
          const format = {
            prefix: "C",
            reportTitle: `Report 4 : ${reportTitleJson["concept"] || 'Subtopic'} report - Division Wise`,
            report: response.response.map((c: any) => {
              return { title: c.title, cluster: c.scores }
            }),
            table: {
              headers: [
                { label: "Serial #", property: 'slNo', width : 80 },
                { label: "Concept Number - Subtopic Name", property: 'conceptNumberSubtopicName', width : 750 }
              ],
              datas: response.tableData.map((c: any, i: number) => {
                return { slNo: 'C' + (i + 1), conceptNumberSubtopicName: c.conceptnumber + " - " + c.subtopic }
              })
            }
          }
          clusterJsonV1.report4.info = format;
          clusterJsonV1.report4.downloaded = true;
          loadReport5()
          checkForDownload()
        })
      } else {
        clusterJsonV1.report4.downloaded = true;
        loadReport5()
        checkForDownload()
      }
    }

    const loadReport5 = () => {
      this.downloadStatus = 'report5';
      if (isReport5Visible && !clusterJsonV1.report5.downloaded) {
        this.http.get(this.api.BASEURL + 'api/report/subconceptwise/allParticipants/v1?courseid=' + courseid, this.api.headers()).subscribe((response: any) => {
          const format = {
            prefix: "SC",
            reportTitle: `Report 5 : ${reportTitleJson["subconcept"] || 'Concept statement'} report - All Participants`,
            report: [
              {
                title: "All Divisions",
                cluster: response.response
              }
            ],
            table: {
              headers: [
                { label: "Serial #", property: 'slNo', width : 80 },
                { label: "Concept Statement", property: 'conceptStatement', width : 650 },
                { label: "Average Score", property: 'averageScore', width : 100 },
              ],
              datas: response.response.map((r: any, i: number) => {
                return { slNo: 'SC' + (i + 1), conceptStatement: r.conceptstatement, averageScore: numify(r.score) }
              })
            }
          };
          clusterJsonV1.report5.info = format;
          clusterJsonV1.report5.downloaded = true;
          loadReport6()
          checkForDownload()
        })
      } else {
        clusterJsonV1.report5.downloaded = true;
        loadReport6()
        checkForDownload()
      }
    }

    const loadReport6 = () => {
      this.downloadStatus = 'report6';
      if (isReport6Visible && !clusterJsonV1.report6.downloaded) {
        this.http.get(this.api.BASEURL + 'api/report/subconceptwise/divisionWise/v1?courseid=' + courseid, this.api.headers()).subscribe((response: any) => {
          const format = {
            prefix: "SC",
            reportTitle: `Report 6 : ${reportTitleJson["subconcept"] || 'Concept statement'} report - Division Wise`,
            report: response.response.map((r: any, i: number) => {
              return { title: r.title, cluster: r.scores }
            }),
            table: {
              headers: [
                { label: "Serial #", property: 'slNo', width : 80 },
                { label: "Concept Statement", property: 'conceptStatement', width : 750 }
              ],
              datas: response.tableData.map((c: any, i: number) => {
                return { slNo: 'SC' + (i + 1), conceptStatement: c.conceptstatement }
              })
            }
          };
          clusterJsonV1.report6.info = format;
          clusterJsonV1.report6.downloaded = true;
          loadReport7()
          checkForDownload()
        })
      } else {
        clusterJsonV1.report6.downloaded = true;
        loadReport7()
        checkForDownload()
      }
    }

    const loadReport7 = () => {
      this.downloadStatus = 'report7';
      if (isReport7Visible && !clusterJsonV1.report7.downloaded) {
        this.http.get(this.api.BASEURL + 'api/report/questionwise/allParticipants/v1?courseid=' + courseid, this.api.headers()).subscribe((response: any) => {
          const format = {
            prefix: "Q",
            reportTitle: "Report 7 : Question wise report - All Participants",
            report: response.response.map((q: any) => {
              return { title: q.title, cluster: q.scores }
            }),
            table: null
          };
          clusterJsonV1.report7.info = format;
          clusterJsonV1.report7.downloaded = true;
          loadReport8()
          checkForDownload()
        })
      } else {
        clusterJsonV1.report7.downloaded = true;
        loadReport8()
        checkForDownload()
      }
    }

    const loadReport8 = () => {
      this.downloadStatus = 'report8';
      if (isReport8Visible && !clusterJsonV1.report8.downloaded) {
        this.http.get(this.api.BASEURL + 'api/report/questionwise/getTestsList?courseid=' + courseid, this.api.headers()).subscribe((response: any) => {
  
          const { testsWithDivisions, tests } = response;
          const questionWiseReportsArr = [];
  
          const loadQuestionWiseReports = (testWithDivisionIndex = 0) => {
            const { testid, division } = testsWithDivisions[testWithDivisionIndex]?._id;
            const testTitle = tests.find(t => t._id == testid).name;
  
            const divisionBuffer = btoa(division)
            this.http.get(this.api.BASEURL + 'api/report/questionwise/divisionWise/v1?testid=' + testid + "&divisionBuffer=" + divisionBuffer, this.api.headers()).subscribe((response: any) => {
              const cluster = response.response;
              const title = testTitle + " / " + division;
              questionWiseReportsArr.push({ title, cluster });
              if (testWithDivisionIndex < (testsWithDivisions.length - 1)) {
                loadQuestionWiseReports(testWithDivisionIndex + 1);
              } else {
                setReport8Format()
              }
            })
          }

          const setReport8Format = () => {
            const format = {
              prefix: "Q",
              reportTitle: "Report 8 : Question wise report - Division Wise",
              report: questionWiseReportsArr,
              table: null
            }
            clusterJsonV1.report8.info = format;
            clusterJsonV1.report8.downloaded = true;
            checkForDownload()
          }
          loadQuestionWiseReports()
        })
      } else {
        clusterJsonV1.report8.downloaded = true;
        checkForDownload()
      }
    }
    
    loadReport1()
  }
}
