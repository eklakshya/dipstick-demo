import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupTableViewConceptwiseComponent } from './popup-table-view-conceptwise.component';

describe('PopupTableViewConceptwiseComponent', () => {
  let component: PopupTableViewConceptwiseComponent;
  let fixture: ComponentFixture<PopupTableViewConceptwiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupTableViewConceptwiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupTableViewConceptwiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
