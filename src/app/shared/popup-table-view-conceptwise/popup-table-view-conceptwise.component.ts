import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../pages/services/http.service';
import { ReportsService } from '../../core/services/reports.service'
import { constant } from '../constant';
import { HexagonLayoutService } from 'src/app/pages/services/hexagon-layout.service';



@Component({
  selector: 'app-popup-table-view-conceptwise',
  templateUrl: './popup-table-view-conceptwise.component.html',
  styleUrls: ['./popup-table-view-conceptwise.component.css']
})
export class PopupTableViewConceptwiseComponent implements OnInit {

  response: any = {	
    levelWiseAgregation:[],
    conceptnumbers:[],
    concepts:[],
    conceptstatements:[],
    levels:[],
    allSubconcepts:[]
  };
  conceptstatements:any[];
  @Input() type : string;
  @Input() division : string;
  @Input() test : any = {_id : "", name : ""}
  @Input() reportType : string;
  @Input() showName : boolean;
  @Input() labelPrefix : string;
  @Input() xLabel : string;
  @Input() apiEndPoint : string = '';
  popUp:String = "reports";
  object: Object;
  courseid : string = localStorage.getItem('courseId');
  range : number[] = [1,2,3,4,5,6]
  levelLimit : number = 6;
  conceptLimit : number = 3;
  allConcepts: any[] = [];
  legend:any = constant.legendImage;
  data:any;  
  Sdata:any;

  levelLabels:any = [5,4,3,2,1];
  subtopics:any = [];
  levels:any = []
  levelAndSubtopicWiseJSON:any = [];
  reportDetails:any = [];
  conceptStatements:any = [];
  questions:any = [];
  constructor(private http:HttpClient,private api : ApiService, public reportService:ReportsService, private hexagonLayout: HexagonLayoutService) { 
  }

  getSvgWidth(rowDistribution:any[]){
    return rowDistribution[Math.ceil(rowDistribution.length%2==1?rowDistribution.length/2:rowDistribution.length)-1]
  }
  getSvgheight(rowDistribution:any[],svgPathLength:number){
    var totalHex = 0;
    var totalHexHeight = 0;
    var hexHeights:number[] = rowDistribution.map((rowdata, number)=>{
    totalHex = totalHex < svgPathLength ? totalHex + rowdata : (totalHex)
    totalHexHeight = totalHex < svgPathLength ? number + 1 : totalHexHeight ;
    return totalHexHeight + 1
    })
    var lastelement = hexHeights[hexHeights.length-1]
    return lastelement
  }
  ngOnInit(): void {
    console.log(this.test)
    this.data = localStorage.getItem('data');
    this.Sdata = localStorage.getItem('S_bread');
    let reportType = this.reportType || 'questionwise';
    // console.log(reportType, this.reportType)
    let apiEndPoint : string =  this.apiEndPoint || `api/report/${reportType}/table/${this.type}?courseid=${this.courseid}` + ( this.test._id ? `&testid=${this.test._id}` : ``) + ( this.division ? `&divisionBuffer=${btoa(this.division)}` : ``) //`/api/report/${reportType}/table/${this.type}?courseId=${this.courseid}&testid=${this.test._id}`
    // reportType == "subconceptwise" ? this.conceptStatementReports() : 
    // this.http.get(this.api.BASEURL + apiEndPoint,this.api.headers())
    // .subscribe((response:any) => {
    //   this.response = response;
    //   (!this.response || !this.response != undefined ) ? (
    //     document.getElementById('loading').style.display = 'none'):console.log("successfully displayed loader..")
    //   this.allConcepts = response.allConcepts || []
    //   this.conceptstatements = (response.conceptstatements && response.conceptstatements.length) ? response.conceptstatements.map((conceptstatementData:any)=>{ return conceptstatementData["_id"].conceptstatement }) : []
    //   this.conceptLimit = this.conceptLimit > this.response.concepts.length ? this.conceptLimit : this.response.concepts.length
    //   console.log(response)
    // },
    // (error:any) => {                           
    //   console.log('error caught for api',error);
    //   (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    // });
    this.conceptStatementReports()
  }
  getNegativeMargin(lastelement:number,rowDistributionLength:number){
    return (rowDistributionLength - lastelement) * -50
  }
  ngAfterViewInit(){
  }
  questionsList(conceptData:any){
    let concept = Object.keys(conceptData)[0];
    return conceptData[concept]
  }
  getFieldValue(index:number, field:string){
    let conceptData = this.response.concepts[index]
    return conceptData[field] || conceptData["_id"][field]
  }
  findLevel(index:number){
    let levelWiseData:any[] = this.response.levelWiseAgregation
    let leveldata = levelWiseData.find(element =>element[0].bl == 'L'+index)
    return (leveldata && leveldata.length) ? true : false;
  }
  findLevelIndex(index:number){
    let levelWiseData:any[] = this.response.levelWiseAgregation
    let levelIndex = levelWiseData.findIndex(element =>element[0].bl == 'L'+index)
    return levelIndex;
  }
  getSizedArray(size:number){
    return new Array(size)
  }
  hexLabelText(topicInfo:any, conceptIndex:number){
    const conceptstatementIndex = this.conceptstatements.length ? this.conceptstatements.findIndex(conceptstatementData => conceptstatementData == topicInfo.conceptstatement) : -1
    return (this.showName && topicInfo?.qnno) ? (''+ topicInfo.qnno) : (this.conceptstatements.length ? ( 'SC'+ (conceptstatementIndex + 1 ) ): ( this.labelPrefix + (conceptIndex+1)))
  }
  getHexagonNumber(topicInfo:any){
    const topic = topicInfo.subtopic || topicInfo.conceptstatement;
    return (this.response?.allSubconcepts?.length ? this.response.allSubconcepts.findIndex(subconcept => subconcept._id.conceptstatement == topic) : this.response.allConcepts.findIndex(concept => concept._id.subtopic == topic))
  }
  
  getTopicNumber(topicInfo:any){
    const conceptstatementIndex = this.conceptstatements.length ? this.conceptstatements.findIndex(conceptstatementData => conceptstatementData == topicInfo.conceptstatement) : -1
    return (this.showName && topicInfo?.qnno) ? (''+ topicInfo.qnno) : this.labelPrefix + (this.getHexagonNumber(topicInfo) + 1)
  }
  getConceptNumber(index:number){
    // console.log(this.response.concepts)
    const subtopic = this.response?.concepts[index]?._id?.subtopic
    const allSubtopics = this.allConcepts.map((conceptdata)=>{return conceptdata?._id?.subtopic})
    // console.log(this.allConcepts.map((conceptdata)=>{return conceptdata?._id?.subtopic}), subtopic)
    // console.log(allSubtopics.indexOf(subtopic))
    // console.log(this.allConcepts.findIndex((conceptData)=>{conceptData._id.subtopic == subtopic}))
    // console.log(allSubtopics, subtopic, allSubtopics.indexOf(subtopic) , index)
    return (allSubtopics.indexOf(subtopic)+1 || index+1)
  }
  conceptStatementReports(){
    let reportType = this.reportType || 'questionwise';
    let apiEndPoint : string =  this.apiEndPoint || `api/report/${reportType}/table/${this.type}?courseid=${this.courseid}` + ( this.test._id ? `&testid=${this.test._id}` : ``) + ( this.division ? `&divisionBuffer=${btoa(this.division)}` : ``) //`/api/report/${reportType}/table/${this.type}?courseId=${this.courseid}&testid=${this.test._id}`
    this.http.get(this.api.BASEURL + apiEndPoint,this.api.headers())
    .subscribe((response:any) => {
      this.response = response;
      // document.getElementById('loading').style.display = 'none';
      response?.data?.length && this.populateData();
    },
    (error:any) => {                           
      document.getElementById('loading').style.display = 'none';
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }
  populateData(){
    this.subtopics = this.response?.allConcepts || [...new Set(this.response?.data.map((data:any) => data._id?.subtopic))]
    this.conceptStatements = [...new Set(this.response?.data.map((data:any) => data._id?.conceptstatement))]
    this.levels = [...new Set(this.response?.data.map((data:any) => data._id?.bl))]
    this.questions = [...new Set(this.response?.data.map((data:any) => data._id?.qnno))]
    this.levelLabels.forEach((level:any, levelIndex:number) => {
      this.levelAndSubtopicWiseJSON[levelIndex] = []
      this.subtopics.forEach((subtopic:any, subtopicIndex:number) => {
        this.levelAndSubtopicWiseJSON[levelIndex][subtopicIndex] = this.getLevelAndSubtopicWiseList(subtopic, level, subtopicIndex, levelIndex);
      })
    })
  }
  getLevelAndSubtopicWiseList(subtopic:any, level:any, subtopicIndex:number, levelIndex:number){
    const topics = this.response.data.filter((data:any) => ((data._id.bl == level) || (data._id.bl == "l"+level) || (data._id.bl == "L"+level)) && (data._id.subtopic == subtopic))
    this.getClusterLayout(topics, levelIndex, subtopicIndex);
    return topics;
  }
  getClusterLayout(cluster:any, levelIndex:any, subtopicIndex:any){
    this.reportDetails[levelIndex] = this.reportDetails[levelIndex] || []
    this.reportDetails[levelIndex][subtopicIndex] = {  };
    const rowDistribution = this.hexagonLayout.getRowDistribution(cluster.length);
    const reportDimensionsPost = this.hexagonLayout.calcDimensions(cluster.length, 1, rowDistribution);
    const hexStructurePost: any = this.hexagonLayout.initHexStructure(rowDistribution, reportDimensionsPost.marginX, reportDimensionsPost.marginY, reportDimensionsPost.hexWidth, reportDimensionsPost.hexRadius);

    const hexCentersXPost:any = [];
    const hexCentersYPost:any = [];
    const hexPathsPost:any = [];
    const hexColorsPost:any = [];
    const { marginX, marginY, hiveHeight, hiveWidth, hexWidth} = reportDimensionsPost;
    this.reportDetails[levelIndex][subtopicIndex].topics = cluster;
    this.reportDetails[levelIndex][subtopicIndex].viewBox = `${cluster.length>2?(marginX - ((Math.floor( Object.keys(hexStructurePost).length / 2) -1 ) * (hexWidth/2))):marginX+(hexWidth/2)} ${marginY} ${hiveWidth} ${hiveHeight}`;
    for(let a = 0; a < 1; a++) {
        let testNumber: number = 0;
        const rowsInStruct = Object.keys(hexStructurePost);

        rowsInStruct.forEach((row, index) => {
            const testsInRow = Object.keys(hexStructurePost[`row${index+1}`]);
            testsInRow.forEach(test => {
                try {
                    testNumber += 1;
                    const scoreValue = cluster[testNumber-1]['score'] >= 0 ? cluster[testNumber-1]['score'] : -1;
                    const score = scoreValue === null ? -1 : scoreValue

                    hexCentersXPost.push(this.hexagonLayout.getHexCenterX(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexCentersYPost.push(this.hexagonLayout.getHexCenterY(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexPathsPost.push(this.hexagonLayout.getHexPath(hexStructurePost,testNumber, index+1, a, reportDimensionsPost));
                    hexColorsPost.push(this.hexagonLayout.getHexColor(score));
                } catch (error) {

                }
            });
        });
    }
    // // console.log({ hexPathsPost, hexColorsPost, hexCentersXPost, hexCentersYPost })
    this.reportDetails[levelIndex][subtopicIndex].hexagonSvgPaths = hexPathsPost;
    this.reportDetails[levelIndex][subtopicIndex].hexagonColors = hexColorsPost;
    this.reportDetails[levelIndex][subtopicIndex].centerX = hexCentersXPost;
    this.reportDetails[levelIndex][subtopicIndex].centerY = hexCentersYPost;
    this.reportDetails[levelIndex][subtopicIndex].rowDistribution = rowDistribution;
    // console.log(this.reportDetails[levelIndex][subtopicIndex])
    // // this.sendReportDetails.emit(this.reportDetails)
    // this.generatePDF();
    // console.log(this.reportDetails)
  }
  getHexNumber(topic:any){
    const { qnno , subtopic, conceptstatement } = topic;
    const conceptStatementNumber = (this.conceptStatements.findIndex(t => t == conceptstatement) + 1);
    const qnnoNumber = qnno//(this.questions.findIndex(t => t == qnno) + 1);
    const subtopicNumber = (this.subtopics.findIndex(t => t == subtopic) + 1);
    return this.reportType=="subconceptwise" ? conceptStatementNumber : ( this.reportType=="conceptwise" ? subtopicNumber : qnnoNumber );
  }
  getTopicIndex(levelIndex, subtopicIndex, i){
    const topic = this.reportType == 'subconceptwise' ? 'conceptstatement' : ( this.reportType != 'conceptwise' ? 'qnno' : 'subtopic' );
    const labelIndex = this.response?.allTopics.findIndex( s => s == this.reportDetails[levelIndex][subtopicIndex].topics[i]?._id[topic]);
    const label = (this.reportType == 'subconceptwise' ? 'SC' : ( this.reportType != 'conceptwise' ? 'Q' : 'C' ) ) + (labelIndex + 1)
    return label;
  }
}
