import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConceptReportsDivisionWiseComponent } from './concept-reports-division-wise.component';

describe('ConceptReportsDivisionWiseComponent', () => {
  let component: ConceptReportsDivisionWiseComponent;
  let fixture: ComponentFixture<ConceptReportsDivisionWiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConceptReportsDivisionWiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConceptReportsDivisionWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
