import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../../pages/services/http.service';
import { ReportsService } from '../../core/services/reports.service';
import { constant } from '../constant';
import { HexagonLayoutService } from 'src/app/pages/services/hexagon-layout.service';

@Component({
  selector: 'app-concept-reports-division-wise',
  templateUrl: './concept-reports-division-wise.component.html',
  styleUrls: ['./concept-reports-division-wise.component.css']
})
export class ConceptReportsDivisionWiseComponent implements OnInit {
  @Input() url : string;
  @Input() reportType : string;
  @Input() labelPrefix : string;
  @Input() listLabel : string;
  @Input() popupXLabel : string;
  courseId: string = localStorage.getItem('courseId');
  view:any
  activeDivision : string;
  activeTest : string;
  legend:any = constant.legendImage;
  @Input() isPopupVisible : Boolean;
  maxPageCount:any = 1
  page:any = 1
  pagenationLoader:boolean=true;
  constructor(private http :HttpClient,  private modalService: NgbModal,private api : ApiService, public reportService:ReportsService, private hexagonLayout: HexagonLayoutService) {
   }
   reportDetails:any
   resp:any;
   concept:any;
   sub_concept:any;

   courseDetails:any;
   scoreList:any;
   tableData:any;
   userRole: string = '';

   ngOnInit(): void {

    this.userRole = localStorage.getItem('userrole') || "";

     // this.tests()
    //  alert("reportType" + this.reportType)
     this.courseDetails = JSON.parse(localStorage.getItem('generalreportsfilters'))?.activeCourse;
    this.getScoreList()
  }

  //------------------lazy loading for report-4/6----------------------------------------------------------------

  tests(){
    let url = this.api.BASEURL + ( this.url+'&page='+ this.page || 'api/report/conceptwise/divisionWise?courseid='+this.courseId+ '&page=' + this.page);
    this.http.get(url,this.api.headers())
     .subscribe((response:any) => { 
       this.resp =response['divisions'];

      const divisionWiseScores = response['divisionWiseScores'] || [];
       const topicssorder =  response['topicssorder'] || []

       this.reportDetails.divisionWiseScores = [ ...this.reportDetails.divisionWiseScores, ...divisionWiseScores]
       this.reportDetails.topicssorder = [ ...this.reportDetails.topicssorder, ...topicssorder]
       this.reportDetails.allTopics =response.allTopics;
       this.reportDetails.course = response.course;
       this.reportDetails.divisions = response.divisions;
       this.reportDetails.topicslisted=  response.topicslisted;
      this.maxPageCount = this.reportDetails.divisionWiseScores[0].maxPageCount;

      // this.reportDetails = response['divisionWiseScores'] || [];
      // this.reportDetails = [...this.reportDetails, ...response['divisionWiseScores']]
      let testslength = this.reportDetails['divisions'].length;
      this.api.noResp(testslength);
      console.log(this.reportDetails);
      testslength ?  (setTimeout(function(){ 
        document.getElementById('loading').style.display = 'none';
      }, 100)):console.log("error in the sub concept")
      if(++this.page <= this.maxPageCount){
        this.tests()
      }else{
      this.pagenationLoader = false;
      document.getElementById('loading1').style.display = 'none'
      }
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }


  hasTest(svgPaths:any[]){
    return svgPaths.find((svgPath:any) => svgPath.length > 0)
  }
  getSvgWidth(rowDistribution:any[]){
    return rowDistribution[Math.ceil(rowDistribution.length%2==1?rowDistribution.length/2:rowDistribution.length)-1]
  }
  getSvgheight(rowDistribution:any[],svgPathLength:number){
    var totalHex = 0;
    var totalHexHeight = 0;
    var hexHeights:number[] = rowDistribution.map((rowdata, number)=>{
    totalHex = totalHex < svgPathLength ? totalHex + rowdata : (totalHex)
    totalHexHeight = totalHex < svgPathLength ? number + 1 : totalHexHeight ;
    return totalHexHeight + 1
    })
    var lastelement = hexHeights[hexHeights.length-1]
    return lastelement
  }
  getNegativeMargin(lastelement:number,rowDistributionLength:number){
    return (rowDistributionLength - lastelement) * -50
  }
  // open(modal) {
  //   this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', size:'xl', backdrop: 'static',keyboard: false});
  // }
  
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl', backdrop: 'static', keyboard: false });;
  }

  findConceptsByTestId (testid:string){
    let divisionIndex = this.reportDetails.divisions.findIndex(element => element.testsData[testid].length > 0)
    return this.reportDetails.divisions[divisionIndex].testsData[testid]
  }
  getHexText (conceptIndex:number, divisionIndex:number) {
    return this.labelPrefix + (this.getConceptNumber(conceptIndex, divisionIndex) + 1)//( conceptIndex + 1 )// + '-' + this.reportDetails.scores[conceptIndex].score.toFixed(2) ;
  }
  getTestCountOfRowDistributionCount (rowDistribution:number[]) {
    return rowDistribution.reduce((a, b) => a + b, 0) > 29
  }
  getConceptNumber(conceptIndex:number, divisionIndex:number){
    // console.log(this.reportDetails.divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.subtopic || this.reportDetails.divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.conceptstatement?.conceptstatement)
    // console.log(this.reportDetails.divisionWiseScores[divisionIndex]?.scores[conceptIndex])
    // console.log(this.reportDetails.divisionWiseScores[divisionIndex]?.scores[conceptIndex]["subtopic"] || this.reportDetails.divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.conceptstatement?.conceptstatement)
    const topic = this.reportDetails.divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.subtopic?.subtopic || this.reportDetails.divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.conceptstatement?.conceptstatement;
    const index = this.reportDetails.allTopics.findIndex(element => (element._id["conceptstatement"] == topic || element._id["subtopic"] == topic))
    // const data = this.reportDetails.allTopics.filter(element => {
    //   if((element._id["conceptstatement"] == topic))
    //   this.concept = "conceptstatement";
    //   if(element._id["subtopic"] == topic)
    //   this.sub_concept = "subtopic";
    // })

    // console.log({topic, index})
    return index

  }
  getScoreList(){
    let url = this.api.BASEURL + ( this.url+'&page='+ this.page || 'api/report/conceptwise/divisionWise?courseid='+this.courseId+ '&page=' + this.page);
    this.http.get(url,this.api.headers()).subscribe((response:any) => {
      this.scoreList = response?.response;
      this.tableData = response?.tableData;
      console.clear()
      this.scoreList.forEach((t, i) => this.getClusterLayout(t?.scores, i));
     })
  }

  getClusterLayout(cluster:any, clusterIndex:number) {
    console.log(cluster, clusterIndex)
    this.reportDetails = this.reportDetails || [];
    this.reportDetails[clusterIndex] = {}
    const rowDistribution = this.hexagonLayout.getRowDistribution(cluster.length);
    const reportDimensionsPost = this.hexagonLayout.calcDimensions(cluster.length, 1, rowDistribution);
    const hexStructurePost: any = this.hexagonLayout.initHexStructure(rowDistribution, reportDimensionsPost.marginX, reportDimensionsPost.marginY, reportDimensionsPost.hexWidth, reportDimensionsPost.hexRadius);
    // alert(JSON.stringify(rowDistribution) + JSON.stringify(reportDimensionsPost) + JSON.stringify(hexStructurePost))

    const hexCentersXPost:any = [];
    const hexCentersYPost:any = [];
    const hexPathsPost:any = [];
    const hexColorsPost:any = [];
    const { marginX, marginY, hiveHeight, hiveWidth, hexWidth} = reportDimensionsPost;
    this.reportDetails[clusterIndex].topics = cluster;
    this.reportDetails[clusterIndex].viewBox = `${cluster.length>2?(marginX - ((Math.floor(Object.keys(hexStructurePost).length / 2) -1 ) * (hexWidth/2))):marginX+(hexWidth/2)} ${marginY} ${hiveWidth} ${hiveHeight}`;
    for(let a = 0; a < 1; a++) {
        let testNumber: number = 0;
        const rowsInStruct = Object.keys(hexStructurePost);

        rowsInStruct.forEach((row, index) => {
            const testsInRow = Object.keys(hexStructurePost[`row${index+1}`]);
            testsInRow.forEach(test => {
                try {
                    testNumber += 1;
                    const scoreIndex = cluster[testNumber-1]['score'];
                    const score = ((scoreIndex != null) && (scoreIndex >= 0)) ? scoreIndex : -1;
                    hexCentersXPost.push(this.hexagonLayout.getHexCenterX(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexCentersYPost.push(this.hexagonLayout.getHexCenterY(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexPathsPost.push(this.hexagonLayout.getHexPath(hexStructurePost,testNumber, index+1, a, reportDimensionsPost));
                    hexColorsPost.push(this.hexagonLayout.getHexColor(score));
                  } catch (error) {
                    
                  }
                });
              });
    }
    // // console.log({ hexPathsPost, hexColorsPost, hexCentersXPost, hexCentersYPost })
    this.reportDetails[clusterIndex].centerX = hexCentersXPost;
    this.reportDetails[clusterIndex].hexagonSvgPaths = hexPathsPost;
    this.reportDetails[clusterIndex].hexagonColors = hexColorsPost;
    this.reportDetails[clusterIndex].centerY = hexCentersYPost;
    this.reportDetails[clusterIndex].rowDistribution = rowDistribution;
    console.log(this.reportDetails)
    // // this.sendReportDetails.emit(this.reportDetails)
    // this.generatePDF();
  }
  getTopicNumber(topicDetails:any){
    const isConceptWise = this.reportType == 'conceptwise';
    const conceptIndex = this.tableData.findIndex(t => isConceptWise ? (t?.subtopic == topicDetails?.subtopic ) : ( t?.conceptstatement == topicDetails?.conceptstatement ) );
    return  this.labelPrefix + (conceptIndex + 1);
  }
  getScoreNumber(num:number){
    // return Number.isInteger(+num) ? num : num.toFixed(2);
    return ( num == 100) ? num : num.toFixed(2);
  }
}
