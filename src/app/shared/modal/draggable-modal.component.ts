import { Router } from '@angular/router';
import { Component, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import * as $ from 'jquery';

@Component({
  selector: 'draggable-modal',
  templateUrl: 'draggable-modal.component.html'
})
export class DraggableModalComponent implements OnInit {
  constructor(
    // Close Bootstrap Modal
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {

    $(document).ready(function () {
      let modalContent: any = $('.modal-content');
      modalContent.draggable({
        handle: '.modal-header',
        revert: true,
        revertDuration: 300
      });
    });

  }

}