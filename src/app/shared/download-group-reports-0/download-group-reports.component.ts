import { Component, OnInit } from '@angular/core';
import { ReportsService } from 'src/app/core/services/reports.service';
import { ApiService } from 'src/app/pages/services/http.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-download-group-reports',
  templateUrl: './download-group-reports.component.html',
  styleUrls: ['./download-group-reports.component.css']
})
export class DownloadGroupReportsComponent implements OnInit {

  isDownLoadGroupReportsBegin = false;
  isPDFDownloaded = false;
  reportConfigurations:any = [];
  clusterJson:any = {};
  constructor(public http: HttpClient, public api:ApiService, public reportService:ReportsService, private router: Router) {
    this.reportService.loadReportDisplayConfigurations((reportConfigurations:any)=>{
      this.reportConfigurations = reportConfigurations;
      // this.isDownLoadGroupReportsBegin && this.beginDownload();
      this.checkDownloadReport()
    });
  }

  checkDownloadReport(buttonClick=false){
    const loadedCourseId = localStorage.getItem("download-group-reports-courseId");
    const courseid = localStorage.getItem('courseId');
    if(loadedCourseId == courseid){
      const isDownLoadGroupReportsBegin = Boolean(localStorage.getItem("isDownLoadGroupReportsBegin") || "");
      if(isDownLoadGroupReportsBegin){
        this.clusterJson = JSON.parse(localStorage.getItem('download-group-reports'));
        this.loadReports()
      } else {
        this.clusterJson = {};
        localStorage.removeItem('download-group-reports')
        buttonClick && this.loadReports()
      }
    } else {
      this.clusterJson = {};
      this.isDownLoadGroupReportsBegin = false;
      this.isPDFDownloaded = false;
      localStorage.removeItem("isPDFDownloaded")
      localStorage.removeItem('download-group-reports')
      localStorage.removeItem("isDownLoadGroupReportsBegin")
      buttonClick && this.loadReports()
    }
  }

  startDownload(){
    const testWiseAllParticipants = this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-testwise-allParticipants')?.isVisible ? this.clusterJson['group-testwise-all'].downloaded : true;
    const testWiseDivisionWise = this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-testwise-divisionWise')?.isVisible ? this.clusterJson['group-testwise-division-wise'].downloaded : true;
    const conceptWiseAllParticipants = this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-conceptwise-allParticipants')?.isVisible ? this.clusterJson['group-conceptwise-all'].downloaded : true;
    const conceptWiseDivisionWise = this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-conceptwise-divisionWise')?.isVisible ? this.clusterJson['group-conceptwise-division-wise'].downloaded : true;
    const subconceptWiseAllParticipants = this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-subconceptwise-allParticipants')?.isVisible ? this.clusterJson['group-subconceptwise-all'].downloaded : true;
    const subconceptWiseDivisionWise = this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-subconceptwise-divisionWise')?.isVisible ? this.clusterJson['group-subconceptwise-division-wise'].downloaded : true;
    const questionWiseAllParticipants = this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-questionwise-allParticipants')?.isVisible ? this.clusterJson['group-questionwise-all'].downloaded : true;
    const questionWiseDivisionWise = this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-questionwise-divisionWise')?.isVisible ? this.clusterJson['group-questionwise-division-wise'].downloaded : true;

    return ( testWiseAllParticipants && testWiseDivisionWise && conceptWiseAllParticipants && conceptWiseDivisionWise && subconceptWiseAllParticipants && subconceptWiseDivisionWise && questionWiseAllParticipants && questionWiseDivisionWise );
  }
  
  loadReports(){
    this.isDownLoadGroupReportsBegin = true;
    localStorage.setItem('isDownLoadGroupReportsBegin', '1');
    const courseid = localStorage.getItem('courseId');
    localStorage.setItem('download-group-reports-courseId', courseid);
    const { activeCourse={}, activeDepartment={}, activeYear={} , activeSemester={}} = JSON.parse(localStorage.getItem("generalreportsfilters")) || {};
    const { name, coursecode } = activeCourse;
    const reportTitles = JSON.parse(localStorage.getItem('configurations')) || [];
    this.clusterJson.courseInfo = { name, coursecode, departmentName : activeDepartment?.name, reportTitles, activeYear,  semesterName:activeSemester?.name};
    if(this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-testwise-allParticipants')?.isVisible){
      !this.clusterJson['group-testwise-all']?.downloaded && this.http.get(this.api.BASEURL + 'api/report/webview/all?id='+courseid, this.api.headers()).subscribe((response:any) => {
        const { centerX, centerY, hexagonSvgPaths, hexagonColors, testArray } = response;
          this.clusterJson['group-testwise-all'] = {}
          this.clusterJson['group-testwise-all'].downloaded = true; 
          this.clusterJson['group-testwise-all'].clusters = [{ centerX, centerY, paths : hexagonSvgPaths, colors : hexagonColors, hexagonLabels: testArray.map((t:any, i:number) => `T${i+1}`), scores : testArray.map((t:any) => t.score || 0) }]
          this.clusterJson['group-testwise-all'].tableData = { headers : [ "Serial #", "Test Name", "Average Score", "Number Of Records" ], data: []};
          this.clusterJson['group-testwise-all'].tableData.data = testArray.map((testData:any, i:number) => [ "T"+(i+1), testData?.name||"", (testData?.score || 0).toFixed(2), testData?.count ]);
          // console.log(response, this.clusterJson);
          this.bufferReports()
        })
    }
    if(this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-testwise-divisionWise')?.isVisible){
      let page = 1;
      const clusterJson = { centerX : [], centerY : [], hexagonSvgPaths : [], hexagonColors : [], testArrays : [], tests : [], divisions : [] }
      const divisionWise = () => {
        this.http.get(this.api.BASEURL + 'api/report/webview/division-wise?id='+courseid + "&page=" + page, this.api.headers()).subscribe((response:any) => {
          const { maxPageCount } = response;
          const { centerX, centerY, hexagonSvgPaths, hexagonColors, testArrays, tests, divisions, } = response;
          clusterJson.centerX = [ ...clusterJson.centerX, ...centerX ]
          clusterJson.centerY = [ ...clusterJson.centerY, ...centerY ]
          clusterJson.hexagonColors = [...clusterJson.hexagonColors, ...hexagonColors ]
          clusterJson.hexagonSvgPaths = [...clusterJson.hexagonSvgPaths, ...hexagonSvgPaths ]
          clusterJson.testArrays = [...clusterJson.testArrays, ...testArrays ]
          clusterJson.tests = [...clusterJson.tests, ...tests ]
          clusterJson.divisions = [...clusterJson.divisions, ...divisions ]
          if (++page <= maxPageCount) {
            divisionWise()
          } else {
            const { centerX, centerY, hexagonSvgPaths, hexagonColors, testArrays, tests, divisions } = clusterJson;
            this.clusterJson['group-testwise-division-wise'] = {}
          this.clusterJson['group-testwise-division-wise'].downloaded = true; 
          this.clusterJson['group-testwise-division-wise'].clusters = hexagonSvgPaths.map((paths:string[], i:number) => {
            return { centerX: centerX[i], centerY: centerY[i], paths, colors : hexagonColors[i], hexagonLabels: tests.map((t:any, j:number) => `T${j+1}`), scores : testArrays[i].map((t:any) => t.score || 0), clusterName : divisions[i]?._id }
          })
          this.clusterJson['group-testwise-division-wise'].tableData = { headers : [ "Serial #", "Test Name" ], data: []};
          this.clusterJson['group-testwise-division-wise'].tableData.data = tests.map((testData:any, i:number) => [ "T"+(i+1), testData?.name||"" ]);
          this.bufferReports()
          }
          // console.log(response)
        })
      }
      !this.clusterJson['group-testwise-division-wise']?.downloaded && divisionWise()
    }
    if(this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-conceptwise-allParticipants')?.isVisible){
      !this.clusterJson['group-conceptwise-all']?.downloaded && this.http.get(this.api.BASEURL + 'api/report/conceptwise/allParticipants?courseid='+courseid, this.api.headers()).subscribe((response:any) => {
        const { centerX, centerY, hexagonSvgPaths, hexagonColors, allTopics, scores } = response;
        this.clusterJson['group-conceptwise-all'] = {}
        this.clusterJson['group-conceptwise-all'].downloaded = true; 
        this.clusterJson['group-conceptwise-all'].clusters = [{ centerX, centerY, paths : hexagonSvgPaths, colors : hexagonColors, hexagonLabels: scores.map((s:any, i:number) => `C${i+1}`), scores : scores.map((c:any) => c.score || 0) }]
        this.clusterJson['group-conceptwise-all'].tableData = { headers : [ "Serial #", "Concept Number  -  Subtopic Name", "Average Score" ], data: []};
        this.clusterJson['group-conceptwise-all'].tableData.data = scores.map((conceptData:any, i:number) => [ "C"+(i+1), conceptData?.conceptnumber + " - " + conceptData?.subtopic?.subtopic, Number.isInteger((conceptData.score || 0)) ? (conceptData.score || 0) : conceptData.score.toFixed(2)]);
        this.bufferReports()
      })
    }
    
    if(this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-conceptwise-divisionWise')?.isVisible){
      let page = 1;
      const clusterJson:any = { divisionWiseScores : [], topicssorder : [] }
      const conceptWise = () => {
        this.http.get(this.api.BASEURL + 'api/report/conceptwise/divisionWise?courseid='+courseid+"&page=" + page, this.api.headers()).subscribe((response:any) => {
          // const {allTopics, course, divisionWiseScores, divisions, topicslisted, topicssorder } = response
          clusterJson.divisionWiseScores = [ ...clusterJson.divisionWiseScores, ...response.divisionWiseScores]
          clusterJson.topicssorder = [ ...clusterJson.topicssorder, ...response.topicssorder]
          clusterJson.allTopics = response.allTopics;
          clusterJson.course = response.course;
          clusterJson.divisions = response.divisions;
          clusterJson.topicslisted=  response.topicslisted;
          const maxPageCount = response.divisionWiseScores[0].maxPageCount;

          if(++page <= maxPageCount){
            conceptWise()
          } else {
            const {allTopics, course, divisionWiseScores, divisions, topicslisted, topicssorder } = clusterJson
            const centerX = divisionWiseScores.map((d) => d.centerX);
            const centerY = divisionWiseScores.map((d) => d.centerY);
            const hexagonColors = divisionWiseScores.map((d) => d.hexagonColors);
            const hexagonSvgPaths = divisionWiseScores.map((d) => d.hexagonSvgPaths);
            const scores = divisionWiseScores.map((d) => d.scores);
            // console.log(centerX, centerY, hexagonColors, hexagonSvgPaths, scores)
            this.clusterJson['group-conceptwise-division-wise'] = {};
            this.clusterJson['group-conceptwise-division-wise'].downloaded = true; 
            const getConceptNumber = (conceptIndex:number, divisionIndex:number) => {
              // console.log({conceptIndex, divisionIndex}, " conceptwise division wise ")
              const topic = divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.subtopic?.subtopic || divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.conceptstatement?.conceptstatement;
              const index = allTopics.findIndex(element => (element._id["conceptstatement"] == topic || element._id["subtopic"] == topic))
              return index
            }
            this.clusterJson['group-conceptwise-division-wise'].clusters = hexagonSvgPaths.map((paths:string[], i:number) => {
              return { centerX: centerX[i], centerY: centerY[i], paths, colors : hexagonColors[i], hexagonLabels: centerX[i].map((t:any, j:number) => `C${getConceptNumber(j, i)+1}`), scores : centerX[i].map((t:any, j:number) => divisionWiseScores[i].scores[j].score || 0), clusterName : divisions[i] }
            });
            this.clusterJson['group-conceptwise-division-wise'].tableData = { headers : [ "Serial #", "Concept Number  -  Subtopic Name" ], data: []};
            this.clusterJson['group-conceptwise-division-wise'].tableData.data = allTopics.map((conceptData:any, i:number) => [ "C"+(i+1), conceptData?.conceptnumber + " - " + conceptData?._id?.subtopic||"" ]);
            this.bufferReports()
          }

        })
      }
      !this.clusterJson['group-conceptwise-division-wise']?.downloaded && conceptWise()
    }
    if(this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-subconceptwise-allParticipants')?.isVisible){
      !this.clusterJson['group-subconceptwise-all']?.downloaded && this.http.get(this.api.BASEURL + 'api/report/subconceptwise/allParticipants?courseid='+courseid, this.api.headers()).subscribe((response:any) => {
        const { centerX, centerY, hexagonSvgPaths, hexagonColors, allTopics, scores } = response;
        this.clusterJson['group-subconceptwise-all'] = {}
        this.clusterJson['group-subconceptwise-all'].downloaded = true; 
        this.clusterJson['group-subconceptwise-all'].clusters = [{ centerX, centerY, paths : hexagonSvgPaths, colors : hexagonColors, hexagonLabels: scores.map((s:any, i:number) => `SC${i+1}`), scores : scores.map((c:any) => c.score || 0) }]
        this.clusterJson['group-subconceptwise-all'].tableData = { headers : [ "Serial #", "Concept Statement", "Average Score" ], data: []};
        this.clusterJson['group-subconceptwise-all'].tableData.data = scores.map((conceptData:any, i:number) => [ "SC"+(i+1), conceptData?.conceptstatement.conceptstatement, Number.isInteger((conceptData.score || 0)) ? (conceptData.score || 0) : conceptData.score.toFixed(2)]);
        this.bufferReports()
      })
    }
    
    if(this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-subconceptwise-divisionWise')?.isVisible){
      let page = 1;
      const clusterJson:any = { divisionWiseScores : [], topicssorder : [] }
      const subconceptwise = () => {
        this.http.get(this.api.BASEURL + 'api/report/subconceptwise/divisionWise?courseid='+courseid+"&page=" + page, this.api.headers()).subscribe((response:any) => {
          // const {allTopics, course, divisionWiseScores, divisions, topicslisted, topicssorder } = response
          clusterJson.divisionWiseScores = [ ...clusterJson.divisionWiseScores, ...response.divisionWiseScores]
          clusterJson.topicssorder = [ ...clusterJson.topicssorder, ...response.topicssorder]
          clusterJson.allTopics = response.allTopics;
          clusterJson.course = response.course;
          clusterJson.divisions = response.divisions;
          clusterJson.topicslisted=  response.topicslisted;
          const maxPageCount = response.divisionWiseScores[0].maxPageCount;

          if(++page <= maxPageCount){
            subconceptwise()
          } else {
            const {allTopics, course, divisionWiseScores, divisions, topicslisted, topicssorder } = clusterJson
            const centerX = divisionWiseScores.map((d) => d.centerX);
            const centerY = divisionWiseScores.map((d) => d.centerY);
            const hexagonColors = divisionWiseScores.map((d) => d.hexagonColors);
            const hexagonSvgPaths = divisionWiseScores.map((d) => d.hexagonSvgPaths);
            const scores = divisionWiseScores.map((d) => d.scores);
            this.clusterJson['group-subconceptwise-division-wise'] = {};
            this.clusterJson['group-subconceptwise-division-wise'].downloaded = true; 
            const getConceptNumber = (conceptIndex:number, divisionIndex:number) => {
              const topic = divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.subtopic?.subtopic || divisionWiseScores[divisionIndex]?.scores[conceptIndex]?.conceptstatement?.conceptstatement;
              const index = allTopics.findIndex(element => (element._id["conceptstatement"] == topic || element._id["subtopic"] == topic))
              return index
            }
            this.clusterJson['group-subconceptwise-division-wise'].clusters = hexagonSvgPaths.map((paths:string[], i:number) => {
              return { centerX: centerX[i], centerY: centerY[i], paths, colors : hexagonColors[i], hexagonLabels: centerX[i].map((t:any, j:number) => `SC${getConceptNumber(j, i)+1}`), scores : centerX[i].map((t:any, j:number) => divisionWiseScores[i].scores[j].score || 0), clusterName : divisions[i] }
            });
            this.clusterJson['group-subconceptwise-division-wise'].tableData = { headers : [ "Serial #", "Concept Statement" ], data: []};
            this.clusterJson['group-subconceptwise-division-wise'].tableData.data = allTopics.map((conceptData:any, i:number) => [ "SC"+(i+1), conceptData?._id.conceptstatement ]);
            this.bufferReports()
          }

        })
      }
      
      !this.clusterJson['group-subconceptwise-division-wise']?.downloaded && subconceptwise()
    }
    if(this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-questionwise-allParticipants')?.isVisible){
      let page = 1
      const globalThis:any = this;
      const questionWiseReportDetails:any = {}
      const questionWise = () => {
        this.http.get(this.api.BASEURL + 'api/report/questionwise/allParticipants?courseid='+ courseid + '&page=' + page ,this.api.headers())
        .subscribe((response:any) => {
          const centerX = questionWiseReportDetails.centerX || [];
          const centerY = questionWiseReportDetails.centerY || [];
          const headerCoordinates = questionWiseReportDetails.headerCoordinates || [];
          const hexagonColors = questionWiseReportDetails.hexagonColors || [];
          const hexagonSvgPaths = questionWiseReportDetails.hexagonSvgPaths || [];
          const rowDistributions = questionWiseReportDetails.rowDistributions || [];
          const testArrays = questionWiseReportDetails.testArrays || [];
          const scores = questionWiseReportDetails.scores || [];
          const viewBoxes = questionWiseReportDetails.viewBoxes || [];
          questionWiseReportDetails.centerX = [ ...centerX, ...response.centerX]
          questionWiseReportDetails.centerY = [ ...centerY, ...response.centerY]
          questionWiseReportDetails.headerCoordinates = [...headerCoordinates, ...response.headerCoordinates]
          questionWiseReportDetails.hexagonColors = [...hexagonColors, ...response.hexagonColors]
          questionWiseReportDetails.hexagonSvgPaths = [...hexagonSvgPaths, ...response.hexagonSvgPaths]
          questionWiseReportDetails.rowDistributions = [...rowDistributions, ...response.rowDistributions]
          questionWiseReportDetails.scores = [...scores, ...response.scores]
          questionWiseReportDetails.viewBoxes = [...viewBoxes, ...response.viewBoxes]
          questionWiseReportDetails.testArrays = [...testArrays, ...response.testArrays ]
          //questionWiseReportDetails.testArrays = response.testArrays;
          questionWiseReportDetails.course = response.course;
          const maxPageCount = response.maxPageCount;

          if(++page <= maxPageCount){
            questionWise()
          }else{
            const { centerX, centerY, hexagonColors, hexagonSvgPaths, scores, testArrays } = questionWiseReportDetails;
          globalThis.clusterJson['group-questionwise-all'] = {};
          globalThis.clusterJson['group-questionwise-all'].downloaded = true;

          globalThis.clusterJson['group-questionwise-all'].clusters = hexagonSvgPaths.map((paths:string[], i:number) => {
            return { centerX: centerX[i], centerY: centerY[i], paths, colors : hexagonColors[i], hexagonLabels: centerX[i].map((t:any, j:number) => `${scores[i][testArrays[i]._id][j].qnno}`), scores : centerX[i].map((t:any, j:number) => scores[i][testArrays[i]._id][j].score || 0), clusterName : testArrays[i].name }
          });
          globalThis.bufferReports()
          }
        },
        (error:any) => {                           
          console.log('error caught for api',error);
          (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
        });
      }
      !this.clusterJson['group-questionwise-all']?.downloaded && questionWise()
    }
    if(this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-questionwise-divisionWise')?.isVisible){
      let page = 1
      const divisionWiseReportDetails:any = {};
      divisionWiseReportDetails.divisions = []
      const questionWise = () => {
        this.http.get(this.api.BASEURL + 'api/report/questionwise/divisionWise?courseid='+ courseid + '&page=' + page ,this.api.headers())
        .subscribe((response:any) => {
          divisionWiseReportDetails.divisions = [...divisionWiseReportDetails.divisions, ...(response.divisions || [])]

          const { maxPageCount } = response;
          if(++page <= maxPageCount){
            questionWise()
          } else {
            this.clusterJson['group-questionwise-division-wise'] = {};
            this.clusterJson['group-questionwise-division-wise'].downloaded = true; 
            this.clusterJson['group-questionwise-division-wise'].clusters = divisionWiseReportDetails?.divisions.filter((divisionInfo:any) => divisionInfo?.centerX[0].length).map((divisionInfo:any) => {
              const { centerX, centerY, division, hexagonColors, hexagonSvgPaths, test, testsData } = divisionInfo;
              return { centerX:centerX[0], centerY:centerY[0], colors : hexagonColors[0], paths : hexagonSvgPaths[0], scores : testsData[test._id].map((testData:any) => testData?.score || 0 ), hexagonLabels : testsData[test._id].map((testData:any) => testData?.qnno || "" ), clusterName : test?.name +" / "+ division   }
            })
            this.bufferReports()
          }
        },
        (error:any) => {                           
          console.log('error caught for api',error);
          (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
        });
      }
      !this.clusterJson['group-questionwise-all']?.downloaded && questionWise()
    }
    this.isPDFDownloaded = Boolean(localStorage.getItem("isPDFDownloaded"))
    !this.isPDFDownloaded && this.bufferReports()
  }
  bufferReports(){
    localStorage.setItem('download-group-reports', JSON.stringify(this.clusterJson))
    const areAllReportsdownloaded = this.startDownload();
    areAllReportsdownloaded && this.reportGeneration()
  }

  ngOnInit(): void {
  }
  startGeneratingReports(){
    // this.isCheckedStatus = true;


  }
  checkReportVisible(){
    if(window.location.pathname.search(/report-1/i) > -1){
      return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-testwise-allParticipants')?.isVisible;
    } else if(window.location.pathname.search(/report-3/i) > -1){
      return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-conceptwise-allParticipants')?.isVisible;
    } else if(window.location.pathname.search(/report-5/i) > -1){
      return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-subconceptwise-allParticipants')?.isVisible;
    } else if(window.location.pathname.search(/report-7/i) > -1){
      return this.reportConfigurations.find((reportConfiguration:any) => reportConfiguration.reportType == 'group-questionwise-allParticipants')?.isVisible;
    }
  }
  reportGeneration(){
    this.http.post(this.api.BASEURL + 'api/download/v2', { clusterJson : this.clusterJson }, {...this.api.headers(), responseType: 'blob' as 'json'})
    .subscribe((response:any) => {
      this.isPDFDownloaded = true;
      let dataType = response.type;
      let binaryData = [];
      binaryData.push(response);
      let downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

      const { name, coursecode } = JSON.parse(localStorage.getItem("generalreportsfilters") || JSON.stringify({}))?.activeCourse;

      downloadLink.setAttribute('download', `${name + " - " + coursecode} - Dipstick Reports.pdf`);
      document.body.appendChild(downloadLink);
      downloadLink.click();
      localStorage.setItem("isPDFDownloaded", "1")
  }, (e) => {
      console.log(e)
    });
  }
}


//const data = {"clusterJson":{"studentName":"student11 student11","courseName":"C Programming for Problem solving backup","studentUSN":"135","breadcrumbPath":"student - reports/C Programming for Problem solving backup/135-student11 student11","student-testwise":{"title":"post test 1","clusters":[{"clusterName":"Attempt 1","paths":["M 420 160 L 454 140 L 488 160 L 488 200 L 454 220 L 420 200 Z"],"colors":["#00b050"],"scores":[100],"centerX":[450],"centerY":[176],"hexagonLabels":["T1"]}],"tableData":{"headers":["Serial #","Test Name","Attempt 1"],"data":[["T1","testposttest","100%"]]}},"reportNames":{"test":"Post Test","concept":"Subtopics","subconcept":"Concept statements"},"student-subconceptwise":{"title":"post test 1","clusters":[{"clusterName":"Attempt 1","paths":["M 420 160 L 454 140 L 488 160 L 488 200 L 454 220 L 420 200 Z"],"colors":["#00b050"],"label":"SC","scores":[100],"centerX":[450],"centerY":[176],"hexagonLabels":["SC1"]}],"tableData":{"headers":["Serial #","Concept Statement","Attempt 1"],"data":[["SC1","1","100%"]]}},"student-conceptwise":{"title":"post test 1","clusters":[{"clusterName":"Attempt 1","paths":["M 420 160 L 454 140 L 488 160 L 488 200 L 454 220 L 420 200 Z"],"colors":["#00b050"],"label":"C","scores":[100],"centerX":[450],"centerY":[176],"hexagonLabels":["C1"]}],"tableData":{"headers":["Serial #","Concept Number  -  Subtopic Name","Attempt 1"],"data":[["C1","1 - 1","100%"]]}}}}