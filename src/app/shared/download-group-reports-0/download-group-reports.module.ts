import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DownloadGroupReportsComponent } from './download-group-reports.component';



@NgModule({
  declarations: [DownloadGroupReportsComponent],
  exports: [DownloadGroupReportsComponent],
  imports: [
    CommonModule
  ]
})
export class DownloadGroupReportsModule { }
