export const constant = {
     homepageImage : "./assets/images/dipstick.jpg",
    // eklLogo : "./assets/images/logo.png",
    // eklminiImage : "./assets/images/ekl.png",
    legendImage: "./assets/images/legend.png",
    downloadPDF : "./assets/images/download.png",
   // legendImage: "./assets/images/legend.png",
   // homepageImage : "https://s3.ap-south-1.amazonaws.com/eklakshya.com/Dipstick+production/cover.png",
    eklLogo : "https://s3.ap-south-1.amazonaws.com/eklakshya.com/Dipstick+production/LOGO_Eklakshya_T_HD.png",
    eklminiImage : "https://s3.ap-south-1.amazonaws.com/eklakshya.com/Dipstick+production/LOGO_E_HD.png",
   // legendImage : "https://s3.ap-south-1.amazonaws.com/eklakshya.com/Dipstick+production/dipstick2.png"
};

export const monthNames:any = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


export const monthShortNames:any = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  