import { Component, Input, OnInit } from '@angular/core';
import {CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';

@Component({
  selector: 'app-breadcrumb-display',
  templateUrl: './breadcrumb-display.component.html',
  styleUrls: ['./breadcrumb-display.component.css']
})
export class BreadcrumbDisplayComponent implements OnInit {
lms:boolean=true;
  constructor(   private router: Router) { }
  data  = localStorage.getItem('data');
  @Input() reportType: string;
  ngOnInit(): void {
    const userRole = (localStorage.getItem('user'))
    this.lms = ["admin", "user" ].includes(userRole)
    console.log(userRole, this.reportType, this.lms)
    // this.lms  = ((localStorage.getItem('user')) == "admin" || (localStorage.getItem('user')) == "user") ? true : false;
  }
  comeBack(){
    this.router.navigate(['/reports']);
  }
}
