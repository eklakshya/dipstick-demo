import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BreadcrumbDisplayComponent } from './breadcrumb-display.component';

describe('BreadcrumbDisplayComponent', () => {
  let component: BreadcrumbDisplayComponent;
  let fixture: ComponentFixture<BreadcrumbDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BreadcrumbDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
