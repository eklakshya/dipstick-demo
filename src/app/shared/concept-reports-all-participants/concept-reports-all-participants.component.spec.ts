import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConceptReportsAllParticipantsComponent } from './concept-reports-all-participants.component';

describe('ConceptReportsAllParticipantsComponent', () => {
  let component: ConceptReportsAllParticipantsComponent;
  let fixture: ComponentFixture<ConceptReportsAllParticipantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConceptReportsAllParticipantsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConceptReportsAllParticipantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
