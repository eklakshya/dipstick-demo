import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../../pages/services/http.service';
import { ReportsService } from '../../core/services/reports.service'
import { constant } from '../constant';
import { HexagonLayoutService } from 'src/app/pages/services/hexagon-layout.service';

@Component({
  selector: 'app-concept-reports-all-participants',
  templateUrl: './concept-reports-all-participants.component.html',
  styleUrls: ['./concept-reports-all-participants.component.css']
})
export class ConceptReportsAllParticipantsComponent implements OnInit {

  @Input() url : string;
  @Input() reportType : string;
  @Input() labelPrefix : string;
  @Input() listLabel : string;
  @Input() popupXLabel : string;
  @Input() POPUPAPIURL : string = '';
  @Input() isPopupVisible : Boolean;
  
  courseId: string = localStorage.getItem('courseId');
  activeTest:string;
  constructor(private http :HttpClient,  private modalService: NgbModal,private api :ApiService, public reportService:ReportsService, private hexagonLayout: HexagonLayoutService) {
   }
   reportDetails:any = {};
   legend:any = constant.legendImage;
   courseDetails:any;
   scoreList:any;
   userRole: string = '';

  ngOnInit(): void {

    this.userRole = localStorage.getItem('userrole') || "";
   
    this.getScoreList()
    this.courseDetails = JSON.parse(localStorage.getItem('generalreportsfilters')).activeCourse;
  }
  getReport(){
    let url = this.api.BASEURL + ( this.url || 'api/report/conceptwise/allParticipants/v1?courseid='+this.courseId );
    this.http.get(url,this.api.headers()).subscribe((response) => { 
      this.reportDetails = response;
      let testslength = this.reportDetails['scores'].length;
      this.api.noResp(testslength);
      console.log(this.reportDetails)
      setTimeout(function(){ 
        document.getElementById('loading').style.display = 'none';
      }, 100);
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
    console.log(url)
    // this.http.get(this.api.BASEURL + 'api/report/student/conceptwise/table?studentid=6103a8ad0f66ad61f3022653&courseid=6103a89dc19d261311bdaf92', this.api.headers()).subscribe((response) => {
    //   console.log(response)
    // })
    // this.http.get(this.api.BASEURL + 'api/report/student/subconceptwise?courseid='+this.courseId+"&studentid=6103f5b50f66ad61f3023f5a", this.api.headers()).subscribe((response) => {
    //   console.log(response)
    // })
  }
  hasTest(svgPaths:any[]){
    return svgPaths.find((svgPath:any) => svgPath.length > 0)
  }
  getSvgWidth(rowDistribution:any[]){
    return rowDistribution[Math.ceil(rowDistribution.length%2==1?rowDistribution.length/2:rowDistribution.length)-1]
  }
  getSvgheight(rowDistribution:any[],svgPathLength:number){
    var totalHex = 0;
    var totalHexHeight = 0;
    var hexHeights:number[] = rowDistribution.map((rowdata, number)=>{
    totalHex = totalHex < svgPathLength ? totalHex + rowdata : (totalHex)
    totalHexHeight = totalHex < svgPathLength ? number + 1 : totalHexHeight ;
    return totalHexHeight + 1
    })
    var lastelement = hexHeights[hexHeights.length-1]
    return lastelement
  }
  getNegativeMargin(lastelement:number,rowDistributionLength:number){
    return (rowDistributionLength - lastelement) * -50
  }
  open1(modal) {
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', size:'xl', backdrop: 'static',keyboard: false});
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'xl', backdrop: 'static', keyboard: false });;
  }
  
  findConceptsByTestId (testid:string){
    let testindex = this.reportDetails.scores.findIndex(element => Object.keys(element).includes(testid))
    return this.reportDetails.scores[testindex][Object.keys(this.reportDetails.scores[testindex])[0]]
  }
  getHexText (conceptIndex:number) {
    // this.getConceptNumber(conceptIndex)
    // this.getHexagonNumber(conceptIndex)
    return this.labelPrefix + ( this.getHexagonNumber(conceptIndex) + 1 )// + '-' + this.reportDetails.scores[conceptIndex].score.toFixed(2) ;
  }
  getHexagonNumber(conceptIndex:any){
    const topic = this.reportDetails?.topics[conceptIndex]?.conceptstatement || this.reportDetails?.topics[conceptIndex]?.subtopic;
    const index = this.reportDetails.topics.findIndex(element => (element["conceptstatement"] == topic || element["subtopic"] == topic))
    console.log( index, topic, this.reportType );
    return conceptIndex //index
  }
  getConceptNumber(index:number){
    const subtopic = this.reportDetails?.concepts[index]?._id?.subtopic
    const allSubtopics = this.reportDetails?.allTopics?.map((conceptdata)=>{return conceptdata?._id?.subtopic})
    return (allSubtopics.indexOf(subtopic)+1 || index+1)
  }
  getScoreByTopic(topicInfo:any){
    const topic = topicInfo?.subtopic || topicInfo?.conceptstatement
    const scoreInfo = this.reportDetails.scores.find(element => (element?.conceptstatement?.conceptstatement == topic || element?.subtopic?.subtopic == topic))
    return (scoreInfo?.score == 100 ? 100 : scoreInfo?.score?.toFixed(2))
  }

  getScoreList(){
    console.log(this.url)
    let url = this.api.BASEURL + ( this.url || 'api/report/conceptwise/allParticipants?courseid='+this.courseId );
    this.http.get(url,this.api.headers()).subscribe((response:any) => {
      this.scoreList = response?.response;
      this.getClusterLayout(this.scoreList);
    })
  }

  getClusterLayout(cluster:any){
    const rowDistribution = this.hexagonLayout.getRowDistribution(cluster.length);
    const reportDimensionsPost = this.hexagonLayout.calcDimensions(cluster.length, 1, rowDistribution);
    const hexStructurePost: any = this.hexagonLayout.initHexStructure(rowDistribution, reportDimensionsPost.marginX, reportDimensionsPost.marginY, reportDimensionsPost.hexWidth, reportDimensionsPost.hexRadius);
    // alert(JSON.stringify(rowDistribution) + JSON.stringify(reportDimensionsPost) + JSON.stringify(hexStructurePost))

    const hexCentersXPost:any = [];
    const hexCentersYPost:any = [];
    const hexPathsPost:any = [];
    const hexColorsPost:any = [];
    const { marginX, marginY, hiveHeight, hiveWidth, hexWidth} = reportDimensionsPost;
    this.reportDetails.topics = cluster;
    this.reportDetails.viewBox = `${cluster.length>2?(marginX - ((Math.floor(Object.keys(hexStructurePost).length / 2) -1 ) * (hexWidth/2))):marginX+(hexWidth/2)} ${marginY} ${hiveWidth} ${hiveHeight}`;
    for(let a = 0; a < 1; a++) {
        let testNumber: number = 0;
        const rowsInStruct = Object.keys(hexStructurePost);

        rowsInStruct.forEach((row, index) => {
            const testsInRow = Object.keys(hexStructurePost[`row${index+1}`]);
            testsInRow.forEach(test => {
                try {
                    testNumber += 1;
                    const scoreValue = cluster[testNumber-1]['score'];
                    const score = ((scoreValue != null) && (scoreValue >= 0)) ? scoreValue : -1;
                    hexCentersXPost.push(this.hexagonLayout.getHexCenterX(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexCentersYPost.push(this.hexagonLayout.getHexCenterY(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexPathsPost.push(this.hexagonLayout.getHexPath(hexStructurePost,testNumber, index+1, a, reportDimensionsPost));
                    hexColorsPost.push(this.hexagonLayout.getHexColor(score));
                  } catch (error) {
                    
                  }
                });
              });
    }
    // // console.log({ hexPathsPost, hexColorsPost, hexCentersXPost, hexCentersYPost })
    this.reportDetails.centerX = hexCentersXPost;
    this.reportDetails.hexagonSvgPaths = hexPathsPost;
    this.reportDetails.hexagonColors = hexColorsPost;
    this.reportDetails.centerY = hexCentersYPost;
    this.reportDetails.rowDistribution = rowDistribution;
    console.clear()
    console.log(this.reportDetails)
    // // this.sendReportDetails.emit(this.reportDetails)
    // this.generatePDF();
  }
  getScoreNumber(score:number) {
    const n = +score;
    return (n == 100) ? n : n.toFixed(2)
  }

}
