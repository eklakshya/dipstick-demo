import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttemptPopupTableViewConceptwiseComponent } from './attempt-popup-table-view-conceptwise.component';
import { CoreModule } from 'src/app/core/core.module';



@NgModule({
  declarations: [AttemptPopupTableViewConceptwiseComponent],
  imports: [
    CommonModule,
    CoreModule
  ],
  exports: [AttemptPopupTableViewConceptwiseComponent]
})
export class AttemptPopupTableViewConceptwiseModule { }
