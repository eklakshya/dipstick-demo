import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../pages/services/http.service';
import { ReportsService } from '../../core/services/reports.service'
import { constant } from '../constant';
import { HexagonLayoutService } from 'src/app/pages/services/hexagon-layout.service';

@Component({
  selector: 'app-attempt-popup-table-view-conceptwise',
  templateUrl: './attempt-popup-table-view-conceptwise.component.html',
  styleUrls: ['./attempt-popup-table-view-conceptwise.component.css']
})
export class AttemptPopupTableViewConceptwiseComponent implements OnInit {

  response: any = {	
    levelWiseAgregation:[],
    conceptnumbers:[],
    concepts:[],
    conceptstatements:[],
    levels:[],
    allSubconcepts:[]
  };
  conceptstatements:any[];
  @Input() type : string;
  @Input() division : string;
  @Input() test : any = {_id : "", name : ""}
  @Input() reportType : string;
  @Input() showName : boolean;
  @Input() labelPrefix : string;
  @Input() xLabel : string;
  @Input() apiEndPoint : string = '';
  @Input() activeAttempt : number = 1;
  popUp:String = "popUp";
  object: Object;
  courseid : string = localStorage.getItem('courseId');
  range : number[] = [1,2,3,4,5,6]
  levelLimit : number = 6;
  conceptLimit : number = 3;
  allConcepts: any[] = [];
  legend:any = constant.legendImage;
  allConceptsList = [];
  concepts:any = [];
  conceptstatementsList = [];
  levelWiseAggregation:any = [];
  levels:any = [];
  reportDetails = [];
  bloomLevels = [];
  Sdata:any;

  constructor(private http:HttpClient,private api : ApiService, public reportService:ReportsService, private HexagonLayout : HexagonLayoutService) { 
  }

  getSvgWidth(rowDistribution:any[]){
    return rowDistribution[Math.ceil(rowDistribution.length%2==1?rowDistribution.length/2:rowDistribution.length)-1]
  }
  getSvgheight(rowDistribution:any[],svgPathLength:number){
    var totalHex = 0;
    var totalHexHeight = 0;
    var hexHeights:number[] = rowDistribution.map((rowdata, number)=>{
    totalHex = totalHex < svgPathLength ? totalHex + rowdata : (totalHex)
    totalHexHeight = totalHex < svgPathLength ? number + 1 : totalHexHeight ;
    return totalHexHeight + 1
    })
    var lastelement = hexHeights[hexHeights.length-1]
    return lastelement
  }
  ngOnInit(): void {
     this.Sdata = localStorage.getItem('S_bread');
    let reportType = this.reportType || 'questionwise';
    // console.log(reportType, this.reportType)
    let apiEndPoint : string =  this.apiEndPoint ? this.apiEndPoint + '&attempt=' + this.activeAttempt : `api/report/${reportType}/table/${this.type}?courseid=${this.courseid}&attempt=${this.activeAttempt}` + ( this.test._id ? `&testid=${this.test._id}` : ``) + ( this.division ? `&divisionBuffer=${btoa(this.division)}` : ``) //`/api/report/${reportType}/table/${this.type}?courseId=${this.courseid}&testid=${this.test._id}`
    this.http.get(this.api.BASEURL + apiEndPoint,this.api.headers())
    .subscribe((response:any) => {
      this.response = response;
      this.allConcepts = response.allConcepts || [];
      this.concepts = response.concepts || [];
      this.conceptstatements = (response.conceptstatements && response.conceptstatements.length) ? response.conceptstatements.map((conceptstatementData:any)=>{ return conceptstatementData["_id"].conceptstatement }) : [];
      this.conceptLimit = this.conceptLimit > this.response.concepts.length ? this.conceptLimit : this.response.concepts.length;
      response?.levelWiseAgregation.forEach((levelWisedata:any, levelIndex:number) => {
        this.bloomLevels[levelIndex] = levelWisedata.bl;
        levelWisedata.conceptWiseScoreList.forEach((conceptWiseData:any, conceptIndex:number) => {
          this.getClusterLayout(conceptWiseData, conceptIndex, levelIndex);
        })
        // console.log(this.reportDetails)
      })
      (!this.response || !this.response != undefined ) ? (
        document.getElementById('loading').style.display = 'none'):console.log("successfully displayed loader..");
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
  }
  getNegativeMargin(lastelement:number,rowDistributionLength:number){
    return (rowDistributionLength - lastelement) * -50
  }
  questionsList(conceptData:any){
    console.log(conceptData)
    let concept = Object.keys(conceptData)[0];
    return conceptData[concept]
  }
  getFieldValue(index:number, field:string){
    let conceptData = this.concepts[index]
    return conceptData[field] || conceptData["_id"][field]
  }
  findLevel(index:number){
    let levelWiseData:any[] = this.bloomLevels
    let leveldata = levelWiseData.find(element =>element == 'L'+index)
    return (leveldata && leveldata.length);
  }
  findLevelIndex(index:number){
    let levelWiseData:any[] = this.bloomLevels
    let levelIndex = levelWiseData.findIndex(element =>element == 'L'+index)
    console.log(levelIndex)
    return levelIndex;
  }
  getSizedArray(size:number){
    return new Array(size)
  }
  hexLabelText(topicInfo:any, conceptIndex:number){
    console.log(this.conceptstatements, this.conceptstatements?.length)
    const conceptstatementIndex = this.conceptstatements.length ? this.conceptstatements.findIndex(conceptstatementData => conceptstatementData == topicInfo.conceptstatement) : -1
    return (this.showName && topicInfo?.qnno) ? (''+ topicInfo.qnno) : (this.conceptstatements.length ? ( 'SC'+ (conceptstatementIndex + 1 ) ): ( this.labelPrefix + (conceptIndex+1)))
  }
  getHexagonNumber(topicInfo:any){
    const topic = topicInfo.subtopic || topicInfo.conceptstatement;
    return (this.response?.allSubconcepts?.length ? this.response.allSubconcepts.findIndex(subconcept => subconcept._id.conceptstatement == topic) : this.response.allConcepts.findIndex(concept => concept._id.subtopic == topic))
  }
  getTopicNumber(topicInfo:any){
    const conceptstatementIndex = this.conceptstatements.length ? this.conceptstatements.findIndex(conceptstatementData => conceptstatementData == topicInfo.conceptstatement) : -1
    return (this.showName && topicInfo?.qnno) ? (''+ topicInfo.qnno) : this.labelPrefix + (this.getHexagonNumber(topicInfo) + 1)
  }
  getConceptNumber(index:number){
    // console.log(this.response.concepts)
    const subtopic = this.response?.concepts[index]?._id?.subtopic
    const allSubtopics = this.allConcepts.map((conceptdata)=>{return conceptdata?._id?.subtopic})
    // console.log(this.allConcepts.map((conceptdata)=>{return conceptdata?._id?.subtopic}), subtopic)
    // console.log(allSubtopics.indexOf(subtopic))
    // console.log(this.allConcepts.findIndex((conceptData)=>{conceptData._id.subtopic == subtopic}))
    // console.log(allSubtopics, subtopic, allSubtopics.indexOf(subtopic) , index)
    return (allSubtopics.indexOf(subtopic)+1 || index+1)
  }
  getClusterLayout(cluster:any, conceptIndex:number, levelIndex:number){

    const conceptData:any = Object.values(cluster)[0];
    this.reportDetails[levelIndex] = this.reportDetails[levelIndex] || [];
    this.reportDetails[levelIndex][conceptIndex] = this.reportDetails[levelIndex][conceptIndex] || {  };
    const rowDistribution = this.HexagonLayout.getRowDistribution(conceptData.length);
    const reportDimensionsPost = this.HexagonLayout.calcDimensions(conceptData.length, 1, rowDistribution);
    const hexStructurePost: any = this.HexagonLayout.initHexStructure(rowDistribution, reportDimensionsPost.marginX, reportDimensionsPost.marginY, reportDimensionsPost.hexWidth, reportDimensionsPost.hexRadius);
    const headerCoordinatesPost = [this.HexagonLayout.getHeaderCoordinates(0, rowDistribution, hexStructurePost, reportDimensionsPost)];
    
    // console.log(rowDistribution, reportDimensionsPost, hexStructurePost, headerCoordinatesPost)
    const hexCentersXPost:any = [];
    const hexCentersYPost:any = [];
    const hexPathsPost:any = [];
    const hexColorsPost:any = [];
    const { marginX, marginY, hiveHeight, hiveWidth, hexWidth} = reportDimensionsPost;
    this.reportDetails[levelIndex][conceptIndex].scores = conceptData;
    this.reportDetails[levelIndex][conceptIndex].viewBox = `${conceptData.length>2?(marginX - ((Math.floor( Object.keys(hexStructurePost).length / 2) -1 ) * (hexWidth/2))):marginX+(hexWidth/2)} ${marginY} ${hiveWidth} ${hiveHeight}`;
    // console.log(this.reportDetails)
    for(let a = 0; a < 1; a++) {
      let testNumber: number = 0;
      const rowsInStruct = Object.keys(hexStructurePost);
      
      rowsInStruct.forEach((row, index) => {
        const testsInRow = Object.keys(hexStructurePost[`row${index+1}`]);
        testsInRow.forEach(test => {
          try {
            testNumber += 1;
            const score = conceptData[testNumber-1]['score'] >= 0 ? conceptData[testNumber-1]['score'] : -1;
            hexCentersXPost.push(this.HexagonLayout.getHexCenterX(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
            hexCentersYPost.push(this.HexagonLayout.getHexCenterY(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
            hexPathsPost.push(this.HexagonLayout.getHexPath(hexStructurePost,testNumber, index+1, a, reportDimensionsPost));
            hexColorsPost.push(this.HexagonLayout.getHexColor(score));
          } catch (error) {
            
          }
        });
      });
    }
    // console.log({ hexPathsPost, hexColorsPost, hexCentersXPost, hexCentersYPost })
    this.reportDetails[levelIndex][conceptIndex].hexagonSvgPaths = hexPathsPost;
    this.reportDetails[levelIndex][conceptIndex].hexagonColors = hexColorsPost;
    this.reportDetails[levelIndex][conceptIndex].centerX = hexCentersXPost;
    this.reportDetails[levelIndex][conceptIndex].centerY = hexCentersYPost;
    this.reportDetails[levelIndex][conceptIndex].rowDistribution = rowDistribution;
  }
}
