import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestWiseReportsSingleClusterComponent } from './test-wise-reports-single-cluster.component';

describe('TestWiseReportsSingleClusterComponent', () => {
  let component: TestWiseReportsSingleClusterComponent;
  let fixture: ComponentFixture<TestWiseReportsSingleClusterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestWiseReportsSingleClusterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWiseReportsSingleClusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
