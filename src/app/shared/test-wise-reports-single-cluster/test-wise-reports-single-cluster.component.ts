import { Component, OnInit, Input } from '@angular/core';
import { DownloadService } from '../../pages/services/download.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { ApiService } from '../../pages/services/http.service';
import { ReportsService } from '../../core/services/reports.service';
import { HexagonLayoutService } from 'src/app/pages/services/hexagon-layout.service';
@Component({
  selector: 'app-test-wise-reports-single-cluster',
  templateUrl: './test-wise-reports-single-cluster.component.html',
  styleUrls: ['./test-wise-reports-single-cluster.component.css']
})
export class TestWiseReportsSingleClusterComponent implements OnInit {
  courseList: Array < Object >;
  coursename: string;
  reportDetails: any = {};
  maxPretestAttempts: number = 5;
  maxPosttestAttempts: number = 1;
  data:any;
  paths ='';
  testslength=0;
  courseId:any;
  @Input() APIURL:string;
  courseDetails : any = {};
constructor(private api:ApiService,private http:HttpClient, private downloads: DownloadService, private modalService: NgbModal, public reportService: ReportsService, private hexagonLayout: HexagonLayoutService) { 
  }

  ngOnInit(): void {
    this.api.getCoursesById()
    .subscribe((response) => {
      this.courseList = response['courses'];
      // console.log(this.courseList, this.courseList.length, this.courseList[0]['name']);
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
    this.courseId = localStorage.getItem('courseId');
    this.data  = localStorage.getItem('data');

    // console.log({APIURL : this.APIURL})
    // this.http.get(this.api.BASEURL + this.APIURL , this.api.headers()) //+ this.courseId
    // .subscribe(async (response) => {
    //   this.reportDetails = response;
    //   console.log(response)
    //   // this.viewBox = response.
    //   let testslength = await this.reportDetails['testArray'].length;
    //   testslength && this.load()
    //   this.api.noResp(testslength);
    //   //testslength >1 ?  this.paths = '100 100 150 122.5' :this.paths='230 100 60 70' 
    //   // 1 hex hight 60
    //  // this.paths = testslength >=1 ?  (testslength<2?'230 100 60 70':(testslength<5?'100 100 150 122.5':'318 140 204 200')):('0 0 0 0') 
    //  this.paths =this.reportDetails.viewBox;
    // },
    // (error:any) => {                           
    //   console.log('error caught for api',error);
    //   (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    // }); 
    this.getScoresList()
    // setTimeout(function(){ 
    //   document.getElementById('loading').style.display = 'none';
    // }, 2000);
  }

  load(){
    setTimeout(function(){
      document.getElementById('loading').style.display = 'none';
    }, 50);
  }

  setCourse(event: Event) {
    this.coursename = (<HTMLInputElement>event.target).value;
  }
  getSvgWidth(rowDistribution:any[]){
    return rowDistribution[Math.ceil(rowDistribution.length%2==1?rowDistribution.length/2:rowDistribution.length)-1]
  }
  getSvgheight(rowDistribution:any[],svgPathLength:number){
    var totalHex = 0;
    var totalHexHeight = 0;
    var hexHeights:number[] = rowDistribution.map((rowdata, number)=>{
    totalHex = totalHex < svgPathLength ? totalHex + rowdata : (totalHex)
    totalHexHeight = totalHex < svgPathLength ? number + 1 : totalHexHeight ;
    return totalHexHeight + 1
    })
    var lastelement = hexHeights[hexHeights.length-1]
    return lastelement
  }
  getNegativeMargin(lastelement:number,rowDistributionLength:number){
    return (rowDistributionLength - lastelement) * -50
  }
  getHexText(testIndex:number) {
    return 'T' + (testIndex + 1) // + '-' + this.reportDetails.testArray[testIndex].score.toFixed(2)
  }
  // ngOnDestroy() {
  //   localStorage.removeItem('courseId')
  // }
  isStudentReport(){
    return Boolean(this.APIURL.match(/student/));
  }
  getScoresList(){
    
    this.http.get(this.api.BASEURL + this.APIURL , this.api.headers()) //+ this.courseId
    .subscribe(async (response:any) => {
      // this.api.noResp(response?.tests?.length);
      this.getClusterLayout(response?.tests);
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    }); 
    this.courseDetails = JSON.parse(localStorage.getItem("generalreportsfilters"))?.activeCourse;
    if(!this.courseDetails){
      this.courseDetails = JSON.parse(localStorage.getItem('reportfilters'))?.activeCourse
      localStorage.setItem('generalreportsfilters', JSON.stringify({ activeCourse : this.courseDetails }))
    }
  }
  
  getClusterLayout(cluster:any){
    const rowDistribution = this.hexagonLayout.getRowDistribution(cluster.length);
    const reportDimensionsPost = this.hexagonLayout.calcDimensions(cluster.length, 1, rowDistribution);
    const hexStructurePost: any = this.hexagonLayout.initHexStructure(rowDistribution, reportDimensionsPost.marginX, reportDimensionsPost.marginY, reportDimensionsPost.hexWidth, reportDimensionsPost.hexRadius);
    // alert(JSON.stringify(rowDistribution) + JSON.stringify(reportDimensionsPost) + JSON.stringify(hexStructurePost))

    const hexCentersXPost:any = [];
    const hexCentersYPost:any = [];
    const hexPathsPost:any = [];
    const hexColorsPost:any = [];
    const { marginX, marginY, hiveHeight, hiveWidth, hexWidth} = reportDimensionsPost;
    this.reportDetails.topics = cluster;
    this.reportDetails.viewBox = `${cluster.length>2?(marginX - ((Math.floor(Object.keys(hexStructurePost).length / 2) -1 ) * (hexWidth/2))):marginX+(hexWidth/2)} ${marginY} ${hiveWidth} ${hiveHeight}`;
    for(let a = 0; a < 1; a++) {
        let testNumber: number = 0;
        const rowsInStruct = Object.keys(hexStructurePost);

        rowsInStruct.forEach((row, index) => {
            const testsInRow = Object.keys(hexStructurePost[`row${index+1}`]);
            testsInRow.forEach(test => {
                try {
                    testNumber += 1;
                    const score = cluster[testNumber-1]['percentage'] >= 0 ? cluster[testNumber-1]['percentage'] : -1;
                    hexCentersXPost.push(this.hexagonLayout.getHexCenterX(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexCentersYPost.push(this.hexagonLayout.getHexCenterY(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexPathsPost.push(this.hexagonLayout.getHexPath(hexStructurePost,testNumber, index+1, a, reportDimensionsPost));
                    hexColorsPost.push(this.hexagonLayout.getHexColor(score));
                  } catch (error) {
                    
                  }
                });
              });
    }
    // // console.log({ hexPathsPost, hexColorsPost, hexCentersXPost, hexCentersYPost })
    this.reportDetails.centerX = hexCentersXPost;
    this.reportDetails.hexagonSvgPaths = hexPathsPost;
    this.reportDetails.hexagonColors = hexColorsPost;
    this.reportDetails.centerY = hexCentersYPost;
    this.reportDetails.rowDistribution = rowDistribution;
    console.clear()
    console.log(this.reportDetails)
    // // this.sendReportDetails.emit(this.reportDetails)
    // this.generatePDF();
  }
}
