import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportsService } from 'src/app/core/services/reports.service';
import { HexagonLayoutService } from 'src/app/pages/services/hexagon-layout.service';
import { ApiService } from 'src/app/pages/services/http.service';
import { constant } from '../constant';

@Component({
  selector: 'app-attempt-concept-reports-all-participants',
  templateUrl: './attempt-concept-reports-all-participants.component.html',
  styleUrls: ['./attempt-concept-reports-all-participants.component.css']
})
export class AttemptConceptReportsAllParticipantsComponent implements OnInit {

  @Input() url : string;
  @Input() reportType : string;
  @Input() labelPrefix : string;
  @Input() listLabel : string;
  @Input() popupXLabel : string;
  @Input() POPUPAPIURL : string = '';
  @Input() isPopupVisible : Boolean;
  @Output() sendReportDetails1 = new EventEmitter();
  
  courseId: string = localStorage.getItem('courseId');
  activeTest:string;

  constructor(private http :HttpClient,  private modalService: NgbModal,private api :ApiService, public reportService:ReportsService, private HexagonLayout : HexagonLayoutService) {
   }
   reportDetails:any = [];
   legend:any =  constant.legendImage;
   allTopics :any = [];
   attempts:any = [];
   activeAttempt:number;
   activeCourse:any;

  ngOnInit(): void {
    this.activeCourse = JSON.parse(localStorage.getItem("studentReportsActiveCourse"));
    console.log("activeCourse",this.activeCourse)
    this.getReport()
  }
  getReport(){
    let url = this.api.BASEURL + ( this.url || 'api/report/conceptwise/allParticipants?courseid='+this.courseId );
    this.http.get(url,this.api.headers()).subscribe((response:any) => {
      console.log(response)
      this.allTopics = response.allTopics;
      this.attempts = response.attempts;

      response?.attemptwiseScores.map((attemptwiseScore:any, index:number) => {
        this.getClusterLayout(attemptwiseScore, index)
      })
    },
    (error:any) => {                           
      console.log('error caught for api',error);
      (error.status ==  401 || error.status ==  403) ? this.api.sessionExp() :  console.log("Something went wrong!")
    });
    console.log(url)
  }
  hasTest(svgPaths:any[]){
    return svgPaths.find((svgPath:any) => svgPath.length > 0)
  }
  getSvgWidth(rowDistribution:any[]){
    return rowDistribution[Math.ceil(rowDistribution.length%2==1?rowDistribution.length/2:rowDistribution.length)-1]
  }
  getSvgheight(rowDistribution:any[],svgPathLength:number){
    var totalHex = 0;
    var totalHexHeight = 0;
    var hexHeights:number[] = rowDistribution.map((rowdata, number)=>{
    totalHex = totalHex < svgPathLength ? totalHex + rowdata : (totalHex)
    totalHexHeight = totalHex < svgPathLength ? number + 1 : totalHexHeight ;
    return totalHexHeight + 1
    })
    var lastelement = hexHeights[hexHeights.length-1]
    return lastelement
  }
  getNegativeMargin(lastelement:number,rowDistributionLength:number){
    return (rowDistributionLength - lastelement) * -50
  }
  open1(modal) {
    console.log( this.attempts[this.activeAttempt])
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', size:'xl', backdrop: 'static',keyboard: false});
  }
  findConceptsByTestId (testid:string){
    let testindex = this.reportDetails.scores.findIndex(element => Object.keys(element).includes(testid))
    return this.reportDetails.scores[testindex][Object.keys(this.reportDetails.scores[testindex])[0]]
  }
  getHexText (attemptIndex:number, conceptIndex:number) {
    // this.getConceptNumber(conceptIndex)
    // this.getHexagonNumber(conceptIndex)
    return this.labelPrefix + ( this.getHexagonNumber(attemptIndex, conceptIndex) + 1 )// + '-' + this.reportDetails.scores[conceptIndex].score.toFixed(2) ;
  }
  getHexagonNumber(attemptIndex:number, conceptIndex:any){
    const topic = this.reportDetails[attemptIndex]?.scores[conceptIndex]?.subtopic?.subtopic || this.reportDetails[attemptIndex]?.scores[conceptIndex]?.conceptstatement?.conceptstatement;
    const index = this.allTopics.findIndex(element => (element._id["conceptstatement"] == topic || element._id["subtopic"] == topic))
    return index
  }
  getConceptNumber(index:number){
    const subtopic = this.reportDetails?.concepts[index]?._id?.subtopic
    const allSubtopics = this.reportDetails?.allTopics?.map((conceptdata)=>{return conceptdata?._id?.subtopic})
    return (allSubtopics.indexOf(subtopic)+1 || index+1)
  }
  getScoreByTopic(attemptIndex, topicInfo:any){
    const topic = topicInfo?.subtopic || topicInfo?.conceptstatement
    const scoreInfo = this.reportDetails[attemptIndex]?.scores?.find(element => (element?.conceptstatement?.conceptstatement == topic || element?.subtopic?.subtopic == topic))
    return scoreInfo?.score?.toFixed(2) == undefined ? '-' : scoreInfo?.score?.toFixed(2) == -1 ? 'N/A' : ((scoreInfo?.score == 100 ? '100' : scoreInfo?.score?.toFixed(2)) + '%');
  }
  getClusterLayout(cluster:any, index:number){

    this.reportDetails[index] = this.reportDetails[index] || {  };
    const rowDistribution = this.HexagonLayout.getRowDistribution(cluster.length);
    const reportDimensionsPost = this.HexagonLayout.calcDimensions(cluster.length, 1, rowDistribution);
    const hexStructurePost: any = this.HexagonLayout.initHexStructure(rowDistribution, reportDimensionsPost.marginX, reportDimensionsPost.marginY, reportDimensionsPost.hexWidth, reportDimensionsPost.hexRadius);
    // const headerCoordinatesPost = [this.HexagonLayout.getHeaderCoordinates(0, rowDistribution, hexStructurePost, reportDimensionsPost)];

    // console.log(rowDistribution, reportDimensionsPost, hexStructurePost, headerCoordinatesPost)
    const hexCentersXPost:any = [];
    const hexCentersYPost:any = [];
    const hexPathsPost:any = [];
    const hexColorsPost:any = [];
    const { marginX, marginY, hiveHeight, hiveWidth, hexWidth} = reportDimensionsPost;
    this.reportDetails[index].scores = cluster;
    this.reportDetails[index].viewBox = `${cluster.length>2?(marginX - ((Math.floor( Object.keys(hexStructurePost).length / 2) -1 ) * (hexWidth/2))):marginX+(hexWidth/2)} ${marginY} ${hiveWidth} ${hiveHeight}`;
    // console.log(this.reportDetails)
    for(let a = 0; a < 1; a++) {
        let testNumber: number = 0;
        const rowsInStruct = Object.keys(hexStructurePost);

        rowsInStruct.forEach((row, index) => {
            const testsInRow = Object.keys(hexStructurePost[`row${index+1}`]);
            testsInRow.forEach(test => {
                try {
                    testNumber += 1;
                    const score = cluster[testNumber-1]['score'] >= 0 ? cluster[testNumber-1]['score'] : -1;
                    hexCentersXPost.push(this.HexagonLayout.getHexCenterX(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexCentersYPost.push(this.HexagonLayout.getHexCenterY(hexStructurePost, a, index+1, testNumber, reportDimensionsPost)-4);
                    hexPathsPost.push(this.HexagonLayout.getHexPath(hexStructurePost,testNumber, index+1, a, reportDimensionsPost));
                    hexColorsPost.push(this.HexagonLayout.getHexColor(score));
                } catch (error) {

                }
            });
        });
    }
    // console.log({ hexPathsPost, hexColorsPost, hexCentersXPost, hexCentersYPost })
    this.reportDetails[index].hexagonSvgPaths = hexPathsPost;
    this.reportDetails[index].hexagonColors = hexColorsPost;
    this.reportDetails[index].centerX = hexCentersXPost;
    this.reportDetails[index].centerY = hexCentersYPost;
    this.reportDetails[index].rowDistribution = rowDistribution;
    // console.log(this.reportDetails)
    this.generatePDF();
  }
  generatePDF(){
    const clusters = this.reportDetails.map((reportDetailData:any, index:number) => {
      return {
        clusterName : "Attempt " + this.attempts[index],
        paths : reportDetailData.hexagonSvgPaths,
        colors : reportDetailData.hexagonColors,
        label : this.labelPrefix,
        scores : reportDetailData.scores.map((conceptData) => conceptData.score),
        centerX : reportDetailData.centerX,
        centerY : reportDetailData.centerY,
        hexagonLabels : reportDetailData.centerY.map((_:any, i:number) => this.getHexText(index, i))
      }
    });
    const columnType = (this.allTopics[0].conceptnumber || (this.allTopics[0].subtopic && this.allTopics[0].subtopic?.conceptnumber)) || (this.allTopics[0]._id?.subtopic || (this.allTopics[0].subtopic && this.allTopics[0].subtopic?.subtopic)) ? "Concept Number  -  Subtopic Name" : "Concept Statement";
    // console.log(columnType, this.attempts);
    const tableData = {
      headers : [ "Serial #", columnType, ...this.attempts.map((a) => "Attempt " + a)],
       data : this.allTopics.map((concept:any, conceptIndex:number) => {
        const topic = (this.labelPrefix=="C" ? ((concept?.conceptnumber || concept?.subtopic?.conceptnumber) + " - " + (concept?.subtopic?.subtopic || concept?._id?.subtopic)) : (concept?.conceptstatement?.conceptstatement || concept?._id?.conceptstatement))
        return [ this.labelPrefix + (conceptIndex+1), (topic.length>49 ? (topic.slice(0, 46).trim() + "...") : topic ), ...this.attempts.map((attemptInfo:any, attemptIndex:number) => this.getScoreByTopic(attemptIndex, concept?._id)) ]
       })
    };

    const data = {
      title : "post test 1",
      clusters, tableData
    };
    console.log(tableData, this.allTopics);
    (clusters.length == this.attempts.length) && this.sendReportDetails1.emit(data);
  }


  open(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title', size:'xl', backdrop: 'static',keyboard: false })
      .result.then(
        (result) => {
        },
        (reason) => {
        }
      );
  }
}

