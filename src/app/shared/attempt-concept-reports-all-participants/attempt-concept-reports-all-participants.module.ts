import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttemptConceptReportsAllParticipantsComponent } from './attempt-concept-reports-all-participants.component';
import { popUpTableModule } from 'src/app/commmon-modules/popUptable.module';
import { AttemptPopupTableViewConceptwiseModule } from '../attempt-popup-table-view-conceptwise/attempt-popup-table-view-conceptwise.module';
import { CoreModule } from 'src/app/core/core.module';



@NgModule({
  declarations: [AttemptConceptReportsAllParticipantsComponent],
  imports: [
    CommonModule,
    popUpTableModule,
    AttemptPopupTableViewConceptwiseModule,
    CoreModule
  ],
  exports: [AttemptConceptReportsAllParticipantsComponent]
})
export class AttemptConceptReportsAllParticipantsModule { }
