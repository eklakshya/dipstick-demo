import { Injectable } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
@Injectable({
  providedIn: 'root'
})
export class ChartsService {
  constructor() {}

  lineChartLabels: Label[] = ['0%', '10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%'];
  lineChartOptions = {
    responsive: true, 
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            stepSize: 2,
            fontColor: '#0003ab',
            gridLines: { color: 'rgba(255,255,255,0.1)' }
          },      
        
    
          scaleLabel: {
            display: true,
            //labelString: 'probability'
          }
        }
      ],
      xAxes: [
        {
          ticks: {
            beginAtZero: true,
            stepSize: 20,
            fontColor: '#0003ab',
            gridLines: { color: 'rgba(255,255,255,0.1)' }
          },      
        
    
          scaleLabel: {
            display: true,
            //labelString: 'probability'
          }
        }
      ],
      // xAxes: [
      //   {
      //     gridLines: {
      //       color: "rgba(0, 0, 0, 0)",
      //     }
      //   }
      // ],      
    },
    fill: true
  };

  lineChartColors: Color[] = [
    {
      borderColor: '#0003ab',
      backgroundColor: 'transparent',
      pointBackgroundColor: [""]
    },
  ];

  lineChartLegend = false;
  lineChartPlugins = [];
  lineChartType = 'line';
}
