import { AfterContentInit, Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, AfterContentInit{
  breadCrumbData: any = [];
  isMobileDevice:boolean;
  deviceInfo:any;
  constructor(public router: Router,private modalService:NgbModal) {
  }
  ngOnInit(){
 this.deviceInfo =this.getMobileOperatingSystem();
 if(this.deviceInfo == "Android"){
  this.isMobileDevice = true;
  }else{
    this.isMobileDevice = false;
  }
}

   getMobileOperatingSystem() {
    let userAgent = navigator.userAgent || navigator.vendor;
    console.log("=========================="+userAgent); 
    if (/windows/i.test(userAgent)) {
        return "Windows";
    }
    if (/android/i.test(userAgent)) {
        return "Android";
    }
     if (/iPad|iPhone|iPod/.test(userAgent)) {
        return "iOS";
    }
    return "unknown";
}


  fullScreenCheck() {
    if (document.fullscreenElement) return;
    return document.documentElement.requestFullscreen();
  }

  async rotate() {
    if( this.isMobileDevice){
      
      try {
       await this.fullScreenCheck();
     } catch (err) {
       console.error(err);
     }
     await screen.orientation.lock('landscape');
    }else{
      console.log("not mobile")
    }
  }
  ngAfterContentInit(){
		setTimeout(() => {
      this.isMobileDevice &&	document.getElementById("btnid").click();
		}, 2000);
 	}

 
   open(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
         },
        (reason) => {
         }
      );
  }

}
