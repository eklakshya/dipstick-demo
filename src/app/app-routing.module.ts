import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenusComponent } from './core/layout/menus/menus.component';
import { DivisionChartComponent } from './shared/division-chart/division-chart.component';
import { AuthGuardService as AuthGuard } from './core/helper/auth.guard.service';
import { IndexService as AccessGuard } from './core/helper/index.service';
import { ReportaccessService as userreportGuard } from './core/helper/reportsAccess.service'
import { StudentaccessService as ReportsGuard } from './core/helper/studentAccess.service'



/*
import { DivisionListForAttendenceComponent } from './core/division-list-for-attendence/division-list-for-attendence.component';
import { FilterReportsComponent } from './core/layout/filter-reports/filter-reports.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { GenerateReportComponent } from './pages/generate-report/generate-report.component';
import { UploadComponent } from './pages/departmnet/upload/upload.component';
import { EditComponent } from './pages/edit/edit.component';
import { StudentReportViewComponent } from './pages/student-report-view/student-report-view.component';
import { DepartmentListComponent } from './pages/departmnet/department-list/department-list.component';
import { YearAndDepartmentComponent } from './pages/year-and-department/year-and-department.component';
import { AllParticipantsComponentT } from './pages/test-wise/all-participants/all-participants.component';
import { DivisionWiseComponent } from './pages/test-wise/division-wise/division-wise.component';
import { AllParticipantsComponentQ } from './pages/question-wise/all-participants/all-participants.component';
import { QDivisionWiseComponent } from './pages/question-wise/q-division-wise/q-division-wise.component';
import { AllParticipantsComponent } from './pages/concept-wise/all-participants/all-participants.component';
import { ConceptwisedivisionwiseComponent } from './pages/concept-wise/conceptwisedivisionwise/conceptwisedivisionwise.component';
import { SubconceptwiseallparticipantsComponent } from './pages/subconcept-wise/subconceptwiseallparticipants/subconceptwiseallparticipants.component';
import { SubconceptwisedivisionwiseComponent } from './pages/subconcept-wise/subconceptwisedivisionwise/subconceptwisedivisionwise.component';
import { AttendanceComponent } from './pages/attendance/attendance.component';
import { PublishComponent } from './core/layout/publish/publish.component';
import { TrashComponent } from './core/layout/trash/trash.component';
import { SignInComponent } from './core/layout/sign-in/sign-in.component';
import { StudentReportsFilterComponent } from './pages/student-reports/student-reports-filter/student-reports-filter.component';
import { SiteConfigComponent } from './pages/site-config/site-config.component';
import { IntigrationComponent } from './pages/intigration/intigration.component';
import { IntigrationMainpageComponent } from './intigration-mainpage/intigration-mainpage.component';
import { ChooseLevelComponent } from './pages/choose-level/choose-level.component';
import { AnalyticsComponent } from './core/layout/analytics/analytics.component';
*/


const routes: Routes = [
  { path: '', loadChildren: () => import('./pages/homepage/homepage.module').then(m => m.HomepageModule) },
  { path: 'department', loadChildren: () => import('./pages/departmnet/department.module').then(m => m.DepartmentModule),canActivate: [AccessGuard], data: {expectedRole: 'admin',breadcrumbs: true} },
  { path: 'publish', loadChildren: () => import('./core/layout/publish/publish.module').then(m => m.PublishModule),canActivate: [AccessGuard] },
  { path: 'trash', loadChildren: () => import('./core/layout/trash/trash.module').then(m => m.TrashModule),canActivate: [AccessGuard] },
  { path: 'reports', loadChildren: () => import('./pages/year-and-department/year-and-department.module').then(m => m.YearAndDepartmentModule), canActivate: [userreportGuard] },
  { path: 'site-config', loadChildren: () => import('./pages/site-config/site-config.module').then(m => m.SiteConfigModule), canActivate: [AccessGuard]},
  { path: 'menus', component: MenusComponent, canActivate: [ReportsGuard] },
  { path: 'report-1', loadChildren: () => import('./pages/test-wise/all-participants/all-participants.module').then(m => m.AllParticipantsModule), canActivate: [ReportsGuard] },
  { path: 'report-2', loadChildren: () => import('./pages/test-wise/division-wise/division-wise.module').then(m => m.DivisionWiseModule), canActivate: [ReportsGuard] },
  { path: 'report-3', loadChildren: () => import('./pages/concept-wise/all-participants/all-participants.module').then(m => m.AllParticipantsModule), canActivate: [ReportsGuard] },
  { path: 'report-4', loadChildren: () => import('./pages/concept-wise/conceptwisedivisionwise/conceptwisedivision-wise.module').then(m => m.ConceptwisedivisionWiseModule), canActivate: [ReportsGuard] },
  { path: 'report-5', loadChildren: () => import('./pages/subconcept-wise/subconceptwiseallparticipants/subconceptwiseallparticipants.module').then(m => m.SubconceptwiseallparticipantsModule), canActivate: [ReportsGuard] },
  { path: 'report-6', loadChildren: () => import('./pages/subconcept-wise/subconceptwisedivisionwise/subconceptwisedivision-wise.module').then(m => m.SubconceptwisedivisionWiseModule), canActivate: [ReportsGuard] },
  { path: 'report-7', loadChildren: () => import('./pages/question-wise/all-participants/q-allparticipants.module').then(m => m.QAllparticipantsModule) , canActivate: [ReportsGuard]},
  { path: 'report-8', loadChildren: () => import('./pages/question-wise/q-division-wise/q-division-wise.module').then(m => m.QDivisionWiseModule), canActivate: [ReportsGuard] },
  
  { path: 'attendance', loadChildren: () => import('./pages/attendance/attendance.module').then(m => m.AttendanceModule) ,canActivate: [AuthGuard]},
  { path: 'student-report', loadChildren: () => import('./pages/student-reports/student-reports.module').then(m => m.StudentReportsModule),canActivate: [ReportsGuard] },
  { path: 'analytics', loadChildren: () => import('./core/layout/analytics/analytics.module').then(m => m.AnalyticsModule),canActivate: [AuthGuard] },
  { path: 'integration', loadChildren: () => import('./intigration-mainpage/intigration-mainpage.module').then(m => m.IntigrationMainpageModule) },
  { path: 'integration/reports', loadChildren: () => import('./pages/intigration/intigration.module').then(m => m.IntigrationModule) },
  { path: 'data-exchange', loadChildren: () => import('./pages/data-exchange/data-exchange.module').then(m => m.DataExchangeModule) },
  { path: 'faculty-access', loadChildren: () => import('./pages/faculty-access/faculty-access.module').then(m => m.FacultyAccessModule) },
  
  
  /*
  { path: 'charts', component: DivisionChartComponent },
  { path: '', component: HomepageComponent},
  { path: 'department', component: DepartmentListComponent, canActivate: [AccessGuard], data: {expectedRole: 'admin',breadcrumbs: true} },
  { path: 'department/:id', component: UploadComponent, canActivate: [AccessGuard], data: {expectedRole: 'admin',breadcrumbs: true} },
  { path: 'publish', component: PublishComponent, canActivate: [AccessGuard]},
  { path: 'trash', component: TrashComponent, canActivate: [AccessGuard]},
  { path: 'generate', component: GenerateReportComponent, canActivate: [AuthGuard] , data: {expectedRole: 'admin',breadcrumbs: true} },
  { path: 'edit', component: EditComponent, canActivate: [AuthGuard], data: {expectedRole: 'admin',breadcrumbs: true}  },
  { path: 'reports', component: YearAndDepartmentComponent, canActivate: [userreportGuard] },
  
  { path: 'site-config', component: SiteConfigComponent, canActivate: [AccessGuard] },
  { path: 'report-1', component: AllParticipantsComponentT, canActivate: [ReportsGuard]  },
  { path: 'report-2', component: DivisionWiseComponent , canActivate: [ReportsGuard] },
  { path: 'report-3', component: AllParticipantsComponent, canActivate: [ReportsGuard]  },
  { path: 'report-4', component: ConceptwisedivisionwiseComponent, canActivate: [ReportsGuard]  },
  { path: 'report-5', component: SubconceptwiseallparticipantsComponent, canActivate: [ReportsGuard]},
  { path: 'report-6', component: SubconceptwisedivisionwiseComponent, canActivate: [ReportsGuard]  },
  { path: 'report-7', component: AllParticipantsComponentQ, canActivate: [ReportsGuard]  },
  { path: 'report-8', component: QDivisionWiseComponent, canActivate: [ReportsGuard]  },
  { path: 'attendance', component: AttendanceComponent, canActivate: [AuthGuard] },
  { path: 'student-report', component: StudentReportsFilterComponent, canActivate: [AccessGuard] },

  //this is for the to view students reports
  { path: 'integration/reports', component: IntigrationComponent},
  {path:'integration',component:IntigrationMainpageComponent},
  this for 1-8 reports
  {path:'integrations/all',component:ChooseLevelComponent},
  {path : 'analytics',component:AnalyticsComponent,canActivate:[AuthGuard]},
  {path : 'filters',component:FilterReportsComponent}
  { path: 'divisionslist', component: DivisionListForAttendenceComponent, canActivate: [AccessGuard], data: {expectedRole: 'admin',breadcrumbs: true}},
  */

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }