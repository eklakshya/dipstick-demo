import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChartsModule } from 'ng2-charts';
import {HttpInterfaceService } from './core/helper/http-interface.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { MenusComponent } from './core/layout/menus/menus.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoutingService } from './core/services/routing.service'
import { AuthGuardService as AuthGuard } from './core/helper/auth.guard.service';
import { IndexService as AccessGuard } from './core/helper/index.service';
import {StudentaccessService as ReportsGuard } from './core/helper/studentAccess.service';
import {ReportaccessService as userreportGuard} from './core/helper/reportsAccess.service'
import { ReportsService } from './core/services/reports.service';
import { MatTabsModule } from '@angular/material/tabs';
import { CoreModule } from './core/core.module';
import { PagenotfoundComponent } from './pages/pagenotfound/pagenotfound.component';
import { UnAuthorizedModule } from './core/helper/un-authorized/un-authorized.module';
import { DownloadGroupReportsModule } from './shared/download-group-reports/download-group-reports.module';
import { ResizeBorderComponent } from './shared/resize-border/resize-border.component';
import { NgbDatepickerModule,NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';

 @NgModule({
  declarations: [
    AppComponent,
    MenusComponent,
    PagenotfoundComponent,
  ], 
  imports: [
    // NgMultiSelectDropDownModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ChartsModule,
    MatTabsModule,
    CoreModule,
    UnAuthorizedModule,
    DownloadGroupReportsModule,
    NgbModalModule     
   ],
  providers: [AuthGuard,AccessGuard,ReportsGuard,userreportGuard,RoutingService ,HttpInterfaceService,ReportsService  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
