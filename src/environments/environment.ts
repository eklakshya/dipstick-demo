export const environment = {
  production: true,
  api:'http://localhost:4200/api/',
  mediaUrlPrefix:'http://eklakshya.com.s3-website.ap-south-1.amazonaws.com/',
  version:"0.3.13"
};

//npm run build --prod --aot --vendor-chunk --common-chunk --delete-output-path --buildOptimizer
